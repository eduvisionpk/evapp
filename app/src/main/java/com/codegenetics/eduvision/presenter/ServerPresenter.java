package com.codegenetics.eduvision.presenter;

import android.util.Log;

import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.networks.RetrofitClient;
import com.codegenetics.eduvision.networks.RetrofitInterface;
import com.codegenetics.eduvision.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class ServerPresenter {
private final String TAG = ServerPresenter.class.getSimpleName();
    private RetrofitInterface retrofitInterface;
    private Observable<ResponseBody> observable;
    private IData data = null;
    private AppEnums enums;


    public void getData(AppEnums enums, String... params) {

        retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        if (data != null) data.showProgress(this.enums = enums);

        StringBuilder param = new StringBuilder("\nparams\n");

        for (String s : params) param.append(s).append("\n");

        if (enums == null) {
            Constants.log("Server call: ", "Enum shouldn't be NULL");
            return;
        }

        Constants.log("Server call: ",
                Constants.BASE_URL
                        + enums.toString()
                        + param.toString()
        );

        switch (enums) {
            case DISTRICTS:
                getDistricts();
                break;
            case CITIES:
                getCities();
                break;
            case LEVELS:
                getLevels();
                break;
            case DISCIPLINE_TYPES:
                getDisciplineTypes();
                break;
            case DISCIPLINES:
            case DISCIPLINES_PROGRAMS:
                getDisciplines(params);
                break;
            case ADMISSIONS:
                getAdmissions(params);
                break;
            case PROGRAMS:
                getPrograms(params);
                break;
            case INSTITUTE_DETAILS:
                getInstituteDetails(params);
                break;
            case HOME_SLIDER:
                getSliderData();
                break;
            case NEWS:
                getNews(params);
                break;
            case NEWS_DETAILS:
                getNewsDetails(params);
                break;
            case SCHOLARSHIPS:
                getScholarships(params);
                break;
            case SCHOLARSHIP_DETAILS:
                getScholarshipDetails(params);
                break;
            case CAREER_LIST:
                getCareers(params);
                break;
            case CAREER_DETAILS:
                getCareerDetails(params);
                break;
            case VIDEO_CATEGORIES:
                getVideoCategories();
                break;
            case VIDEO_DETAILS:
                getVideoDetails(params);
                break;
            case ENTRY_TEST:
                getEntryTests(params);
                break;
            case ENTRY_TEST_DETAILS:
                getEntryTestDetails(params);
                break;
            case ENTRY_TEST_CATEGORIES:
                getEntryTestCategories();
                break;
            case MERIT_CALCULATORS_LIST:
                getMeritCalculatorsList();
                break;
            case MERIT_CALCULATOR:
                getMeritCalculator(params);
                break;
            case NOTIFICATIONS:
                getNotifications();
                break;
            case CAREER_COUNSELING:
                getCareerCounseling();
                break;
            case CAREER_COUNSELING_DETAILS:
                getCareerCounselingDetails(params);
                break;
            case PAST_PAPERS_BOARDS_CLASSES:
                getPastPapersBoardsClasses();
                break;
            case PAST_PAPERS_SUBJECTS:
                getPastPapersSubjects(params);
                break;
            case PAST_PAPERS_DETAILS:
                getPastPapersDetails(params);
                break;
            case LIST_OF_BOARDS:
                getListOfBoard();
                break;
            case DIST_PROVINCE:
                getProvinceAndDistricts();
                break;
            case BOARD_WISE_RANKING:
                getBoardWiseRanking(params);
                break;
            case AREA_WISE_RANKING:
                getAreaWiseRanking(params);
                break;
            case LOW_FEE:
                getLowFeeInstitutes(params);
                break;
            case LOW_MERIT:
                getLowMeritInstitutes(params);
                break;
            case HIGH_MERIT:
                getHighMeritInstitutes(params);
                break;
            default:
                sendError(new Exception("Case not found in Server Presenter class"));
        }


    }


    private void getNotifications() {
        observable = retrofitInterface.getNotifications();
        subscribeObservable();
    }

    private void getMeritCalculator(String[] params) {
        Log.e(TAG, "getMeritCalculator: "+ Arrays.toString(params));
        observable = retrofitInterface.calculateMerit(
                params[0],  /*  obtained marks metric */
                params[1],  /*  total marks metric */
                params[2],  /*  obtained marks fsc */
                params[3],  /*  total marks fsc */
                params[4],  /*  entry test percentage */
                params[5]   /*  institute */
        );
        subscribeObservable();
    }

    private void getMeritCalculatorsList() {
        observable = retrofitInterface.getMeritCalculatorsList();
        subscribeObservable();
    }

    private void getEntryTestDetails(String[] params) {
        observable = retrofitInterface.getEntryTestDetails(
                params[0] /* entry test id */
        );
        subscribeObservable();
    }

    private void getEntryTests(String[] params) {
        observable = retrofitInterface.getEntryTests(
                params[0] /* type  */
        );
        subscribeObservable();
    }
    private void getEntryTestCategories() {
        observable = retrofitInterface.getEntryTestCategories(
                );
        subscribeObservable();
    }

    private void getVideoDetails(String[] params) {
        observable = retrofitInterface.getVideoDetails(
                params[0], /* page number */
                params[1] /* type  */
        );
        subscribeObservable();
    }

    private void getVideoCategories() {
        observable = retrofitInterface.getVideoCategories();
        subscribeObservable();
    }

    private void getCareerDetails(String[] params) {
        observable = retrofitInterface.getCareerDetails(params[0]);
        subscribeObservable();
    }

    private void getCareers(String[] params) {
        observable = retrofitInterface.getCareers(params[0]);
        subscribeObservable();
    }

    private void getScholarshipDetails(String[] params) {
        observable = retrofitInterface.getScholarshipDetail(params[0]);
        subscribeObservable();
    }

    private void getScholarships(String[] params) {
        observable = retrofitInterface.getScholarships(
                params[0], /* page num */
                params[1], /* authority */
                params[2], /* level */
                params[3], /* field */
                params[4], /* category */
                params[5]  /* type */
        );
        subscribeObservable();
    }

    private void getNewsDetails(String[] params) {
        observable = retrofitInterface.getNewsDetail(params[0]);
        subscribeObservable();
    }

    private void getNews(String[] params) {
        observable = retrofitInterface.getNews(params[0], params[1]);
        subscribeObservable();
    }

    private void getSliderData() {
        observable = retrofitInterface.getSliderData();
        subscribeObservable();
    }

    private void getInstituteDetails(String[] params) {
        observable = retrofitInterface.getInstituteDetails(params[0]);
        subscribeObservable();
    }

    private void getPrograms(String[] params) {
        observable = retrofitInterface.getPrograms(params[0], params[1], params[2], params[3], params[4]);
        subscribeObservable();
    }

    private void getAdmissions(String[] params) {
        observable = retrofitInterface.getAdmissions(params[0], params[1], params[2], params[3], params[4]);
        subscribeObservable();
    }

    private void getCities() {
        observable = retrofitInterface.getCities();
        subscribeObservable();
    }

    private void getDistricts() {
        observable = retrofitInterface.getDistricts();
        subscribeObservable();
    }

    private void getLevels() {
        observable = retrofitInterface.getLevels();
        subscribeObservable();
    }

    private void getDisciplineTypes() {
        observable = retrofitInterface.getDisciplineTypes();
        subscribeObservable();
    }

    private void getDisciplines(String[] params) {
        observable = retrofitInterface.getDisciplines(params[0], params[1]);
        subscribeObservable();
    }

    private void getCareerCounseling(){
        observable = retrofitInterface.careerCounseling();
        subscribeObservable();
    }

    private void getCareerCounselingDetails(String[] params){
        observable = retrofitInterface.careerCounselingDetails(params[0]);
        subscribeObservable();
    }


    private void getPastPapersBoardsClasses() {
        observable = retrofitInterface.getPastPaperBoardsAndClasses();
        subscribeObservable();
    }

    private void getPastPapersSubjects(String[] params){
        observable = retrofitInterface.getPastPapersSubjects(params[0], params[1]);
        subscribeObservable();
    }

    private void getPastPapersDetails(String[] params){
        observable = retrofitInterface.getPastPapersDetails(params[0], params[1], params[2]);
        subscribeObservable();
    }


    private void getListOfBoard() {
        observable = retrofitInterface.getListOfBoards();
        subscribeObservable();
    }


    private void getProvinceAndDistricts() {
        observable = retrofitInterface.getProvinceAndDistricts();
        subscribeObservable();
    }

    private void getBoardWiseRanking(String[] params){
        //board,category,level
        observable = retrofitInterface.getBoardWiseRanking(params[0], params[1], params[2]);
        subscribeObservable();
    }

    private void getAreaWiseRanking(String[] params){
        //city,category,level
        observable = retrofitInterface.getAreaWiseRanking(params[0], params[1], params[2]);
        subscribeObservable();
    }

    private void getLowFeeInstitutes(String[] params){
        //discipline_type,discipline,level_code
        observable = retrofitInterface.getLowFeeInstitutes(params[0], params[1], params[2],params[3]);
        subscribeObservable();
    }

    private void getLowMeritInstitutes(String[] params){
        //discipline_type,discipline,level_code
        observable = retrofitInterface.getLowMeritInstitutes(params[0], params[1], params[2],params[3]);
        subscribeObservable();
    }

    private void getHighMeritInstitutes(String[] params){
        //discipline_type,discipline,level_code
        observable = retrofitInterface.getHighMeritInstitutes(params[0], params[1], params[2],params[3]);
        subscribeObservable();
    }














//==================================================================================================
//             *** Constant Methods ***
//==================================================================================================


    private ServerPresenter(IData data) {
        this.data = data;
    }

    private ServerPresenter() { }

    public static ServerPresenter getInstance(IData iData) { return new ServerPresenter(iData); }

    public static ServerPresenter getInstance() { return new ServerPresenter(); }

    private void sendResponse(ResponseBody body) {
        if (body != null) {
            try {
                String d = body.string();
                if (!d.isEmpty()) {
                    Constants.log("ServerPresenter success = ", d);
                    if (data != null) data.displayData(d);
                } else {
                    sendError(new Throwable("Something went wrong. Please try again."));
                }

            } catch (IOException e) {
                sendError(e);
            }
        } else {
            sendError(new Throwable("Nothing to display."));
        }
    }

    private void sendError(Throwable e) {
        if (e != null && e.getMessage() != null) {
            Constants.log("ServerPresenter error = ", e.getMessage());
            if (data != null) {
                data.displayError(e.getMessage());
                data.hideProgress(enums == null ? AppEnums.DISCIPLINES : enums);
            }
        }
    }

    private void subscribeObservable() {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DefaultObserver<ResponseBody>() {
                    @Override
                    public void onNext(@NotNull ResponseBody body) {
                        sendResponse(body);
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        sendError(e);
                    }

                    @Override
                    public void onComplete() {
                        if (data != null) data.hideProgress(enums);
                    }
                });
    }

//    @NonNull
//    private RequestBody createPartFromString(String descriptionString) {
//        return RequestBody.create(MultipartBody.FORM, descriptionString);
//    }
//
//    @NonNull
//    private MultipartBody.Part prepareFilePart(String partName, String fileUri) {
//
//        File file = new File(fileUri);
//
//        RequestBody requestFile =
//                RequestBody.create(
//                        MediaType.parse("multipart/form-data"),
//                        file
//                );
//
//        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
//    }



}
