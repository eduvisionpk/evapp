package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.enums.AppEnums;

public interface IData {

    void showProgress(AppEnums enums);

    void hideProgress(AppEnums enums);

    void displayData(String response);

    void displayError(String error);

}
