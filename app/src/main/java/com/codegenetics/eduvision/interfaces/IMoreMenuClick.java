package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.models.MoreModel;

public interface IMoreMenuClick {
    void onMoreMenuSelected(MoreModel moreModel);
}
