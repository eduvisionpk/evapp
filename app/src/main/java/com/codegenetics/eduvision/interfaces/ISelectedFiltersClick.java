package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.models.FilterModel;

public interface ISelectedFiltersClick {
    void onFilterDelete(FilterModel data);
}
