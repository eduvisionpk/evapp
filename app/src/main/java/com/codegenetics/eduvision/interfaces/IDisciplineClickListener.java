package com.codegenetics.eduvision.interfaces;

public interface IDisciplineClickListener {
    void onItemClicked(String disciplineType, boolean isChecked);
}
