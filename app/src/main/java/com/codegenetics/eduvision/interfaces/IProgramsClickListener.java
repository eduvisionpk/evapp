package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.enums.AppEnums;

public interface IProgramsClickListener {
    void onItemClicked(AppEnums enums, int position);
}
