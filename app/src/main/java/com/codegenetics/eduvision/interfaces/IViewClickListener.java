package com.codegenetics.eduvision.interfaces;

public interface IViewClickListener {
    void onItemClicked(int position);
}
