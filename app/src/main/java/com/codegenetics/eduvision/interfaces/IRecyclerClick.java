package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.models.Career.CareersModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;

public interface IRecyclerClick {
     default void onRecyclerItemClicked(CareersModel.CareersData data){}
     default void onRecyclerItemClicked(LevelsModel.LevelsData levelsData){}
}
