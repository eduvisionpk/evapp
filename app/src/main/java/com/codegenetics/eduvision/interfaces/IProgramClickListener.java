package com.codegenetics.eduvision.interfaces;

public interface IProgramClickListener {
    void onProgramClicked(String disciplineType, String disciplineName);
}
