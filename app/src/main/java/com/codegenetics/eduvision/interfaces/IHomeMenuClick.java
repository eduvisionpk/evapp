package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.models.HomeModel;

public interface IHomeMenuClick {
    void onHomeMenuSelected(HomeModel model);
}
