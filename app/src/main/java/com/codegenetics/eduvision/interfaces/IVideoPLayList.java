package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.models.Videos.VideoDetailsModel;

public interface IVideoPLayList {
    void onVideoSelected(VideoDetailsModel.VideoDetailsData data);
}
