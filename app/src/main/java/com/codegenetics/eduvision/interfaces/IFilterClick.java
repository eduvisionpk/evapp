package com.codegenetics.eduvision.interfaces;

import com.codegenetics.eduvision.enums.SearchFilter;

public interface IFilterClick {
    void onFilterSelected(SearchFilter searchFilter);
}
