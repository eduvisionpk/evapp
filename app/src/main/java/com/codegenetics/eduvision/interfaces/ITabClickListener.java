package com.codegenetics.eduvision.interfaces;

public interface ITabClickListener {
    void onTabsSelected(String s);
}
