package com.codegenetics.eduvision.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.FirebaseConstants;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.adapters.CitiesSpinnerAdapter;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private final ArrayList<CitiesModel.CitiesData> citiesDataArrayList = new ArrayList<>();
    private EditText etName, etEmail, etMobile, etPassword, etConfirmPassword;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;
    private ImageView ivPasswordVisibility, ivConfirmPasswordVisibility;
    private AutoCompleteTextView etCity;
    private CheckBox cbTerms;
    private Button btnSignUp;
    private MKLoader loader;

    private CitiesModel.CitiesData citiesData = null;
    private boolean isPasswordVisible = false;

    private void init() {
        mAuth = FirebaseAuth.getInstance();

        citiesDataArrayList.addAll(Constants.citiesList);
        etName = findViewById(R.id.signUp_etName);
        etEmail = findViewById(R.id.signUp_etEmail);
        etMobile = findViewById(R.id.signUp_etMobileNumber);
        etPassword = findViewById(R.id.signUp_etPassword);
        etConfirmPassword = findViewById(R.id.signUp_etConfirmPassword);

        inputLayoutEmail = findViewById(R.id.inputLayoutEmail);
        inputLayoutPassword = findViewById(R.id.inputLayoutPassword);

        ivPasswordVisibility = findViewById(R.id.signUp_ivPasswordVisibility);
        ivConfirmPasswordVisibility = findViewById(R.id.signUp_ivConfirmPasswordVisibility);

        etCity = findViewById(R.id.signUp_etCity);

        btnSignUp = findViewById(R.id.signUp_btnSignUp);

        loader = findViewById(R.id.signUp_loader);

        cbTerms = findViewById(R.id.signUp_cbTerms);

        Spanned policy = Html.fromHtml(getString(R.string.terms_conditions));
        cbTerms.setMovementMethod(LinkMovementMethod.getInstance());
        cbTerms.setText(policy);
        cbTerms.setMovementMethod(LinkMovementMethod.getInstance());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();

        setCities(citiesDataArrayList);

    }

    private void setCities(ArrayList<CitiesModel.CitiesData> arrayList) {
        CitiesSpinnerAdapter adapter = new CitiesSpinnerAdapter(this, R.layout.layout_spinner, arrayList);
        etCity.setAdapter(adapter);
        etCity.setOnItemClickListener((adapterView, view, position, l) -> citiesData = adapter.getItem(position));
        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                citiesData = null;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void createNewUser() {

        if (!validated()) return;

        showProgress();

        mAuth.createUserWithEmailAndPassword(etEmail.getText().toString().trim(), etPassword.getText().toString())
                .addOnCompleteListener(task -> {

                    if (task.isSuccessful()) {

                        FirebaseUser user = mAuth.getCurrentUser();

                        if (user != null) {

                            String name = etName.getText().toString().trim();
                            String mobile = etMobile.getText().toString().trim();
                            String cityCode = citiesData.getCityCode();
                            String cityName = citiesData.getCityName();

                            Map<String, String> map = new HashMap<>();
                            map.put(FirebaseConstants.ChildNodes.FULL_NAME, name);
                            map.put(FirebaseConstants.ChildNodes.MOBILE_NUMBER, mobile);
                            map.put(FirebaseConstants.ChildNodes.CITY_CODE, cityCode);
                            map.put(FirebaseConstants.ChildNodes.CITY_NAME, cityName);

                            FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.USERS)
                                    .child(user.getUid())
                                    .setValue(map, (error, ref) -> {
                                        if (error == null) {
                                            Toast.makeText(this, "Sign up successfully.", Toast.LENGTH_SHORT).show();
                                            moveToLogin();
//                                            sendVerificationEmail(user);
                                        } else {
                                            Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                        hideProgress();
                                    });


                        } else {
                            hideProgress();
                            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        hideProgress();
                        if (task.getException() != null) {
                            Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                });
    }

    private boolean validated() {

        String name = etName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        if (name.isEmpty()) {
            displayError(getString(R.string.error_full_name_empty));
            return false;
        }

        if (email.isEmpty() || !Constants.getInstance().isValidEmail(email)) {
            displayError(Constants.INVALID_EMAIL);
            inputLayoutEmail.setError("e.g. ali@gmail.com");
            return false;
        }

        if (citiesData == null) {
            displayError(getString(R.string.error_cities));
            return false;
        }

        if (password.isEmpty() || !Constants.getInstance().isValidPassword(password)) {
            displayError(Constants.INVALID_PASSWORD);
            inputLayoutPassword.setError("e.g. Ali@123");
            return false;
        }

        if (!password.equals(confirmPassword)) {
            displayError(getString(R.string.error_passwords_mismatched));
            return false;
        }

        if (!cbTerms.isChecked()) {
            displayError(getString(R.string.error_terms_conditions_unchecked));
            return false;
        }

        return true;
    }

    /*
    private void sendVerificationEmail(FirebaseUser user) {

        user.sendEmailVerification()
                .addOnCompleteListener(this, task -> {

                    if (task.isSuccessful()) {
                        Toast.makeText(this,
                                "Sign up successfully and Verification email sent to " + user.getEmail(),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Sign up successfully", Toast.LENGTH_SHORT).show();
                    }

                    moveToLogin();

                });
    }
*/

    private void moveToLogin() {
        Intent intent = getIntent();
        intent.putExtra(IntentEnums.EMAIL.name(), etEmail.getText().toString().trim());
        intent.putExtra(IntentEnums.PASSWORD.name(), etPassword.getText().toString().trim());
        setResult(RESULT_OK, intent);
        finish();
    }


    public void onClick(View view) {

        final int btnSignUp = R.id.signUp_btnSignUp, btnSignIn = R.id.signUp_tvSignIn;
        final int ivBack = R.id.signUp_ivBack, ivPasswordVisibility = R.id.signUp_ivPasswordVisibility;
        final int ivConfirmPasswordVisibility = R.id.signUp_ivConfirmPasswordVisibility;

        switch (view.getId()) {

            case btnSignUp:
                createNewUser();
                break;
            case btnSignIn:
            case ivBack:
                finish();
                break;
            case ivPasswordVisibility:
            case ivConfirmPasswordVisibility:
                togglePasswordVisibility();
                break;
        }

    }


    private void togglePasswordVisibility() {

        if (isPasswordVisible) {
            etPassword.setTransformationMethod(new PasswordTransformationMethod());
            etConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
            isPasswordVisible = false;
            ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_on);
            ivConfirmPasswordVisibility.setImageResource(R.drawable.ic_visibility_on);
        } else {
            etPassword.setTransformationMethod(null);
            etConfirmPassword.setTransformationMethod(null);
            isPasswordVisible = true;
            ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_off);
            ivConfirmPasswordVisibility.setImageResource(R.drawable.ic_visibility_off);
        }
        etPassword.setSelection(etPassword.getText().toString().length());
        etConfirmPassword.setSelection(etConfirmPassword.getText().toString().length());
    }

    public void showProgress() {
        loader.setVisibility(View.VISIBLE);
        btnSignUp.setVisibility(View.GONE);
    }

    public void hideProgress() {
        loader.setVisibility(View.GONE);
        btnSignUp.setVisibility(View.VISIBLE);
    }

    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

}