package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.AdmissionsDAO;
import com.codegenetics.eduvision.databases.AdmissionsDBModel;
import com.codegenetics.eduvision.databases.AdmissionsDBModelBuilder;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.FirebaseConstants;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.Admissions.AdmissionsModel.AdmissionsData;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.activities.FullScreenImages;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class AdmissionsRecyclerAdapter extends RecyclerView.Adapter<AdmissionsRecyclerAdapter.AdmissionsHolder> {

    private final ArrayList<AdmissionsData> admissionsDataArrayList;
    private AdmissionsDAO admissionsDAO;

    private final BroadcastReceiver  broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            notifyDataSetChanged();
        }
    };

    public AdmissionsRecyclerAdapter(ArrayList<AdmissionsData> admissionsDataArrayList) {
        this.admissionsDataArrayList = admissionsDataArrayList;
    }

    @NonNull
    @Override
    public AdmissionsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        admissionsDAO = DatabaseInjection.getAdmissionsDAO(parent.getContext());
        parent.getContext().registerReceiver(broadcastReceiver, new IntentFilter(Constants.Cases.ADMISSIONS));
        return new AdmissionsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_admissions_planner_result, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdmissionsHolder holder, int position) {
        holder.bindItems(admissionsDataArrayList.get(holder.getAdapterPosition()), admissionsDAO);
    }

    @Override
    public int getItemCount() {
        return admissionsDataArrayList == null ? 0 : admissionsDataArrayList.size();
    }

    static class AdmissionsHolder extends RecyclerView.ViewHolder {
        private final ImageView ivLogo;
        private final TextView tvInstituteName, tvLevel, tvDiscipline, tvDisciplineName, tvDeadline, tvAppliedThrough, tvStatus;
        private final MKLoader loaderImage;

        public AdmissionsHolder(@NonNull View itemView) {
            super(itemView);
            ivLogo = itemView.findViewById(R.id.iv_logo);
            tvInstituteName = itemView.findViewById(R.id.tv_institute_name);
            tvLevel = itemView.findViewById(R.id.tv_level);
            tvDiscipline = itemView.findViewById(R.id.tv_discipline);
            tvDisciplineName = itemView.findViewById(R.id.tv_discipline_name);
            tvDeadline = itemView.findViewById(R.id.tv_deadline);
            tvAppliedThrough = itemView.findViewById(R.id.tv_applied_through);
            tvStatus = itemView.findViewById(R.id.tv_status);
            loaderImage = itemView.findViewById(R.id.loader_image);
        }

        public void bindItems(AdmissionsData data, AdmissionsDAO admissionsDAO) {

            try {

                AtomicReference<AdmissionsDBModel> item = new AtomicReference<>(admissionsDAO.getItem(data.getId()));

                tvAppliedThrough.setVisibility(
                        data.getApply_ev().equalsIgnoreCase("no") ?
                                View.GONE :View.VISIBLE
                );

                tvStatus.setText(R.string.have_you_applied);
                tvAppliedThrough.setText(R.string.apply_through_eduvision);

                if (item.get() != null) {

                    switch (item.get().getApplyStatus()) {
                        case 0:
                            tvStatus.setText(R.string.have_you_applied);
                            tvStatus.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.selector_red_leaf));
                            break;
                        case 1:
                            tvStatus.setText(R.string.admission_confirmed_question);
                            tvStatus.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.selector_yellow_leaf));
                            break;
                        case 2:
                            tvStatus.setText(R.string.admission_confirm);
                            tvStatus.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.selector_green_leaf));
                    }

                    if (item.get().getAppliedThroughEduvision() == 1) tvAppliedThrough.setVisibility(View.GONE);

                }

                tvStatus.setOnClickListener(v -> {

                    switch (tvStatus.getText().toString()) {
                        case "Have you applied? Yes | No":
                            showStatusAlert(
                                    "Are you sure you have applied in this institute?",
                                    position -> {
                                        if (item.get() == null) {
                                            item.set(new AdmissionsDBModelBuilder()
                                                    .setId(data.getId())
                                                    .setApplyStatus(1)
                                                    .setAppliedThroughEduvision(0)
                                                    .setInstitute_id(data.getInstitute_id())
                                                    .setIns_logo(data.getInstituteLogo())
                                                    .setIns_abb(data.getInstituteAbbrivation())
                                                    .setCity_name(data.getCityName())
                                                    .setName(data.getName())
                                                    .setDeadline(data.getDeadline())
                                                    .setLevel_code(data.getLevel_code())
                                                    .setLevel_name(data.getLevelName())
                                                    .setDiscipline_name(data.getDisciplineName())
                                                    .setDiscipline_type(data.getDisciplineType())
                                                    .setInstitute_ad(data.getInstituteAd())
                                                    .setApply_ev(data.getApply_ev())
                                                    .createAdmissionsDBModel());
                                            admissionsDAO.addNewItem(item.get());
                                        } else {
                                            admissionsDAO.updateApplyStatus(data.getId(), 1);
                                        }
                                        tvStatus.setText(R.string.admission_confirmed_question);
                                        tvStatus.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.selector_yellow_leaf));

                                    });
                            break;
                        case "Admission Confirmed?":
                            showStatusAlert(
                                    "Are you sure your admission is confirmed in this institute?",
                                    position -> {
                                        if (item.get() == null) {
                                            item.set(new AdmissionsDBModelBuilder().setId(data.getId()).setApplyStatus(2).setAppliedThroughEduvision(0).createAdmissionsDBModel());
                                            admissionsDAO.addNewItem(item.get());
                                        }else {
                                            admissionsDAO.updateApplyStatus(data.getId(), 2);
                                        }
                                        tvStatus.setText(R.string.admission_confirm);
                                        tvStatus.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.selector_green_leaf));
                                    });
                            break;
                    }
                });

                tvAppliedThrough.setOnClickListener(v ->
                        showAppliedAlert(position -> {
                            if (item.get() == null) {
                                item.set(new AdmissionsDBModelBuilder().setId(data.getId()).setApplyStatus(0).setAppliedThroughEduvision(1).createAdmissionsDBModel());
                                admissionsDAO.addNewItem(item.get());
                            } else {
                                admissionsDAO.updateEduvisionStatus(data.getId(), 1);
                            }

                            tvAppliedThrough.setVisibility(View.GONE);

                            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

                            if (currentUser != null) {

                                Map<String, String> map = new HashMap<>();
                                map.put(FirebaseConstants.ChildNodes.FULL_NAME, Session.getInstance().getName());
                                map.put(FirebaseConstants.ChildNodes.MOBILE_NUMBER, Session.getInstance().getPhone());
                                map.put(FirebaseConstants.ChildNodes.EMAIL, Constants.getInstance().encode(Session.getInstance().getEmail()));
                                map.put(FirebaseConstants.ChildNodes.INSTITUTE, data.getName());
                                map.put(FirebaseConstants.ChildNodes.PROGRAM, data.getDisciplineType());
                                map.put(FirebaseConstants.ChildNodes.LEVEL, data.getLevelName());

                                FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.USER_FORMS)
                                        .child(currentUser.getUid())
                                        .child(data.getId())
                                        .setValue(map);
                            }

                        }));

                Glide.with(itemView)
                        .load(data.getInstituteLogo())
                        .placeholder(R.color.gray_light1)
                        .placeholder(R.drawable.ic_no_image)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                loaderImage.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                loaderImage.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivLogo);

                tvInstituteName.setText(
                        String.format(
                                Locale.getDefault(),
                                "%s, %s, %s admission",
                                data.getName(), data.getCityName(), data.getInstituteAbbrivation()
                        )
                );

                tvLevel.setText(data.getLevelName());

                tvDiscipline.setText(data.getDisciplineType());

                tvDisciplineName.setText(data.getDisciplineName() == null ? "" : data.getDisciplineName());

                tvDeadline.setText(data.getDeadline());

                itemView.setOnClickListener(view ->
                        view.getContext().startActivity(
                                new Intent(view.getContext(), FullScreenImages.class)
                                        .putExtra(IntentEnums.DEADLINE.toString(), data.getDeadline())
                                        .putExtra(IntentEnums.IMAGES.toString(), data.getInstituteAd().split(";"))
                                        .putExtra(IntentEnums.POSITION.toString(), 0)
                                        .putExtra(IntentEnums.HIDE.toString(), true)
                        )
                );


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        private void showStatusAlert(String message, IViewClickListener listener) {
            AlertDialog.Builder builder = new AlertDialog.Builder( itemView.getContext());
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", (dialog, which) -> listener.onItemClicked(1));
            builder.setNegativeButton("No", null);
            builder.show();
        }
        private void showAppliedAlert(IViewClickListener listener) {
            AlertDialog.Builder builder = new AlertDialog.Builder( itemView.getContext());
            builder.setMessage("Are you sure you want to apply through EDUVISION?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", (dialog, which) -> listener.onItemClicked(1));
            builder.setNegativeButton("No", null);
            builder.show();
        }

    }
}
