package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

@SuppressWarnings("unchecked")
public class DisciplinesSpinnerAdapter extends ArrayAdapter<DisciplinesModel.DisciplinesData> {

    private final ArrayList<DisciplinesModel.DisciplinesData> values, tempValues, suggestions;

    public DisciplinesSpinnerAdapter(Context context, int textViewResourceId, ArrayList<DisciplinesModel.DisciplinesData> values) {
        super(context, textViewResourceId, values);
        this.values = values;
        tempValues = new ArrayList<>(values);
        suggestions = new ArrayList<>();
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public DisciplinesModel.DisciplinesData getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getDisciplineName());
        return label;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return fruitFilter;
    }

    private final Filter fruitFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            DisciplinesModel.DisciplinesData data = (DisciplinesModel.DisciplinesData) resultValue;
            return data.getDisciplineName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if (charSequence != null) {
                suggestions.clear();
                for (DisciplinesModel.DisciplinesData data: tempValues) {
                    if (data.getDisciplineName().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(data);
                    }
                }FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<DisciplinesModel.DisciplinesData> tempValues = (ArrayList<DisciplinesModel.DisciplinesData>) filterResults.values;
            if (filterResults.count > 0) {
                clear();
                for (DisciplinesModel.DisciplinesData obj : tempValues) {
                    add(obj);
                }
                notifyDataSetChanged();
            } else {
                clear();
                notifyDataSetChanged();
            }
        }

    };
}