package com.codegenetics.eduvision.views.activities;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.helper.EndlessNestedScrollViewListener;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.interfaces.ISelectedFiltersClick;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.models.DisciplineTypes.DisciplineTypesModel;
import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel;
import com.codegenetics.eduvision.models.FilterModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.models.Programs.ProgramsOfferedModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.utils.Utils;
import com.codegenetics.eduvision.views.adapters.CitiesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.DisciplinesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.FilterRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.LevelsSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.SelectedFilterRecycler;
import com.codegenetics.eduvision.views.adapters.UniversitiesRecycler;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class Universities extends AppCompatActivity implements IData, IFilterClick, ISelectedFiltersClick {

    private ServerPresenter presenter;
    private LinearLayout llCourses, llBottomSheetBottom;
    private MKLoader loader, loaderBottomSheet;
    private Button btnSearchBottomSheet;
    private TextView tvUniversitiesTop;
    private View viewUniversities, viewNoData;

    private final ArrayList<CitiesModel.CitiesData> citiesDataArrayList = new ArrayList<>();
    private final ArrayList<String> disciplinesStringArray = new ArrayList<>();
    private ArrayList<ProgramsOfferedModel.ProgramsOfferedData> arrayList;
    private UniversitiesRecycler universitiesRecycler;

    private Spinner spDisciplineType, spLevel;
    private AutoCompleteTextView etDisciplineName, etCity;

    private CitiesModel.CitiesData citiesData;
    private DisciplinesModel.DisciplinesData disciplinesData;

    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    private SelectedFilterRecycler selectedFilterRecycler;
    private final List<FilterModel> selectedFilters = new ArrayList<>();

    private String levelCode = "", disciplineName = "", disciplineType = "";
    private String cityName = "", levelName = "";

    private void init() {

        disciplineType = getIntent().getStringExtra(IntentEnums.DISCIPLINE_TYPE.name());
        disciplineName = getIntent().getStringExtra(IntentEnums.DISCIPLINE_NAME.name());
        cityName = getIntent().getStringExtra(IntentEnums.CITY.name());
        levelCode = getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name());
        levelName = getIntent().getStringExtra(IntentEnums.LEVEL_NAME.name());

        presenter = ServerPresenter.getInstance(this);

        citiesDataArrayList.addAll(Constants.citiesList);
        arrayList = new ArrayList<>();
        universitiesRecycler = new UniversitiesRecycler(arrayList);

        viewNoData = findViewById(R.id.no_data);
        viewUniversities = findViewById(R.id.view_universities);

        loader = findViewById(R.id.loader);
        tvUniversitiesTop = findViewById(R.id.tv_universities_top);
        NestedScrollView nestedScrollView = findViewById(R.id.nested_scroll);

        RecyclerView rvFilters = findViewById(R.id.rv_filters);
        RecyclerView rvFiltersSelected = findViewById(R.id.rv_filters_selected);

        rvFilters.setHasFixedSize(true);
        rvFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvFilters.setAdapter(new FilterRecyclerAdapter(ConstantData.getInstance().getAdmissionFilter(), this));

        rvFiltersSelected.setHasFixedSize(true);
        rvFiltersSelected.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectedFilterRecycler = new SelectedFilterRecycler(selectedFilters, this);
        rvFiltersSelected.setAdapter(selectedFilterRecycler);

        RecyclerView recyclerView = findViewById(R.id.rv_universities);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(universitiesRecycler);


        nestedScrollView.setOnScrollChangeListener(new EndlessNestedScrollViewListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                fetchData(page);
            }
        });

        View v = findViewById(R.id.bottomSheet_Layout);
        mBehavior = BottomSheetBehavior.from(v);

        for (DisciplineTypesModel.DisciplineTypesData data : Constants.disciplineTypeList) {
            disciplinesStringArray.add(data.getDisciplineType());
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universities);

        init();

        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        Constants.getInstance().showInterstitial(this);

        setTitle();

        fetchData(1);

    }

    private void fetchData(int page) {

        if (Internet.isConnected(this)) {

            presenter.getData(AppEnums.PROGRAMS, String.valueOf(page), disciplineType, disciplineName, cityName, levelCode);

        } else {
            Internet.showOfflineDialog(this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        if (enums.equals(AppEnums.DISCIPLINES)) {
            loaderBottomSheet.setVisibility(View.VISIBLE);
            btnSearchBottomSheet.setVisibility(View.INVISIBLE);
        } else {
            loader.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress(AppEnums enums) {
        if (enums.equals(AppEnums.DISCIPLINES)) {
            loaderBottomSheet.setVisibility(View.GONE);
            btnSearchBottomSheet.setVisibility(View.VISIBLE);
        } else loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {

            if (!response.isEmpty()) {

                CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);

                switch (caseModel.getCase()) {

                    case Constants.Cases.PROGRAMS:
                        ProgramsOfferedModel model = new Gson().fromJson(response, ProgramsOfferedModel.class);
                        if (model.isSuccess()) {

                            if (model.getData() != null) {
                                arrayList.addAll(model.getData());
                                universitiesRecycler.notifyDataSetChanged();
                                AppCompatTextView tvNumber = findViewById(R.id.tv_number);
                                tvNumber.setText(model.getRecords());
                                tvUniversitiesTop.setText(
                                        String.format(
                                                Locale.getDefault(),
                                                "universities/institutes are offering %s at %s Level in %s.",
                                                disciplineName, levelName, cityName.isEmpty() ? "Pakistan" : cityName
                                        )
                                );

                                viewUniversities.setVisibility(View.VISIBLE);
                                viewNoData.setVisibility(View.GONE);

                            } else {
                                if (arrayList.size() <= 0) {
                                    viewNoData.setVisibility(View.VISIBLE);
                                    viewUniversities.setVisibility(View.GONE);
                                }
                            }

                        }
                        else {
                            displayError(model.getMessage());
                        }
                        break;
                    case Constants.Cases.DISCIPLINES:

                        DisciplinesModel disciplinesModel = new Gson().fromJson(response, DisciplinesModel.class);

                        if (disciplinesModel.isSuccess()) {

                            if (disciplinesModel.isSuccess() && disciplinesModel.getData() != null) {

                                DisciplinesSpinnerAdapter disciplinesSpinnerAdapter = new DisciplinesSpinnerAdapter(this, R.layout.layout_spinner, disciplinesModel.getData());
                                etDisciplineName.setAdapter(disciplinesSpinnerAdapter);
                                etDisciplineName.setOnItemClickListener((adapterView, view1, i, l) -> disciplinesData = disciplinesSpinnerAdapter.getItem(i));
                                etDisciplineName.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                        disciplinesData = null;
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable editable) {

                                    }
                                });

                                llBottomSheetBottom.setWeightSum(2);
                                llCourses.setVisibility(View.VISIBLE);

                            }
                            else {

                                llBottomSheetBottom.setWeightSum(1);
                                llCourses.setVisibility(View.GONE);
                            }

                        }
                        else {
                            displayError(disciplinesModel.getMessage());
                        }

                }


            }

        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {

                @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_admissions, null);

                spDisciplineType = view.findViewById(R.id.sp_discipline_type);
                spLevel = view.findViewById(R.id.sp_level);
                etCity = view.findViewById(R.id.et_city);
                etDisciplineName = view.findViewById(R.id.et_courses);
                loaderBottomSheet = view.findViewById(R.id.loader);
                llCourses = view.findViewById(R.id.linear_layout_courses);
                llBottomSheetBottom = view.findViewById(R.id.linear_layout_bottom);

                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                btnSearchBottomSheet = view.findViewById(R.id.btn_search);

                fillSpinnerAdapter();

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearchBottomSheet.setOnClickListener(view12 -> {

                    disciplineType = spDisciplineType.getSelectedItem().toString();

                    if (disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {
                        displayError("Please select discipline type.");
                        return;
                    }

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();
                    if (levelsData != null) {
                        levelCode = levelsData.getLevelCode();
                        levelName = levelsData.getLevelName();
                    }
                    if (levelName.equalsIgnoreCase(ConstantData.SELECT)) {
                        displayError("Please select level.");
                        return;
                    }

                    if (llCourses.getVisibility() == View.VISIBLE) {
                        if (disciplinesData != null)
                            disciplineName = disciplinesData.getDisciplineName();
                        else {
                            displayError("Invalid course. Please select course.");
                            return;
                        }
                    } else {
                        disciplineName = "";
                    }

                    if (citiesData != null) cityName = citiesData.getCityName();
                    else cityName = "";

                    arrayList.clear();
                    universitiesRecycler.notifyDataSetChanged();
                    viewUniversities.setVisibility(View.GONE);

                    mBottomSheetDialog.dismiss();

                    setTitle();

                    prepareDataForSelectedFilterRecyclerView();

                    fetchData(1);

                });

                mBottomSheetDialog = new BottomSheetDialog(this);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
            }

            if (mBottomSheetDialog != null) mBottomSheetDialog.show();
//            mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);


        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTitle() {
        String title = "Search Results";

        if (!disciplineName.isEmpty()) title = disciplineName;
        else if (!disciplineType.isEmpty()) title = disciplineType;
        else if (!cityName.isEmpty()) title = cityName;
        else if (!levelName.isEmpty()) title = levelName;

        if (getSupportActionBar() != null) getSupportActionBar().setTitle(title);

    }

    private void fillSpinnerAdapter() {

        if (spDisciplineType == null || spDisciplineType.getSelectedItem() == null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, disciplinesStringArray);
            spDisciplineType.setAdapter(adapter);

            CitiesSpinnerAdapter citiesSpinnerAdapter = new CitiesSpinnerAdapter(this, R.layout.layout_spinner, citiesDataArrayList);
            etCity.setAdapter(citiesSpinnerAdapter);
            etCity.setOnItemClickListener((adapterView, view1, i, l) -> citiesData = citiesSpinnerAdapter.getItem(i));
            etCity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    citiesData = null;
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            LevelsSpinnerAdapter levelsSpinnerAdapter = new LevelsSpinnerAdapter(this, R.layout.layout_spinner, Constants.levelsList);
            spLevel.setAdapter(levelsSpinnerAdapter);

            spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    String disciplineType = spDisciplineType.getSelectedItem().toString();
                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(levelsData.getLevelName())) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {

                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            spDisciplineType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(adapter.getItem(i))) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!levelsData.getLevelName().equalsIgnoreCase(ConstantData.SELECT)) {

                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
    }

    private void prepareDataForSelectedFilterRecyclerView() {

        selectedFilters.clear();

        if (!disciplineType.isEmpty())
            if (!disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {
                selectedFilters.add(new FilterModel(0, disciplineType, SearchFilter.DISCIPLINE_TYPE));
            }

        if (!levelName.isEmpty())
            if (!levelName.equalsIgnoreCase(ConstantData.SELECT)) {
                selectedFilters.add(new FilterModel(0, levelName, SearchFilter.LEVEL));
            }

        if (!disciplineName.isEmpty()) {
            selectedFilters.add(new FilterModel(0, disciplineName, SearchFilter.COURSES));
        }

        if (!cityName.isEmpty()) {
            selectedFilters.add(new FilterModel(0, cityName, SearchFilter.CITY));
        }

        selectedFilterRecycler.notifyDataSetChanged();
    }

    private void handleBottomSheetViewsOnFilterSelection(SearchFilter searchFilter) {
        switch (searchFilter) {
            case DISCIPLINE_TYPE:
                spDisciplineType.performClick();
                break;
            case LEVEL:
                spLevel.performClick();
                break;
            case CITY:
                etCity.requestFocus();
                Utils.showKeyboard(this);
                break;
            case COURSES:
                if (etDisciplineName.getVisibility() == View.VISIBLE) {
                    etDisciplineName.requestFocus();
                    Utils.showKeyboard(this);
                } else {
                    Toast.makeText(this, "Please ", Toast.LENGTH_SHORT).show();
                }

            default:
                break;
        }
    }

    @Override
    public void onFilterSelected(SearchFilter searchFilter) {
        showBottomSheet();
        new Handler().postDelayed(() -> handleBottomSheetViewsOnFilterSelection(searchFilter), 100);

    }

    @Override
    public void onFilterDelete(FilterModel data) {
        switch (data.getSearchFilter()) {
            case DISCIPLINE_TYPE:
                disciplineType = "";
                spDisciplineType.setSelection(0);
                break;
            case LEVEL:
                levelCode = "";
                spLevel.setSelection(0);
                break;
            case CITY:
                etCity.setText("");
                cityName = "";
                break;
            case COURSES:
                disciplineName = "";
                etDisciplineName.setText("");
                break;
        }
        if (selectedFilters.size() > 1) {
            btnSearchBottomSheet.performClick();
        } else {
            selectedFilters.clear();
            selectedFilterRecycler.notifyDataSetChanged();

            presenter.getData(AppEnums.PROGRAMS, String.valueOf(1), "", "", "", "");
        }

    }


}