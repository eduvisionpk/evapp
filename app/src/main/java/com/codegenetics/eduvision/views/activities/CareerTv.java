package com.codegenetics.eduvision.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.EndlessRecyclerViewScrollListener;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.interfaces.IVideoPLayList;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.models.Videos.VideoCategoriesModel;
import com.codegenetics.eduvision.models.Videos.VideoDetailsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.CareerTvTabRecycler;
import com.codegenetics.eduvision.views.adapters.VideoPlaylistRecycler;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class CareerTv extends AppCompatActivity implements ITabClickListener, IData, IVideoPLayList {

    private ServerPresenter presenter;
    private RecyclerView rvTabs;
    private final ArrayList<VideoCategoriesModel.VideoCategoriesData> tabsArrayList = new ArrayList<>();
    private final ArrayList<VideoDetailsModel.VideoDetailsData> videoPlaylistArrayList = new ArrayList<>();
    private VideoPlaylistRecycler videoPlaylistRecycler;
    private CareerTvTabRecycler tabRecycler;
    private MKLoader loader;
    private String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_tv);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.career_tv);
            getSupportActionBar().setElevation(0);
        }

        initViews();

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            int position = getIntent().getIntExtra(IntentEnums.POSITION.name(), 0);

            tabsArrayList.addAll(getIntent().getParcelableArrayListExtra(IntentEnums.DATA.name()));
            tabRecycler.updateSelectedPosition(position);
            tabRecycler.notifyDataSetChanged();

            if (tabsArrayList.size() > position) {
                type = tabsArrayList.get(position).getType();
                rvTabs.scrollToPosition(position);
                getVideosUsingType(type, 1);
            }
        }
        else if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.VIDEO_CATEGORIES);
        }
        else {
            Internet.showOfflineDialog(this);
        }

    }

    private void initViews() {
        presenter = ServerPresenter.getInstance(this);

        loader = findViewById(R.id.loader);

        rvTabs = findViewById(R.id.rv_tabs);
        rvTabs.setHasFixedSize(true);
        rvTabs.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        tabRecycler = new CareerTvTabRecycler(tabsArrayList, this);
        rvTabs.setAdapter(tabRecycler);

        RecyclerView rvVideoPlaylist = findViewById(R.id.rv_video_playlist);
        rvVideoPlaylist.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvVideoPlaylist.setLayoutManager(layoutManager);
        videoPlaylistRecycler = new VideoPlaylistRecycler(videoPlaylistArrayList, this);
        rvVideoPlaylist.setAdapter(videoPlaylistRecycler);

        rvVideoPlaylist.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                getVideosUsingType(type, page);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabsSelected(String s) {
        getVideosUsingType(type = s, 1);
    }

    private void getVideosUsingType(String type, int page) {
        if (Internet.isConnected(this)) {
            if (page == 1) {
                videoPlaylistArrayList.clear();
                videoPlaylistRecycler.notifyDataSetChanged();
            }
            presenter.getData(AppEnums.VIDEO_DETAILS, String.valueOf(page), type);
        } else {
            Internet.showOfflineDialog(this);
        }
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        try {
            if (!response.isEmpty()) {
                CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);

                switch (caseModel.getCase()) {
                    case Constants.Cases.VIDEO_CATEGORIES:

                        VideoCategoriesModel categoriesModel = new Gson().fromJson(response, VideoCategoriesModel.class);

                        if (categoriesModel.isSuccess()) {
                            if (categoriesModel.getData() != null) {

                                tabsArrayList.addAll(categoriesModel.getData());
                                tabRecycler.notifyDataSetChanged();
                            }

                            if (tabsArrayList.size() > 0) {
                                type = tabsArrayList.get(0).getType();
                                getVideosUsingType(type, 1);
                            }

                            break;
                        }
                    case Constants.Cases.VIDEO_DETAILS:
                        VideoDetailsModel videoDetailsModel = new Gson().fromJson(response, VideoDetailsModel.class);
                        if (videoDetailsModel.isSuccess()) {

                            if (videoDetailsModel.getData() != null) {
                                videoPlaylistArrayList.addAll(videoDetailsModel.getData());
                                videoPlaylistRecycler.notifyDataSetChanged();
                            }

                            break;
                        }
                }
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVideoSelected(VideoDetailsModel.VideoDetailsData data) {
        startActivity(new Intent(this, CareerTvDetails.class)
                .putExtra(IntentEnums.DATA.name(), data));
    }
}