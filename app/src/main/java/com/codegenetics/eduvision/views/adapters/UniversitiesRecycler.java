package com.codegenetics.eduvision.views.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.databases.UniversitiesDAO;
import com.codegenetics.eduvision.databases.UniversityDBModel;
import com.codegenetics.eduvision.enums.BroadcastReceiverEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.Programs.ProgramsOfferedModel;
import com.codegenetics.eduvision.views.activities.UniversityDetails;

import java.util.ArrayList;
import java.util.Locale;

public class UniversitiesRecycler extends RecyclerView.Adapter<UniversitiesRecycler.UniversitiesHolder> {

    private final ArrayList<ProgramsOfferedModel.ProgramsOfferedData> arrayList;
    private UniversitiesDAO universitiesDAO;
    private boolean fromFavourites = false;

    private final BroadcastReceiver receiverFavourite = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {

                String id = intent.getStringExtra(IntentEnums.INSTITUTE_ID.name());

                for (int i = 0; i < arrayList.size(); i++) {

                    if (
                            (arrayList.get(i).getInstituteId() + "_" +
                                    arrayList.get(i).getDiscipline_name())
                                    .equalsIgnoreCase(id)
                    ) {
                        notifyDataSetChanged();
                        break;
                    }

                }
            }

        }
    };

    public UniversitiesRecycler(ArrayList<ProgramsOfferedModel.ProgramsOfferedData> arrayList, boolean fromFavourites) {
        this.arrayList = arrayList;
        this.fromFavourites = fromFavourites;
    }

    public UniversitiesRecycler(ArrayList<ProgramsOfferedModel.ProgramsOfferedData> arrayList) {
        this.arrayList = arrayList;

    }

    @NonNull
    @Override
    public UniversitiesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        universitiesDAO = DatabaseInjection.getUniversitiesDAO(parent.getContext());
        parent.getContext().registerReceiver(receiverFavourite, new IntentFilter(BroadcastReceiverEnums.FAVOURITE_RECEIVER.name()));
        return new UniversitiesHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_universities, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UniversitiesHolder holder, int position) {

        ProgramsOfferedModel.ProgramsOfferedData data = arrayList.get(holder.getAdapterPosition());
        holder.bindItems(data);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class UniversitiesHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView tvUniName, tvCity, tvDegreeDuration, tvFee, tvDeadline, tvFavourite;
        private final View viewAddToFav;
        private final CheckBox cbFavIcon;

        public UniversitiesHolder(@NonNull View itemView) {
            super(itemView);
            tvUniName = itemView.findViewById(R.id.tv_uni_name);
            tvCity = itemView.findViewById(R.id.tv_city);
            tvDegreeDuration = itemView.findViewById(R.id.tv_degree_duration);
            tvFee = itemView.findViewById(R.id.tv_fee);
            tvDeadline = itemView.findViewById(R.id.tv_deadline);
            tvFavourite = itemView.findViewById(R.id.tv_favourite);
            viewAddToFav = itemView.findViewById(R.id.view_favorite);
            cbFavIcon = itemView.findViewById(R.id.check_box_favourite);

        }

        public void bindItems(ProgramsOfferedModel.ProgramsOfferedData data) {
            if (data.getIns_abb() !=null && !data.getIns_abb().isEmpty()) {
                tvUniName.setText(String.format(Locale.getDefault(), "%s - %s", data.getIns_abb(), data.getName()));
            } else {
                tvUniName.setText(data.getName());
            }
            tvCity.setText(data.getCityName());
            tvDegreeDuration.setText(
                    String.format(Locale.getDefault(), "%s/%s %s", data.getDegreeAbb(), data.getDuration(), data.getDurationMode())
            );
            tvFee.setText(data.getFeeLocal());
            tvDeadline.setText(data.getDeadline());

            cbFavIcon.setChecked(universitiesDAO.getItem(data.getInstituteId() + "_" + data.getDiscipline_name()) > 0);

            tvFavourite.setText(
                    String.format(Locale.getDefault(), "%s favourite",
                            cbFavIcon.isChecked() ? "Remove" : "Add to"
                    )
            );

            itemView.setOnClickListener(view ->
                    view.getContext().startActivity(new Intent(view.getContext(), UniversityDetails.class)
                            .putExtra(IntentEnums.INSTITUTE_ID.name(), data.getInstituteId())
                            .putExtra(IntentEnums.DISCIPLINE_NAME.name(), data.getDiscipline_name())
                    )
            );

            viewAddToFav.setOnClickListener(view -> {
                if (cbFavIcon.isChecked()) {
                    universitiesDAO.deleteItem(data.getInstituteId() + "_" + data.getDiscipline_name());

//                    cbFavIcon.setChecked(false);
                    if (fromFavourites) {
                        arrayList.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                    }

                    Intent intent = new Intent();
                    intent.setAction(BroadcastReceiverEnums.FAVOURITE_RECEIVER.name());
                    intent.putExtra(IntentEnums.INSTITUTE_ID.name(), data.getInstituteId() + "_" + data.getDiscipline_name());
                    view.getContext().sendBroadcast(intent);

                } else {
                    universitiesDAO.addNewItem(UniversityDBModel.getDBModel(data));
                    cbFavIcon.setChecked(true);
                }
                tvFavourite.setText(
                        String.format(Locale.getDefault(), "%s favourite",
                                cbFavIcon.isChecked() ? "Remove" : "Add to"
                        )
                );
            });
        }
    }
}
