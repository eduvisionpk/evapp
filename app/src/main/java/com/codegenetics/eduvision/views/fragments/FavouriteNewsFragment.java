package com.codegenetics.eduvision.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.databases.NewsDBModel;
import com.codegenetics.eduvision.views.adapters.NewsAdapter;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;


public class FavouriteNewsFragment extends Fragment {

    private View view, viewNoData;
    private ArrayList<Object> arrayList;
    private NewsAdapter adapter;
    private MKLoader loader;

    public FavouriteNewsFragment() {
        // Required empty public constructor
    }

    private void init() {

        arrayList = new ArrayList<>();
        adapter = new NewsAdapter(arrayList);

        loader = view.findViewById(R.id.loader);
        viewNoData = view.findViewById(R.id.no_data);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    public static FavouriteNewsFragment newInstance() {
        return new FavouriteNewsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_news, container, false);

        init();
        fetchNews();

        return view;
    }

    private void fetchNews() {

        loader.setVisibility(View.VISIBLE);

        DatabaseInjection.getNewsDAO(getContext())
                .getAll()
                .observeForever(newsDBModels -> {

                    arrayList.clear();

                    for (NewsDBModel model : newsDBModels) {
                        arrayList.add(NewsDBModel.getModel(model));
                    }

                    adapter.notifyDataSetChanged();
                    loader.setVisibility(View.GONE);

                    viewNoData.setVisibility(arrayList.size() > 0 ? View.GONE : View.VISIBLE);

                });

    }

}