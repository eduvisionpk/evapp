package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.codegenetics.eduvision.models.Districts.DistrictsModel.DistrictsData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

@SuppressWarnings({"unchecked"})
public class DistrictsSpinnerAdapter extends ArrayAdapter<DistrictsData> {

    private final ArrayList<DistrictsData> values, tempValues, suggestions;

    public DistrictsSpinnerAdapter(Context context, int textViewResourceId, ArrayList<DistrictsData> values) {
        super(context, textViewResourceId, values);
        this.values = values;
        tempValues = new ArrayList<>(values);
        suggestions = new ArrayList<>();
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public DistrictsData getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getDistrict());
        return label;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            DistrictsData data = (DistrictsData) resultValue;
            return data.getDistrict();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            try {
                if (charSequence != null) {
                    suggestions.clear();
                    for (DistrictsData data : tempValues) {
                        if (data.getDistrict().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                            suggestions.add(data);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            try {
                ArrayList<DistrictsData> tempValues = (ArrayList<DistrictsData>) filterResults.values;
                if (filterResults != null && filterResults.count > 0) {
                    clear();
                    for (DistrictsData obj : tempValues) {
                        add(obj);
                    }
                    notifyDataSetChanged();
                } else {
                    clear();
                    notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    };
}