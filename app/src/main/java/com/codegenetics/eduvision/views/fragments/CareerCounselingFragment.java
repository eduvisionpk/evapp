package com.codegenetics.eduvision.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IRecyclerClick;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.models.Career.CareersModel;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.activities.Details;
import com.codegenetics.eduvision.views.activities.Messages;
import com.codegenetics.eduvision.views.adapters.CareerCounsellingDataRecycler;
import com.codegenetics.eduvision.views.adapters.CareerCounsellingTabRecycler;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class CareerCounselingFragment extends Fragment implements IData, ITabClickListener, IRecyclerClick {

    private View view;
    private Context context;
    private MKLoader loader;
    private TextView tvBookCounselingSession;
    private final ArrayList<CareersModel.CatNamesData> tabsList = new ArrayList<>();
    private final ArrayList<CareersModel.CareersData> dataList = new ArrayList<>();
    private final ArrayList<CareersModel.CareersData> filteredDataList = new ArrayList<>();
    private CareerCounsellingTabRecycler tabRecycler;
    private CareerCounsellingDataRecycler dataRecycler;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_career_counseling, container, false);

        setHasOptionsMenu(true);

        initViews();

        tvBookCounselingSession.setOnClickListener(v -> Constants.getInstance().openLink(context, Constants.BOOK_A_SESSION_LINK));

        return view;
    }


    private void initViews() {
        loader = view.findViewById(R.id.loader);
        tvBookCounselingSession = view.findViewById(R.id.tv_book_counseling_session);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) tvBookCounselingSession.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, 11);

        RecyclerView rvTabs = view.findViewById(R.id.rv_tabs);
        rvTabs.setHasFixedSize(true);
        rvTabs.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        tabRecycler = new CareerCounsellingTabRecycler(tabsList, this);
        rvTabs.setAdapter(tabRecycler);

        RecyclerView rvData = view.findViewById(R.id.rv_data);
        rvData.setHasFixedSize(true);
        rvData.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        dataRecycler = new CareerCounsellingDataRecycler(filteredDataList, this);
        rvData.setAdapter(dataRecycler);

        ServerPresenter presenter = ServerPresenter.getInstance(this);
        if (Internet.isConnected(context)) presenter.getData(AppEnums.CAREER_COUNSELING);
        else Internet.showOfflineDialog((Activity) context);


    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        if (response != null) {
            CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);

            if (Constants.Cases.CAREER_COUNSELING.equals(caseModel.getCase())) {

                CareersModel careersModel = new Gson().fromJson(response, CareersModel.class);

                if (careersModel.getData() != null && careersModel.getData().size() > 0) {
                    dataList.addAll(careersModel.getData());
                }

                tabsList.clear();
                if (careersModel.getCat_names() != null && careersModel.getCat_names().size() > 0) {
                    tabsList.addAll(careersModel.getCat_names());
                }

                tabRecycler.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTabsSelected(String s) {
        filteredDataList.clear();
        filteredDataList.addAll(getFilteredData(s));
        dataRecycler.notifyDataSetChanged();
    }

    private ArrayList<CareersModel.CareersData> getFilteredData(String s) {
        ArrayList<CareersModel.CareersData> mData = new ArrayList<>();

        for (CareersModel.CareersData data : dataList) {
            if (data.getCat_name().equals(s)) mData.add(data);
        }

        return mData;
    }

    @Override
    public void onRecyclerItemClicked(CareersModel.CareersData data) {
        startActivity(
                new Intent(context, Details.class)
                        .putExtra(IntentEnums.DATA.toString(), data)
                        .putExtra(IntentEnums.TYPE.toString(), Constants.Cases.CAREER_COUNSELING)
        );
    }

}