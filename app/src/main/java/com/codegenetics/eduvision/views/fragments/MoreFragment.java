package com.codegenetics.eduvision.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.BuildConfig;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.helper.LayoutType;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.interfaces.IHomeMenuClick;
import com.codegenetics.eduvision.interfaces.IMoreMenuClick;
import com.codegenetics.eduvision.models.HomeModel;
import com.codegenetics.eduvision.models.MoreModel;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.activities.AdmissionPlanner.ConfirmRequirements;
import com.codegenetics.eduvision.views.activities.AdmissionPlanner.Planner;
import com.codegenetics.eduvision.views.activities.AdmissionPlanner.PlannerInfoSession;
import com.codegenetics.eduvision.views.activities.Admissions;
import com.codegenetics.eduvision.views.activities.CareerCounseling;
import com.codegenetics.eduvision.views.activities.CareerTvCategories;
import com.codegenetics.eduvision.views.activities.Careers;
import com.codegenetics.eduvision.views.activities.EditProfile;
import com.codegenetics.eduvision.views.activities.EntryTest;
import com.codegenetics.eduvision.views.activities.FullScreenImages;
import com.codegenetics.eduvision.views.activities.Jobs;
import com.codegenetics.eduvision.views.activities.MeritCalculator;
import com.codegenetics.eduvision.views.activities.Messages;
import com.codegenetics.eduvision.views.activities.News;
import com.codegenetics.eduvision.views.activities.PastPaperSelection;
import com.codegenetics.eduvision.views.activities.Programs;
import com.codegenetics.eduvision.views.activities.Ranking;
import com.codegenetics.eduvision.views.activities.Scholarships;
import com.codegenetics.eduvision.views.adapters.HomeRecycler;
import com.codegenetics.eduvision.views.adapters.MoreRecycler;

import de.hdodenhof.circleimageview.CircleImageView;


public class MoreFragment extends Fragment implements IMoreMenuClick, IHomeMenuClick {

    private View view;
    private Context context;
    private CircleImageView ivUser;
    private TextView tvName, tvEmail, tvPhone, tvEditProfile;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }

    public MoreFragment() {
        // Required empty public constructor
    }

    private void init() {

        ivUser = view.findViewById(R.id.iv_image);
        tvName = view.findViewById(R.id.tv_name);
        tvEmail = view.findViewById(R.id.tv_email);
        tvPhone = view.findViewById(R.id.tv_phone);
        tvEditProfile = view.findViewById(R.id.tv_edit_profile);

        RecyclerView rvMoreMenu = view.findViewById(R.id.rv_more_menu);
        RecyclerView rvHomeMenu = view.findViewById(R.id.rv_home_menus);
        rvMoreMenu.setHasFixedSize(true);
        rvMoreMenu.setLayoutManager(new LinearLayoutManager(context));
        rvMoreMenu.setAdapter(new MoreRecycler(ConstantData.getInstance().getMoreData(), this));

        HomeRecycler homeRecycler = new HomeRecycler(ConstantData.getInstance().getHomeData(), this, LayoutType.HORIZONTAL);
        rvHomeMenu.setHasFixedSize(true);
        rvHomeMenu.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        rvHomeMenu.setAdapter(homeRecycler);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_more, container, false);

        init();

        tvEditProfile.setOnClickListener(view1 -> startActivity(new Intent(context, EditProfile.class)));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        fillData();
    }


    private void fillData() {

        Session session = Session.getInstance();

        tvName.setText(session.getName());
        tvEmail.setText(session.getEmail());

        if (!session.getPhone().isEmpty()) {
            tvPhone.setVisibility(View.VISIBLE);
            tvPhone.setText(session.getPhone());
        } else {
            tvPhone.setVisibility(View.GONE);
        }

        if (!session.getImage().isEmpty()) {

            Glide.with(ivUser)
                    .load(session.getImage())
                    .placeholder(R.drawable.ic_profile_avatar)
                    .error(R.drawable.ic_profile_avatar)
                    .into(ivUser);

            ivUser.setOnClickListener(view1 ->
                    startActivity(
                            new Intent(view.getContext(), FullScreenImages.class)
                                    .putExtra(IntentEnums.IMAGES.toString(), session.getImage().split(";"))
                                    .putExtra(IntentEnums.POSITION.toString(), 0)
                                    .putExtra(IntentEnums.HIDE.toString(), true)
                    )
            );

        }

    }

    @Override
    public void onMoreMenuSelected(MoreModel moreModel) {
        switch (moreModel.getMoreMenu()) {
            case CHANGE_PASSWORD:
                Constants.getInstance().showChangePasswordDialog((Activity) context);
                break;
            case CONTACT_US:
                Constants.getInstance().openLink(context, "https://www.eduvision.edu.pk/about-us/contact.php");
                break;
            case PRIVACY_POLICY:
                Constants.getInstance().openLink(context, "https://www.eduvision.edu.pk/about-us/app-policy.php");
                break;
            case TERMS_CONDITION:
                Constants.getInstance().openLink(context, "https://www.eduvision.edu.pk/about-us/app-terms.php");
                break;
            case ABOUT_US:
                Constants.getInstance().openLink(context, "https://www.eduvision.edu.pk/about-us/eduvision-app.php");
                break;
            case RATE_US:
                Constants.getInstance().rateApp(context);
                break;
            case SHARE:
                String shareMessage = "\nLet me recommend you this application\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                Constants.getInstance().share(context, shareMessage);
                break;
            case LOGOUT:
                Constants.getInstance().showLogoutDialog((Activity) context);
                break;
        }
    }

    @Override
    public void onHomeMenuSelected(HomeModel model) {
        switch (model.getHomeMenu()) {
            case NEWS:
                startActivity(new Intent(context, News.class));
                break;
            case SCHOLARSHIPS:
                startActivity(new Intent(context, Scholarships.class));
                break;
            case ADMISSION:
                startActivity(
                        new Intent(context, Admissions.class)
                                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), "")
                                .putExtra(IntentEnums.DISCIPLINE_NAME.name(), "")
                                .putExtra(IntentEnums.CITY.name(), "")
                                .putExtra(IntentEnums.LEVEL_CODE.name(), "")
                                .putExtra(IntentEnums.LEVEL_NAME.name(), "")
                );
                break;
            case PROGRAM:
                startActivity(new Intent(context, Programs.class));
                break;
            case CAREERS:
                startActivity(new Intent(context, Careers.class));
                break;
            case CAREERS_TV:
                startActivity(new Intent(context, CareerTvCategories.class));
                break;
            case MERIT_CALCULATOR:
                startActivity(new Intent(context, MeritCalculator.class));
                break;
            case ENTRY_TEST:
                startActivity(new Intent(context, EntryTest.class));
                break;
            case CAREER_COUNSELING:
                startActivity(new Intent(context, CareerCounseling.class));
                break;
            case PAST_PAPER:
                startActivity(new Intent(context, PastPaperSelection.class));
                break;
            case BOOK_A_COUNSELING_SESSION:
                startActivity(new Intent(context, Messages.class));
                break;
            case RANKING:
                startActivity(new Intent(context, Ranking.class));
                break;
            case JOBS:
                startActivity(new Intent(context, Jobs.class));
                break;
            case ADMISSION_PLANNER:
                if (PlannerInfoSession.getInstance().getDisciplineTypes().size() > 0) {
                    startActivity(new Intent(context, ConfirmRequirements.class)
                            .putExtra(IntentEnums.LEVEL_CODE.name(), PlannerInfoSession.getInstance().getLevelCode())
                            .putExtra(IntentEnums.LEVEL_NAME.name(), PlannerInfoSession.getInstance().getLevelName())
                            .putExtra(IntentEnums.DATA.name(), PlannerInfoSession.getInstance().getDisciplineTypes())
                    );
                } else {
                    startActivity(new Intent(context, Planner.class));
                }

        }
    }


}