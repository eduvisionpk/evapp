package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.views.activities.FullScreenImages;
import com.github.chrisbanes.photoview.PhotoView;
import com.tuyenmonkey.mkloader.MKLoader;

public class FullScreenViewPagerAdapter extends PagerAdapter {

    private final String[] imageUrls;
    private final boolean isClickable;

    public FullScreenViewPagerAdapter(String[] imageUrls, boolean isClickable) {
        this.imageUrls = imageUrls;
        this.isClickable = isClickable;
    }

    @Override
    public int getCount() {
        if (imageUrls!=null)
            return imageUrls.length;
        else
            return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.view_pager_images_full_screen, container, false);
        final PhotoView photoView = layout.findViewById(R.id.viewPager_ivImage);
        final MKLoader loader = layout.findViewById(R.id.viewPager_loader);

        photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        if (isClickable) {

            Glide.with(photoView)
                    .load(imageUrls[position])
                    .placeholder(R.color.gray_dark)
                    .error(R.drawable.ic_no_image)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            new Handler().post(() -> loader.setVisibility(View.GONE));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            loader.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .signature(new ObjectKey(imageUrls[position]))
                    .into(photoView);

            photoView.setOnClickListener(v -> v.getContext().startActivity(new Intent(v.getContext(), FullScreenImages.class)
                    .putExtra(IntentEnums.IMAGES.toString(), imageUrls)
                    .putExtra(IntentEnums.POSITION.toString(), position)
            ));

        } else {

            Glide.with(photoView)
                    .load(imageUrls[position])
                    .placeholder(R.color.black)
                    .error(R.drawable.ic_no_image)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            new Handler().post(() -> loader.setVisibility(View.GONE));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            loader.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .signature(new ObjectKey(imageUrls[position]))
                    .into(photoView);

        }

        container.addView(layout);

        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
