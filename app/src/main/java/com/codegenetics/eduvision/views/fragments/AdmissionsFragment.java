package com.codegenetics.eduvision.views.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.helper.EndlessNestedScrollViewListener;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.interfaces.ISelectedFiltersClick;
import com.codegenetics.eduvision.models.Admissions.AdmissionsModel;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.models.DisciplineTypes.DisciplineTypesModel;
import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel;
import com.codegenetics.eduvision.models.FilterModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.utils.Utils;
import com.codegenetics.eduvision.views.adapters.AdmissionsAdapter;
import com.codegenetics.eduvision.views.adapters.CitiesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.DisciplinesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.FilterRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.LevelsSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.SelectedFilterRecycler;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class AdmissionsFragment extends Fragment implements IData, IFilterClick, ISelectedFiltersClick {

    private View view;
    private Context context;
    private ServerPresenter presenter;

    private final ArrayList<CitiesModel.CitiesData> citiesDataArrayList = new ArrayList<>();
    private final ArrayList<String> disciplinesStringArray = new ArrayList<>();
    private ArrayList<Object> arrayList;
    private int arraySize = 0;
    private AdLoader adLoader;
    private AdmissionsAdapter adapter;
    private LinearLayout llCourses, llBottomSheetBottom;

    private MKLoader loader, loaderBottomSheet;
    private Spinner spDisciplineType, spLevel;
    private TextView tvNumberOfRecords, tvDescription;
    private AutoCompleteTextView etDisciplineName, etCity;
    private Button btnSearchBottomSheet;

    private CitiesModel.CitiesData citiesData;
    private DisciplinesModel.DisciplinesData disciplinesData;
    private boolean appliedFilters = false;

    private View viewNoData, viewData;

    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private SelectedFilterRecycler selectedFilterRecycler;
    private final List<FilterModel> selectedFilters = new ArrayList<>();

    private String cityCode = "", levelCode = "", disciplineName = "", disciplineType = "";
    private String cityName = "", levelName = "";

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }

    public AdmissionsFragment() {
        // Required empty public constructor
    }

    private void init() {

        View v = view.findViewById(R.id.bottomSheet_Layout);
        mBehavior = BottomSheetBehavior.from(v);

        citiesDataArrayList.addAll(Constants.citiesList);
        arrayList = new ArrayList<>();
        presenter = ServerPresenter.getInstance(this);

        loader = view.findViewById(R.id.loader);
        viewNoData = view.findViewById(R.id.no_data);
        viewData = view.findViewById(R.id.view_data);
        tvNumberOfRecords = view.findViewById(R.id.tv_number_of_records);
        tvDescription = view.findViewById(R.id.tv_description);
        adapter = new AdmissionsAdapter(arrayList);

        RecyclerView rv = view.findViewById(R.id.recycler_view);
        RecyclerView rvFilters = view.findViewById(R.id.rv_filters);
        RecyclerView rvFiltersSelected = view.findViewById(R.id.rv_filters_selected);

        rvFilters.setHasFixedSize(true);
        rvFilters.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        rvFilters.setAdapter(new FilterRecyclerAdapter(ConstantData.getInstance().getAdmissionFilter(), this));

        rvFiltersSelected.setHasFixedSize(true);
        rvFiltersSelected.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        selectedFilterRecycler = new SelectedFilterRecycler(selectedFilters, this);
        rvFiltersSelected.setAdapter(selectedFilterRecycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);
        rv.setNestedScrollingEnabled(false);

        NestedScrollView nestedScrollView = view.findViewById(R.id.nested_scroll);
        nestedScrollView.setOnScrollChangeListener(new EndlessNestedScrollViewListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                fetchAdmissions(page);
            }
        });

        for (DisciplineTypesModel.DisciplineTypesData data : Constants.disciplineTypeList) {
            disciplinesStringArray.add(data.getDisciplineType());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_admissions, container, false);

        setHasOptionsMenu(true);
        init();
        fetchAdmissions(1);

        return view;
    }

    private void fetchAdmissions(int page) {

        if (Internet.isConnected(context)) {

            if (appliedFilters) {

                if (citiesData != null) {
                    cityCode = citiesData.getCityCode();
                    cityName = citiesData.getCityName();
                } else {
                    etCity.setText("");
                    cityCode = "";
                    cityName = "";
                }

                LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();
                if (levelsData != null) {
                    levelCode = levelsData.getLevelCode();
                    levelName = levelsData.getLevelName();
                }
                if (levelName.equalsIgnoreCase(ConstantData.SELECT))
                    levelCode = "";

                disciplineType = spDisciplineType.getSelectedItem().toString();

                if (disciplineType.equalsIgnoreCase(ConstantData.SELECT))
                    disciplineType = "";

                if (llCourses.getVisibility() == View.VISIBLE) {
                    if (disciplinesData != null) {
                        disciplineName = disciplinesData.getDisciplineName();
                    } else {
                        etDisciplineName.setText("");
                        disciplineName = "";
                    }
                }

                prepareDataForSelectedFilterRecyclerView();

            }

            viewNoData.setVisibility(View.GONE);

            presenter.getData(
                    AppEnums.ADMISSIONS,
                    String.valueOf(page),
                    cityCode, levelCode,
                    disciplineName, disciplineType
            );

        } else {
            Internet.showOfflineDialog((Activity) context);
        }

    }

    private void prepareDataForSelectedFilterRecyclerView() {
        selectedFilters.clear();

        if (!disciplineType.isEmpty())
            if (!disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {
                selectedFilters.add(new FilterModel(0, disciplineType, SearchFilter.DISCIPLINE_TYPE));
            }

        if (!levelCode.isEmpty())
            if (!levelName.equalsIgnoreCase(ConstantData.SELECT)) {
                selectedFilters.add(new FilterModel(0, levelName, SearchFilter.LEVEL));
            }

        if (!disciplineName.isEmpty()) {
            selectedFilters.add(new FilterModel(0, disciplineName, SearchFilter.COURSES));
        }

        if (!cityName.isEmpty()) {
            selectedFilters.add(new FilterModel(0, cityName, SearchFilter.CITY));
        }

        selectedFilterRecycler.notifyDataSetChanged();
    }

    private void insertAdsInList() {
        if (Constants.mNativeAds.size() > 0) {

            int offset = 10;//(arrayList.size() / Constants.mNativeAds.size()) + 1;

//            Log.e("testing", "offset = " + offset);
//            Log.e("testing", "page = " + page);

            int index = arraySize + 2;

            arraySize = arrayList.size();

            for (UnifiedNativeAd ad : Constants.mNativeAds) {
                Log.e("testing", "index  = " + index);
                if (index < arrayList.size()) {
                    arrayList.add(index, ad);
                    Log.e("testing", "index added to arraylist = " + index);
                    index = index + offset;
                }
            }

        }

        adapter.notifyDataSetChanged();

        Log.e("testing",
                "Loaded ads = " + Constants.mNativeAds.size() + "\n" +
                        "arraylist size = " + (arrayList.size() - Constants.mNativeAds.size())
        );


    }

    private void loadNativeAds() {

        try {
            if (context != null) {

                Log.e("testing", "Loading ads = " + 5);

                Constants.mNativeAds.clear();

                AdLoader.Builder builder = new AdLoader.Builder(context, getResources().getString(R.string.native_ad_id));

                adLoader = builder.forUnifiedNativeAd(
                        unifiedNativeAd -> {

                            // A native ad loaded successfully, check if the ad loader has finished loading
                            // and if so, insert the ads into the list.
                            Log.e("testing", "The previous native ad loaded successfully."
                                    + " loading another.");
                            Constants.mNativeAds.add(unifiedNativeAd);
                            if (!adLoader.isLoading()) {
                                insertAdsInList();
                                Log.e("testing", "insertAdsInEpisodes called from success.");
                            }

                        }).withAdListener(
                        new AdListener() {
                            @Override
                            public void onAdFailedToLoad(int errorCode) {
                                // A native ad failed to load, check if the ad loader has finished loading
                                // and if so, insert the ads into the list.
                                Log.e("testing", "The previous native ad failed to load. Attempting to");
                                if (!adLoader.isLoading()) {
                                    insertAdsInList();
                                    Log.e("testing", "insertAdsInEpisodes called from failed.");
                                }
                            }
                        }).build();

                // Load the Native ads.
                adLoader.loadAds(new AdRequest.Builder().build(), 5);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void showProgress(AppEnums enums) {
        if (enums.equals(AppEnums.DISCIPLINES)) {
            loaderBottomSheet.setVisibility(View.VISIBLE);
            btnSearchBottomSheet.setVisibility(View.INVISIBLE);
        } else {
            loader.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress(AppEnums enums) {
        if (enums.equals(AppEnums.DISCIPLINES)) {
            loaderBottomSheet.setVisibility(View.GONE);
            btnSearchBottomSheet.setVisibility(View.VISIBLE);
        } else loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                CaseModel model = new Gson().fromJson(response, CaseModel.class);

                switch (model.getCase()) {
                    case Constants.Cases.ADMISSIONS:
                        AdmissionsModel admissionsModel = new Gson().fromJson(response, AdmissionsModel.class);
                        if (admissionsModel.isSuccess()) {
                            if (admissionsModel.getData() != null) {
                                arrayList.addAll(admissionsModel.getData());
                                adapter.notifyDataSetChanged();

                                loadNativeAds();

                                tvNumberOfRecords.setText(admissionsModel.getRecords());

                                if (disciplineType.isEmpty() || levelName.isEmpty()) {

                                    tvDescription.setText(
                                            String.format(Locale.getDefault(), "universities/institutes admission open in %s",
                                                    cityName.isEmpty() ? "Pakistan" : cityName
                                            )
                                    );

                                } else {

                                    tvDescription.setText(
                                            String.format(Locale.getDefault(), "universities/institutes in %s at %s in %s",
                                                    disciplineType, levelName, cityName.isEmpty() ? "Pakistan" : cityName
                                            )
                                    );

                                }

                            }

                            viewNoData.setVisibility(arrayList.size() > 0 ? View.GONE : View.VISIBLE);
                            viewData.setVisibility(arrayList.size() > 0 ? View.VISIBLE : View.GONE);

                        }
                        break;
                    case Constants.Cases.DISCIPLINES:

                        DisciplinesModel disciplinesModel = new Gson().fromJson(response, DisciplinesModel.class);
                        if (disciplinesModel.isSuccess() && disciplinesModel.getData() != null) {

                            DisciplinesSpinnerAdapter disciplinesSpinnerAdapter = new DisciplinesSpinnerAdapter(context, R.layout.layout_spinner, disciplinesModel.getData());
                            etDisciplineName.setAdapter(disciplinesSpinnerAdapter);
                            etDisciplineName.setOnItemClickListener((adapterView, view1, i, l) -> disciplinesData = disciplinesSpinnerAdapter.getItem(i));
                            etDisciplineName.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                    disciplinesData = null;
                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                }
                            });

                            llBottomSheetBottom.setWeightSum(2);
                            llCourses.setVisibility(View.VISIBLE);

                        } else {

                            llBottomSheetBottom.setWeightSum(1);
                            llCourses.setVisibility(View.GONE);
                        }
                }

            }
        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {
                @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_admissions, null);

                ((TextView) view.findViewById(R.id.tv_title)).setText(R.string.admission_finder);

                spDisciplineType = view.findViewById(R.id.sp_discipline_type);
                spLevel = view.findViewById(R.id.sp_level);
                etCity = view.findViewById(R.id.et_city);
                etDisciplineName = view.findViewById(R.id.et_courses);
                loaderBottomSheet = view.findViewById(R.id.loader);
                llCourses = view.findViewById(R.id.linear_layout_courses);
                llBottomSheetBottom = view.findViewById(R.id.linear_layout_bottom);
                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                btnSearchBottomSheet = view.findViewById(R.id.btn_search);

                fillSpinnerAdapter();

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearchBottomSheet.setOnClickListener(view12 -> {
                    mBottomSheetDialog.dismiss();

                    appliedFilters = true;

                    arrayList.clear();
                    adapter.notifyDataSetChanged();

                    fetchAdmissions(1);

                });

                mBottomSheetDialog = new BottomSheetDialog(context);

                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
            }


            if (mBottomSheetDialog != null) {
                mBottomSheetDialog.show();

                if (mBottomSheetDialog.getWindow() != null)
                    mBottomSheetDialog.getWindow()
                            .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
//            mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleBottomSheetViewsOnFilterSelection(SearchFilter searchFilter) {
        switch (searchFilter) {
            case DISCIPLINE_TYPE:
                spDisciplineType.performClick();
                break;
            case LEVEL:
                spLevel.performClick();
                break;
            case CITY:
                etCity.requestFocus();
                Utils.showKeyboard(getActivity());
                break;
            case COURSES:
                if (llCourses.getVisibility() == View.VISIBLE) {
                    etDisciplineName.requestFocus();
                    Utils.showKeyboard(getActivity());
                }

            default:
                break;
        }
    }

    private void fillSpinnerAdapter() {

        if (spDisciplineType == null || spDisciplineType.getSelectedItem() == null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.layout_spinner, disciplinesStringArray);
            spDisciplineType.setAdapter(adapter);

            CitiesSpinnerAdapter citiesSpinnerAdapter = new CitiesSpinnerAdapter(context, R.layout.layout_spinner, citiesDataArrayList);
            etCity.setAdapter(citiesSpinnerAdapter);
            etCity.setOnItemClickListener((adapterView, view1, i, l) -> citiesData = citiesSpinnerAdapter.getItem(i));
            etCity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    citiesData = null;
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
//            etCity.setOnFocusChangeListener((view1, b) -> { if (b) etCity.showDropDown(); });

            LevelsSpinnerAdapter levelsSpinnerAdapter = new LevelsSpinnerAdapter(context, R.layout.layout_spinner, Constants.levelsList);
            spLevel.setAdapter(levelsSpinnerAdapter);

            spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    String disciplineType = spDisciplineType.getSelectedItem().toString();
                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(levelsData.getLevelName())) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {
                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            spDisciplineType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(adapter.getItem(i))) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!levelsData.getLevelName().equalsIgnoreCase(ConstantData.SELECT)) {
                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
    }

    @Override
    public void onFilterSelected(SearchFilter searchFilter) {
        showBottomSheet();
        new Handler().postDelayed(() ->
                handleBottomSheetViewsOnFilterSelection(searchFilter), 100);

    }

    @Override
    public void onFilterDelete(FilterModel data) {
        switch (data.getSearchFilter()) {
            case DISCIPLINE_TYPE:
                disciplineType = "";
                spDisciplineType.setSelection(0);
//                searchData();
                break;
            case LEVEL:
                levelCode = "";
                spLevel.setSelection(0);
//                searchData();
                break;
            case CITY:
                cityCode = "";
                etCity.setText("");
                cityName = "";
//                searchData();
                break;
            case COURSES:
                disciplineName = "";
                etDisciplineName.setText("");
                break;
        }
        if (selectedFilters.size() > 1) {
            btnSearchBottomSheet.performClick();
        } else {

            selectedFilters.clear();
            selectedFilterRecycler.notifyDataSetChanged();

            presenter.getData(
                    AppEnums.ADMISSIONS,
                    String.valueOf(1),
                    "", "",
                    "", ""
            );
        }

    }

}