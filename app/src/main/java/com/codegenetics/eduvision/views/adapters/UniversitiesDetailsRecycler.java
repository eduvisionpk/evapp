package com.codegenetics.eduvision.views.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.databases.UniversitiesDAO;
import com.codegenetics.eduvision.databases.UniversityDBModel;
import com.codegenetics.eduvision.enums.BroadcastReceiverEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.Institution.InstituteDetailsModel;

import java.util.ArrayList;
import java.util.Locale;

public class UniversitiesDetailsRecycler extends RecyclerView.Adapter<UniversitiesDetailsRecycler.UniversitiesHolder> {

    ArrayList<InstituteDetailsModel.InstituteDetailsPrograms> arrayList;
    private UniversitiesDAO universitiesDAO;


    private final BroadcastReceiver receiverFavourite = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {

                String id = intent.getStringExtra(IntentEnums.INSTITUTE_ID.name());

                for (int i = 0; i < arrayList.size(); i++) {

                    if (
                            (arrayList.get(i).getInstitute_id() + "_" +
                                    arrayList.get(i).getDiscipline_name())
                                    .equalsIgnoreCase(id)
                    ) {
                        notifyDataSetChanged();
                        break;
                    }

                }
            }

        }
    };


    public UniversitiesDetailsRecycler(ArrayList<InstituteDetailsModel.InstituteDetailsPrograms> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public UniversitiesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        universitiesDAO = DatabaseInjection.getUniversitiesDAO(parent.getContext());
        parent.getContext().registerReceiver(receiverFavourite, new IntentFilter(BroadcastReceiverEnums.FAVOURITE_RECEIVER.name()));
        return new UniversitiesHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_universities_details, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UniversitiesHolder holder, int position) {
        InstituteDetailsModel.InstituteDetailsPrograms data = arrayList.get(holder.getAdapterPosition());
        holder.bindItems(data);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class UniversitiesHolder extends RecyclerView.ViewHolder {

        private final TextView tvUniName, tvDegree, tvDuration, tvFee, tvDeadline, tvFavourite;
        private final View viewAddToFav;
        private final CheckBox cbFavIcon;

        public UniversitiesHolder(@NonNull View itemView) {
            super(itemView);
            tvUniName = itemView.findViewById(R.id.tv_uni_name);
            tvDegree = itemView.findViewById(R.id.tv_degree);
            tvDuration = itemView.findViewById(R.id.tv_duration);
            tvFee = itemView.findViewById(R.id.tv_fee);
            tvDeadline = itemView.findViewById(R.id.tv_deadline);
            tvFavourite = itemView.findViewById(R.id.tv_favourite);
            viewAddToFav = itemView.findViewById(R.id.view_favorite);
            cbFavIcon = itemView.findViewById(R.id.check_box_favourite);
        }

        public void bindItems(InstituteDetailsModel.InstituteDetailsPrograms data) {

            tvUniName.setText(data.getDiscipline_name());
            tvDegree.setText(data.getDegree_abb());
            tvDuration.setText(data.getDuration());
            tvFee.setText(data.getFee_local());
            tvDeadline.setText(data.getDeadline());

            cbFavIcon.setChecked(universitiesDAO.getItem(
                    data.getInstitute_id() + "_" + data.getDiscipline_name()) > 0);

            viewAddToFav.setOnClickListener(view -> {
                if (cbFavIcon.isChecked()) {
                    universitiesDAO.deleteItem(data.getInstitute_id() + "_" + data.getDiscipline_name());
                    cbFavIcon.setChecked(false);
                } else {
                    universitiesDAO.addNewItem(UniversityDBModel.getDBModel(data));
                    cbFavIcon.setChecked(true);
                }
                tvFavourite.setText(
                        String.format(Locale.getDefault(), "%s favourite",
                                cbFavIcon.isChecked() ? "Remove" : "Add to"
                        )
                );
            });
        }
    }
}
