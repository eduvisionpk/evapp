package com.codegenetics.eduvision.views.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.EntryTest.EntryTestDetailsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Internet;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import im.delight.android.webview.AdvancedWebView;

public class EntryTestDetails extends AppCompatActivity implements IData, AdvancedWebView.Listener {

    private AdvancedWebView webView;
    private String title;
    private MKLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_test_details);
        initData();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(title);
        }

    }

    private void initData() {

        webView = findViewById(R.id.web_view_details);
        loader = findViewById(R.id.loader);

        String id = getIntent().getStringExtra(IntentEnums.ID.name());
        title = getIntent().getStringExtra(IntentEnums.NAME.name());

        initWebView();

        ServerPresenter presenter = ServerPresenter.getInstance(this);
        if (Internet.isConnected(this)) presenter.getData(AppEnums.ENTRY_TEST_DETAILS, id);
        else Internet.showOfflineDialog(this);

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {

        webView.setListener(this, this);
        webView.setGeolocationEnabled(true);
        webView.setMixedContentAllowed(true);
        webView.setCookiesEnabled(true);
        webView.setThirdPartyCookiesEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
                return true;
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        try {
            if (response != null) {
                EntryTestDetailsModel detailsModel = new Gson().fromJson(response, EntryTestDetailsModel.class);
                if (detailsModel.getData() != null && detailsModel.getData().size() > 0) {

                    webView.loadDataWithBaseURL("",
                            detailsModel.getData().get(0).getDetail(),
                            "text/html",
                            null, "");
                }
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void displayError(String error) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
    }

    @Override
    protected void onPause() {
        webView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (webView != null) webView.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        webView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!webView.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) { loader.setVisibility(View.VISIBLE); }

    @Override
    public void onPageFinished(String url) { loader.setVisibility(View.GONE); }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType,
                                    long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}