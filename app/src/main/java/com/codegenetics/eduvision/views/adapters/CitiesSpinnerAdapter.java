package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.codegenetics.eduvision.models.Cities.CitiesModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

@SuppressWarnings({"unchecked"})
public class CitiesSpinnerAdapter extends ArrayAdapter<CitiesModel.CitiesData> {

    private final ArrayList<CitiesModel.CitiesData> values, tempValues, suggestions;

    public CitiesSpinnerAdapter(Context context, int textViewResourceId, ArrayList<CitiesModel.CitiesData> values) {
        super(context, textViewResourceId, values);
        this.values = values;
        tempValues = new ArrayList<>(values);
        suggestions = new ArrayList<>();
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public CitiesModel.CitiesData getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getCityName());
        return label;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            CitiesModel.CitiesData data = (CitiesModel.CitiesData) resultValue;
            return data.getCityName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            try {
                if (charSequence != null) {
                    suggestions.clear();
                    for (CitiesModel.CitiesData data : tempValues) {
                        if (data.getCityName().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                            suggestions.add(data);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            try {
                ArrayList<CitiesModel.CitiesData> tempValues = (ArrayList<CitiesModel.CitiesData>) filterResults.values;
                if (filterResults != null && filterResults.count > 0) {
                    clear();
                    for (CitiesModel.CitiesData obj : tempValues) {
                        add(obj);
                    }
                    notifyDataSetChanged();
                } else {
                    clear();
                    notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    };
}