package com.codegenetics.eduvision.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.NavigationMenu;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.fragments.AdmissionsFragment;
import com.codegenetics.eduvision.views.fragments.CareerCounselingFragment;
import com.codegenetics.eduvision.views.fragments.HomeFragment;
import com.codegenetics.eduvision.views.fragments.MoreFragment;
import com.codegenetics.eduvision.views.fragments.ProgramFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

@SuppressWarnings("ALL")
public class HomeActivity extends AppCompatActivity {

    private static final int action_home = R.id.action_home;
    private static final int action_more = R.id.action_more;
    private static final int action_admissions = R.id.action_admissions;
    private static final int action_programs = R.id.action_programs;
    private static final int action_career_counseling = R.id.action_career_counseling;

//    private AppCompatTextView tvUnreadCount;
    private FloatingActionButton floatingActionButton;
    private static BottomNavigationView bottomNavigationView;
    private int currentMenu;
    private String uuid = "";

    private DatabaseReference referenceChats;
    /*private final ValueEventListener chatsListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            try {

                int unread = 0;

                if (dataSnapshot.getValue() != null) {
                    if (dataSnapshot.hasChild(FirebaseConstants.ChildNodes.UNREAD)) {
                        unread += Integer.parseInt(
                                dataSnapshot.child(FirebaseConstants.ChildNodes.UNREAD).getValue(String.class));

                        if (unread > 99) {
                            tvUnreadCount.setText("99+");
                        }else {
                            tvUnreadCount.setText(String.valueOf(unread));
                        }

                        if (unread > 0) {
                            tvUnreadCount.setVisibility(View.VISIBLE);
                        } else {
                            tvUnreadCount.setVisibility(View.GONE);
                        }

                    } else {
                        tvUnreadCount.setVisibility(View.GONE);
                    }
                }


            } catch (NumberFormatException | Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }



        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };*/

    private void init() {

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) uuid = currentUser.getUid();

        bottomNavigationView = findViewById(R.id.home_bottom_navigation);
        floatingActionButton = findViewById(R.id.fab_chats);

        //        tvUnreadCount = findViewById(R.id.tv_unread_count);

        //        if (!uuid.isEmpty()) {
//
//            referenceChats = FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.CHATS);
//            referenceChats
//                    .child(uuid)
//                    .addValueEventListener(chatsListener);
//        }

        if (Session.getInstance().getCurrentUserId().isEmpty()) {
            if (!uuid.isEmpty()) Session.getInstance().setUserId(uuid);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.actionbar_title);
        }

        init();
        getIntentData();

        HomeFragment homeFragment = new HomeFragment();
        FragmentManager transaction = getSupportFragmentManager();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && getIntent().hasExtra(IntentEnums.POSITION.name())) homeFragment.setArguments(bundle);

        switch (currentMenu) {
            case NavigationMenu.HOME:
                transaction.beginTransaction()
                        .replace(R.id.home_parentRelativeLayout, homeFragment)
                        .addToBackStack(null).commit();
                bottomNavigationView.getMenu().getItem(NavigationMenu.HOME).setChecked(true);
                break;
            case NavigationMenu.ADMISSION:
                transaction.beginTransaction()
                        .replace(R.id.home_parentRelativeLayout, new AdmissionsFragment())
                        .addToBackStack(null).commit();
                bottomNavigationView.getMenu().getItem(NavigationMenu.ADMISSION).setChecked(true);
                break;
            case NavigationMenu.PROGRAM:
                transaction.beginTransaction()
                        .replace(R.id.home_parentRelativeLayout, new ProgramFragment())
                        .addToBackStack(null).commit();
                bottomNavigationView.getMenu().getItem(NavigationMenu.PROGRAM).setChecked(true);
                break;
            case NavigationMenu.CAREER_COUNSELING:
                transaction.beginTransaction()
                        .replace(R.id.home_parentRelativeLayout, new CareerCounselingFragment())
                        .addToBackStack(null).commit();
                bottomNavigationView.getMenu().getItem(NavigationMenu.CAREER_COUNSELING).setChecked(true);
                break;
            case NavigationMenu.MORE:
                transaction.beginTransaction()
                        .replace(R.id.home_parentRelativeLayout, new MoreFragment())
                        .addToBackStack(null).commit();
                bottomNavigationView.getMenu().getItem(NavigationMenu.MORE).setChecked(true);

                break;
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {

                    item.setCheckable(true);

                    switch (item.getItemId()) {
                        case action_home:
                            transaction.beginTransaction()
                                    .replace(R.id.home_parentRelativeLayout, new HomeFragment())
                                    .addToBackStack(null)
                                    .commit();

                            return true;
                        case action_more:

                            transaction.beginTransaction()
                                    .replace(R.id.home_parentRelativeLayout, new MoreFragment())
                                    .addToBackStack(null)
                                    .commit();

                            return true;
                        case action_admissions:
                            transaction.beginTransaction()
                                    .replace(R.id.home_parentRelativeLayout, new AdmissionsFragment())
                                    .addToBackStack(null)
                                    .commit();

                            return true;
                        case action_programs:
                            transaction.beginTransaction()
                                    .replace(R.id.home_parentRelativeLayout, new ProgramFragment())
                                    .addToBackStack(null)
                                    .commit();

                            return true;
                        case action_career_counseling:
                            transaction.beginTransaction()
                                    .replace(R.id.home_parentRelativeLayout, new CareerCounselingFragment())
                                    .addToBackStack(null)
                                    .commit();

                            return true;
                    }

                    return false;
                });

        floatingActionButton.setOnClickListener(view ->
                startActivity(new Intent(this, Messages.class))
        );

    }

    private void getIntentData() {
        if (getIntent() != null) {
            currentMenu = getIntent().getIntExtra(NavigationMenu.SET_CURRENT_ITEM, 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public static void setMenus(Activity activity, Menu menu) {
        MenuItem itemFavourite = menu.findItem(R.id.action_favourites);
        MenuItem itemNotifications = menu.findItem(R.id.action_notifications);
        MenuItem itemProfile = menu.findItem(R.id.action_profile);

        itemFavourite.setOnMenuItemClickListener(menuItem -> {
            activity.startActivity(new Intent(activity, Favourites.class));
            return true;
        });
        itemNotifications.setOnMenuItemClickListener(menuItem -> {
            activity.startActivity(new Intent(activity, Notifications.class));
            return true;
        });
        itemProfile.setOnMenuItemClickListener(menuItem -> {
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.putExtra(NavigationMenu.SET_CURRENT_ITEM, NavigationMenu.MORE);
            activity.startActivity(intent);
            activity.finishAffinity();
            return true;
        });
    }

    @Override
    protected void onDestroy() {

        try {
//            if (referenceChats != null && !uuid.isEmpty()) {
//                referenceChats
//                        .child(uuid)
//                        .removeEventListener(chatsListener);
//
//            }

            super.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (bottomNavigationView.getSelectedItemId() == action_home) {
            Constants.getInstance().showClosingAppDialog(this);
        }
        else {

            bottomNavigationView.setSelectedItemId(action_home);

            FragmentManager transaction = getSupportFragmentManager();
            transaction.beginTransaction()
                    .replace(R.id.home_parentRelativeLayout, new HomeFragment())
                    .addToBackStack(null)
                    .commit();
        }
    }
}