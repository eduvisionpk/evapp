package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import java.util.ArrayList;
import java.util.Arrays;

import io.paperdb.Paper;

public class PlannerInfoSession {

    private static PlannerInfoSession instance;
    private final String BOOK_NAME = "planner_info_Session";
    private final String NAME = "name";
    private final String ADDRESS = "address";
    private final String CITY = "city";
    private final String DOMICILE = "dom";
    private final String DOB = "dob";
    private final String DEGREE = "degree";
    private final String OBTAINED_MARKS = "ot";
    private final String TOTAL_MARKS = "tm";
    private final String PASSING_YEAR = "py";
    private final String LEVEL_CODE = "level_code";
    private final String LEVEL_NAME = "level_name";
    private final String DISCIPLINE_TYPES = "discipline_types";

    public static PlannerInfoSession getInstance() {
        if (instance == null) instance = new PlannerInfoSession();
        return instance;
    }

    public void setName(String s){
        Paper.book(BOOK_NAME).write(NAME,s);
    }
    public String getName(){
        return Paper.book(BOOK_NAME).read(NAME,"");
    }

    public void setAddress(String s){
        Paper.book(BOOK_NAME).write(ADDRESS,s);
    }
    public String getAddress(){
        return Paper.book(BOOK_NAME).read(ADDRESS,"");
    }

    public void setCity(String s){
        Paper.book(BOOK_NAME).write(CITY,s);
    }
    public String getCity(){
        return Paper.book(BOOK_NAME).read(CITY,"");
    }

    public void setDomicile(String s){
        Paper.book(BOOK_NAME).write(DOMICILE,s);
    }
    public String getDomicile(){
        return Paper.book(BOOK_NAME).read(DOMICILE,"");
    }

    public void setDob(String s){
        Paper.book(BOOK_NAME).write(DOB,s);
    }
    public String getDob(){
        return Paper.book(BOOK_NAME).read(DOB,"");
    }

    public void setDegree(String s){
        Paper.book(BOOK_NAME).write(DEGREE,s);
    }
    public String getDegree(){
        return Paper.book(BOOK_NAME).read(DEGREE,"");
    }

    public void setObtainedMarks(String s){
        Paper.book(BOOK_NAME).write(OBTAINED_MARKS,s);
    }
    public String getObtainedMarks(){
        return Paper.book(BOOK_NAME).read(OBTAINED_MARKS,"");
    }

    public void setTotalMarks(String s){
        Paper.book(BOOK_NAME).write(TOTAL_MARKS,s);
    }
    public String getTotalMarks(){
        return Paper.book(BOOK_NAME).read(TOTAL_MARKS,"");
    }

    public void setPassingYear(String s){
        Paper.book(BOOK_NAME).write(PASSING_YEAR,s);
    }
    public String getPassingYear(){
        return Paper.book(BOOK_NAME).read(PASSING_YEAR,"");
    }

    public void setLevelCode(String s){
        Paper.book(BOOK_NAME).write(LEVEL_CODE,s);
    }
    public String getLevelCode(){
        return Paper.book(BOOK_NAME).read(LEVEL_CODE,"");
    }

    public void setLevelName(String s){
        Paper.book(BOOK_NAME).write(LEVEL_NAME,s);
    }
    public String getLevelName(){
        return Paper.book(BOOK_NAME).read(LEVEL_NAME,"");
    }

    public void setDisciplineTypes(ArrayList<String> disciplineTypes){
        if (disciplineTypes.size() > 0) {
            StringBuilder builder = new StringBuilder();
            for (String disciplineType : disciplineTypes)
                builder.append(disciplineType).append(";");
            if (builder.toString().length() > 0) {
                Paper.book(BOOK_NAME).write(DISCIPLINE_TYPES, builder.toString().substring(0, builder.toString().length() - 1));
            }
        } else {
            Paper.book(BOOK_NAME).write(DISCIPLINE_TYPES, "");
        }
    }
    public ArrayList<String> getDisciplineTypes() {
        String disciplines = Paper.book(BOOK_NAME).read(DISCIPLINE_TYPES, "");
        if (disciplines.isEmpty()) return new ArrayList<>();
        return new ArrayList<>(Arrays.asList(disciplines.split(";")));
    }


}
