package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.models.Videos.VideoCategoriesModel;

import java.util.ArrayList;

public class CareerTvTabRecycler extends RecyclerView.Adapter<CareerTvTabRecycler.CareerTvTabHolder> {
    private final ArrayList<VideoCategoriesModel.VideoCategoriesData> tabsArrayList;
    private final ITabClickListener iTabClickListener;
    private int mSelectedItem = 0;

    public CareerTvTabRecycler(ArrayList<VideoCategoriesModel.VideoCategoriesData> tabsArrayList, ITabClickListener iTabClickListener) {
        this.tabsArrayList = tabsArrayList;
        this.iTabClickListener = iTabClickListener;
    }

    @NonNull
    @Override
    public CareerTvTabHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CareerTvTabHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_level_tab, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CareerTvTabHolder holder, int position) {
        holder.tvTitle.setText(tabsArrayList.get(position).getType());

        if (position == mSelectedItem) {
            holder.tvTitle.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.blue_dark));
            holder.tvTitle.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.white));
        } else {
            holder.tvTitle.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.white));
            holder.tvTitle.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.black));
        }

        holder.itemView.setOnClickListener(v -> {
            mSelectedItem = holder.getAdapterPosition();
            iTabClickListener.onTabsSelected(tabsArrayList.get(holder.getAdapterPosition()).getType());
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return tabsArrayList.size();
    }

    public void updateSelectedPosition(int position) {
        mSelectedItem = position;
    }

    static class CareerTvTabHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;

        public CareerTvTabHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }
}
