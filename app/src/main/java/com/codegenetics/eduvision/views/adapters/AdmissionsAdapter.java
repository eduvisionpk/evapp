package com.codegenetics.eduvision.views.adapters;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.Admissions.AdmissionsModel;
import com.codegenetics.eduvision.views.activities.FullScreenImages;
import com.codegenetics.eduvision.views.adapters.viewholders.UnifiedNativeAdViewHolder;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Locale;

public class AdmissionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Object> arrayList;

    private static final int MENU_ITEM_VIEW_TYPE = 0;
    private static final int UNIFIED_NATIVE_AD_VIEW_TYPE = 1;

    public AdmissionsAdapter(ArrayList<Object> arrayList) {
        this.arrayList = arrayList;
    }

    /**
     * Determines the view type for the given position.
     */
    @Override
    public int getItemViewType(int position) {

        Object recyclerViewItem = arrayList.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return UNIFIED_NATIVE_AD_VIEW_TYPE;
        }
        return MENU_ITEM_VIEW_TYPE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                View unifiedNativeLayoutView = LayoutInflater.from(
                        parent.getContext()).inflate(R.layout.ad_unified,
                        parent, false);
                return new UnifiedNativeAdViewHolder(unifiedNativeLayoutView);
            case MENU_ITEM_VIEW_TYPE:
            default:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_admissions, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) arrayList.get(position);
                populateNativeAdView(nativeAd, ((UnifiedNativeAdViewHolder) viewHolder).getAdView());
                break;
            case MENU_ITEM_VIEW_TYPE:
            default:

                final ViewHolder holder = (ViewHolder) viewHolder;

                AdmissionsModel.AdmissionsData data =
                        (AdmissionsModel.AdmissionsData) arrayList.get(holder.getAdapterPosition());

                holder.bindItems(data);
        }


    }


    private void populateNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        NativeAd.Image icon = nativeAd.getIcon();

        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivLogo;
        private final TextView tvInstituteName, tvLevel, tvDiscipline, tvDeadline;
        private final MKLoader loaderImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivLogo = itemView.findViewById(R.id.iv_logo);
            loaderImage = itemView.findViewById(R.id.loader_image);
            tvInstituteName = itemView.findViewById(R.id.tv_institute_name);
            tvLevel = itemView.findViewById(R.id.tv_level);
            tvDiscipline = itemView.findViewById(R.id.tv_discipline);
            tvDeadline = itemView.findViewById(R.id.tv_deadline);
        }

        public void bindItems(AdmissionsModel.AdmissionsData data) {

            Glide.with(itemView)
                    .load(data.getInstituteLogo())
                    .placeholder(R.color.gray_light1)
                    .placeholder(R.drawable.ic_no_image)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            loaderImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            loaderImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(ivLogo);

            tvInstituteName.setText(
                    String.format(
                            Locale.getDefault(),
                            "%s, %s, %s admission",
                            data.getName(), data.getCityName(), data.getInstituteAbbrivation()
                    )
            );

            tvLevel.setText(data.getLevelName());

            tvDiscipline.setText(data.getDisciplineType());

            tvDeadline.setText(data.getDeadline());

            itemView.setOnClickListener(view ->
                    view.getContext().startActivity(
                            new Intent(view.getContext(), FullScreenImages.class)
                                    .putExtra(IntentEnums.DEADLINE.toString(), data.getDeadline())
                                    .putExtra(IntentEnums.IMAGES.toString(), data.getInstituteAd().split(";"))
                                    .putExtra(IntentEnums.POSITION.toString(), 0)
                                    .putExtra(IntentEnums.HIDE.toString(), true)
                    )
            );

        }
    }

}
