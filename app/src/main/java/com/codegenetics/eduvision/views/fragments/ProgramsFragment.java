package com.codegenetics.eduvision.views.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.DisciplineTypes.DisciplineTypesModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.models.Programs.ProgramsModel;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.activities.ProgramsDisciplines;
import com.codegenetics.eduvision.views.adapters.LevelsSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.ProgramsAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.Objects;


public class ProgramsFragment extends Fragment implements IViewClickListener {

    private View view;
    private Context context;
    private ArrayList<ProgramsModel> arrayList;

    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private Spinner spDisciplineType, spLevels;

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public ProgramsFragment() {
        // Required empty public constructor
    }

    private void init() {

        View v = view.findViewById(R.id.bottomSheet_Layout);
        mBehavior = BottomSheetBehavior.from(v);

        arrayList = new ArrayList<>(ConstantData.getInstance().getProgramsData());

        RecyclerView rvPrograms = view.findViewById(R.id.recycler_view);
        rvPrograms.setHasFixedSize(true);
        rvPrograms.setLayoutManager(new GridLayoutManager(context, 2));
        rvPrograms.setAdapter(new ProgramsAdapter(arrayList, this));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_programs, container, false);
        init();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_filter, menu);

        MenuItem item = menu.findItem(R.id.action_filter);
        item.setOnMenuItemClickListener(menuItem -> {
            showBottomSheet();
            return true;
        });
    }

    @Override
    public void onItemClicked(int position) {
        startActivity(new Intent(context, ProgramsDisciplines.class)
                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), arrayList.get(position).getTitle())
                .putExtra(IntentEnums.LEVEL_CODE.name(), "7")
                .putExtra(IntentEnums.LEVEL_NAME.name(), "Bachelor")
        );
    }


    private void setDisciplineType(ArrayList<DisciplineTypesModel.DisciplineTypesData> arrayList) {

        ArrayList<String> items = new ArrayList<>();

        for (DisciplineTypesModel.DisciplineTypesData item : arrayList) items.add(item.getDisciplineType());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.layout_spinner, items);
        spDisciplineType.setAdapter(adapter);

    }

    private void setLevels(ArrayList<LevelsModel.LevelsData> arrayList) {
        LevelsSpinnerAdapter adapter = new LevelsSpinnerAdapter(context, R.layout.layout_spinner, arrayList);
        spLevels.setAdapter(adapter);
    }


    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {

                @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_programs, null);

                spDisciplineType = view.findViewById(R.id.sp_discipline_type);
                spLevels = view.findViewById(R.id.sp_level);
                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                Button btnSearch = view.findViewById(R.id.btn_search);

                setDisciplineType(Constants.disciplineTypeList);
                setLevels(Constants.levelsList);

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearch.setOnClickListener(view12 -> {
                    mBottomSheetDialog.dismiss();

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevels.getSelectedItem();

                    startActivity(new Intent(context, ProgramsDisciplines.class)
                            .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), spDisciplineType.getSelectedItem().toString())
                            .putExtra(IntentEnums.LEVEL_CODE.name(), levelsData.getLevelCode())
                            .putExtra(IntentEnums.LEVEL_NAME.name(), levelsData.getLevelName())
                    );
                });

                mBottomSheetDialog = new BottomSheetDialog(context);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
            }

            if (mBottomSheetDialog != null) mBottomSheetDialog.show();

//            mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}