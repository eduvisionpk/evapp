package com.codegenetics.eduvision.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.helper.LayoutType;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IHomeMenuClick;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.HomeModel;
import com.codegenetics.eduvision.models.HomeSlider.HomeSliderModel;
import com.codegenetics.eduvision.models.News.NewsModel;
import com.codegenetics.eduvision.models.Scholarships.ScholarshipsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.activities.AdmissionPlanner.ConfirmRequirements;
import com.codegenetics.eduvision.views.activities.AdmissionPlanner.Planner;
import com.codegenetics.eduvision.views.activities.AdmissionPlanner.PlannerInfoSession;
import com.codegenetics.eduvision.views.activities.Admissions;
import com.codegenetics.eduvision.views.activities.CareerCounseling;
import com.codegenetics.eduvision.views.activities.CareerTvCategories;
import com.codegenetics.eduvision.views.activities.Careers;
import com.codegenetics.eduvision.views.activities.Details;
import com.codegenetics.eduvision.views.activities.EntryTest;
import com.codegenetics.eduvision.views.activities.Jobs;
import com.codegenetics.eduvision.views.activities.MeritCalculator;
import com.codegenetics.eduvision.views.activities.Messages;
import com.codegenetics.eduvision.views.activities.News;
import com.codegenetics.eduvision.views.activities.PastPaperSelection;
import com.codegenetics.eduvision.views.activities.Programs;
import com.codegenetics.eduvision.views.activities.Ranking;
import com.codegenetics.eduvision.views.activities.Scholarships;
import com.codegenetics.eduvision.views.adapters.HomeNewsRecycler;
import com.codegenetics.eduvision.views.adapters.HomeRecycler;
import com.codegenetics.eduvision.views.adapters.ViewPagerAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Locale;

public class HomeFragment extends Fragment implements IData, IHomeMenuClick, IViewClickListener {

    private View view;
    private Context context;
    private ServerPresenter presenter;
    private ArrayList<HomeSliderModel.HomeSlidersData> viewPagerArrayList;
    private ArrayList<HomeSliderModel.HomeNewsScholarshipsData> newsScholarshipsArrayList;
    private HomeNewsRecycler homeNewsRecycler;

    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private MKLoader loader;
    private Handler handler;
    private int page = 0;
    private final int delay = 8 * 1000;

    private final Runnable runnable = new Runnable() {
        public void run() {
            if (viewPagerAdapter.getCount() == page) {
                page = 0;
            } else {
                page++;
            }
            viewPager.setCurrentItem(page, true);
            handler.postDelayed(this, delay);
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    private void init() {

        handler = new Handler();
        presenter = ServerPresenter.getInstance(this);

        HomeRecycler homeRecycler = new HomeRecycler(ConstantData.getInstance().getHomeData(), this, LayoutType.GRID);

        newsScholarshipsArrayList = new ArrayList<>();
        homeNewsRecycler = new HomeNewsRecycler(newsScholarshipsArrayList, this);

        viewPagerArrayList = new ArrayList<>();
        viewPagerAdapter = new ViewPagerAdapter(viewPagerArrayList, this);

        TextView tvWelcome = view.findViewById(R.id.tv_welcome);
        tvWelcome.setText(String.format(Locale.getDefault(), "%s\n%s",
                "WELCOME", Session.getInstance().getName()));


        loader = view.findViewById(R.id.loader_view_pager);
        viewPager = view.findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                page = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        RecyclerView rvHomeMenu = view.findViewById(R.id.rv_home_menus);
        rvHomeMenu.setHasFixedSize(true);
        rvHomeMenu.setLayoutManager(new GridLayoutManager(context, 3));
        rvHomeMenu.setAdapter(homeRecycler);


        RecyclerView rvNews = view.findViewById(R.id.rv_news);
        rvNews.setHasFixedSize(true);
        rvNews.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        rvNews.setAdapter(homeNewsRecycler);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        init();

        fetchSliderData();

        return view;
    }

    private void fetchSliderData() {

        if (context != null) {

            if (Internet.isConnected(context)) {
                presenter.getData(AppEnums.HOME_SLIDER);
            } else {
                Internet.showOfflineDialog((Activity) context);
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(runnable, delay);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                HomeSliderModel model = new Gson().fromJson(response, HomeSliderModel.class);

                if (model.isSuccess()) {

                    if (model.getData() != null) {
                        viewPagerArrayList.addAll(model.getSliders());
                        viewPagerAdapter.notifyDataSetChanged();

                        newsScholarshipsArrayList.addAll(model.getData());
                        homeNewsRecycler.notifyDataSetChanged();
                    }

                }

            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        try {
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onHomeMenuSelected(HomeModel model) {

        switch (model.getHomeMenu()) {
            case NEWS:
                startActivity(new Intent(context, News.class));
                break;
            case SCHOLARSHIPS:
                startActivity(new Intent(context, Scholarships.class));
                break;
            case ADMISSION:
                startActivity(
                        new Intent(context, Admissions.class)
                                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), "")
                                .putExtra(IntentEnums.DISCIPLINE_NAME.name(), "")
                                .putExtra(IntentEnums.CITY.name(), "")
                                .putExtra(IntentEnums.LEVEL_CODE.name(), "")
                                .putExtra(IntentEnums.LEVEL_NAME.name(), "")
                );
                break;
            case PROGRAM:
                startActivity(new Intent(context, Programs.class));
                break;
            case CAREERS:
                startActivity(new Intent(context, Careers.class));
                break;
            case CAREERS_TV:
                startActivity(new Intent(context, CareerTvCategories.class));
                break;
            case MERIT_CALCULATOR:
                startActivity(new Intent(context, MeritCalculator.class));
                break;
            case ENTRY_TEST:
                startActivity(new Intent(context, EntryTest.class));
                break;
            case CAREER_COUNSELING:
                startActivity(new Intent(context, CareerCounseling.class));
                break;
            case PAST_PAPER:
                startActivity(new Intent(context, PastPaperSelection.class));
                break;
            case BOOK_A_COUNSELING_SESSION:
                startActivity(new Intent(context, Messages.class));
                break;
            case RANKING:
                startActivity(new Intent(context, Ranking.class));
                break;
            case JOBS:
                startActivity(new Intent(context, Jobs.class));
                break;
            case ADMISSION_PLANNER:
                if (PlannerInfoSession.getInstance().getDisciplineTypes().size() > 0) {
                    startActivity(new Intent(context, ConfirmRequirements.class)
                            .putExtra(IntentEnums.LEVEL_CODE.name(), PlannerInfoSession.getInstance().getLevelCode())
                            .putExtra(IntentEnums.LEVEL_NAME.name(), PlannerInfoSession.getInstance().getLevelName())
                            .putExtra(IntentEnums.DATA.name(), PlannerInfoSession.getInstance().getDisciplineTypes())
                    );
                } else {
                    startActivity(new Intent(context, Planner.class));
                }
                break;
        }

    }

    /* view pager click */
    @Override
    public void onItemClicked(int position) {

        if (position < 0) return;

        HomeSliderModel.HomeNewsScholarshipsData data = newsScholarshipsArrayList.get(position);

        if (data.getType().equalsIgnoreCase(AppEnums.NEWS.toString())) {
            startActivity(
                    new Intent(view.getContext(), Details.class)
                            .putExtra(IntentEnums.NEWS_DATA.toString(),
                                    new NewsModel.NewsData(
                                            data.getId(), data.getTitletext(), data.getImage(),
                                            data.getCatname(), data.getCatid(), data.getTime())
                            ).putExtra(IntentEnums.TYPE.toString(), Constants.Cases.NEWS_DETAILS)
            );
        } else {
            startActivity(
                    new Intent(view.getContext(), Details.class)
                            .putExtra(IntentEnums.SCHOLARSHIP_DATA.toString(),
                                    new ScholarshipsModel.ScholarshipsData(
                                            data.getId(), data.getTitletext(), data.getImage(), data.getTime()
                                    )
                            ).putExtra(IntentEnums.TYPE.toString(), Constants.Cases.SCHOLARSHIP_DETAILS)
            );
        }

    }


}