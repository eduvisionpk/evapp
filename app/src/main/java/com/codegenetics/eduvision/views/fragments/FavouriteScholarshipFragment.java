package com.codegenetics.eduvision.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.databases.ScholarshipDBModel;
import com.codegenetics.eduvision.models.Scholarships.ScholarshipsModel;
import com.codegenetics.eduvision.views.adapters.ScholarshipsAdapter;
import com.google.android.gms.ads.AdLoader;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;


public class FavouriteScholarshipFragment extends Fragment {

    private View view, viewNoData;
    private MKLoader loader;
    private ArrayList<Object> arrayList;
    private ScholarshipsAdapter adapter;
    private int arraySize = 0;
    private AdLoader adLoader;

    public FavouriteScholarshipFragment() {
        // Required empty public constructor
    }

    private void init() {

        arrayList = new ArrayList<>();
        adapter = new ScholarshipsAdapter(arrayList);

        loader = view.findViewById(R.id.loader);
        viewNoData = view.findViewById(R.id.no_data);

        RecyclerView rvData = view.findViewById(R.id.rv_data);
        rvData.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvData.setLayoutManager(layoutManager);
        rvData.setAdapter(adapter);


    }

    public static FavouriteScholarshipFragment newInstance() { return new FavouriteScholarshipFragment(); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favourite_scholarships, container, false);

        init();
        fetchScholarships();

        return view;
    }

    private void fetchScholarships() {

        loader.setVisibility(View.VISIBLE);

        DatabaseInjection.getScholarshipsDAO(getContext())
                .getAll()
                .observeForever(scholarshipDBModels -> {

                    arrayList.clear();

                    for (ScholarshipDBModel model : scholarshipDBModels) {
                        arrayList.add(ScholarshipDBModel.getModel(model));
                    }

                    adapter.notifyDataSetChanged();
                    loader.setVisibility(View.GONE);

                    viewNoData.setVisibility(arrayList.size() > 0 ? View.GONE : View.VISIBLE);

                });
    }


}