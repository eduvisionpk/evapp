package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IRecyclerClick;
import com.codegenetics.eduvision.models.Career.CareersModel;

import java.util.ArrayList;

public class CareerCounsellingDataRecycler extends RecyclerView.Adapter<CareerCounsellingDataRecycler.CareerCounsellingDataHolder> {

    private final ArrayList<CareersModel.CareersData> mData;
    private final IRecyclerClick recyclerClick;

    public CareerCounsellingDataRecycler(ArrayList<CareersModel.CareersData> mData, IRecyclerClick recyclerClick) {
        this.mData = mData;
        this.recyclerClick = recyclerClick;
    }

    @NonNull
    @Override
    public CareerCounsellingDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CareerCounsellingDataHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_counselling, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CareerCounsellingDataHolder holder, int position) {
        CareersModel.CareersData data = mData.get(holder.getAdapterPosition());

        holder.view.setCardBackgroundColor(
                holder.view.getContext().getResources().getColor(ConstantData.getInstance().getCareerCounselingColor(holder.getAdapterPosition()))
        );

        Glide.with(holder.ivAvatar)
                .load(data.getThumbnail())
                .placeholder(R.drawable.ic_dummy)
                .into(holder.ivAvatar);

        holder.tvTitle.setText(data.getCat_name());
        holder.tvSubtitle.setText(data.getTitle());
        holder.itemView.setOnClickListener(v -> recyclerClick.onRecyclerItemClicked(data));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class CareerCounsellingDataHolder extends RecyclerView.ViewHolder {
        private final ImageView ivAvatar;
        private final TextView tvTitle, tvSubtitle;
        private final CardView view;

        public CareerCounsellingDataHolder(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.iv_avatar);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvSubtitle = itemView.findViewById(R.id.tv_subtitle);
            view = itemView.findViewById(R.id.card_view_root);
        }
    }
}
