package com.codegenetics.eduvision.views.activities;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.helper.EndlessRecyclerViewScrollListener;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.News.NewsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.NewsAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class Jobs extends AppCompatActivity implements IData {

    private View noDataFound;
    private MKLoader loader;
    private ServerPresenter presenter;
    private NewsAdapter newsAdapter;
    private ArrayList<Object> arrayList;
    private AdLoader adLoader;
    private int arraySize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.Jobs);
            getSupportActionBar().setElevation(0);
        }

        initViews();

        fetchNews(1);
    }

    private void initViews() {
        presenter = ServerPresenter.getInstance(this);
        arrayList = new ArrayList<>();
        newsAdapter = new NewsAdapter(arrayList);

        noDataFound = findViewById(R.id.no_data);
        loader = findViewById(R.id.loader);

        RecyclerView rvJobs = findViewById(R.id.rv_jobs);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvJobs.setLayoutManager(layoutManager);
        rvJobs.setAdapter(newsAdapter);

        rvJobs.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                fetchNews(page);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    private void fetchNews(int page) {
        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.NEWS, String.valueOf(page), Constants.Cases.JOBS);
        } else {
            Internet.showOfflineDialog(this);
        }
    }

    private void insertAdsInList() {
        if (Constants.mNativeAds.size() > 0) {

            int offset = 10;//(arrayList.size() / Constants.mNativeAds.size()) + 1;

//            Log.e("testing", "offset = " + offset);
//            Log.e("testing", "page = " + page);

            int index = arraySize + 2;

            arraySize = arrayList.size();

            for (UnifiedNativeAd ad : Constants.mNativeAds) {
                Log.e("testing", "index  = " + index);
                if (index < arrayList.size()) {
                    arrayList.add(index, ad);
                    Log.e("testing", "index added to arraylist = " + index);
                    index = index + offset;
                }
            }

        }

        newsAdapter.notifyDataSetChanged();

        Log.e("testing",
                "Loaded ads = " + Constants.mNativeAds.size() + "\n" +
                        "arraylist size = " + (arrayList.size() - Constants.mNativeAds.size())
        );


    }

    private void loadNativeAds() {

        try {
            Log.e("testing", "Loading ads = " + 5);

            Constants.mNativeAds.clear();

            AdLoader.Builder builder = new AdLoader.Builder(this, getResources().getString(R.string.native_ad_id));

            adLoader = builder.forUnifiedNativeAd(
                    unifiedNativeAd -> {

                        // A native ad loaded successfully, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        Log.e("testing", "The previous native ad loaded successfully."
                                + " loading another.");
                        Constants.mNativeAds.add(unifiedNativeAd);
                        if (!adLoader.isLoading()) {
                            insertAdsInList();
                            Log.e("testing", "insertAdsInEpisodes called from success.");
                        }

                    }).withAdListener(
                    new AdListener() {
                        @Override
                        public void onAdFailedToLoad(int errorCode) {
                            // A native ad failed to load, check if the ad loader has finished loading
                            // and if so, insert the ads into the list.
                            Log.e("testing", "The previous native ad failed to load. Attempting to");
                            if (!adLoader.isLoading()) {
                                insertAdsInList();
                                Log.e("testing", "insertAdsInEpisodes called from failed.");
                            }
                        }
                    }).build();

            // Load the Native ads.
            adLoader.loadAds(new AdRequest.Builder().build(), 5);


        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                NewsModel newsModel = new Gson().fromJson(response, NewsModel.class);

                if (newsModel.isSuccess()) {

                    if (newsModel.getData() != null && newsModel.getData().size() > 0) {
                        arrayList.addAll(newsModel.getData());
                        newsAdapter.notifyDataSetChanged();
                        if (arrayList.size() > 0) loadNativeAds();
                        else noDataFound.setVisibility(View.VISIBLE);
                    } else {
                        displayError("No records found.");
                    }

                }

            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}