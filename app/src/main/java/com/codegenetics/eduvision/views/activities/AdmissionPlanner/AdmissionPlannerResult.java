package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.AdmissionsDAO;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.interfaces.ISelectedFiltersClick;
import com.codegenetics.eduvision.models.Admissions.AdmissionsModel;
import com.codegenetics.eduvision.models.Admissions.AdmissionsModel.AdmissionsData;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.models.FilterModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.utils.Utils;
import com.codegenetics.eduvision.views.activities.HomeActivity;
import com.codegenetics.eduvision.views.adapters.CitiesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.FilterRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.LevelsSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.SelectedFilterRecycler;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class AdmissionPlannerResult extends AppCompatActivity implements IData, IFilterClick, ISelectedFiltersClick {

    private ServerPresenter presenter;
    private MKLoader loader;
    private View viewNoData, viewData;
    private TextView tvHistory;

    private AdmissionsDAO admissionsDAO;

    private AdmissionsRecyclerAdapter adapter;
    private ArrayList<String> arrayListDisciplines;
    private final ArrayList<AdmissionsData> admissionsDataArrayList = new ArrayList<>();
    private int counter = 0;

    private String cityName = "", cityCode = "";
    private LinearLayout llCourses, llBottomSheetBottom;
    private Spinner spDisciplineType, spLevel;
    private AutoCompleteTextView etDisciplineName, etCity;
    private Button btnSearchBottomSheet;
    private CitiesModel.CitiesData citiesData;

    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    private SelectedFilterRecycler selectedFilterRecycler;
    private final List<FilterModel> selectedFilters = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admission_planner_result);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.admissions);
        }

        initViews();
        fetchAdmissions();

        admissionsDAO.getAll().observeForever(admissionsDBModels -> {
            try {
                if (admissionsDBModels.size() > 0) {
                    tvHistory.setVisibility(View.VISIBLE);
                    tvHistory.setOnClickListener(v -> startActivity(new Intent(this, AdmissionPlannerResultHistory.class)));
                } else {
                    tvHistory.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private void initViews() {

        admissionsDAO = DatabaseInjection.getAdmissionsDAO(this);
        arrayListDisciplines = getIntent().getStringArrayListExtra(IntentEnums.DATA.name());

        presenter = ServerPresenter.getInstance(this);
        adapter = new AdmissionsRecyclerAdapter(admissionsDataArrayList);

        tvHistory = findViewById(R.id.tv_history);
        loader = findViewById(R.id.loader);
        viewNoData = findViewById(R.id.no_data);
        viewData = findViewById(R.id.view_data);

        RecyclerView rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);


        RecyclerView rvFilters = findViewById(R.id.rv_filters);
        RecyclerView rvFiltersSelected = findViewById(R.id.rv_filters_selected);
        List<FilterModel> filterModels = new ArrayList<>();
        filterModels.add(new FilterModel(R.drawable.ic_filter_primary, "Filter", SearchFilter.FILTER));
        filterModels.add(new FilterModel(0, "City", SearchFilter.CITY));
        rvFilters.setHasFixedSize(true);
        rvFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvFilters.setAdapter(new FilterRecyclerAdapter(filterModels, this));

        rvFiltersSelected.setHasFixedSize(true);
        rvFiltersSelected.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectedFilterRecycler = new SelectedFilterRecycler(selectedFilters, this);
        rvFiltersSelected.setAdapter(selectedFilterRecycler);

        View v = findViewById(R.id.bottomSheet_Layout);
        mBehavior = BottomSheetBehavior.from(v);

    }

    private void fetchAdmissions() {
        for (String discipline : arrayListDisciplines) {
            try {
                if (Internet.isConnected(this)) {

                    viewNoData.setVisibility(View.GONE);
                    presenter.getData(
                            AppEnums.ADMISSIONS,
                            "1", /* page */
                            cityCode, /* city code */
                            getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name()), /* level code */
                            discipline.split("#")[0], /* discipline name */
                            discipline.split("#")[1]  /* discipline type */
                    );

                } else {
                    Internet.showOfflineDialog(this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        if (arrayListDisciplines.size() == (counter + 1)) loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                AdmissionsModel admissionsModel = new Gson().fromJson(response, AdmissionsModel.class);

                if (admissionsModel.isSuccess()) {
                    if (admissionsModel.getData() != null) {
                        admissionsDataArrayList.addAll(admissionsModel.getData());
                    }

                    ++counter;
                    if (arrayListDisciplines.size() == counter) {
                        Collections.sort(admissionsDataArrayList, new CustomComparator());
                        adapter.notifyDataSetChanged();

                        counter = 0;

                        viewNoData.setVisibility(admissionsDataArrayList.size() > 0 ? View.GONE : View.VISIBLE);
                        viewData.setVisibility(admissionsDataArrayList.size() > 0 ? View.VISIBLE : View.GONE);
                    }

                } else {
                  displayError(getString(R.string.something_went_wrong));
                }


            }
        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    static class CustomComparator implements Comparator<AdmissionsData> {
        @Override
        public int compare(AdmissionsData o1, AdmissionsData o2) {
            try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                return Objects.requireNonNull(
                        dateFormat.parse(o1.getDeadline())).compareTo(dateFormat.parse(o2.getDeadline()));
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
    }

    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {

                @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_admissions, null);

                spDisciplineType = view.findViewById(R.id.sp_discipline_type);
                spLevel = view.findViewById(R.id.sp_level);
                etCity = view.findViewById(R.id.et_city);
                etDisciplineName = view.findViewById(R.id.et_courses);
                llCourses = view.findViewById(R.id.linear_layout_courses);
                llBottomSheetBottom = view.findViewById(R.id.linear_layout_bottom);
                View viewDisciplineLevel = view.findViewById(R.id.view_discipline_level);
                viewDisciplineLevel.setVisibility(View.GONE);
                TextView tvTitle=view.findViewById(R.id.tv_title);
                tvTitle.setText(R.string.city_finder);

                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                btnSearchBottomSheet = view.findViewById(R.id.btn_search);

                fillSpinnerAdapter();

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearchBottomSheet.setOnClickListener(view12 -> {

                    if (citiesData != null) {
                        cityName = citiesData.getCityName();
                        cityCode = citiesData.getCityCode();
                    } else {
                        cityName = "";
                        cityCode = "";
                    }

                    admissionsDataArrayList.clear();
                    adapter.notifyDataSetChanged();
                    viewData.setVisibility(View.GONE);

                    mBottomSheetDialog.dismiss();

                    prepareDataForSelectedFilterRecyclerView();

                    fetchAdmissions();

                });

                mBottomSheetDialog = new BottomSheetDialog(this);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
            }

            if (mBottomSheetDialog != null) mBottomSheetDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void fillSpinnerAdapter() {

        if (spDisciplineType == null || spDisciplineType.getSelectedItem() == null) {
            ArrayList<String> disciplinesStringArray = new ArrayList<>();
            disciplinesStringArray.add(ConstantData.SELECT);

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, disciplinesStringArray);
            spDisciplineType.setAdapter(adapter);

            CitiesSpinnerAdapter citiesSpinnerAdapter = new CitiesSpinnerAdapter(this, R.layout.layout_spinner, Constants.citiesList);
            etCity.setAdapter(citiesSpinnerAdapter);
            etCity.setOnItemClickListener((adapterView, view1, i, l) -> citiesData = citiesSpinnerAdapter.getItem(i));
            etCity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    citiesData = null;
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            LevelsSpinnerAdapter levelsSpinnerAdapter = new LevelsSpinnerAdapter(this, R.layout.layout_spinner, Constants.levelsList);
            spLevel.setAdapter(levelsSpinnerAdapter);

            spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    String disciplineType = spDisciplineType.getSelectedItem().toString();
                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(levelsData.getLevelName())) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {

                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            spDisciplineType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(adapter.getItem(i))) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!levelsData.getLevelName().equalsIgnoreCase(ConstantData.SELECT)) {

                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
    }


    private void prepareDataForSelectedFilterRecyclerView() {

        selectedFilters.clear();

//        if (!disciplineType.isEmpty())
//            if (!disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {
//                selectedFilters.add(new FilterModel(0, disciplineType, SearchFilter.DISCIPLINE_TYPE));
//            }
//
//        if (!levelName.isEmpty())
//            if (!levelName.equalsIgnoreCase(ConstantData.SELECT)) {
//                selectedFilters.add(new FilterModel(0, levelName, SearchFilter.LEVEL));
//            }
//
//        if (!disciplineName.isEmpty()) {
//            selectedFilters.add(new FilterModel(0, disciplineName, SearchFilter.COURSES));
//        }

        if (!cityName.isEmpty()) { selectedFilters.add(new FilterModel(0, cityName, SearchFilter.CITY));
        }

        selectedFilterRecycler.notifyDataSetChanged();
    }

    private void handleBottomSheetViewsOnFilterSelection(SearchFilter searchFilter) {
        switch (searchFilter) {
            case DISCIPLINE_TYPE:
                spDisciplineType.performClick();
                break;
            case LEVEL:
                spLevel.performClick();
                break;
            case CITY:
                etCity.requestFocus();
                Utils.showKeyboard(this);
                break;
            case COURSES:
                if (etDisciplineName.getVisibility() == View.VISIBLE) {
                    etDisciplineName.requestFocus();
                    Utils.showKeyboard(this);
                } else {
                    Toast.makeText(this, "Please ", Toast.LENGTH_SHORT).show();
                }

            default:
                break;
        }
    }

    @Override
    public void onFilterSelected(SearchFilter searchFilter) {
        showBottomSheet();
        new Handler().postDelayed(() -> handleBottomSheetViewsOnFilterSelection(searchFilter), 100);

    }

    @Override
    public void onFilterDelete(FilterModel data) {
        if (data.getSearchFilter() == SearchFilter.CITY) {
            etCity.setText("");
            cityName = "";
            cityCode = "";
        }

        btnSearchBottomSheet.performClick();
    }
}