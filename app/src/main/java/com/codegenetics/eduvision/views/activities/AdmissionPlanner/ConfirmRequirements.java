package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IProgramsClickListener;
import com.codegenetics.eduvision.views.activities.HomeActivity;

import java.util.ArrayList;

public class ConfirmRequirements extends AppCompatActivity implements IProgramsClickListener {

    private TextView tvName, tvDob, tvDomicile, tvDegree, tvPassingYear;
    private Button btnContinue, btnAddMore;
    private ArrayList<String> arrayList;
    private String levelName, levelCode;
    private TextView tvEditInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_requirements);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.admission_planner);
        }

        initViews();
        updateTopView();
        clickEvents();

    }

    private void initViews() {
        tvName = findViewById(R.id.tv_name);
        tvDob = findViewById(R.id.tv_dob);
        tvDomicile = findViewById(R.id.tv_domicile);
        tvDegree = findViewById(R.id.tv_degree);
        tvPassingYear = findViewById(R.id.tv_passing_year);
        btnContinue = findViewById(R.id.btn_continue);
        btnAddMore = findViewById(R.id.btn_add_more);
        tvEditInfo = findViewById(R.id.tv_edit_profile);

        arrayList = getIntent().getStringArrayListExtra(IntentEnums.DATA.name());
        levelName = getIntent().getStringExtra(IntentEnums.LEVEL_NAME.name());
        levelCode = getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name());

        ConfirmRequirementsRecyclerAdapter adapter = new ConfirmRequirementsRecyclerAdapter(arrayList, this);

        RecyclerView rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        btnAddMore.setVisibility(arrayList.size() < 3 ? View.VISIBLE : View.GONE);

    }

    private void updateTopView() {
        tvName.setText(PlannerInfoSession.getInstance().getName());
        tvDob.setText(PlannerInfoSession.getInstance().getDob());
        tvDomicile.setText(PlannerInfoSession.getInstance().getDomicile());
        tvDegree.setText(PlannerInfoSession.getInstance().getDegree());
        tvPassingYear.setText(PlannerInfoSession.getInstance().getPassingYear());
    }

    private void clickEvents() {
        btnContinue.setOnClickListener(v ->
                startActivity(new Intent(this, AdmissionPlannerResult.class)
                        .putExtra(IntentEnums.LEVEL_CODE.name(), getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name()))
                        .putExtra(IntentEnums.DATA.name(), arrayList)
                ));

        tvEditInfo.setOnClickListener(v-> {
            startActivity(new Intent(this, HomeActivity.class));
            startActivity(new Intent(this, Planner.class));
            finishAffinity();
        });

        btnAddMore.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, Discipline.class)
                .putExtra(IntentEnums.LEVEL_NAME.name(), levelName)
                .putExtra(IntentEnums.LEVEL_CODE.name(), levelCode)
                .putExtra(IntentEnums.DATA.name(), arrayList)
        );
        finishAffinity();
    }

    @Override
    public void onItemClicked(AppEnums enums, int position) {

        if (position == -1) {
            btnAddMore.setVisibility(View.VISIBLE);
            return;
        }

        startActivity(new Intent(this, AdmissionPlannerInstitutes.class)
                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), arrayList.get(position).split("#")[1])
                .putExtra(IntentEnums.DISCIPLINE_NAME.name(), arrayList.get(position).split("#")[0])
                .putExtra(IntentEnums.LEVEL_CODE.name(), levelCode)
                .putExtra(IntentEnums.LEVEL_NAME.name(), levelName)
                .putExtra(IntentEnums.TYPE.name(), enums)
        );
    }
}