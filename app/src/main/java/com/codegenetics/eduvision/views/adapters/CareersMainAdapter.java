package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.Career.CareersMainModel;
import com.codegenetics.eduvision.views.activities.CareerList;

import java.util.ArrayList;

public
class CareersMainAdapter extends RecyclerView.Adapter<CareersMainAdapter.ViewHolder> {

    private Context context;
    private final ArrayList<CareersMainModel> arrayList;
    private final RecyclerView.RecycledViewPool recycledViewPool;

    public CareersMainAdapter(ArrayList<CareersMainModel> arrayList) {
        this.arrayList = arrayList;
        recycledViewPool = new RecyclerView.RecycledViewPool();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_programs_main, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.recyclerView.setRecycledViewPool(recycledViewPool);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CareersMainModel data = arrayList.get(holder.getAdapterPosition());

        holder.recyclerView.setAdapter(new CareersInnerAdapter(data.getArrayList()));

        holder.tvTitle.setText(data.getTitle());

        holder.tvViewAll.setOnClickListener(view ->
                view.getContext().startActivity(new Intent(view.getContext(), CareerList.class)
                        .putExtra(IntentEnums.CATEGORY_ID.name(), data.getCategoryId())
                        .putExtra(IntentEnums.CATEGORY_NAME.name(), data.getTitle())
                )
        );
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle, tvViewAll;
        private final RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvViewAll = itemView.findViewById(R.id.tv_view_all);
            recyclerView = itemView.findViewById(R.id.recycler_view_programs_inner);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        }
    }
}
