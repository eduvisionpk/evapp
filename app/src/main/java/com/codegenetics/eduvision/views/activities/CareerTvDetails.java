package com.codegenetics.eduvision.views.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.EndlessRecyclerViewScrollListener;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IVideoPLayList;
import com.codegenetics.eduvision.models.Videos.VideoDetailsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.VideoPlaylistRecycler;
import com.google.gson.Gson;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class CareerTvDetails extends AppCompatActivity implements IData, IVideoPLayList {

    private ServerPresenter presenter;
    private MKLoader loader;
    private ImageView ivShare;
    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayerTracker youTubePlayerTracker;

    private VideoDetailsModel.VideoDetailsData detailsData;
    private final ArrayList<VideoDetailsModel.VideoDetailsData> videoPlaylistArrayList = new ArrayList<>();
    private VideoPlaylistRecycler videoPlaylistRecycler;

    private void init() {

        detailsData = getIntent().getParcelableExtra(IntentEnums.DATA.name());

        presenter = ServerPresenter.getInstance(this);

        loader = findViewById(R.id.loader);
        ivShare = findViewById(R.id.iv_share);

        TextView tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText(detailsData.getTitle());

        RecyclerView rvVideoPlaylist = findViewById(R.id.recycler_view);
        rvVideoPlaylist.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvVideoPlaylist.setLayoutManager(layoutManager);
        videoPlaylistRecycler = new VideoPlaylistRecycler(videoPlaylistArrayList, this);
        rvVideoPlaylist.setAdapter(videoPlaylistRecycler);
        rvVideoPlaylist.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                getVideosUsingType(detailsData.getType(), page);
            }
        });

        youTubePlayerView = findViewById(R.id.youtube_player);
        getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.enableBackgroundPlayback(true);
        youTubePlayerTracker = new YouTubePlayerTracker();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_tv_details);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.career_tv);
        }

        ivShare.setOnClickListener(v ->
                Constants.getInstance().share(this,
                        String.format(Locale.getDefault(),
                                "I'm watching '%s' on Eduvision app. Please click link below to watch it now.\n\n%s",
                                detailsData.getTitle(), detailsData.getLink()
                        ))
        );

        setYoutubePlayer();

        getVideosUsingType(detailsData.getType(), 1);

    }

    private void setYoutubePlayer() {

        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {

            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                super.onReady(youTubePlayer);

                try {

                    youTubePlayer.addListener(youTubePlayerTracker);

                    String videoId = Constants.getInstance().splitVideoIdFromURL(detailsData.getLink());

                    youTubePlayer.loadVideo(videoId, 0);

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStateChange(@NonNull YouTubePlayer youTubePlayer,
                                      @NonNull PlayerConstants.PlayerState state) {

                if (state.equals(PlayerConstants.PlayerState.ENDED)) {
                    if (videoPlaylistArrayList != null && videoPlaylistArrayList.size() > 0)
                        onVideoSelected(videoPlaylistArrayList.get(0));
                }
            }

            @Override
            public void onError(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerError error) {
                super.onError(youTubePlayer, error);
            }
        });

        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                hideSystemUI();
            }

            @Override
            public void onYouTubePlayerExitFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                showSystemUI();
            }
        });

    }

    private void getVideosUsingType(String type, int page) {
        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.VIDEO_DETAILS, String.valueOf(page), type);
        } else {
            Internet.showOfflineDialog(this);
        }
    }


    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }


    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (youTubePlayerView != null) youTubePlayerView.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                VideoDetailsModel videoDetailsModel = new Gson().fromJson(response, VideoDetailsModel.class);

                if (videoDetailsModel.isSuccess()) {

                    if (videoDetailsModel.getData() != null && videoDetailsModel.getData().size() > 0) {
                        videoPlaylistArrayList.addAll(videoDetailsModel.getData());
                        Collections.shuffle(videoPlaylistArrayList);
                    }

                    videoPlaylistRecycler.notifyDataSetChanged();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVideoSelected(VideoDetailsModel.VideoDetailsData data) {
        startActivity(new Intent(this, CareerTvDetails.class)
                .putExtra(IntentEnums.DATA.name(), data));
        finish();
    }
}