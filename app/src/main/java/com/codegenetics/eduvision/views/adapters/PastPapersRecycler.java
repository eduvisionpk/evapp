package com.codegenetics.eduvision.views.adapters;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.KeyValue;

import java.util.ArrayList;

public class PastPapersRecycler extends RecyclerView.Adapter<PastPapersRecycler.PastPaperHolder> {

    private final ArrayList<KeyValue> mData;
    private final IViewClickListener listener;

    public PastPapersRecycler(ArrayList<KeyValue> mData, IViewClickListener clickListener) {
        this.mData = mData;
        listener = clickListener;
    }

    @NonNull
    @Override
    public PastPaperHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PastPaperHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_past_papers, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PastPaperHolder holder, int position) {
        KeyValue data = mData.get(holder.getAdapterPosition());

        holder.tvTitle.setText(data.getKey());

        holder.ivView.setOnClickListener(v -> {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.getValue()));
                browserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                v.getContext().startActivity(browserIntent);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        holder.ivDownload.setOnClickListener(v -> listener.onItemClicked(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class PastPaperHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;
        private final ImageView ivView, ivDownload;

        public PastPaperHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            ivView = itemView.findViewById(R.id.iv_view);
            ivDownload = itemView.findViewById(R.id.iv_download);
        }
    }

}
