package com.codegenetics.eduvision.views.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.models.Institution.InstituteDetailsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.UniversitiesDetailsRecycler;
import com.codegenetics.eduvision.views.adapters.UniversityDetailTabRecycler;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Locale;

public class UniversityDetails
        extends AppCompatActivity
        implements IData, ITabClickListener {

    private MKLoader loader;
    private ImageView iv_logo;
    private TextView tvUniName, tvPhone, tvEmail, tvAddress, tvWebsite, tvCity;

    private ServerPresenter presenter;

    private String instituteId;
    private ArrayList<InstituteDetailsModel.InstituteDetailsPrograms> programsArrayList, filteredProgramsArrayList;
    private ArrayList<InstituteDetailsModel.InstituteDetailsLevels> levelsArrayList;
    private UniversityDetailTabRecycler tabRecycler;
    private UniversitiesDetailsRecycler detailRecycler;

    private void init() {
        instituteId = getIntent().getStringExtra(IntentEnums.INSTITUTE_ID.name());

        iv_logo = findViewById(R.id.iv_logo);
        tvUniName = findViewById(R.id.tv_uni_name);
        tvPhone = findViewById(R.id.tv_phone);
        tvEmail = findViewById(R.id.tv_email);
        tvAddress = findViewById(R.id.tv_address);
        tvWebsite = findViewById(R.id.tv_website);
        tvCity = findViewById(R.id.tv_city);
        loader = findViewById(R.id.loader);
        presenter = ServerPresenter.getInstance(this);
        programsArrayList = new ArrayList<>();
        filteredProgramsArrayList = new ArrayList<>();
        levelsArrayList = new ArrayList<>();
        tabRecycler = new UniversityDetailTabRecycler(levelsArrayList, this);

        RecyclerView rvTabs = findViewById(R.id.rv_level);
        rvTabs.setHasFixedSize(true);
        rvTabs.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvTabs.setAdapter(tabRecycler);

        RecyclerView rvDetails = findViewById(R.id.rv_details);
        rvDetails.setHasFixedSize(true);
        rvDetails.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        detailRecycler = new UniversitiesDetailsRecycler(filteredProgramsArrayList);
        rvDetails.setAdapter(detailRecycler);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university_details);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getIntent().getStringExtra(IntentEnums.DISCIPLINE_NAME.name()));
        }

        fetchUniversityDetails();

    }

    private void fetchUniversityDetails() {

        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.INSTITUTE_DETAILS, instituteId);
        } else {
            Internet.showOfflineDialog(this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        try {
            if (!response.isEmpty()) {
                InstituteDetailsModel model = new Gson().fromJson(response, InstituteDetailsModel.class);

                if (model.isSuccess()
                        && model.getLevels() != null && model.getLevels().size() > 0
                        && model.getPrograms() != null && model.getPrograms().size() > 0
                ) {

                    programsArrayList.addAll(model.getPrograms());

                    levelsArrayList.addAll(model.getLevels());
                    tabRecycler.notifyDataSetChanged();

                    onTabsSelected(levelsArrayList.get(0).getLevel_name());

                    setDataOnTopView(model.getData().get(0));

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataOnTopView(InstituteDetailsModel.InstituteDetailsData instituteDetailsData) {
        Glide.with(iv_logo)
                .load(instituteDetailsData.getIns_logo())
                .placeholder(R.drawable.ic_dummy)
                .error(R.drawable.ic_dummy)
                .into(iv_logo);
        tvUniName.setText(instituteDetailsData.getName());
        tvPhone.setText(String.format(Locale.getDefault(), "Phone: %s", instituteDetailsData.getPhone1()));
        tvEmail.setText(String.format(Locale.getDefault(), "Email: %s", instituteDetailsData.getEmail1()));
        tvAddress.setText(String.format(Locale.getDefault(), "Address: %s", instituteDetailsData.getAddress()));
        tvWebsite.setText(String.format(Locale.getDefault(), "Website: %s", instituteDetailsData.getUrl()));
        tvCity.setText(String.format(Locale.getDefault(), "City: %s", instituteDetailsData.getCity_name()));
    }

    private ArrayList<InstituteDetailsModel.InstituteDetailsPrograms> getDetailsOnLevelSelection(String levelName) {
        ArrayList<InstituteDetailsModel.InstituteDetailsPrograms> mData = new ArrayList<>();
        for (InstituteDetailsModel.InstituteDetailsPrograms programs : programsArrayList) {
            if (programs.getLevel_name().equalsIgnoreCase(levelName)) mData.add(programs);
        }
        return mData;
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTabsSelected(String s) {
        filteredProgramsArrayList.clear();
        filteredProgramsArrayList.addAll(getDetailsOnLevelSelection(s));
        detailRecycler.notifyDataSetChanged();
    }

}