package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IProgramClickListener;
import com.codegenetics.eduvision.models.ProgramsMainModel;

import java.util.ArrayList;

public
class ProgramsMainAdapter extends RecyclerView.Adapter<ProgramsMainAdapter.ViewHolder> implements IProgramClickListener {

    private Context context;
    private final ArrayList<ProgramsMainModel> arrayList;
    private final RecyclerView.RecycledViewPool recycledViewPool;
    private final IProgramClickListener listener;

    public ProgramsMainAdapter(ArrayList<ProgramsMainModel> arrayList, IProgramClickListener clickListener) {
        this.arrayList = arrayList;
        recycledViewPool = new RecyclerView.RecycledViewPool();
        listener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_programs_main, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.recyclerView.setRecycledViewPool(recycledViewPool);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProgramsMainModel data = arrayList.get(holder.getAdapterPosition());

        holder.recyclerView.setAdapter(new ProgramsInnerAdapter(data.getArrayList(), this));

        holder.tvTitle.setText(data.getTitle());
        holder.tvViewAll.setOnClickListener(view -> {
            listener.onProgramClicked(
                    ConstantData.getInstance().getProgramType(data.getProgramsEnums()), "");});

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public void onProgramClicked(String disciplineType, String disciplineName) {
        listener.onProgramClicked(disciplineType, disciplineName);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle, tvViewAll;
        private final RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvViewAll = itemView.findViewById(R.id.tv_view_all);
            recyclerView = itemView.findViewById(R.id.recycler_view_programs_inner);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        }
    }
}
