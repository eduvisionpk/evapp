package com.codegenetics.eduvision.views.activities;

import android.Manifest.permission;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.helper.FileDownloader;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.models.KeyValue;
import com.codegenetics.eduvision.models.PastPapers.PastPapersBoardsClassesModel;
import com.codegenetics.eduvision.models.PastPapers.PastPapersSubjectsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.adapters.PastPapersRecycler;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PastPaperSelection extends AppCompatActivity implements IData, IViewClickListener {
    private final String TAG = PastPaperSelection.class.getSimpleName();

    public static final int WRITE_PERMISSION_CODE = 1;
    private static final String[] PERMISSIONS = {permission.WRITE_EXTERNAL_STORAGE};

    private View viewStep1, viewStep2;
    private Spinner spBoard, spClass, spSubject;
    private TextView tvBoardClassSelection, tvOneStep1, tvOneStep2;
    private Button btnContinue1, btnContinue2, btnCancel;
    private RecyclerView rvPastPapers;
    private ServerPresenter presenter;
    private MKLoader loader;
    private View viewNoData;
    private PastPapersRecycler pastPapersAdapter;
    private final ArrayList<KeyValue> paperList = new ArrayList<>();
    private boolean isDataLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_paper_selection);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.Past_papers);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initViews();
        clickEvents();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void clickEvents() {

        btnContinue1.setOnClickListener(v -> {
            String board = spBoard.getSelectedItem().toString();
            String class_ = spClass.getSelectedItem().toString();
            if (spBoard.getSelectedItemPosition() > 0 && spClass.getSelectedItemPosition() > 0) {
                presenter.getData(AppEnums.PAST_PAPERS_SUBJECTS, board, class_);
                tvBoardClassSelection.setText(String.format(Locale.getDefault(), "%s, %s", board, class_));
                switchStep(2);

            } else {
                Toast.makeText(this, getString(R.string.please_choose_all_fields), Toast.LENGTH_SHORT).show();
            }

        });

        btnContinue2.setOnClickListener(v -> {
            String board = spBoard.getSelectedItem().toString();
            String class_ = spClass.getSelectedItem().toString();
            String subject = spSubject.getSelectedItem().toString();
            if (spBoard.getSelectedItemPosition() > 0
                    && spClass.getSelectedItemPosition() > 0
                    && spSubject.getSelectedItemPosition() > 0) {
                presenter.getData(AppEnums.PAST_PAPERS_DETAILS, board, class_, subject);
                switchStep(3);
            } else {
                Toast.makeText(this, getString(R.string.please_choose_all_fields), Toast.LENGTH_SHORT).show();
            }

        });

        btnCancel.setOnClickListener(v -> switchStep(1));

        tvBoardClassSelection.setOnClickListener(v -> switchStep(1));

        tvOneStep1.setOnClickListener(v -> switchStep(1));

        tvOneStep2.setOnClickListener(v -> switchStep(1));

    }

    private void initViews() {
        viewStep1 = findViewById(R.id.view_step_1);
        viewStep2 = findViewById(R.id.view_step_2);
        spBoard = findViewById(R.id.sp_board);
        spClass = findViewById(R.id.sp_class);
        spSubject = findViewById(R.id.sp_subject);
        btnContinue1 = findViewById(R.id.btn_continue_step_1);
        btnContinue2 = findViewById(R.id.btn_continue_step_2);
        btnCancel = findViewById(R.id.btn_cancel);
        tvBoardClassSelection = findViewById(R.id.tv_selected_board_class);
        tvOneStep1 = findViewById(R.id.tv_one_step_1);
        tvOneStep2 = findViewById(R.id.tv_one_step_2);
        rvPastPapers = findViewById(R.id.rv_past_papers);
        loader = findViewById(R.id.loader);
        viewNoData = findViewById(R.id.no_data);
        presenter = ServerPresenter.getInstance(this);
        presenter.getData(AppEnums.PAST_PAPERS_BOARDS_CLASSES);

        rvPastPapers.setHasFixedSize(true);
        rvPastPapers.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        pastPapersAdapter = new PastPapersRecycler(paperList, this);
        rvPastPapers.setAdapter(pastPapersAdapter);
    }

    private void switchStep(int step) {
        switch (step) {
            case 1:
                viewStep1.setVisibility(View.VISIBLE);
                viewStep2.setVisibility(View.GONE);
                rvPastPapers.setVisibility(View.GONE);
                viewNoData.setVisibility(View.GONE);
                paperList.clear();
                pastPapersAdapter.notifyDataSetChanged();
                isDataLoaded = false;
                break;
            case 2:
                viewStep2.setVisibility(View.VISIBLE);
                viewStep1.setVisibility(View.GONE);
                rvPastPapers.setVisibility(View.GONE);
                viewNoData.setVisibility(View.GONE);
                paperList.clear();
                pastPapersAdapter.notifyDataSetChanged();
                isDataLoaded = false;

                break;
            case 3:
                viewStep2.setVisibility(View.GONE);
                viewStep1.setVisibility(View.GONE);
                rvPastPapers.setVisibility(View.VISIBLE);
                viewNoData.setVisibility(View.GONE);
                isDataLoaded = true;
                break;
        }
    }

    private void prepareDataForRecycler(String response) {

        ArrayList<KeyValue> data = new ArrayList<>();

        try {
            JSONObject res = new JSONObject(response);

            JSONArray jsonArray = res.getJSONArray("data");
            res = jsonArray.getJSONObject(0);

            for (int i = 1; i <= res.length() / 2; i++) {
                if (!res.getString("file" + i).isEmpty()) {
                    data.add(new KeyValue(spSubject.getSelectedItem().toString() + " past paper " + res.getString("year" + i), res.getString("file" + i)));
                }
            }

            paperList.clear();

            if (data.size() > 0) {
                paperList.addAll(data);
                pastPapersAdapter.notifyDataSetChanged();
            } else {
                viewNoData.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            Log.e(TAG, "prepareDataForRecycler: ", e);
        }
    }

    private void initSubjectSpinner(List<PastPapersSubjectsModel.SubjectsData> data) {
        ArrayList<String> items = new ArrayList<>();
        items.add(getString(R.string.select_your_subject));

        for (PastPapersSubjectsModel.SubjectsData item : data) items.add(item.getSubject());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, items);
        spSubject.setAdapter(adapter);
    }

    private void initClassSpinner(List<PastPapersBoardsClassesModel.ClassesData> classes) {
        ArrayList<String> items = new ArrayList<>();
        items.add(getString(R.string.select_your_class));

        for (PastPapersBoardsClassesModel.ClassesData item : classes)
            if (!item.getClassX().isEmpty()) items.add(item.getClassX());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, items);
        spClass.setAdapter(adapter);

    }

    private void initBoardSpinner(List<PastPapersBoardsClassesModel.BoardsData> boards) {
        ArrayList<String> items = new ArrayList<>();
        items.add(getString(R.string.select_your_board));

        for (PastPapersBoardsClassesModel.BoardsData item : boards)
            if (!item.getBoard().isEmpty()) items.add(item.getBoard());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, items);
        spBoard.setAdapter(adapter);
    }

    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        try {

            if (response != null) {

                CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);

                switch (caseModel.getCase()) {
                    case Constants.Cases.PAST_PAPERS_BOARDS_CLASSES:
                        PastPapersBoardsClassesModel boardsClassesModel = new Gson().fromJson(response, PastPapersBoardsClassesModel.class);

                        if (boardsClassesModel.getBoards() != null) initBoardSpinner(boardsClassesModel.getBoards());
                        else noDataFound(getString(R.string.no_boards_available), spBoard);

                        if (boardsClassesModel.getClasses() != null) initClassSpinner(boardsClassesModel.getClasses());
                        else noDataFound(getString(R.string.no_classes_available), spClass);

                        break;

                    case Constants.Cases.PAST_PAPERS_SUBJECTS:
                        PastPapersSubjectsModel subjectsModel = new Gson().fromJson(response, PastPapersSubjectsModel.class);

                        if (subjectsModel.getData() != null) initSubjectSpinner(subjectsModel.getData());
                        else noDataFound(getString(R.string.no_subjects_available), spSubject);

                        break;
                    case Constants.Cases.PAST_PAPERS_DETAILS:
                        prepareDataForRecycler(response);
                        break;
                }

            }

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private void noDataFound(String message, Spinner spinner) {
        ArrayList<String> items = new ArrayList<>();
        items.add(message);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, items);
        spinner.setAdapter(adapter);
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (isDataLoaded) switchStep(2);
        else finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == WRITE_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Thank you for granting permission. You can download it now.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onItemClicked(int position) {

        if (!hasPermissions(PERMISSIONS)) {
            Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
            ActivityCompat.requestPermissions(this, PERMISSIONS, WRITE_PERMISSION_CODE);
        } else {
            Log.v(TAG, "download() Method HAVE PERMISSIONS ");
            new DownloadFile().execute(paperList.get(position).getValue(), paperList.get(position).getKey());
            Toast.makeText(this, "File downloaded successfully.", Toast.LENGTH_SHORT).show();
        }
        Log.v(TAG, "download() Method completed ");
    }

    static class DownloadFile extends AsyncTask<String, Void, Void> {

        private static final String TAG = "DownloadFile";

        @Override
        protected Void doInBackground(String... strings) {
            Log.v(TAG, "doInBackground() Method invoked ");

            try {

                String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
                String fileName = strings[1] + ".pdf";  // -> maven.pdf
                File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                File pdfFile = new File(folder, fileName);
                Log.v(TAG, "doInBackground() pdfFile invoked " + pdfFile.getAbsolutePath());
                Log.v(TAG, "doInBackground() pdfFile invoked " + pdfFile.getAbsoluteFile());

                try {
                    pdfFile.createNewFile();
                    Log.v(TAG, "doInBackground() file created" + pdfFile);

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "doInBackground() error" + e.getMessage());

                }

                FileDownloader.downloadFile(fileUrl, pdfFile);
                Log.v(TAG, "doInBackground() file download completed");

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

    }
}