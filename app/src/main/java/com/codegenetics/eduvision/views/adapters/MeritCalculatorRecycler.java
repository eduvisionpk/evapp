package com.codegenetics.eduvision.views.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.MeritCalculator.MeritCalculatorListModel;
import com.codegenetics.eduvision.views.activities.MeritCalculatorDetails;

import java.util.ArrayList;

public class MeritCalculatorRecycler extends RecyclerView.Adapter<MeritCalculatorRecycler.MeritCalculatorHolder> {
    private int lastPosition = -1;
    private final ArrayList<MeritCalculatorListModel.MeritCalculatorListData> mData;

    public MeritCalculatorRecycler(ArrayList<MeritCalculatorListModel.MeritCalculatorListData> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public MeritCalculatorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MeritCalculatorHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_merit_calculator, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MeritCalculatorHolder holder, int position) {
        MeritCalculatorListModel.MeritCalculatorListData meritCalculatorListData = mData.get(position);

        holder.tvTitle.setText(meritCalculatorListData.getInstitute());
        holder.tvDescription.setText(String.format("Admission probability Calculator for\n%s", meritCalculatorListData.getInstitute()));
        setAnimation(holder.itemView, position);

        holder.ivExpand.setOnClickListener(v -> {
            if (!holder.isClicked) {
                holder.viewCalculator.setVisibility(View.VISIBLE);
                holder.isClicked = true;
                holder.ivExpand.setImageResource(R.drawable.ic_round_keyboard_arrow_up_24);
            } else {
                holder.viewCalculator.setVisibility(View.GONE);
                holder.isClicked = false;
                holder.ivExpand.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
            }
        });

        holder.btnSearch.setOnClickListener(v -> {
            if (isValidate(holder)) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(v.getContext(), MeritCalculatorDetails.class);
                bundle.putString(IntentEnums.FSC_OM.name(), holder.etObtainedFsc.getText().toString());
                bundle.putString(IntentEnums.FSC_TM.name(), holder.etTotalFsc.getText().toString());
                bundle.putString(IntentEnums.METRIC_OM.name(), holder.etObtainedMatric.getText().toString());
                bundle.putString(IntentEnums.METRIC_TM.name(), holder.etTotalMatric.getText().toString());
                bundle.putString(IntentEnums.PERCENTAGE.name(), holder.etPercentage.getText().toString());
                bundle.putString(IntentEnums.INSTITUTE_ID.name(), meritCalculatorListData.getIns());
                bundle.putString(IntentEnums.INSTITUTE.name(), meritCalculatorListData.getInstitute());
                intent.putExtras(bundle);
                v.getContext().startActivity(intent);
            }
        });
    }
    private boolean isValidate(MeritCalculatorHolder holder){
        if(holder.etObtainedFsc.getText().toString().isEmpty()){
            holder.etObtainedFsc.setError("Field is required");
            return false;
        }
        if(holder.etTotalFsc.getText().toString().isEmpty()){
            holder.etTotalFsc.setError("Field is required");
            return false;
        }
        if(holder.etObtainedMatric.getText().toString().isEmpty()){
            holder.etObtainedMatric.setError("Field is required");
            return false;
        }
        if(holder.etTotalMatric.getText().toString().isEmpty()){
            holder.etTotalMatric.setError("Field is required");
            return false;
        }
        if(holder.etPercentage.getText().toString().isEmpty()){
            holder.etPercentage.setError("Field is required");
            return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.item_animation_fall_down);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public static class MeritCalculatorHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescription;
        ImageView ivExpand;
        EditText etObtainedFsc, etTotalFsc, etObtainedMatric, etTotalMatric,etPercentage;
        Button btnSearch;
        View viewCalculator;
        boolean isClicked = false;

        public MeritCalculatorHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDescription = itemView.findViewById(R.id.tv_description);
            ivExpand = itemView.findViewById(R.id.iv_expand);
            etObtainedFsc = itemView.findViewById(R.id.et_fsc_obtained_marks);
            etTotalFsc = itemView.findViewById(R.id.et_fsc_total_marks);
            etObtainedMatric = itemView.findViewById(R.id.et_matric_obtained_marks);
            etTotalMatric = itemView.findViewById(R.id.et_matric_total_marks);
            etPercentage = itemView.findViewById(R.id.et_entry_test_percentage);
            btnSearch = itemView.findViewById(R.id.btn_search);
            viewCalculator = itemView.findViewById(R.id.view_calculator);
        }
    }
}
