package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IRecyclerClick;
import com.codegenetics.eduvision.models.Levels.LevelsModel;

import java.util.ArrayList;

public class DegreeRecycler extends RecyclerView.Adapter<DegreeRecycler.DegreeHolder> {
    private final ArrayList<LevelsModel.LevelsData> mData;
    private final IRecyclerClick recyclerClick;

    public DegreeRecycler(ArrayList<LevelsModel.LevelsData> levelsList, IRecyclerClick recyclerClick) {
        this.mData = levelsList;
        this.recyclerClick = recyclerClick;
    }

    @NonNull
    @Override
    public DegreeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DegreeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simple_list_with_card,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull DegreeHolder holder, int position) {
        holder.tvTitle.setText(mData.get(position).getLevelName());
        holder.itemView.setOnClickListener(v -> recyclerClick.onRecyclerItemClicked(mData.get(position)));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class DegreeHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        public DegreeHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle=itemView.findViewById(R.id.tv_title);
        }
    }
}
