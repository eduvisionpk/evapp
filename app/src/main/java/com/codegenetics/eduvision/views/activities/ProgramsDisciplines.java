package com.codegenetics.eduvision.views.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.interfaces.IProgramsClickListener;
import com.codegenetics.eduvision.interfaces.ISelectedFiltersClick;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.models.DisciplineTypes.DisciplineTypesModel;
import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel;
import com.codegenetics.eduvision.models.FilterModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.utils.Utils;
import com.codegenetics.eduvision.views.adapters.CitiesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.DisciplinesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.FilterRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.LevelsSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.ProgramsDisciplinesAdapter;
import com.codegenetics.eduvision.views.adapters.SelectedFilterRecycler;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ProgramsDisciplines extends AppCompatActivity implements
        IData, IProgramsClickListener, IFilterClick, ISelectedFiltersClick {

    private TextView tvDescription;
    private LinearLayout llCourses, llBottomSheetBottom;
    private MKLoader loader, loaderBottomSheet;
    private Button btnSearchBottomSheet;
    private View viewProgramDisciplines, viewNoData;

    private CitiesModel.CitiesData citiesData;
    private DisciplinesModel.DisciplinesData disciplinesData;
    private boolean isFromFilter = false;

    private ArrayList<DisciplinesModel.DisciplinesData> arrayList;
    private ProgramsDisciplinesAdapter adapter;
    private ServerPresenter presenter;

    private final ArrayList<String> disciplinesStringArray = new ArrayList<>();
    private final ArrayList<CitiesModel.CitiesData> citiesDataArrayList = new ArrayList<>();

    private Spinner spDisciplineType, spLevel;
    private AutoCompleteTextView etDisciplineName, etCity;

    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    RecyclerView rvFilters, rvFiltersSelected;
    SelectedFilterRecycler selectedFilterRecycler;
    List<FilterModel> selectedFilters = new ArrayList<>();

    private String cityCode = "", levelCode = "", levelName = "", disciplineType = "";
    private View viewHeader;

    private void init() {

        disciplineType = getIntent().getStringExtra(IntentEnums.DISCIPLINE_TYPE.name());
        levelCode = getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name());
        levelName = getIntent().getStringExtra(IntentEnums.LEVEL_NAME.name());

        presenter = ServerPresenter.getInstance(this);

        View v = findViewById(R.id.bottomSheet_Layout);
        mBehavior = BottomSheetBehavior.from(v);

        citiesDataArrayList.addAll(Constants.citiesList);
        arrayList = new ArrayList<>();
        adapter = new ProgramsDisciplinesAdapter(arrayList, this);

        viewProgramDisciplines = findViewById(R.id.view_program_disciplines);
        viewNoData = findViewById(R.id.no_data);

        loader = findViewById(R.id.loader);
        loader = findViewById(R.id.loader);
        tvDescription = findViewById(R.id.tv_description);
        viewHeader = findViewById(R.id.view_header);
        viewHeader.setVisibility(View.GONE);

        ImageView ivIcon = findViewById(R.id.iv_icon);
        ivIcon.setImageResource(Constants.getInstance().getIcon(disciplineType));

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        rvFilters = findViewById(R.id.rv_filters);
        rvFiltersSelected = findViewById(R.id.rv_filters_selected);

        rvFilters.setHasFixedSize(true);
        rvFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvFilters.setAdapter(new FilterRecyclerAdapter(ConstantData.getInstance().getAdmissionFilter(), this));

        rvFiltersSelected.setHasFixedSize(true);
        rvFiltersSelected.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectedFilterRecycler = new SelectedFilterRecycler(selectedFilters, this);
        rvFiltersSelected.setAdapter(selectedFilterRecycler);


        for (DisciplineTypesModel.DisciplineTypesData data : Constants.disciplineTypeList) {
            disciplinesStringArray.add(data.getDisciplineType());
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programs_disciplines);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(disciplineType);
        }

        fetchDisciplines();

    }

    private void fetchDisciplines() {

        if (Internet.isConnected(this)) {

            isFromFilter = false;
            presenter.getData(
                    AppEnums.DISCIPLINES_PROGRAMS,
                    disciplineType,
                    levelCode
            );

        } else {
            Internet.showOfflineDialog(this);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        if (enums.equals(AppEnums.DISCIPLINES)) {
            loaderBottomSheet.setVisibility(View.VISIBLE);
            btnSearchBottomSheet.setVisibility(View.INVISIBLE);
        } else {
            loader.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress(AppEnums enums) {
        if (enums.equals(AppEnums.DISCIPLINES)) {
            loaderBottomSheet.setVisibility(View.GONE);
            btnSearchBottomSheet.setVisibility(View.VISIBLE);
        } else loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                DisciplinesModel disciplinesModel = new Gson().fromJson(response, DisciplinesModel.class);

                if (disciplinesModel.isSuccess()) {

                    if (isFromFilter) {

                        if (disciplinesModel.isSuccess() && disciplinesModel.getData() != null) {

                            DisciplinesSpinnerAdapter disciplinesSpinnerAdapter = new DisciplinesSpinnerAdapter(this, R.layout.layout_spinner, disciplinesModel.getData());
                            etDisciplineName.setAdapter(disciplinesSpinnerAdapter);
                            etDisciplineName.setOnItemClickListener((adapterView, view1, i, l) -> disciplinesData = disciplinesSpinnerAdapter.getItem(i));
                            etDisciplineName.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                    disciplinesData = null;
                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                }
                            });

                            llBottomSheetBottom.setWeightSum(2);
                            llCourses.setVisibility(View.VISIBLE);

                        }
                        else {

                            llBottomSheetBottom.setWeightSum(1);
                            llCourses.setVisibility(View.GONE);
                        }
                    }
                    else {

                        if (disciplinesModel.getData() != null) {

                            arrayList.addAll(disciplinesModel.getData());
                            adapter.notifyDataSetChanged();

                            TextView tvNumber = findViewById(R.id.tv_number);
                            TextView tvDisciplineType = findViewById(R.id.tv_discipline_type);

                            tvNumber.setText(String.valueOf(arrayList.size()));
                            tvDisciplineType.setText(disciplineType);

                            tvDescription.setText(String.format(
                                    Locale.getDefault(),
                                    "programs / courses \nare offered at\n%s Level in Pakistan", levelName
                            ));

                            viewHeader.setVisibility(View.VISIBLE);
                            viewProgramDisciplines.setVisibility(View.VISIBLE);
                            viewNoData.setVisibility(View.GONE);

                        }
                        else {
                            viewProgramDisciplines.setVisibility(View.GONE);
                            viewNoData.setVisibility(View.VISIBLE);
                        }

                    }

                }
                else {
                    displayError(disciplinesModel.getMessage());
                }

            }
        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {

                @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_admissions, null);

                spDisciplineType = view.findViewById(R.id.sp_discipline_type);
                spLevel = view.findViewById(R.id.sp_level);
                etCity = view.findViewById(R.id.et_city);
                etDisciplineName = view.findViewById(R.id.et_courses);
                loaderBottomSheet = view.findViewById(R.id.loader);
                llCourses = view.findViewById(R.id.linear_layout_courses);
                llBottomSheetBottom = view.findViewById(R.id.linear_layout_bottom);

                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                btnSearchBottomSheet = view.findViewById(R.id.btn_search);

                fillSpinnerAdapter();

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearchBottomSheet.setOnClickListener(view12 -> {

                    String disciplineType = spDisciplineType.getSelectedItem().toString();

                    if (disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {
                        displayError("Please select discipline type.");
                        return;
                    }

                    String disciplineName = "", levelCode = "", levelName = "", city = "";

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();
                    if (levelsData != null) {
                        levelCode = levelsData.getLevelCode();
                        levelName = levelsData.getLevelName();
                    }

                    if (levelName.equalsIgnoreCase(ConstantData.SELECT)) {
                        displayError("Please select level.");
                        return;
                    }

                    if (llCourses.getVisibility() == View.VISIBLE) {
                        if (disciplinesData != null) {
                            disciplineName = disciplinesData.getDisciplineName();
                        } else {
                            displayError("Invalid course. Please select course.");
                            return;
                        }
                    }

                    if (citiesData != null) city = citiesData.getCityName();
                    else etCity.setText("");

                    mBottomSheetDialog.dismiss();

                    startActivity(
                            new Intent(this, Universities.class)
                                    .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), disciplineType)
                                    .putExtra(IntentEnums.DISCIPLINE_NAME.name(), disciplineName)
                                    .putExtra(IntentEnums.CITY.name(), city)
                                    .putExtra(IntentEnums.LEVEL_CODE.name(), levelCode)
                                    .putExtra(IntentEnums.LEVEL_NAME.name(), levelName)
                    );

                });

                mBottomSheetDialog = new BottomSheetDialog(this);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
            }

            if (mBottomSheetDialog != null) mBottomSheetDialog.show();
//            mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);


        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleBottomSheetViewsOnFilterSelection(SearchFilter searchFilter) {
        switch (searchFilter) {
            case DISCIPLINE_TYPE:
                spDisciplineType.performClick();
                break;
            case LEVEL:
                spLevel.performClick();
                break;
            case CITY:
                etCity.requestFocus();
                Utils.showKeyboard(this);
                break;
            default:
                break;
        }
    }

    private void fillSpinnerAdapter() {

        if (spDisciplineType == null || spDisciplineType.getSelectedItem() == null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, disciplinesStringArray);
            spDisciplineType.setAdapter(adapter);

            CitiesSpinnerAdapter citiesSpinnerAdapter = new CitiesSpinnerAdapter(this, R.layout.layout_spinner, citiesDataArrayList);
            etCity.setAdapter(citiesSpinnerAdapter);

            etCity.setOnItemClickListener((adapterView, view1, i, l) -> citiesData = citiesSpinnerAdapter.getItem(i));

            etCity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    citiesData = null;
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            LevelsSpinnerAdapter levelsSpinnerAdapter = new LevelsSpinnerAdapter(this, R.layout.layout_spinner, Constants.levelsList);
            spLevel.setAdapter(levelsSpinnerAdapter);

            spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    String disciplineType = spDisciplineType.getSelectedItem().toString();
                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(levelsData.getLevelName())) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!disciplineType.equalsIgnoreCase(ConstantData.SELECT)) {
                        isFromFilter = true;
                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spDisciplineType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(adapter.getItem(i))) {
                        llBottomSheetBottom.setWeightSum(1);
                        llCourses.setVisibility(View.GONE);
                    } else if (!levelsData.getLevelName().equalsIgnoreCase(ConstantData.SELECT)) {
                        isFromFilter = true;
                        presenter.getData(
                                AppEnums.DISCIPLINES,
                                spDisciplineType.getSelectedItem().toString(),
                                levelsData.getLevelCode()
                        );
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
    }

    @Override
    public void onItemClicked(AppEnums enums, int position) {
        switch (enums) {
            case UNIVERSITY:
                startActivity(
                        new Intent(this, Universities.class)
                                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), disciplineType)
                                .putExtra(IntentEnums.LEVEL_CODE.name(), levelCode)
                                .putExtra(IntentEnums.LEVEL_NAME.name(), levelName)
                                .putExtra(IntentEnums.DISCIPLINE_NAME.name(), arrayList.get(position).getDisciplineName())
                                .putExtra(IntentEnums.CITY.name(), "")
                );
                break;
            case ADMISSIONS:
                startActivity(
                        new Intent(this, Admissions.class)
                                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), disciplineType)
                                .putExtra(IntentEnums.DISCIPLINE_NAME.name(), arrayList.get(position).getDisciplineName())
                                .putExtra(IntentEnums.CITY.name(), cityCode)
                                .putExtra(IntentEnums.LEVEL_CODE.name(), levelCode)
                                .putExtra(IntentEnums.LEVEL_NAME.name(), levelName)
                );
                break;
            case CAREER_SCOPE:
                startActivity(
                        new Intent(this, Details.class)
                                .putExtra(IntentEnums.CAREER_ID.toString(), arrayList.get(position).getCareerId())
                                .putExtra(IntentEnums.TYPE.toString(), Constants.Cases.CAREER_DETAILS)
                );
        }
    }

    @Override
    public void onFilterSelected(SearchFilter searchFilter) {
        showBottomSheet();
        new Handler().postDelayed(() -> handleBottomSheetViewsOnFilterSelection(searchFilter), 100);

    }

    @Override
    public void onFilterDelete(FilterModel data) {
        switch (data.getSearchFilter()) {
            case DISCIPLINE_TYPE:
                disciplineType = "";
                spDisciplineType.setSelection(0);
//                searchData();
                break;
            case LEVEL:
                levelCode = "";
                spLevel.setSelection(0);
//                searchData();
                break;
            case CITY:
                cityCode = "0";
                etCity.setText("");
                //                searchData();
                break;
        }
        if (selectedFilters.size() > 1) {
            btnSearchBottomSheet.performClick();
        } else {
            selectedFilters.clear();
            selectedFilterRecycler.notifyDataSetChanged();
            presenter.getData(
                    AppEnums.ADMISSIONS,
                    String.valueOf(1),
                    "", "",
                    "", ""
            );
        }

    }
}