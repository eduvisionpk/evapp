package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.models.MeritCalculator.MeritCalculatorModel;

import java.util.ArrayList;

public class MeritCalculatorDetailRecycler extends RecyclerView.Adapter<MeritCalculatorDetailRecycler.MeritCalculatorDetailHolder> {

    private final ArrayList<MeritCalculatorModel.MeritCalculatorData> data;

    public MeritCalculatorDetailRecycler(ArrayList<MeritCalculatorModel.MeritCalculatorData> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MeritCalculatorDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MeritCalculatorDetailHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_institute_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MeritCalculatorDetailHolder holder, int position) {

        try {
            MeritCalculatorModel.MeritCalculatorData calculatorData = data.get(holder.getAdapterPosition());

            holder.tvSno.setText(String.valueOf(holder.getAdapterPosition() + 1));
            holder.tvUniName.setText(calculatorData.getInstitute());
            holder.tvProgram.setText(calculatorData.getDiscipline());

            if (holder.getAdapterPosition() % 2 == 0) {
                holder.viewParent.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
            } else {
                holder.viewParent.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.grey_10));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    static class MeritCalculatorDetailHolder extends RecyclerView.ViewHolder {

        private final TextView tvSno, tvUniName, tvProgram;
        private final View viewParent;

        public MeritCalculatorDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvSno = itemView.findViewById(R.id.tv_sno);
            tvUniName = itemView.findViewById(R.id.tv_uni_name);
            tvProgram = itemView.findViewById(R.id.tv_programs_Title);
            viewParent = itemView.findViewById(R.id.view_parent);
        }
    }
}
