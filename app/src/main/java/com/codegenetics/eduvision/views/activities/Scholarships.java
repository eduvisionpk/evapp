package com.codegenetics.eduvision.views.activities;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.helper.EndlessNestedScrollViewListener;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.interfaces.ISelectedFiltersClick;
import com.codegenetics.eduvision.models.FilterModel;
import com.codegenetics.eduvision.models.KeyValue;
import com.codegenetics.eduvision.models.Scholarships.ScholarshipsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.FilterRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.KeyValueSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.ScholarshipsAdapter;
import com.codegenetics.eduvision.views.adapters.SelectedFilterRecycler;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Scholarships extends AppCompatActivity implements IData, IFilterClick, ISelectedFiltersClick {

    private ServerPresenter presenter;
    private MKLoader loader;
    private ArrayList<Object> arrayList;
    private ScholarshipsAdapter adapter;

    private SelectedFilterRecycler selectedFilterRecycler;
    private final List<FilterModel> selectedFilters = new ArrayList<>();

    private Spinner spOfferingAuthority, spLevel, spFieldOfStudy, spCategory, spScholarshipType;
    private KeyValue offeringAuthority = new KeyValue("", "select"), level = new KeyValue("", "select"), fieldOfStudy = new KeyValue("", "select"), category = new KeyValue("", "select"), scholarshipType = new KeyValue("", "select");
    private View viewNoData;

    private final ArrayList<KeyValue> offeringAuthorityArrayList = new ArrayList<>();
    private final ArrayList<KeyValue> levelArrayList = new ArrayList<>();
    private final ArrayList<KeyValue> fieldOfStudyArrayList = new ArrayList<>();
    private final ArrayList<KeyValue> categoryArrayList = new ArrayList<>();
    private final ArrayList<KeyValue> scholarshipTypeArrayList = new ArrayList<>();

    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private Button btnSearchBottomSheet;
    private boolean isSpinnerFilled = false;

    private void init() {

        View v = findViewById(R.id.layout_bottom_sheet);
        mBehavior = BottomSheetBehavior.from(v);

        presenter = ServerPresenter.getInstance(this);
        loader = findViewById(R.id.loader);
        viewNoData = findViewById(R.id.no_data);

        arrayList = new ArrayList<>();
        adapter = new ScholarshipsAdapter(arrayList);

        RecyclerView rvData = findViewById(R.id.rv_data);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvData.setLayoutManager(layoutManager);
        rvData.setNestedScrollingEnabled(false);
        rvData.setAdapter(adapter);

        NestedScrollView nestedScrollView = findViewById(R.id.nested_scroll);
        nestedScrollView.setOnScrollChangeListener(new EndlessNestedScrollViewListener(layoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount) {
                        fetchScholarships(page);
                    }
                });

        RecyclerView rvFilters = findViewById(R.id.rv_filters);
        RecyclerView rvFiltersSelected = findViewById(R.id.rv_filters_selected);

        rvFilters.setHasFixedSize(true);
        rvFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvFilters.setAdapter(new FilterRecyclerAdapter(ConstantData.getInstance().getScholarshipFilters(), this));

        rvFiltersSelected.setHasFixedSize(true);
        rvFiltersSelected.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectedFilterRecycler = new SelectedFilterRecycler(selectedFilters, this);
        rvFiltersSelected.setAdapter(selectedFilterRecycler);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scholarships);
        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.scholarships);
        }

        fetchScholarships(1);

    }

    private void fetchScholarships(int page) {
        if (Internet.isConnected(this)) {

            presenter.getData(AppEnums.SCHOLARSHIPS, String.valueOf(page),
                    offeringAuthority.getKey(),
                    level.getKey(),
                    fieldOfStudy.getKey(),
                    category.getKey(),
                    scholarshipType.getKey());
        } else {
            Internet.showOfflineDialog(this);
        }

        prepareDataForSelectedFilterRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                ScholarshipsModel scholarshipsModel = new Gson().fromJson(response, ScholarshipsModel.class);

                if (scholarshipsModel.getData() != null) arrayList.addAll(scholarshipsModel.getData());

                viewNoData.setVisibility(arrayList.size() > 0 ? View.GONE : View.VISIBLE);
                adapter.notifyDataSetChanged();

            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {
                @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_scholarships, null);

                spOfferingAuthority = view.findViewById(R.id.sp_offering_authority);
                spLevel = view.findViewById(R.id.sp_level);
                spFieldOfStudy = view.findViewById(R.id.sp_field_of_study);
                spCategory = view.findViewById(R.id.sp_category);
                spScholarshipType = view.findViewById(R.id.sp_scholarship_type);

                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                btnSearchBottomSheet = view.findViewById(R.id.btn_search);

                fillSpinnerData();

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearchBottomSheet.setOnClickListener(view12 -> {
                    mBottomSheetDialog.dismiss();

                    arrayList.clear();
                    adapter.notifyDataSetChanged();

                    offeringAuthority = (KeyValue) spOfferingAuthority.getSelectedItem();
                    level = (KeyValue) spLevel.getSelectedItem();
                    fieldOfStudy = (KeyValue) spFieldOfStudy.getSelectedItem();
                    category = (KeyValue) spCategory.getSelectedItem();
                    scholarshipType = (KeyValue) spScholarshipType.getSelectedItem();

                    fetchScholarships(1);

                });


                mBottomSheetDialog = new BottomSheetDialog(this);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow())
                            .addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }

            }


            if (mBottomSheetDialog != null) mBottomSheetDialog.show();
//            mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);


        } catch (Exception e) {
//            Log.e(TAG, "showBottomSheet: ", e);
        }
    }

    private void fillSpinnerData() {
        offeringAuthorityArrayList.add(new KeyValue("", ConstantData.SELECT));
        offeringAuthorityArrayList.addAll(ConstantData.getInstance().getAuthoritiesData());
        levelArrayList.add(new KeyValue("", ConstantData.SELECT));
        levelArrayList.addAll(ConstantData.getInstance().getLevelData());
        fieldOfStudyArrayList.add(new KeyValue("", ConstantData.SELECT));
        fieldOfStudyArrayList.addAll(ConstantData.getInstance().getFieldData());
        categoryArrayList.add(new KeyValue("", ConstantData.SELECT));
        categoryArrayList.addAll(ConstantData.getInstance().getCatData());
        scholarshipTypeArrayList.add(new KeyValue("", ConstantData.SELECT));
        scholarshipTypeArrayList.addAll(ConstantData.getInstance().getTypeData());

        if (!isSpinnerFilled) {
            KeyValueSpinnerAdapter offeringAuthoritiesAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, offeringAuthorityArrayList);
            KeyValueSpinnerAdapter levelAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, levelArrayList);
            KeyValueSpinnerAdapter fieldOfStudyAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, fieldOfStudyArrayList);
            KeyValueSpinnerAdapter categoryAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, categoryArrayList);
            KeyValueSpinnerAdapter scholarshipAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, scholarshipTypeArrayList);

            spOfferingAuthority.setAdapter(offeringAuthoritiesAdapter);
            spLevel.setAdapter(levelAdapter);
            spFieldOfStudy.setAdapter(fieldOfStudyAdapter);
            spCategory.setAdapter(categoryAdapter);
            spScholarshipType.setAdapter(scholarshipAdapter);
            isSpinnerFilled = true;
        }

    }

    private void handleBottomSheetViewsOnFilterSelection(SearchFilter searchFilter) {
        switch (searchFilter) {
            case OFFERING_AUTHORITY:
                spOfferingAuthority.performClick();
                break;
            case LEVEL:
                spLevel.performClick();
                break;
            case FILED_OF_STUDY:
                spFieldOfStudy.performClick();
                break;
            case CATEGORY:
                spCategory.performClick();
                break;
            case SCHOLARSHIP_TYPE:
                spScholarshipType.performClick();
                break;
            default:
                break;
        }
    }

    private void prepareDataForSelectedFilterRecyclerView() {
        selectedFilters.clear();

        if (offeringAuthority != null) {
            if (!offeringAuthority.getValue().equalsIgnoreCase(ConstantData.SELECT))
                selectedFilters.add(new FilterModel(0, offeringAuthority.getValue(), SearchFilter.OFFERING_AUTHORITY));
        }
        if (level != null) {
            if (!level.getValue().equalsIgnoreCase(ConstantData.SELECT))
                selectedFilters.add(new FilterModel(0, level.getValue(), SearchFilter.LEVEL));
        }
        if (fieldOfStudy != null) {
            if (!fieldOfStudy.getValue().equalsIgnoreCase(ConstantData.SELECT))
                selectedFilters.add(new FilterModel(0, fieldOfStudy.getValue(), SearchFilter.FILED_OF_STUDY));
        }
        if (category != null) {
            if (!category.getValue().equalsIgnoreCase(ConstantData.SELECT))
                selectedFilters.add(new FilterModel(0, category.getValue(), SearchFilter.CATEGORY));
        }
        if (scholarshipType != null) {
            if (!scholarshipType.getValue().equalsIgnoreCase(ConstantData.SELECT))
                selectedFilters.add(new FilterModel(0, scholarshipType.getValue(), SearchFilter.SCHOLARSHIP_TYPE));
        }

        selectedFilterRecycler.notifyDataSetChanged();
    }

    @Override
    public void onFilterSelected(SearchFilter searchFilter) {
        showBottomSheet();
        new Handler().postDelayed(() -> handleBottomSheetViewsOnFilterSelection(searchFilter), 100);
    }

    @Override
    public void onFilterDelete(FilterModel data) {
        switch (data.getSearchFilter()) {
            case OFFERING_AUTHORITY:
                spOfferingAuthority.setSelection(0);
                break;
            case LEVEL:
                spLevel.setSelection(0);
                break;
            case FILED_OF_STUDY:
                spFieldOfStudy.setSelection(0);
                break;
            case CATEGORY:
                spCategory.setSelection(0);
                break;
            case SCHOLARSHIP_TYPE:
                spScholarshipType.setSelection(0);
                break;
            default:
                break;
        }
        btnSearchBottomSheet.performClick();
//        if (selectedFilters.size() > 1) {
//            btnSearchBottomSheet.performClick();
//        } else {
//            selectedFilters.clear();
//            selectedFilterRecycler.notifyDataSetChanged();
//            presenter.getData(
//                    AppEnums.ADMISSIONS,
//                    String.valueOf(1),
//                    "", "",
//                    "", ""
//            );
//        }

    }
}