package com.codegenetics.eduvision.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.helper.EndlessNestedScrollViewListener;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.News.NewsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.NewsAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class CounselingNewsFragment extends Fragment implements IData {

    private View view;
    private Context context;
    private ServerPresenter presenter;
    private MKLoader loader;
    private ArrayList<Object> arrayList;
    private NewsAdapter adapter;
    private int arraySize = 0;
    private AdLoader adLoader;

    public CounselingNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        context = null;
    }

    public static CounselingNewsFragment newInstance() {
        return new CounselingNewsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_news, container, false);

        init();
        fetchNews(1);

        return view;
    }

    private void init() {
        presenter = ServerPresenter.getInstance(this);

        arrayList = new ArrayList<>();
        adapter = new NewsAdapter(arrayList);

        loader = view.findViewById(R.id.loader);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        NestedScrollView nestedScrollView = view.findViewById(R.id.nested_scroll);
        nestedScrollView.setOnScrollChangeListener(new EndlessNestedScrollViewListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                fetchNews(page);
            }
        });

    }

    private void fetchNews(int page) {
        if (Internet.isConnected(context)) {
            presenter.getData(AppEnums.NEWS, String.valueOf(page), Constants.Cases.COUNSELING_NEWS);
        } else {
            Internet.showOfflineDialog((Activity) context);
        }
    }

    private void insertAdsInList() {
        if (Constants.mNativeAds.size() > 0) {

            int offset = 10;//(arrayList.size() / Constants.mNativeAds.size()) + 1;

//            Log.e("testing", "offset = " + offset);
//            Log.e("testing", "page = " + page);

            int index = arraySize + 2;

            arraySize = arrayList.size();

            for (UnifiedNativeAd ad : Constants.mNativeAds) {
                Log.e("testing", "index  = " + index);
                if (index < arrayList.size()) {
                    arrayList.add(index, ad);
                    Log.e("testing", "index added to arraylist = " + index);
                    index = index + offset;
                }
            }

        }

        adapter.notifyDataSetChanged();

        Log.e("testing",
                "Loaded ads = " + Constants.mNativeAds.size() + "\n" +
                        "arraylist size = " + (arrayList.size() - Constants.mNativeAds.size())
        );


    }

    private void loadNativeAds() {


        try {
            if (context != null) {

                Log.e("testing", "Loading ads = " + 5);

                Constants.mNativeAds.clear();

                AdLoader.Builder builder = new AdLoader.Builder(context, getResources().getString(R.string.native_ad_id));

                adLoader = builder.forUnifiedNativeAd(
                        unifiedNativeAd -> {

                            // A native ad loaded successfully, check if the ad loader has finished loading
                            // and if so, insert the ads into the list.
                            Log.e("testing", "The previous native ad loaded successfully."
                                    + " loading another.");
                            Constants.mNativeAds.add(unifiedNativeAd);
                            if (!adLoader.isLoading()) {
                                insertAdsInList();
                                Log.e("testing", "insertAdsInEpisodes called from success.");
                            }

                        }).withAdListener(
                        new AdListener() {
                            @Override
                            public void onAdFailedToLoad(int errorCode) {
                                // A native ad failed to load, check if the ad loader has finished loading
                                // and if so, insert the ads into the list.
                                Log.e("testing", "The previous native ad failed to load. Attempting to");
                                if (!adLoader.isLoading()) {
                                    insertAdsInList();
                                    Log.e("testing", "insertAdsInEpisodes called from failed.");
                                }
                            }
                        }).build();

                // Load the Native ads.
                adLoader.loadAds(new AdRequest.Builder().build(), 5);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                NewsModel newsModel = new Gson().fromJson(response, NewsModel.class);

                if (newsModel.isSuccess()) {

                    if (newsModel.getData() != null && newsModel.getData().size() > 0) {
                        arrayList.addAll(newsModel.getData());
                        adapter.notifyDataSetChanged();
                        loadNativeAds();
                    }
                    else {
                        displayError("No records found.");
                    }

                }

            }
        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

}