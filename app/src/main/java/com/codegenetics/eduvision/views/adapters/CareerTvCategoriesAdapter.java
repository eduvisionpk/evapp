package com.codegenetics.eduvision.views.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.models.Videos.VideoCategoriesModel;
import com.codegenetics.eduvision.views.activities.CareerTv;

import java.util.ArrayList;

public
class CareerTvCategoriesAdapter extends RecyclerView.Adapter<CareerTvCategoriesAdapter.ViewHolder> {

    private final ArrayList<VideoCategoriesModel.VideoCategoriesData> arrayList;

    public CareerTvCategoriesAdapter(ArrayList<VideoCategoriesModel.VideoCategoriesData> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_horizontal, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VideoCategoriesModel.VideoCategoriesData data = arrayList.get(holder.getAdapterPosition());

        holder.tvTitle.setText(data.getType());

        Glide.with(holder.itemView)
                .load(ConstantData.getInstance().getCareerTvCategoryImage(data.getType()))
                .into(holder.ivIcon);

        holder.itemView.setOnClickListener(view -> {
            view.getContext()
                    .startActivity(new Intent(view.getContext(), CareerTv.class)
                            .putExtra(IntentEnums.POSITION.name(), holder.getAdapterPosition())
                            .putExtra(IntentEnums.DATA.name(), arrayList)
                    );
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivIcon;
        private final AppCompatTextView tvTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }
}
