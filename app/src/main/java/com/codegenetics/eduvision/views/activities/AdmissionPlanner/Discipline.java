package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.Programs.ProgramsModel;
import com.codegenetics.eduvision.views.activities.HomeActivity;

import java.util.ArrayList;
import java.util.Locale;

public class Discipline extends AppCompatActivity implements IViewClickListener {

    private static final String TAG = "DisciplineActivity";
    private ArrayList<ProgramsModel> arrayList;
    private ArrayList<String> arrayListChecked;
    private boolean isCleared = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_programs);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.step_3_of_3);
        }

        initViews();
        updateViews();

    }

    private void initViews() {
        arrayList = new ArrayList<>(ConstantData.getInstance().getProgramsData());
        arrayListChecked = getIntent().getStringArrayListExtra(IntentEnums.DATA.name());

        RecyclerView rvPrograms = findViewById(R.id.recycler_view);
        rvPrograms.setHasFixedSize(true);
        rvPrograms.setLayoutManager(new GridLayoutManager(this, 2));
        rvPrograms.setAdapter(new DisciplineRecyclerAdapter(arrayList, this));

    }

    private void updateViews() {
        TextView tvDisciplineCount = findViewById(R.id.tv_selected_count);
        TextView tvClear = findViewById(R.id.tv_clear);
        LinearLayout llCount = findViewById(R.id.ll_selected_items);

        if (arrayListChecked != null && arrayListChecked.size() > 0) {
            llCount.setVisibility(View.VISIBLE);
            tvDisciplineCount.setText(String.format(Locale.getDefault(),
                    arrayListChecked.size() < 2 ? "%d discipline selected" : "%d disciplines selected",
                    arrayListChecked.size()));
            llCount.setOnClickListener(v -> {
                startActivity(new Intent(this, ConfirmRequirements.class)
                        .putExtra(IntentEnums.LEVEL_CODE.name(), getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name()))
                        .putExtra(IntentEnums.LEVEL_NAME.name(), getIntent().getStringExtra(IntentEnums.LEVEL_NAME.name()))
                        .putExtra(IntentEnums.DATA.name(), arrayListChecked)
                );
                finishAffinity();
            });
            tvClear.setOnClickListener(v -> {
                AlertDialog.Builder builder = new AlertDialog.Builder( this);
                builder.setMessage("Are you sure you want to clear selected disciplines?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", (dialog, which) -> {
                    arrayListChecked.clear();
                    PlannerInfoSession.getInstance().setDisciplineTypes(arrayListChecked);
                    isCleared = true;
                    llCount.setVisibility(View.GONE);
                });
                builder.setNegativeButton("No", null);
                builder.show();
            });
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClicked(int position) {
        startActivity(new Intent(this, DisciplineSub.class)
                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), arrayList.get(position).getTitle())
                .putExtra(IntentEnums.LEVEL_CODE.name(), getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name()))
                .putExtra(IntentEnums.LEVEL_NAME.name(), getIntent().getStringExtra(IntentEnums.LEVEL_NAME.name()))
                .putExtra(IntentEnums.DATA.name(), arrayListChecked)
        );
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: " + getIntent().getStringArrayListExtra(IntentEnums.DATA.name()).size());
        if (getIntent().getStringArrayListExtra(IntentEnums.DATA.name()).size() > 0 || isCleared) {
            startActivity(new Intent(this, HomeActivity.class));
            finishAffinity();
        } else {
            super.onBackPressed();
        }
    }
}