package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.models.Career.CareersModel;
import com.codegenetics.eduvision.models.EntryTest.EntryTestCategories;

import java.util.ArrayList;
import java.util.List;

public class CareerCounsellingTabRecycler extends RecyclerView.Adapter<CareerCounsellingTabRecycler.CareerCounsellingTabHolder> {

    private final List<CareersModel.CatNamesData> mData;
    private final ITabClickListener iTabClickListener;
    private int mSelectedItem = 0;

    public CareerCounsellingTabRecycler(List<CareersModel.CatNamesData> mData, ITabClickListener iTabClickListener) {
        this.mData = mData;
        this.iTabClickListener = iTabClickListener;
    }

    @NonNull
    @Override
    public CareerCounsellingTabHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CareerCounsellingTabHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_level_tab, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CareerCounsellingTabHolder holder, int position) {
        holder.tvTitle.setText(mData.get(position).getCat_name());

        if (position == mSelectedItem) {
            holder.tvTitle.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.blue_dark));
            holder.tvTitle.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.white));
        } else {
            holder.tvTitle.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.white));
            holder.tvTitle.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.black));
        }

        holder.itemView.setOnClickListener(v -> {
            mSelectedItem = holder.getAdapterPosition();
            iTabClickListener.onTabsSelected(mData.get(holder.getAdapterPosition()).getCat_name());
            notifyDataSetChanged();
        });

        iTabClickListener.onTabsSelected(mData.get(mSelectedItem).getCat_name());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class CareerCounsellingTabHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        public CareerCounsellingTabHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }
}
