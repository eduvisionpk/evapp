package com.codegenetics.eduvision.views.activities;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.adapters.FullScreenViewPagerAdapter;
import com.rd.PageIndicatorView;

public class FullScreenImages extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_images);

        Window w = getWindow(); // in Activity's onCreate() for instance
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        int pos = getIntent().getIntExtra(IntentEnums.POSITION.toString(), 0);

        ViewPager viewPager = findViewById(R.id.fullScreen_ViewPager);
        final PageIndicatorView pageIndicatorView = findViewById(R.id.fullScreen_anyViewIndicator);

        findViewById(R.id.fullScreen_btnClose).setOnClickListener(v -> finish());

//        Constants.getInstance().showInterstitial(this);

        String[] images = getIntent().getStringArrayExtra(IntentEnums.IMAGES.toString());

        if (images!=null) pageIndicatorView.setCount(images.length);

        pageIndicatorView.setSelection(pos);


        if (getIntent().getBooleanExtra(IntentEnums.HIDE.toString(), false)) {
            pageIndicatorView.setVisibility(View.GONE);
        }

        if (getIntent().hasExtra(IntentEnums.DEADLINE.name())) {
            LinearLayout linearLayout = findViewById(R.id.fullScreen_llBottom);
            TextView tvDeadline = findViewById(R.id.fullScreen_tvDeadline);
            tvDeadline.setText(getIntent().getStringExtra(IntentEnums.DEADLINE.name()));
            linearLayout.setVisibility(View.VISIBLE);
            pageIndicatorView.setVisibility(View.GONE);
        }

        viewPager.setAdapter(new FullScreenViewPagerAdapter(images, false));
        viewPager.setCurrentItem(pos);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelection(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }
}
