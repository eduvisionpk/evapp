package com.codegenetics.eduvision.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.databases.UniversitiesDAO;
import com.codegenetics.eduvision.databases.UniversityDBModel;
import com.codegenetics.eduvision.models.Programs.ProgramsOfferedModel;
import com.codegenetics.eduvision.views.adapters.UniversitiesRecycler;

import java.util.ArrayList;

public class FavouritesUniversitiesFragment extends Fragment {

    private View view, viewNoData;
    private ArrayList<ProgramsOfferedModel.ProgramsOfferedData> arrayList;
    private UniversitiesRecycler universitiesRecycler;
    private UniversitiesDAO universitiesDAO;

    public FavouritesUniversitiesFragment() {
        // Required empty public constructor
    }

    public static FavouritesUniversitiesFragment newInstance() {
        return new FavouritesUniversitiesFragment();
    }

    private void init() {
        universitiesDAO = DatabaseInjection.getUniversitiesDAO(getContext());
        arrayList = new ArrayList<>();
        universitiesRecycler = new UniversitiesRecycler(arrayList, true);

        viewNoData = view.findViewById(R.id.no_data);
        RecyclerView recyclerView = view.findViewById(R.id.rv_universities);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(universitiesRecycler);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favourite_universities, container, false);

        init();
        fetchAdmissions();

        return view;
    }

    private void fetchAdmissions() {
        universitiesDAO.getAll()
                .observe(getViewLifecycleOwner(), universityDBModels -> {

                    arrayList.clear();

                    for (UniversityDBModel model : universityDBModels) {
                        arrayList.add(UniversityDBModel.getModel(model));
                    }

                    universitiesRecycler.notifyDataSetChanged();

                    viewNoData.setVisibility(
                            arrayList.size() > 0 ? View.GONE : View.VISIBLE
                    );

                });



    }
}