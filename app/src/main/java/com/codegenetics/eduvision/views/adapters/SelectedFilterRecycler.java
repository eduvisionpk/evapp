package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.ISelectedFiltersClick;
import com.codegenetics.eduvision.models.FilterModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectedFilterRecycler extends RecyclerView.Adapter<SelectedFilterRecycler.SelectedFilterHolder> {

    private final List<FilterModel> mData;
    private final ISelectedFiltersClick filtersClick;

    public SelectedFilterRecycler(List<FilterModel> mData, ISelectedFiltersClick filtersClick) {
        this.mData = mData;
        this.filtersClick = filtersClick;
    }

    @NonNull
    @Override
    public SelectedFilterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SelectedFilterHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(
                                R.layout.row_filter_selected,
                                parent,
                                false));
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedFilterHolder holder, int position) {
        holder.tvTitle.setText(mData.get(position).getTitle());
        holder.ivDelete.setOnClickListener(v -> {
            if (filtersClick != null) filtersClick.onFilterDelete(mData.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class SelectedFilterHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;
        private final CircleImageView ivDelete;

        public SelectedFilterHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            ivDelete = itemView.findViewById(R.id.iv_delete);
        }
    }
}
