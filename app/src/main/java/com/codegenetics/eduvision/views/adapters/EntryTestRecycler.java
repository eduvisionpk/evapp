package com.codegenetics.eduvision.views.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.EntryTest.EntryTestModel.EntryTestData;
import com.codegenetics.eduvision.views.activities.EntryTestDetails;

import java.util.List;

public class EntryTestRecycler extends RecyclerView.Adapter<EntryTestRecycler.EntryTestHolder> {

    private final List<EntryTestData> mData ;

    public EntryTestRecycler(List<EntryTestData> mData) { this.mData = mData; }

    @NonNull
    @Override
    public EntryTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EntryTestHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_entry_test, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EntryTestHolder holder, int position) {
        try {

            EntryTestData data = mData.get(holder.getAdapterPosition());
            holder.tvUniName.setText(data.getName());

            if (data.getSchedule() == null || data.getSchedule().isEmpty()) {
                holder.tvScheduleLabel.setVisibility(View.GONE);
                holder.tvSchedule.setVisibility(View.GONE);
            } else {
                holder.tvSchedule.setVisibility(View.VISIBLE);
                holder.tvScheduleLabel.setVisibility(View.VISIBLE);
                holder.tvSchedule.setText(String.format(" %s", data.getSchedule()));
            }

            if (data.getDeadline() == null || data.getDeadline().isEmpty()) {
                holder.tvDeadLine.setVisibility(View.GONE);
                holder.tvDeadLineLabel.setVisibility(View.GONE);
            } else {
                holder.tvDeadLine.setVisibility(View.VISIBLE);
                holder.tvDeadLineLabel.setVisibility(View.VISIBLE);
                holder.tvDeadLine.setText(String.format(" %s", data.getDeadline()));
            }

            holder.tvViewDetails.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), EntryTestDetails.class);
                intent.putExtra(IntentEnums.ID.name(), data.getId());
                intent.putExtra(IntentEnums.NAME.name(), data.getName());
                v.getContext().startActivity(intent);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class EntryTestHolder extends RecyclerView.ViewHolder {

        private final TextView tvUniName, tvSchedule, tvScheduleLabel, tvDeadLine, tvDeadLineLabel, tvViewDetails;

        public EntryTestHolder(@NonNull View itemView) {
            super(itemView);
            tvUniName = itemView.findViewById(R.id.tv_uni_name);
            tvSchedule = itemView.findViewById(R.id.tv_schedule);
            tvScheduleLabel = itemView.findViewById(R.id.tv_schedule_label);
            tvDeadLine = itemView.findViewById(R.id.tv_deadline);
            tvDeadLineLabel = itemView.findViewById(R.id.tv_deadline_label);
            tvViewDetails = itemView.findViewById(R.id.tv_view_details);
        }
    }
}
