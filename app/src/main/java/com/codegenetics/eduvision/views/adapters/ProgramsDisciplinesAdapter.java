package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.interfaces.IProgramsClickListener;
import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel;

import java.util.ArrayList;

public class ProgramsDisciplinesAdapter extends RecyclerView.Adapter<ProgramsDisciplinesAdapter.ViewHolder> {

    private final ArrayList<DisciplinesModel.DisciplinesData> arrayList;
    private final IProgramsClickListener listener;

    public ProgramsDisciplinesAdapter(ArrayList<DisciplinesModel.DisciplinesData> arrayList, IProgramsClickListener listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.row_programs_discipline, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DisciplinesModel.DisciplinesData data = arrayList.get(holder.getAdapterPosition());

        holder.tvTitle.setText(data.getDisciplineName());

        if (data.getCareerId() == null || data.getCareerId().equalsIgnoreCase("0")) {
            holder.btnCareerScope.setBackground(
                    ResourcesCompat.getDrawable(holder.itemView.getResources(),
                            R.drawable.back_btn_light_yellow_color_cor5, holder.itemView.getContext().getTheme())
            );
        }

        holder.itemView.setOnClickListener(view -> holder.btnUniversities.performClick());
        holder.btnUniversities.setOnClickListener(view -> listener.onItemClicked(AppEnums.UNIVERSITY, holder.getAdapterPosition()));
        holder.btnAdmissions.setOnClickListener(view -> listener.onItemClicked(AppEnums.ADMISSIONS, holder.getAdapterPosition()));
        holder.btnCareerScope.setOnClickListener(view -> {
            if (data.getCareerId() != null && !data.getCareerId().equalsIgnoreCase("0")) {
                listener.onItemClicked(AppEnums.CAREER_SCOPE, holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;
        private final Button btnUniversities, btnAdmissions, btnCareerScope;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            btnUniversities = itemView.findViewById(R.id.btn_universities);
            btnAdmissions = itemView.findViewById(R.id.btn_admissions);
            btnCareerScope = itemView.findViewById(R.id.btn_career_scope);
        }

    }

}
