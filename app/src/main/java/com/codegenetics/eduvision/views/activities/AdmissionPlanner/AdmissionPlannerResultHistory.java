package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.AdmissionsDAO;
import com.codegenetics.eduvision.databases.AdmissionsDBModel;
import com.codegenetics.eduvision.databases.DatabaseInjection;

import java.util.ArrayList;

public class AdmissionPlannerResultHistory extends AppCompatActivity {

    private AdmissionsDAO admissionsDAO;
    private AdmissionsHistoryRecyclerAdapter adapter;
    private final ArrayList<AdmissionsDBModel> admissionsDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admission_planner_result);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.history);
        }

        initViews();
        fetchAdmissions();

    }

    private void initViews() {

        admissionsDAO = DatabaseInjection.getAdmissionsDAO(this);

        adapter = new AdmissionsHistoryRecyclerAdapter(admissionsDataArrayList);

        RecyclerView rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        findViewById(R.id.rv_filters).setVisibility(View.GONE);
        findViewById(R.id.rv_filters_selected).setVisibility(View.GONE);

    }

    private void fetchAdmissions() {

        admissionsDAO.getAll().observeForever(admissionsDBModels -> {
            try {
                admissionsDataArrayList.clear();
                admissionsDataArrayList.addAll(admissionsDBModels);
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


}