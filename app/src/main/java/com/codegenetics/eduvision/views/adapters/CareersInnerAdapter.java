package com.codegenetics.eduvision.views.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.Career.CareersModel;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.activities.Details;

import java.util.ArrayList;

public
class CareersInnerAdapter extends RecyclerView.Adapter<CareersInnerAdapter.ViewHolder> {

    private final ArrayList<CareersModel.CareersData> arrayList;

    public CareersInnerAdapter(ArrayList<CareersModel.CareersData> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.row_programs_inner, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CareersModel.CareersData model = arrayList.get(holder.getAdapterPosition());
        holder.tvType.setText(model.getTitle());
        Glide.with(holder.ivTypeImage)
                .load(model.getImage())
                .placeholder(R.drawable.ic_dummy)
                .error(R.drawable.ic_dummy1)
                .into(holder.ivTypeImage);
        holder.itemView.setOnClickListener(view ->
                view.getContext().startActivity(
                        new Intent(view.getContext(), Details.class)
                                .putExtra(IntentEnums.CAREER_ID.toString(), arrayList.get(position).getId())
                                .putExtra(IntentEnums.TYPE.toString(), Constants.Cases.CAREER_DETAILS)
                )
        );
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivTypeImage;
        private final TextView tvType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivTypeImage = itemView.findViewById(R.id.iv_image);
            tvType = itemView.findViewById(R.id.tv_name);
        }
    }

}
