package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IMoreMenuClick;
import com.codegenetics.eduvision.models.MoreModel;

import java.util.List;

public class MoreRecycler extends RecyclerView.Adapter<MoreRecycler.MoreHolder> {

    private final List<MoreModel> mData;
    private final IMoreMenuClick iMoreMenuClick;

    public MoreRecycler(List<MoreModel> mData, IMoreMenuClick iMoreMenuClick) {
        this.mData = mData;
        this.iMoreMenuClick = iMoreMenuClick;
    }

    @NonNull
    @Override
    public MoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MoreHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_more, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MoreHolder holder, int position) {
        holder.ivIcon.setImageResource(mData.get(position).getIcon());
        holder.tvTitle.setText(mData.get(position).getTitle());
        holder.itemView.setOnClickListener(v -> iMoreMenuClick.onMoreMenuSelected(mData.get(position)));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class MoreHolder extends RecyclerView.ViewHolder {

        private final ImageView ivIcon;
        private final AppCompatTextView tvTitle;

        public MoreHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }
}
