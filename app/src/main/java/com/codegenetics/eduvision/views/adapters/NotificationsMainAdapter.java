package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.models.Notifications.NotificationsMainModel;

import java.util.ArrayList;

public
class NotificationsMainAdapter extends RecyclerView.Adapter<NotificationsMainAdapter.ViewHolder> {

    private Context context;
    private final ArrayList<NotificationsMainModel> arrayList;
    private final RecyclerView.RecycledViewPool recycledViewPool;

    public NotificationsMainAdapter(ArrayList<NotificationsMainModel> arrayList) {
        this.arrayList = arrayList;
        recycledViewPool = new RecyclerView.RecycledViewPool();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_notifications_main, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.recyclerView.setRecycledViewPool(recycledViewPool);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationsMainModel data = arrayList.get(holder.getAdapterPosition());

        holder.recyclerView.setAdapter(new NotificationsInnerAdapter(data.getArrayList()));

        holder.tvTitle.setText(data.getTitle());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;
        private final RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            recyclerView = itemView.findViewById(R.id.recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
    }
}
