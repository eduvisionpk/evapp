package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.helper.LayoutType;
import com.codegenetics.eduvision.interfaces.IHomeMenuClick;
import com.codegenetics.eduvision.models.HomeModel;

import java.util.List;

public class HomeRecycler extends RecyclerView.Adapter<HomeRecycler.HomeHolder> {

    private final List<HomeModel> mData;
    private final IHomeMenuClick menuClick;
    private final int layoutType;

    public HomeRecycler(List<HomeModel> mData, IHomeMenuClick iHomeMenuClick, int layoutType) {
        this.mData = mData;
        this.menuClick = iHomeMenuClick;
        this.layoutType = layoutType;
    }

    @NonNull
    @Override
    public HomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutType == LayoutType.GRID)
            return new HomeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home, parent, false));
        else if (layoutType == LayoutType.HORIZONTAL)
            return new HomeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_horizontal, parent, false));
        else
            return new HomeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_horizontal, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull HomeHolder holder, int position) {
        holder.ivIcon.setImageResource(mData.get(position).getIcon());
        holder.tvTitle.setText(mData.get(position).getTitle());
        holder.itemView.setOnClickListener(v -> menuClick.onHomeMenuSelected(mData.get(position)));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class HomeHolder extends RecyclerView.ViewHolder {

        private final ImageView ivIcon;
        private final AppCompatTextView tvTitle;

        public HomeHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }
}
