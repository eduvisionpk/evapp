package com.codegenetics.eduvision.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IRecyclerClick;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.models.Career.CareersModel;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.CareerCounsellingDataRecycler;
import com.codegenetics.eduvision.views.adapters.CareerCounsellingTabRecycler;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class CareerCounseling extends AppCompatActivity implements IData, ITabClickListener, IRecyclerClick {

    private MKLoader loader;
    private TextView tvBookCounselingSession;
    private final ArrayList<CareersModel.CatNamesData> tabsList = new ArrayList<>();
    private final ArrayList<CareersModel.CareersData> dataList = new ArrayList<>();
    private final ArrayList<CareersModel.CareersData> filteredDataList = new ArrayList<>();
    private CareerCounsellingTabRecycler tabRecycler;
    private CareerCounsellingDataRecycler dataRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_counseling);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.career_counseling);
        }

        initViews();

        tvBookCounselingSession.setOnClickListener(v -> Constants.getInstance().openLink(this, Constants.BOOK_A_SESSION_LINK));

    }

    private void initViews() {
        loader = findViewById(R.id.loader);
        tvBookCounselingSession = findViewById(R.id.tv_book_counseling_session);

        RecyclerView rvTabs = findViewById(R.id.rv_tabs);
        rvTabs.setHasFixedSize(true);
        rvTabs.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        tabRecycler = new CareerCounsellingTabRecycler(tabsList, this);
        rvTabs.setAdapter(tabRecycler);

        RecyclerView rvData = findViewById(R.id.rv_data);
        rvData.setHasFixedSize(true);
        rvData.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        dataRecycler = new CareerCounsellingDataRecycler(filteredDataList, this);
        rvData.setAdapter(dataRecycler);

        ServerPresenter presenter = ServerPresenter.getInstance(this);
        if (Internet.isConnected(this)) presenter.getData(AppEnums.CAREER_COUNSELING);
        else Internet.showOfflineDialog(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        if (response != null) {
            CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);

            if (Constants.Cases.CAREER_COUNSELING.equals(caseModel.getCase())) {

                CareersModel careersModel = new Gson().fromJson(response, CareersModel.class);

                if (careersModel.getData() != null && careersModel.getData().size() > 0) {
                    dataList.addAll(careersModel.getData());
                }

                tabsList.clear();
                if (careersModel.getCat_names() != null && careersModel.getCat_names().size() > 0) {
                    tabsList.addAll(careersModel.getCat_names());
                }

                tabRecycler.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTabsSelected(String s) {
        filteredDataList.clear();
        filteredDataList.addAll(getFilteredData(s));
        dataRecycler.notifyDataSetChanged();
    }

    private ArrayList<CareersModel.CareersData> getFilteredData(String s) {
        ArrayList<CareersModel.CareersData> mData = new ArrayList<>();

        for (CareersModel.CareersData data : dataList) {
            if (data.getCat_name().equals(s)) mData.add(data);
        }

        return mData;
    }

    @Override
    public void onRecyclerItemClicked(CareersModel.CareersData data) {
        startActivity(
                new Intent(this, Details.class)
                        .putExtra(IntentEnums.DATA.toString(), data)
                        .putExtra(IntentEnums.TYPE.toString(), Constants.Cases.CAREER_COUNSELING)
        );
    }

}