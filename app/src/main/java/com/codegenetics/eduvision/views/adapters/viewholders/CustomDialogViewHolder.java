package com.codegenetics.eduvision.views.adapters.viewholders;

import android.view.View;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.models.MessagesModels.Dialog;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;


public class CustomDialogViewHolder extends DialogsListAdapter.DialogViewHolder<Dialog> {

    private final View onlineIndicator;

    public CustomDialogViewHolder(View itemView) {
        super(itemView);
        onlineIndicator = itemView.findViewById(R.id.onlineIndicator);
    }

    @Override
    public void onBind(Dialog dialog) {
        super.onBind(dialog);

        if (dialog.getUsers().size() > 1) {
            onlineIndicator.setVisibility(View.GONE);
        } else {
            boolean isOnline = dialog.getUsers().get(0).isOnline();
            onlineIndicator.setVisibility(View.VISIBLE);
            if (isOnline) {
                onlineIndicator.setBackgroundResource(R.drawable.shape_bubble_online);
            } else {
                onlineIndicator.setBackgroundResource(R.drawable.shape_bubble_offline);
            }
        }
    }
}
