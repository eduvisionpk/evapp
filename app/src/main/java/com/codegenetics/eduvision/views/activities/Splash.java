package com.codegenetics.eduvision.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.codegenetics.eduvision.BuildConfig;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.models.DisciplineTypes.DisciplineTypesModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.google.gson.Gson;

import java.util.Locale;

import io.paperdb.Paper;

public class Splash extends AppCompatActivity implements IData {

    private ServerPresenter presenter;
    private boolean isCitiesFetched, isLevelsFetched, isDisciplineTypesFetched;
    private final int TIMEOUT = 60 * 1000;
    private final Handler handler = new Handler();
    private final Runnable runnable = () -> Internet.showOfflineDialog(this);

    private void init() {
        presenter = ServerPresenter.getInstance(this);
        isCitiesFetched = isLevelsFetched = isDisciplineTypesFetched = false;
        ((TextView) findViewById(R.id.tv_version)).setText(
                String.format(Locale.getDefault(), "version: %s", BuildConfig.VERSION_NAME)
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        Paper.init(this);

        Constants.getInstance().loadInterstitial(this);

        init();

        fetchData();

        handler.postDelayed(runnable, TIMEOUT);

    }

    private void fetchData() {
        fetchCities();
        fetchLevels();
        fetchDisciplineTypes();
    }

    private void fetchCities() {
        if (Internet.isConnected(this)) presenter.getData(AppEnums.CITIES);
        else Internet.showOfflineDialog(this);
    }


    private void fetchLevels() {
        if (Internet.isConnected(this)) presenter.getData(AppEnums.LEVELS);
        else Internet.showOfflineDialog(this);
    }


    private void fetchDisciplineTypes() {
        if (Internet.isConnected(this)) presenter.getData(AppEnums.DISCIPLINE_TYPES);
        else Internet.showOfflineDialog(this);
    }

    private void moveToNextScreen() {
        if (isCitiesFetched && isLevelsFetched && isDisciplineTypesFetched) {

            if (Session.getInstance().isUserLogin())
                startActivity(new Intent(this, HomeActivity.class));
            else
                startActivity(new Intent(this, Login.class));

            finish();
        }
    }

    @Override
    public void showProgress(AppEnums enums) {

    }

    @Override
    public void hideProgress(AppEnums enums) {

    }

    @Override
    public void displayData(String response) {

        try {

            if (!response.isEmpty()) {

                CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);

                switch (caseModel.getCase()) {

                    case Constants.Cases.CITIES: {
                        CitiesModel citiesModel = new Gson().fromJson(response, CitiesModel.class);
                        if (citiesModel.isSuccess()) {
                            if (citiesModel.getData() != null) {
                                Constants.citiesList.addAll(citiesModel.getData());
                                isCitiesFetched = true;
                            } else {
                                fetchCities();
                            }
                        } else {
                            fetchCities();
                        }
                    }
                    break;

                    case Constants.Cases.LEVELS: {
                        LevelsModel levelsModel = new Gson().fromJson(response, LevelsModel.class);
                        if (levelsModel.isSuccess()) {
                            if (levelsModel.getData() != null) {
                                Constants.levelsList.add(new LevelsModel.LevelsData(ConstantData.SELECT, "-1"));
                                Constants.levelsList.addAll(levelsModel.getData());
                                isLevelsFetched = true;
                            } else {
                                fetchLevels();
                            }
                        } else {
                            fetchLevels();
                        }
                    }
                    break;
                    case Constants.Cases.DISCIPLINE_TYPE: {
                        DisciplineTypesModel disciplineTypesModel = new Gson().fromJson(response, DisciplineTypesModel.class);
                        if (disciplineTypesModel.isSuccess()) {
                            if (disciplineTypesModel.getData() != null) {
                                Constants.disciplineTypeList.add(new DisciplineTypesModel.DisciplineTypesData(ConstantData.SELECT));
                                Constants.disciplineTypeList.addAll(disciplineTypesModel.getData());
                                isDisciplineTypesFetched = true;
                            } else {
                                fetchDisciplineTypes();
                            }
                        } else {
                            fetchDisciplineTypes();
                        }
                    }
                }

                moveToNextScreen();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, TIMEOUT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

}