package com.codegenetics.eduvision.views.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.databases.DatabaseInjection;
import com.codegenetics.eduvision.databases.NewsDAO;
import com.codegenetics.eduvision.databases.NewsDBModel;
import com.codegenetics.eduvision.databases.ScholarshipDAO;
import com.codegenetics.eduvision.databases.ScholarshipDBModel;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.Career.CareerCounselingDetailsModel;
import com.codegenetics.eduvision.models.Career.CareerDetailsModel;
import com.codegenetics.eduvision.models.Career.CareersModel;
import com.codegenetics.eduvision.models.News.NewsDetailsModel;
import com.codegenetics.eduvision.models.News.NewsModel;
import com.codegenetics.eduvision.models.Scholarships.ScholarshipDetailsModel;
import com.codegenetics.eduvision.models.Scholarships.ScholarshipsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.Locale;

import im.delight.android.webview.AdvancedWebView;

public class Details extends AppCompatActivity implements IData, AdvancedWebView.Listener {

    private ServerPresenter presenter;

    private ImageView ivMain, ivAd, ivShare;
    private TextView tvTitle, tvDate, tvBookCounselingSession;
    private CheckBox cbFavourite;
    private MKLoader loader;
    private AdvancedWebView webView;

    private NewsDAO newsDAO;
    private ScholarshipDAO scholarshipDAO;

    private String id, name, title, image, date, link;
    private AppEnums enums;

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        try {

            presenter = ServerPresenter.getInstance(this);
            ivMain = findViewById(R.id.image);
            ivAd = findViewById(R.id.iv_ad);
            ivShare = findViewById(R.id.iv_share);
            tvBookCounselingSession = findViewById(R.id.tv_book_counseling_session);
            tvTitle = findViewById(R.id.tv_title);
            tvDate = findViewById(R.id.tv_date);
            cbFavourite = findViewById(R.id.check_box_favourite);
            webView = findViewById(R.id.web_view);
            loader = findViewById(R.id.loader);

            webView.setListener(this, this);
            webView.setGeolocationEnabled(true);
            webView.setMixedContentAllowed(true);
            webView.setCookiesEnabled(true);
            webView.setThirdPartyCookiesEnabled(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDefaultTextEncodingName("utf-8");
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
                    startActivity(intent);
                    return true;
                }
            });

            String type = getIntent().getStringExtra(IntentEnums.TYPE.name());

            if (type != null && !type.isEmpty()) {

                switch (type) {
                    case Constants.Cases.NEWS_DETAILS:
                        NewsModel.NewsData data = getIntent().getParcelableExtra(IntentEnums.NEWS_DATA.name());
                        enums = AppEnums.NEWS_DETAILS;
                        id = data.getId();
                        name = data.getTitleText();
                        image = data.getImage();
                        date = data.getTime();
                        title = data.getCatid().equalsIgnoreCase(Constants.Cases.JOBS) ? getString(R.string.Jobs) : getString(R.string.news);
                        newsDAO = DatabaseInjection.getNewsDAO(this);
                        cbFavourite.setChecked(newsDAO.getItem(id) > 0);
                        break;
                    case Constants.Cases.SCHOLARSHIP_DETAILS:
                        ScholarshipsModel.ScholarshipsData data1 = getIntent().getParcelableExtra(IntentEnums.SCHOLARSHIP_DATA.name());
                        id = data1.getScholarshipId();
                        name = data1.getScholarshipName();
                        image = data1.getLogo();
                        date = data1.getUpdated();
                        title = getString(R.string.scholarships);
                        enums = AppEnums.SCHOLARSHIP_DETAILS;
                        scholarshipDAO = DatabaseInjection.getScholarshipsDAO(this);
                        cbFavourite.setChecked(scholarshipDAO.getItem(id) > 0);
                        break;
                    case Constants.Cases.CAREER_DETAILS:
                        id = getIntent().getStringExtra(IntentEnums.CAREER_ID.name());
                        title = getString(R.string.career_scope);
                        enums = AppEnums.CAREER_DETAILS;
                        cbFavourite.setVisibility(View.INVISIBLE);
                        tvDate.setVisibility(View.INVISIBLE);
                        break;
                    case Constants.Cases.CAREER_COUNSELING:
                        CareersModel.CareersData careersData = getIntent().getParcelableExtra(IntentEnums.DATA.name());
                        id = careersData.getId();
                        name = careersData.getTitle();
                        image = careersData.getThumbnail();
                        title = getString(R.string.career_counseling_details);
                        enums = AppEnums.CAREER_COUNSELING_DETAILS;
                        cbFavourite.setVisibility(View.INVISIBLE);
                        ivShare.setVisibility(View.INVISIBLE);
                        tvBookCounselingSession.setVisibility(View.VISIBLE);
                }

                if (!type.equalsIgnoreCase(Constants.Cases.CAREER_DETAILS)) {
                    Glide.with(ivMain)
                            .load(image)
                            .placeholder(R.color.gray_light1)
                            .error(R.drawable.ic_no_image)
                            .into(ivMain);

                    if (date != null && !date.isEmpty()) {
                        tvDate.setText(String.format(Locale.getDefault(), "Date: %s", date));
                    }

                }

                tvTitle.setText(name);

            } else {
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        fetchDetails();

        tvBookCounselingSession.setOnClickListener(v -> Constants.getInstance().openLink(this, Constants.BOOK_A_SESSION_LINK));

        cbFavourite.setOnCheckedChangeListener((compoundButton, isChecked) -> {

            if (!isChecked) {
                if (newsDAO != null) newsDAO.deleteItem(id);
                else scholarshipDAO.deleteItem(id);
            } else {
                if (newsDAO != null) {
                    newsDAO.addNewItem(NewsDBModel.getDBModel(
                            new NewsModel.NewsData(
                                    id,
                                    name,
                                    image,
                                    date
                            )));
                } else {
                    scholarshipDAO.addNewItem(ScholarshipDBModel.getDBModel(
                            new ScholarshipsModel.ScholarshipsData(
                                    id,
                                    name,
                                    image,
                                    date
                            )
                    ));
                }
            }
        });

    }

    private void fetchDetails() {
        if (Internet.isConnected(this)) {
            presenter.getData(enums, id);
        } else {
            Internet.showOfflineDialog(this);
        }
    }

    private void openImage(String image) {
        startActivity(
                new Intent(this, FullScreenImages.class)
                        .putExtra(IntentEnums.IMAGES.toString(), image.split(";"))
                        .putExtra(IntentEnums.POSITION.toString(), 0)
                        .putExtra(IntentEnums.HIDE.toString(), true)
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        try {
            if (!response.isEmpty()) {

                boolean success = false;
                String description = "", ad = null;

                if (enums.equals(AppEnums.NEWS_DETAILS)) {
                    NewsDetailsModel detailsModel = new Gson().fromJson(response, NewsDetailsModel.class);

                    if (success = detailsModel.isSuccess()) {

                        if (success = detailsModel.getData() != null && detailsModel.getData().size() > 0) {

                            NewsDetailsModel.NewsDetailsData data = detailsModel.getData().get(0);

                            ad = data.getAd();
                            image = data.getImage();
                            name = data.getTitletext();
                            link = data.getLink();
                            description = data.getMaintext();

                        }
                    }
                }
                else if (enums.equals(AppEnums.SCHOLARSHIP_DETAILS)) {
                    ScholarshipDetailsModel detailsModel = new Gson().fromJson(response, ScholarshipDetailsModel.class);

                    if (success = detailsModel.isSuccess()) {

                        if (success = detailsModel.getData() != null && detailsModel.getData().size() > 0) {

                            ScholarshipDetailsModel.ScholarshipDetailsData data = detailsModel.getData().get(0);

                            ad = data.getAd();
                            link = data.getLink();
                            image = data.getLogo();
                            name = data.getScholarshipName();
                            description = data.getDetail();

                        }
                    }
                }
                else if (enums.equals(AppEnums.CAREER_DETAILS)) {
                    CareerDetailsModel detailsModel = new Gson().fromJson(response, CareerDetailsModel.class);

                    if (success = detailsModel.isSuccess()) {

                        if (success = detailsModel.getData() != null && detailsModel.getData().size() > 0) {

                            CareerDetailsModel.CareerDetailsData data = detailsModel.getData().get(0);

                            link = data.getLink();
                            image = data.getImage();
                            name = data.getTitle();
                            description = data.getStory();

                        }

                    }
                }
                else if (enums.equals(AppEnums.CAREER_COUNSELING_DETAILS)) {
                    CareerCounselingDetailsModel detailsModel = new Gson().fromJson(response, CareerCounselingDetailsModel.class);

                    if (success = detailsModel.isSuccess()) {

                        if (success = detailsModel.getData() != null && detailsModel.getData().size() > 0) {

                            CareerCounselingDetailsModel.CareerCounselingDetailsData data =
                                    detailsModel.getData().get(0);

                            image = data.getImage();
                            name = data.getTitle();
                            date = data.getUpdated();
                            description = data.getContent();

                        }
                    }
                }

                if (success) {

                    tvTitle.setText(name);
                    tvDate.setText(String.format(Locale.getDefault(), "Date: %s", date));

                    webView.loadDataWithBaseURL("",
                            description,
                            "text/html",
                            null ,"");

                    Glide.with(ivMain)
                            .load(image)
                            .placeholder(R.color.gray_light1)
                            .error(R.drawable.ic_no_image)
                            .into(ivMain);

                    ivMain.setOnClickListener(view -> openImage(image));

                    ivShare.setOnClickListener(view ->
                            Constants.getInstance().share(this,
                                    String.format(Locale.getDefault(), "%s. For details please visit %s",
                                            name, link
                                    ))
                    );

                    if (ad != null && !ad.isEmpty()) {

                        String finalAd = ad;

                        ivAd.setVisibility(View.VISIBLE);

                        Glide.with(ivAd).load(ad).into(ivAd);

                        ivAd.setOnClickListener(view -> openImage(finalAd));

                    }
                }
                else {
                    displayError("Failed to fetch details.");
                }
            }
        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        webView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (webView != null) webView.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
//        webView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
//        if (!webView.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
//        mWebView.setVisibility(View.INVISIBLE);
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
//        mWebView.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
//        Toast.makeText(MainActivity.this, "onPageError(errorCode = "+errorCode+",  description = "+description+",  failingUrl = "+failingUrl+")", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType,
                                    long contentLength, String contentDisposition, String userAgent) {
//        Toast.makeText(MainActivity.this, "onDownloadRequested(url = "+url+",  suggestedFilename = "+suggestedFilename+",  mimeType = "+mimeType+",  contentLength = "+contentLength+",  contentDisposition = "+contentDisposition+",  userAgent = "+userAgent+")", Toast.LENGTH_LONG).show();

		/*if (AdvancedWebView.handleDownload(this, url, suggestedFilename)) {
			// download successfully handled
		}
		else {
			// download couldn't be handled because user has disabled download manager app on the device
		}*/
    }

    @Override
    public void onExternalPageRequest(String url) {
//        Toast.makeText(MainActivity.this, "onExternalPageRequest(url = "+url+")", Toast.LENGTH_SHORT).show();
    }
}