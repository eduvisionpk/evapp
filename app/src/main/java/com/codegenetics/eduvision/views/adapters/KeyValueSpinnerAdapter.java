package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.codegenetics.eduvision.models.KeyValue;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class KeyValueSpinnerAdapter extends ArrayAdapter<KeyValue> {
    private final ArrayList<KeyValue> mData;

    public KeyValueSpinnerAdapter(Context context, int textViewResourceId, ArrayList<KeyValue> mData) {
        super(context, textViewResourceId, mData);
        this.mData = mData;
    }

    @Override
    public int getCount(){
        return mData.size();
    }

    @Override
    public KeyValue getItem(int position){
        return mData.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(mData.get(position).getValue());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, @NotNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(mData.get(position).getValue());
        return label;
    }

}
