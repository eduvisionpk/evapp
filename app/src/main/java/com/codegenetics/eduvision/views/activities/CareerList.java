package com.codegenetics.eduvision.views.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.Career.CareersModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.CareersInnerAdapter;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class CareerList extends AppCompatActivity implements IData {

    private ServerPresenter presenter;
    private MKLoader loader;
    private CareersInnerAdapter adapter;
    private ArrayList<CareersModel.CareersData> arrayList;

    private void init() {

        presenter = ServerPresenter.getInstance(this);

        arrayList = new ArrayList<>();
        adapter = new CareersInnerAdapter(arrayList);

        loader = findViewById(R.id.loader);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_careers);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_careers);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getIntent().getStringExtra(IntentEnums.CATEGORY_NAME.name()));
        }

        fetchCareers();
    }

    private void fetchCareers() {
        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.CAREER_LIST, getIntent().getStringExtra(IntentEnums.CATEGORY_ID.name()));
        } else {
            Internet.showOfflineDialog(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {

            if (!response.isEmpty()) {

                CareersModel model = new Gson().fromJson(response, CareersModel.class);

                if (model.isSuccess() && model.getData() != null && model.getData().size() > 0) {
                    arrayList.addAll(model.getData());
                    adapter.notifyDataSetChanged();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

}