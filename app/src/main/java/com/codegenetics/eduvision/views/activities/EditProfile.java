package com.codegenetics.eduvision.views.activities;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.helper.FirebaseConstants;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.utils.Constants;
import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.views.adapters.CitiesSpinnerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.tuyenmonkey.mkloader.MKLoader;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;
import com.yanzhenjie.album.AlbumFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@SuppressWarnings("ALL")
public class EditProfile extends AppCompatActivity {

    private Button btnUpdateProfile;
    private MKLoader loader, loaderImage;
    private RelativeLayout rlProfileImage;
    private CircleImageView ivUser;
    private ImageView ivUpload;
    private EditText etName, etEmail, etPhone;
    private AutoCompleteTextView etCity;
    private ArrayList<CitiesModel.CitiesData> citiesDataArrayList = new ArrayList<>();
    private Map<String, Object> map = new HashMap<>();
    private boolean haveUploadedImage = false;
    private CitiesModel.CitiesData citiesData = null;

    private void init() {

        citiesDataArrayList.addAll(Constants.citiesList);
        ivUser = findViewById(R.id.iv_image);
        ivUpload = findViewById(R.id.iv_upload);
        etName = findViewById(R.id.et_name);
        etEmail = findViewById(R.id.et_email);
        etPhone = findViewById(R.id.et_phone);
        btnUpdateProfile = findViewById(R.id.btn_update_profile);
        loader = findViewById(R.id.loader);
        loaderImage = findViewById(R.id.loader_image);
        rlProfileImage = findViewById(R.id.rl_image);
        etCity = findViewById(R.id.et_city);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.edit_profile));
        }

        fillData();

    }

    private void setCities(ArrayList<CitiesModel.CitiesData> arrayList) {
        CitiesSpinnerAdapter adapter = new CitiesSpinnerAdapter(this, R.layout.layout_spinner, arrayList);
        etCity.setAdapter(adapter);
        etCity.setOnItemClickListener((adapterView, view, position, l) -> citiesData = adapter.getItem(position));
        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                citiesData = null;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void fillData() {

        Session session = Session.getInstance();

        etName.setText(session.getName());
        etEmail.setText(session.getEmail());
        etPhone.setText(session.getPhone());

        if (!session.getImage().isEmpty()) {

            Glide.with(ivUser)
                    .load(session.getImage())
                    .placeholder(R.drawable.ic_profile_avatar)
                    .error(R.drawable.ic_profile_avatar)
                    .into(ivUser);

            ivUpload.setVisibility(View.GONE);

        }

//        rlProfileImage.setOnClickListener(view -> openGallery());

        setCities(citiesDataArrayList);

        for (int i = 0; i < citiesDataArrayList.size(); i++) {
            if (session.getCity().equalsIgnoreCase(citiesDataArrayList.get(i).getCityName())) {
                etCity.setText(session.getCity());
                citiesData = citiesDataArrayList.get(i);
                break;
            }
        }
    }

    private void openGallery() {

        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new Constants.MediaLoader())
                .build());

        Album.image(this)
                .singleChoice()
                .camera(false)
                .columnCount(3)
                .onResult(result -> {

                    showProgress(true);
                    ivUpload.setVisibility(View.GONE);

                    for (AlbumFile albumFile : result) {

                        new Handler().post(() ->

                                new Compressor(this)
                                        .compressToFileAsFlowable(new File(albumFile.getPath()), "img_" + System.currentTimeMillis() + ".jpg")
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                                file -> {

                                                    if (file != null) {
                                                        Constants.log(this, "image compressed path " + file.getPath());
                                                        uploadImage(file.getPath());
                                                    }
                                                    else {
                                                        Constants.log(this, "image compressed failed " + albumFile.getPath());
                                                        uploadImage(albumFile.getPath());
                                                    }

                                                },
                                                throwable -> {
                                                    hideProgress(true);
                                                    Toast.makeText(this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();;
                                                }

                                        ));

                    }

                })
                .start();

    }

    private void uploadImage(String path) {

        Glide.with(ivUpload).load(path).into(ivUser);

        path = "file://" + path;

        FirebaseStorage.getInstance()
                .getReference(FirebaseConstants.ParentNodes.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .putFile(Uri.parse(path))
                .addOnSuccessListener(taskSnapshot -> {
                    hideProgress(true);
                    haveUploadedImage = true;
                })
                .addOnFailureListener(e -> {
                    hideProgress(true);
                    Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                });

    }

    private void saveToDatabase() {

        if (!haveUploadedImage) {
            uploadToDatabase(map);
            return;
        }

        FirebaseStorage.getInstance()
                .getReference(FirebaseConstants.ParentNodes.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    map.put(FirebaseConstants.ChildNodes.IMAGE, uri.toString());
                    uploadToDatabase(map);
                })
                .addOnFailureListener(e -> {
            hideProgress(false);
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    private void uploadToDatabase(Map<String, Object> map) {

        try {

            String cityName = "", cityCode = "";

            if (citiesData != null) {
                cityName = citiesData.getCityName();
                cityCode = citiesData.getCityCode();
            }

            map.put(FirebaseConstants.ChildNodes.FULL_NAME, etName.getText().toString().trim());
            map.put(FirebaseConstants.ChildNodes.MOBILE_NUMBER, etPhone.getText().toString().trim());
            map.put(FirebaseConstants.ChildNodes.CITY_NAME, cityName);
            map.put(FirebaseConstants.ChildNodes.CITY_CODE, cityCode);

            FirebaseDatabase.getInstance()
                    .getReference(FirebaseConstants.ParentNodes.USERS)
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .updateChildren(map, (databaseError, databaseReference) -> {
                        if (databaseError == null) {

                            Session session = Session.getInstance();
                            session.setName(etName.getText().toString().trim());
                            session.setPhone(etPhone.getText().toString().trim());
                            session.setCity(map.get(FirebaseConstants.ChildNodes.CITY_NAME).toString());
                            Object imageObj = map.get(FirebaseConstants.ChildNodes.IMAGE);
                            if (haveUploadedImage && imageObj!=null) session.setImage(imageObj.toString());

                            Toast.makeText(EditProfile.this, "Profile updated successfully.", Toast.LENGTH_SHORT).show();
                            finish();

                        } else {
                            Toast.makeText(EditProfile.this, "Something went wrong. Try again.", Toast.LENGTH_SHORT).show();
                        }
                        hideProgress(false);
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgress(boolean isUploadingImage) {
        if (isUploadingImage) {
            loaderImage.setVisibility(View.VISIBLE);
            rlProfileImage.setEnabled(false);
        } else {
            loader.setVisibility(View.VISIBLE);
            btnUpdateProfile.setVisibility(View.GONE);
        }
    }

    private void hideProgress(boolean isUploadingImage) {
        if (isUploadingImage) {
            loaderImage.setVisibility(View.GONE);
            rlProfileImage.setEnabled(true);
        } else {
            loader.setVisibility(View.GONE);
            btnUpdateProfile.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_update_profile:
                if (validated()) {
                    showProgress(false);
                    saveToDatabase();
                }
                break;

        }

    }

    private boolean validated() {
        String name = etName.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        if (name.isEmpty()) {
            Toast.makeText(this, R.string.error_full_name_empty, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (citiesData == null) {
            Toast.makeText(this, getString(R.string.error_cities), Toast.LENGTH_SHORT).show();;
            return false;
        }

        return true;
    }
}