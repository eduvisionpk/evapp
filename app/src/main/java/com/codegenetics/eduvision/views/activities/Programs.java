package com.codegenetics.eduvision.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.interfaces.IProgramClickListener;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.DisciplineTypes.DisciplineTypesModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.models.Programs.ProgramsModel;
import com.codegenetics.eduvision.models.ProgramsMainModel;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.adapters.FilterRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.LevelsSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.ProgramsAdapter;
import com.codegenetics.eduvision.views.adapters.ProgramsMainAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.Objects;

public class Programs extends AppCompatActivity implements
        IProgramClickListener,
        IFilterClick,
        IViewClickListener {

    private final ArrayList<ProgramsMainModel> arrayList = new ArrayList<>();
    private final ArrayList<ProgramsModel> arrayListTop = ConstantData.getInstance().getProgramsData();
    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private Spinner spDisciplineType, spLevel;


    private void init() {

        View v = findViewById(R.id.bottomSheet_Layout);
        mBehavior = BottomSheetBehavior.from(v);

        arrayList.addAll(ConstantData.getInstance().getProgramTypes());
        ProgramsMainAdapter adapter = new ProgramsMainAdapter(arrayList, this);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_programs_main);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        RecyclerView rvFilters = findViewById(R.id.rv_filters);
        rvFilters.setHasFixedSize(true);
        rvFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvFilters.setAdapter(new FilterRecyclerAdapter(ConstantData.getInstance().getProgramFilters(), this));

        RecyclerView rvPrograms = findViewById(R.id.rv_programs);
        ProgramsAdapter programsAdapter = new ProgramsAdapter(arrayListTop, this);
        rvPrograms.setHasFixedSize(true);
        rvPrograms.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvPrograms.setAdapter(programsAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_program);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.programs);
        }

        init();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {

                @SuppressLint("InflateParams")
                final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_programs, null);

                spDisciplineType = view.findViewById(R.id.sp_discipline_type);
                spLevel = view.findViewById(R.id.sp_level);
                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                Button btnSearch = view.findViewById(R.id.btn_search);

                setDisciplineType(Constants.disciplineTypeList);
                setLevels(Constants.levelsList);

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearch.setOnClickListener(view12 -> {

                    if (spDisciplineType == null || spDisciplineType.getSelectedItem() == null)
                        return;

                    if (ConstantData.SELECT.equalsIgnoreCase(spDisciplineType.getSelectedItem().toString())) {
                        Toast.makeText(this, "Please select Field of study.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    LevelsModel.LevelsData levelsData = (LevelsModel.LevelsData) spLevel.getSelectedItem();

                    if (ConstantData.SELECT.equalsIgnoreCase(levelsData.getLevelName())) {
                        Toast.makeText(this, "Please select Level.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    mBottomSheetDialog.dismiss();

                    startActivity(new Intent(this, ProgramsDisciplines.class)
                            .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), spDisciplineType.getSelectedItem().toString())
                            .putExtra(IntentEnums.LEVEL_CODE.name(), levelsData.getLevelCode())
                            .putExtra(IntentEnums.LEVEL_NAME.name(), levelsData.getLevelName())
                    );
                });

                mBottomSheetDialog = new BottomSheetDialog(this);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
            }

            if (mBottomSheetDialog != null) mBottomSheetDialog.show();

//            mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDisciplineType(ArrayList<DisciplineTypesModel.DisciplineTypesData> arrayList) {

        ArrayList<String> items = new ArrayList<>();

        for (DisciplineTypesModel.DisciplineTypesData item : arrayList) items.add(item.getDisciplineType());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner, items);
        spDisciplineType.setAdapter(adapter);

    }

    private void setLevels(ArrayList<LevelsModel.LevelsData> arrayList) {
        LevelsSpinnerAdapter adapter = new LevelsSpinnerAdapter(this, R.layout.layout_spinner, arrayList);
        spLevel.setAdapter(adapter);
    }

    private void handleBottomSheetViewsOnFilterSelection(SearchFilter searchFilter) {
        switch (searchFilter) {
            case FILED_OF_STUDY:
                spDisciplineType.performClick();
                break;
            case LEVEL:
                spLevel.performClick();
                break;
            default:
                break;
        }
    }

    @Override
    public void onFilterSelected(SearchFilter searchFilter) {
        showBottomSheet();
        new Handler().postDelayed(() -> handleBottomSheetViewsOnFilterSelection(searchFilter), 100);
    }

    @Override
    public void onItemClicked(int position) {
        startActivity(new Intent(this, ProgramsDisciplines.class)
                .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), arrayListTop.get(position).getTitle())
                .putExtra(IntentEnums.LEVEL_CODE.name(), "7")
                .putExtra(IntentEnums.LEVEL_NAME.name(), "Bachelor")
        );
    }

    @Override
    public void onProgramClicked(String disciplineType, String disciplineName) {
        if (disciplineName.isEmpty()) {
            startActivity(new Intent(this, ProgramsDisciplines.class)
                    .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), disciplineType)
                    .putExtra(IntentEnums.LEVEL_CODE.name(), "7")
                    .putExtra(IntentEnums.LEVEL_NAME.name(), "Bachelor")
            );
        } else {
            startActivity(
                    new Intent(this, Universities.class)
                            .putExtra(IntentEnums.DISCIPLINE_TYPE.name(), disciplineType)
                            .putExtra(IntentEnums.LEVEL_CODE.name(), "7")
                            .putExtra(IntentEnums.LEVEL_NAME.name(), "Bachelor")
                            .putExtra(IntentEnums.DISCIPLINE_NAME.name(), disciplineName)
                            .putExtra(IntentEnums.CITY.name(), "")
            );
        }
    }



}