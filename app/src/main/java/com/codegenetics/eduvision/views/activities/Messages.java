package com.codegenetics.eduvision.views.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.FirebaseConstants;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.models.MessagesModels.Message;
import com.codegenetics.eduvision.models.MessagesModels.User;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.adapters.viewholders.IncomingVoiceMessageViewHolder;
import com.codegenetics.eduvision.views.adapters.viewholders.OutcomingVoiceMessageViewHolder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Messages extends AppCompatActivity
        implements
        MessageInput.InputListener,
        MessagesListAdapter.SelectionListener,
        MessageHolders.ContentChecker<Message>,
        MessagesListAdapter.OnLoadMoreListener {

    private final ArrayList<Message> messages = new ArrayList<>();
    private MessagesList messagesList;
    private MessagesListAdapter<Message> messagesAdapter;
    private ImageLoader imageLoader;
    private ImageView ivUser;
    private DatabaseReference referenceChats, referenceMessages, messageListener;
    private static final byte CONTENT_TYPE_VOICE = 1;
    private int TOTAL_MESSAGES_COUNT = 10;
    private String lastKey = "0";
    private String previousKey = "0";
    private boolean fetchLastKey = true;

    private String myEmail, myUsername, myUserAvatar;
    private boolean isChatListenerActive = false;
    public static boolean isActive = false;
    private String uuid = "";
    private Menu menu;

    private final ValueEventListener chatsListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            Log.e("test", "chat listener executed");
            isChatListenerActive = true;
            if (isActive && !uuid.isEmpty())
                referenceChats
                        .child(uuid)
                        .child(FirebaseConstants.ChildNodes.UNREAD)
                        .setValue("0", (databaseError, databaseReference) ->
                                Constants.log("FirebaseSetValue", "setValue=0")
                        );
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            isChatListenerActive = true;
        }
    };

    private void init() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) uuid = currentUser.getUid();

        messageListener = FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.MESSAGES);
        referenceChats = FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.CHATS);
        referenceMessages = FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.MESSAGES);

        Constants.map.put(FirebaseConstants.CONSULTANT, new ArrayList<>());
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) notificationManager.cancel(1);

        myEmail = Session.getInstance().getEmail();
        myUsername = Session.getInstance().getName();
        myUserAvatar = Session.getInstance().getImage();

        messagesList = findViewById(R.id.messagesList);
        ivUser = findViewById(R.id.toolbar_ivUser);

        MessageInput input = findViewById(R.id.input);
        input.setInputListener(this);

        imageLoader = (imageView, url, payload) ->
                Glide.with(imageView)
                        .load(url)
                        .placeholder(R.drawable.ic_user_male)
                        .error(R.drawable.ic_user_male)
                        .into(imageView);

        if (Constants.getInstance().getFirebaseToken().isEmpty()) Constants.getInstance().getFirebaseToken();

    }

    private void  initAdapter() {

        MessageHolders holders = new MessageHolders()
                .registerContentType(
                        CONTENT_TYPE_VOICE,
                        IncomingVoiceMessageViewHolder.class,
                        R.layout.item_custom_incoming_voice_message,
                        OutcomingVoiceMessageViewHolder.class,
                        R.layout.item_custom_outcoming_voice_message,
                        this);


        messagesAdapter = new MessagesListAdapter<>(myEmail, holders, imageLoader);
        messagesAdapter.enableSelectionMode(this);
        messagesAdapter.setLoadMoreListener(this);
        messagesList.setAdapter(messagesAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        init();

        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initAdapter();

        Toolbar toolbar = findViewById(R.id.top_bar_activity_toolbar);
        toolbar.setLogo(R.drawable.ic_back);
        setSupportActionBar(toolbar);

        View logoView = toolbar.getChildAt(1); //0 ~ toolbar title - 1 ~ toolbar logo
        logoView.setOnClickListener(v -> finish());

        final TextView tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setText(R.string.consultant);

        ivUser.setOnClickListener(v -> tvTitle.performClick());

    }

    private void fetchMessages() {
        messageListener
                .child(uuid)
                .limitToLast(20)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        fetchLastKey = true;
                        messagesAdapter.clear();

                        Log.e("test", "new message");

                        if (snapshot.getChildrenCount() > 0) startChatListener();

                        TOTAL_MESSAGES_COUNT = (int) snapshot.getChildrenCount() + 1;
                        Constants.log(Messages.this, "total message = " + TOTAL_MESSAGES_COUNT);

                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                            String date = dataSnapshot.getKey();

                            if (fetchLastKey){
                                lastKey = date;
                                previousKey = date;
                                fetchLastKey = false;
                            }

                            Message message = new Message(
                                    date,
                                    FirebaseConstants.RECEIVED
                                            .equalsIgnoreCase(dataSnapshot.child(FirebaseConstants.ChildNodes.STATUS).getValue(String.class)) ?
                                            new User(
                                                    "-1",
                                                    FirebaseConstants.CONSULTANT,
                                                    "",
                                                    true
                                            ) :
                                            Constants.getInstance().getMyDetails(),
                                    dataSnapshot.child(FirebaseConstants.ChildNodes.MESSAGE).getValue(String.class),
                                    Constants.getInstance().getDate(Long.parseLong(date == null ? String.valueOf(System.currentTimeMillis()) : date))
                            );

                            messagesAdapter.addToStart(message, false);


                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(Messages.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
//        messageListener.keepSynced(true);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {

        Constants.log(this, "load more called.. page = " + page + " totalItemsCount =  " + totalItemsCount);
        Constants.log(this, "last message key = " + lastKey);

        messageListener
                .child(uuid)
                .orderByKey()
                .endAt(lastKey)
                .limitToLast(20)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        messages.clear();
                        fetchLastKey = true;
                        TOTAL_MESSAGES_COUNT = (int) snapshot.getChildrenCount() + 1;

                        Constants.log(Messages.this, "total message = " + TOTAL_MESSAGES_COUNT);

                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                            String date = dataSnapshot.getKey();

                            Message message = new Message(
                                    date,
                                    FirebaseConstants.RECEIVED
                                            .equalsIgnoreCase(dataSnapshot.child(FirebaseConstants.ChildNodes.STATUS).getValue(String.class)) ?
                                            new User(
                                                    "-1",
                                                    FirebaseConstants.CONSULTANT,
                                                    "",
                                                    true
                                            ) :
                                            Constants.getInstance().getMyDetails(),
                                    dataSnapshot.child(FirebaseConstants.ChildNodes.MESSAGE).getValue(String.class),
                                    Constants.getInstance().getDate(Long.parseLong(date == null ? String.valueOf(System.currentTimeMillis()) : date))
                            );

                            Constants.log(Messages.this, "message = " + dataSnapshot.child(FirebaseConstants.ChildNodes.MESSAGE).getValue(String.class));

                            if (!previousKey.equalsIgnoreCase(date)) {
                                messages.add(message);
                            } else {
                                previousKey = lastKey;
                            }

                            if (fetchLastKey){
                                lastKey = date;
                                fetchLastKey = false;
                            }


                        }
                        messagesAdapter.addToEnd(messages, true);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

//        if (totalItemsCount < TOTAL_MESSAGES_COUNT) loadMessages();
    }

    private void startChatListener() {
        if (!isChatListenerActive && !uuid.isEmpty())
            referenceChats.child(uuid).addValueEventListener(chatsListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
        fetchMessages();
    }

    @Override
    protected void onDestroy() {
        try {
            isActive = false;
            if (!uuid.isEmpty()) referenceChats.child(uuid).removeEventListener(chatsListener);
            Log.e("test", "onDestroy");
        } catch (Exception e) {
            Log.e("test", "onDestroy Exception");
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean onSubmit(CharSequence input) {

        Map<String, String> map = new HashMap<>();
        map.put(FirebaseConstants.ChildNodes.DATE, String.valueOf(System.currentTimeMillis()));
        map.put(FirebaseConstants.ChildNodes.LAST_MESSAGE, input.toString().trim());

//        messageListener.child(myUserId).child(otherUserId).removeEventListener(messagesListener);

        messagesAdapter.addToStart(getTextMessage(input.toString()), true);

        addToUser1Chat(map);

        return true;
    }

    private void addToUser1Chat(final Map<String, String> map) {

        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.SENT);
        map.put(FirebaseConstants.ChildNodes.UNREAD, "0");

        referenceChats
                .child(uuid)
                .setValue(map, (databaseError, databaseReference) -> {
                    if (databaseError == null) addToUser2Chat(map);
                    else addToUser1Chat(map);
                });


    }

    private void addToUser2Chat(final Map<String, String> map) {

        referenceChats
                .child(FirebaseConstants.CONSULTANT)
                .child(uuid)
                .child(FirebaseConstants.ChildNodes.UNREAD)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {
                            map.put(FirebaseConstants.ChildNodes.UNREAD,
                                    String.valueOf(Integer.parseInt(
                                            Objects.requireNonNull(dataSnapshot.getValue(String.class))) + 1)
                            );
                        } else {
                            map.put(FirebaseConstants.ChildNodes.UNREAD, "1");
                        }

                        String token = Constants.getInstance().getFirebaseToken();

                        map.put(FirebaseConstants.ChildNodes.DEVICE_ID, token.isEmpty() ? Constants.getInstance().getFirebaseToken() : token);
                        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.RECEIVED);
                        map.put(FirebaseConstants.ChildNodes.USER_PHOTO, myUserAvatar);
                        map.put(FirebaseConstants.ChildNodes.USERNAME, myUsername);

                        Constants.log(Messages.this, "unread count for other user = " + map.get(FirebaseConstants.ChildNodes.UNREAD));

                        referenceChats
                                .child(FirebaseConstants.CONSULTANT)
                                .child(uuid)
                                .setValue(map, (databaseError, databaseReference) -> {
                                    if (databaseError != null) {
                                        addToUser2Chat(map);
                                    }
                                    else {
                                        addToUser1Messages(map);
                                    }
                                });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(Messages.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void addToUser1Messages(final Map<String, String> map) {

        map.put(FirebaseConstants.ChildNodes.MESSAGE, map.get(FirebaseConstants.ChildNodes.LAST_MESSAGE));
        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.SENT);

        map.remove(FirebaseConstants.ChildNodes.LAST_MESSAGE);
        map.remove(FirebaseConstants.ChildNodes.DEVICE_ID);
        map.remove(FirebaseConstants.ChildNodes.USER_PHOTO);
        map.remove(FirebaseConstants.ChildNodes.USERNAME);
        map.remove(FirebaseConstants.ChildNodes.UNREAD);
        map.remove(FirebaseConstants.ChildNodes.DATE);

        referenceMessages
                .child(uuid)
                .child(String.valueOf(System.currentTimeMillis()))
                .setValue(map, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        addToUser2Messages(map);
                    }
                    else {
                        Toast.makeText(Messages.this, "Message sending failed.", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void addToUser2Messages(final Map<String, String> map) {
        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.RECEIVED);

        referenceMessages
                .child(FirebaseConstants.CONSULTANT)
                .child(uuid)
                .child(String.valueOf(System.currentTimeMillis()))
                .setValue(map, (databaseError, databaseReference) -> {
                    if (databaseError != null){
                        Toast.makeText(Messages.this, "Message sending failed.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public boolean hasContentFor(Message message, byte type) {
        if (type == CONTENT_TYPE_VOICE) {
            return message.getVoice() != null
                    && message.getVoice().getUrl() != null
                    && !message.getVoice().getUrl().isEmpty();
        }
        return false;
    }

    private Message getTextMessage(String text) {
        return new Message(
                String.valueOf(new Random().nextInt(999999 - 100000) + 10000),
                Constants.getInstance().getMyDetails(),
                text);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(IntentEnums.GO_TO_HOME.toString(), false)) {
            startActivity(new Intent(this, HomeActivity.class));
            finishAffinity();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.chat_actions_menu, menu);
        onSelectionChanged(0);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_copy:
                messagesAdapter.copySelectedMessagesText(this, getMessageStringFormatter(), true);
                Toast.makeText(this, "Message copied", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }


    private MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
        return message -> {
            String text = message.getText();
            if (text == null) text = "[attachment]";

            return String.format(Locale.getDefault(), "%s", text);
        };
    }

    @Override
    public void onSelectionChanged(int count) {
        menu.findItem(R.id.action_copy).setVisible(count > 0);
    }
}