package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.models.Institution.InstituteDetailsModel;

import java.util.ArrayList;

public class UniversityDetailTabRecycler extends RecyclerView.Adapter<UniversityDetailTabRecycler.UniversityTabHolder> {

    private final ArrayList<InstituteDetailsModel.InstituteDetailsLevels> levelsArrayList;
    private final ITabClickListener iTabClickListener;
    private int mSelectedItem = 0;

    public UniversityDetailTabRecycler(ArrayList<InstituteDetailsModel.InstituteDetailsLevels> levelsArrayList, ITabClickListener iTabClickListener) {
        this.levelsArrayList = levelsArrayList;
        this.iTabClickListener = iTabClickListener;
    }

    @NonNull
    @Override
    public UniversityTabHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UniversityTabHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_level_tab, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UniversityTabHolder holder, int position) {

        holder.tvTitle.setText(levelsArrayList.get(position).getLevel_name());

        if (position == mSelectedItem) {
            holder.tvTitle.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.dialog_primary));
            holder.tvTitle.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.white));
        } else {
            holder.tvTitle.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.white));
            holder.tvTitle.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.black));
        }

    }

    @Override
    public int getItemCount() {
        return levelsArrayList.size();
    }

    public class UniversityTabHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;

        public UniversityTabHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            itemView.setOnClickListener(v -> {
                mSelectedItem = getAdapterPosition();
                iTabClickListener.onTabsSelected(levelsArrayList.get(getAdapterPosition()).getLevel_name());
                notifyDataSetChanged();
            });
        }
    }
}
