package com.codegenetics.eduvision.views.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.models.FilterModel;
import com.bumptech.glide.Glide;

import java.util.List;

public class FilterRecyclerAdapter extends RecyclerView.Adapter<FilterRecyclerAdapter.FilterHolder> {
    List<FilterModel> mData;
    IFilterClick iFilterClick;

    public FilterRecyclerAdapter(List<FilterModel> mData, IFilterClick iFilterClick) {
        this.mData = mData;
        this.iFilterClick = iFilterClick;
    }

    @NonNull
    @Override
    public FilterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilterHolder holder, int position) {
        Glide.with(holder.itemView).load(mData.get(position).getIcon()).addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.ivIcon.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.ivIcon.setVisibility(View.VISIBLE);
                holder.ivIcon.setImageResource(mData.get(position).getIcon());
                return true;
            }
        }).into(holder.ivIcon);
        holder.tvTitle.setText(mData.get(position).getTitle());

        holder.itemView.setOnClickListener(v -> {
            iFilterClick.onFilterSelected(mData.get(position).getSearchFilter());
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class FilterHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView ivIcon;

        public FilterHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            ivIcon = itemView.findViewById(R.id.iv_icon);
        }
    }
}
