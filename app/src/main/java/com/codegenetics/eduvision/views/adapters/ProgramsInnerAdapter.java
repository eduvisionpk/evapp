package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IProgramClickListener;
import com.codegenetics.eduvision.models.ProgramsInnerModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public
class ProgramsInnerAdapter extends RecyclerView.Adapter<ProgramsInnerAdapter.ViewHolder> {

    private final ArrayList<ProgramsInnerModel> arrayList;
    private final IProgramClickListener listener;

    public ProgramsInnerAdapter(ArrayList<ProgramsInnerModel> arrayList, IProgramClickListener clickListener) {
        this.arrayList = arrayList;
        listener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.row_programs_inner, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProgramsInnerModel model = arrayList.get(holder.getAdapterPosition());
        holder.tvType.setText(model.getTitle());
        Glide.with(holder.ivTypeImage)
                .load(model.getUrl())
                .placeholder(R.drawable.ic_dummy)
                .error(R.drawable.ic_dummy1)
                .into(holder.ivTypeImage);
        holder.itemView.setOnClickListener(view ->
                listener.onProgramClicked(
                ConstantData.getInstance().getProgramType(model.getProgramsEnums()),
                model.getTitle()
                )
        );
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivTypeImage;
        private final TextView tvType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivTypeImage = itemView.findViewById(R.id.iv_image);
            tvType = itemView.findViewById(R.id.tv_name);
        }
    }

}
