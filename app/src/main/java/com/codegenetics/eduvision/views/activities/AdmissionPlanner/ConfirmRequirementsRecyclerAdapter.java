package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.interfaces.IProgramsClickListener;

import java.util.ArrayList;

public class ConfirmRequirementsRecyclerAdapter extends RecyclerView.Adapter<ConfirmRequirementsRecyclerAdapter.ConfirmRequirementHolder> {

    private final ArrayList<String> arrayList;
    private final IProgramsClickListener listener;

    public ConfirmRequirementsRecyclerAdapter(ArrayList<String> arrayList, IProgramsClickListener listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ConfirmRequirementHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ConfirmRequirementHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_confirm_requirements, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmRequirementHolder holder, int position) {
        holder.bind(arrayList.get(holder.getAdapterPosition()));
        holder.btnLowFee.setOnClickListener(v -> listener.onItemClicked(AppEnums.LOW_FEE, holder.getAdapterPosition()));
        holder.btnLowMerit.setOnClickListener(v -> listener.onItemClicked(AppEnums.LOW_MERIT, holder.getAdapterPosition()));
        holder.btnHighMerit.setOnClickListener(v -> listener.onItemClicked(AppEnums.HIGH_MERIT, holder.getAdapterPosition()));
        holder.btnAllUniversities.setOnClickListener(v -> listener.onItemClicked(AppEnums.ALL_UNIVERSITIES, holder.getAdapterPosition()));
        holder.ivClose.setOnClickListener(v -> {
            try {

                if (arrayList.size() > 1) {
                    listener.onItemClicked(null, -1);
                    arrayList.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    PlannerInfoSession.getInstance().setDisciplineTypes(arrayList);
                } else {
                    Toast.makeText(v.getContext(), "You cannot remove all disciplines.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public static class ConfirmRequirementHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final Button btnHighMerit, btnLowMerit, btnLowFee, btnAllUniversities;
        private final ImageView ivClose;

        public ConfirmRequirementHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            btnHighMerit = itemView.findViewById(R.id.btn_high_merit);
            btnLowMerit = itemView.findViewById(R.id.btn_low_merit);
            btnLowFee = itemView.findViewById(R.id.btn_low_fee);
            btnAllUniversities = itemView.findViewById(R.id.btn_all_universities);
            ivClose = itemView.findViewById(R.id.iv_close);
        }

        public void bind(String discipline) {
            tvTitle.setText(discipline.split("#")[0]);
        }
    }
}
