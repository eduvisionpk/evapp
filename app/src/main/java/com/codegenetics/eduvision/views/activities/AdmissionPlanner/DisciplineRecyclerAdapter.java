package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.Programs.ProgramsModel;

import java.util.List;

public class DisciplineRecyclerAdapter extends RecyclerView.Adapter<DisciplineRecyclerAdapter.ProgramsHolder> {

    private final List<ProgramsModel> mData;
    private final IViewClickListener listener;

    public DisciplineRecyclerAdapter(List<ProgramsModel> mData, IViewClickListener clickListener) {
        this.mData = mData;
        this.listener = clickListener;
    }

    @NonNull
    @Override
    public ProgramsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProgramsHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_programs, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProgramsHolder holder, int position) {
        ProgramsModel model = mData.get(holder.getAdapterPosition());

        holder.ivIcon.setImageResource(model.getIcon());
        holder.tvTitle.setText(model.getTitle());
        holder.itemView.setOnClickListener(v -> listener.onItemClicked(holder.getAdapterPosition()));

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class ProgramsHolder extends RecyclerView.ViewHolder {
        private final ImageView ivIcon;
        private final AppCompatTextView tvTitle;

        public ProgramsHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }

}
