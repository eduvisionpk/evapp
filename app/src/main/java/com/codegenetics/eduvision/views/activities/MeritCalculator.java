package com.codegenetics.eduvision.views.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.MeritCalculator.MeritCalculatorListModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.MeritCalculatorRecycler;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

public class MeritCalculator extends AppCompatActivity implements IData {

    private MKLoader loader;
    private ServerPresenter presenter;
    private RecyclerView rvMeritCalculator;

    private void init() {
        rvMeritCalculator = findViewById(R.id.rv_merit_calculator);
        loader = findViewById(R.id.loader);
        presenter = ServerPresenter.getInstance(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merit_calculator);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.merit_calculator);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getInstituteList();

    }

    private void getInstituteList() {
        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.MERIT_CALCULATORS_LIST);
        } else {
            Internet.showOfflineDialog(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        try {
            if (!response.isEmpty()) {

                MeritCalculatorListModel meritCalculatorListModel = new Gson().fromJson(response, MeritCalculatorListModel.class);
                rvMeritCalculator.setHasFixedSize(true);
                rvMeritCalculator.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));


                if (meritCalculatorListModel.getData() != null && meritCalculatorListModel.getData().size() > 0) {
                    MeritCalculatorRecycler meritCalculatorRecycler = new MeritCalculatorRecycler(meritCalculatorListModel.getData());
                    rvMeritCalculator.setAdapter(meritCalculatorRecycler);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}