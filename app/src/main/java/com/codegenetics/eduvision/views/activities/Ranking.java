package com.codegenetics.eduvision.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.utils.Constants;

public class Ranking extends AppCompatActivity {
    private Button btnAreaRanking, btnBoardRanking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.ranking);
        }

        initViews();
        clickEvents();

    }

    private void initViews() {
        btnAreaRanking = findViewById(R.id.btn_area_ranking);
        btnBoardRanking = findViewById(R.id.btn_board_ranking);
    }

    private void clickEvents(){
        btnAreaRanking.setOnClickListener(v -> {
            Intent intent = new Intent(this, RankingDetails.class);
            intent.putExtra(Constants.Keys.RANKING_TYPE, Constants.VALUES.AREA_WISE_RANKING);
            startActivity(intent);
        });

        btnBoardRanking.setOnClickListener(v -> {
            Intent intent = new Intent(this, RankingDetails.class);
            intent.putExtra(Constants.Keys.RANKING_TYPE, Constants.VALUES.BOARD_WISE_RANKING);
            startActivity(intent);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

}