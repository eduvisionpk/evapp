package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IRecyclerClick;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel.LevelsData;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.activities.HomeActivity;

import java.util.ArrayList;

public class AddDiscipline extends AppCompatActivity implements IRecyclerClick {

    private TextView tvName, tvDob, tvDomicile, tvDegree, tvPassingYear;
    private final ArrayList<LevelsData> levelsData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_discipline);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.step_2_of_3);
        }

        initViews();
        updateViews();

    }

    private void initViews() {
        tvName = findViewById(R.id.tv_name);
        tvDob = findViewById(R.id.tv_dob);
        tvDomicile = findViewById(R.id.tv_domicile);
        tvDegree = findViewById(R.id.tv_degree);
        tvPassingYear = findViewById(R.id.tv_passing_year);

        levelsData.addAll(Constants.levelsList);
        levelsData.remove(0);

        RecyclerView rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rv.setAdapter(new DegreeRecycler(levelsData, this));
    }


    private void updateViews() {
        tvName.setText(PlannerInfoSession.getInstance().getName());
        tvDob.setText(PlannerInfoSession.getInstance().getDob());
        tvDomicile.setText(PlannerInfoSession.getInstance().getDomicile());
        tvDegree.setText(PlannerInfoSession.getInstance().getDegree());
        tvPassingYear.setText(PlannerInfoSession.getInstance().getPassingYear());
    }

    @Override
    public void onRecyclerItemClicked(LevelsData levelsData) {
        PlannerInfoSession.getInstance().setLevelCode(levelsData.getLevelCode());
        PlannerInfoSession.getInstance().setLevelName(levelsData.getLevelName());
        startActivity(new Intent(this, Discipline.class)
                .putExtra(IntentEnums.LEVEL_NAME.name(), levelsData.getLevelName())
                .putExtra(IntentEnums.LEVEL_CODE.name(), levelsData.getLevelCode())
                .putExtra(IntentEnums.DATA.name(), new ArrayList<String>())
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }
}