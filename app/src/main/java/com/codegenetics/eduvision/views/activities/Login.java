package com.codegenetics.eduvision.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.tuyenmonkey.mkloader.MKLoader;

public class Login extends AppCompatActivity {

    private static final int requestCodeSignUp = 1;

    private static final int login_btnSignIn = R.id.login_btnSignIn;
    private static final int login_btnSignUp = R.id.login_tvSignUp;
    private static final int login_tvForgotPassword = R.id.login_tvForgotPassword;
    private static final int login_ivPasswordVisibility = R.id.login_ivPasswordVisibility;

    private EditText etEmail, etPassword;
    private ImageView ivPasswordVisibility;
    private MKLoader loader;
    private Button btnSignIn;
    private CheckBox cbRememberMe;

    private FirebaseAuth mAuth;
    private boolean isPasswordVisible = false;

    private void init() {
        mAuth = FirebaseAuth.getInstance();

        etEmail = findViewById(R.id.login_etEmail);
        etPassword = findViewById(R.id.login_etPassword);
        ivPasswordVisibility = findViewById(R.id.login_ivPasswordVisibility);
        loader = findViewById(R.id.login_loader);
        btnSignIn = findViewById(R.id.login_btnSignIn);
        cbRememberMe = findViewById(R.id.login_cbRememberMe);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        if (Session.getInstance().isRememberMe()) {
            etEmail.setText(Session.getInstance().getEmail());
            cbRememberMe.setChecked(true);
        }

    }

    private void authenticateUser() {

        if (Internet.isConnected(this)) {

            if (validate()) {
                showProgress();

                String email = etEmail.getText().toString().trim();
                String password = etPassword.getText().toString();

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(task -> {

                            if (task.isSuccessful()) {

                                FirebaseUser user = mAuth.getCurrentUser();

                                if (user != null) {
                                    Session.getInstance().setLogin(user, cbRememberMe.isChecked(), this);
                                    //                                    if (user.isEmailVerified()) {
//
//
//
////                                        startActivity(new Intent(this, HomeActivity.class));
////                                        finish();
//
//                                    }
//                                    else {
//                                        hideProgress();
//                                        Constants.getInstance().showVerificationDialog(this, user);
//                                    }
                                }
                                else {
                                    hideProgress();
                                    displayError(getString(R.string.something_went_wrong));
                                }

                            }
                            else {
                                hideProgress();
                                if (task.getException() != null)
                                    displayError(task.getException().getLocalizedMessage());
                            }

                        });

            }

        }
        else {
            Internet.showOfflineDialog(this);
        }

    }

    private boolean validate() {

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString();

        if (email.isEmpty() || !Constants.getInstance().isValidEmail(email)) {
            displayError(Constants.INVALID_EMAIL);
            return false;
        }

        if (password.isEmpty()) {
            displayError("Password shouldn't be empty.");
            return false;
        }

        return true;
    }

    public void onClick(View view) {

        switch (view.getId()) {

            case login_btnSignIn:
                authenticateUser();
                break;
            case login_btnSignUp:
                startActivityForResult(new Intent(this, SignUp.class), requestCodeSignUp);
                break;
            case login_tvForgotPassword:
                Constants.getInstance().showForgotDetailsDialog(this);
                break;
            case login_ivPasswordVisibility:
                togglePasswordVisibility();
                break;
        }
    }

    private void togglePasswordVisibility() {

        if (isPasswordVisible) {
            etPassword.setTransformationMethod(new PasswordTransformationMethod());
            isPasswordVisible = false;
            ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_on);
        } else {
            etPassword.setTransformationMethod(null);
            isPasswordVisible = true;
            ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_off);
        }
        etPassword.setSelection(etPassword.getText().toString().length());
    }

    public void showProgress() {
        loader.setVisibility(View.VISIBLE);
        btnSignIn.setVisibility(View.GONE);
    }

    public void hideProgress() {
        loader.setVisibility(View.GONE);
        btnSignIn.setVisibility(View.VISIBLE);
    }

    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == requestCodeSignUp) {

                if (data != null) {
                    etEmail.setText(data.getStringExtra(IntentEnums.EMAIL.name()));
                    etPassword.setText(data.getStringExtra(IntentEnums.PASSWORD.name()));
                }

            }

        }

    }

}