package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.models.Rankings.RankingDetailsModel;

import java.util.ArrayList;

public class RankDetailRecyclerAdapter extends RecyclerView.Adapter<RankDetailRecyclerAdapter.RankDetailHolder> {

    private final ArrayList<RankingDetailsModel.DataBean> data;

    public RankDetailRecyclerAdapter(ArrayList<RankingDetailsModel.DataBean> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RankDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RankDetailHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rank_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RankDetailHolder holder, int position) {

        try {
            RankingDetailsModel.DataBean rankingData = data.get(holder.getAdapterPosition());

            holder.tvSno.setText(String.valueOf(holder.getAdapterPosition() + 1));
            holder.tvUniName.setText(rankingData.getName());
            holder.tvBoard.setText(rankingData.getBoard());
            holder.tvScore.setText(rankingData.getFinalScore());

            if (holder.getAdapterPosition() % 2 == 1) {
                holder.viewParent.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
            } else {
                holder.viewParent.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.grey_10));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    static class RankDetailHolder extends RecyclerView.ViewHolder {

        private final TextView tvSno, tvUniName, tvBoard, tvScore;
        private final View viewParent;

        public RankDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvSno = itemView.findViewById(R.id.tv_rank);
            tvUniName = itemView.findViewById(R.id.tv_uni_name);
            tvBoard = itemView.findViewById(R.id.tv_board);
            tvScore = itemView.findViewById(R.id.tv_score);
            viewParent = itemView.findViewById(R.id.view_parent);
        }
    }
}
