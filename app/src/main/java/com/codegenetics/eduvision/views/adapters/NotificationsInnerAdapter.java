package com.codegenetics.eduvision.views.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.models.News.NewsModel;
import com.codegenetics.eduvision.models.Notifications.NotificationsModel;
import com.codegenetics.eduvision.models.Scholarships.ScholarshipsModel;
import com.codegenetics.eduvision.models.Videos.VideoDetailsModel;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.views.activities.CareerTvDetails;
import com.codegenetics.eduvision.views.activities.Details;
import com.codegenetics.eduvision.views.activities.FullScreenImages;

import java.util.ArrayList;
import java.util.Locale;

public
class NotificationsInnerAdapter extends RecyclerView.Adapter<NotificationsInnerAdapter.ViewHolder> {

    private final ArrayList<NotificationsModel.NotificationsData> arrayList;

    public NotificationsInnerAdapter(ArrayList<NotificationsModel.NotificationsData> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.row_notifications_inner, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationsModel.NotificationsData model = arrayList.get(holder.getAdapterPosition());

        holder.tvDescription.setText(model.getTitletext());
        holder.tvDate.setText(model.getTime());

        Glide.with(holder.ivTypeImage)
                .load(model.getImage())
                .placeholder(R.drawable.ic_dummy)
                .error(R.drawable.ic_dummy1)
                .into(holder.ivTypeImage);

        holder.itemView.setOnClickListener(view -> {
            switch (model.getType().toLowerCase(Locale.getDefault())) {
                case Constants.Cases.NOTIFICATION_TYPE_NEWS:
                    view.getContext().startActivity(new Intent(view.getContext(), Details.class)
                            .putExtra(IntentEnums.NEWS_DATA.toString(), new NewsModel.NewsData(model.getId(), model.getTitletext(), model.getImage(), model.getTime()))
                            .putExtra(IntentEnums.TYPE.toString(), Constants.Cases.NEWS_DETAILS));
                    break;
                case Constants.Cases.NOTIFICATION_TYPE_SCHOLARSHIPS:
                    view.getContext().startActivity(new Intent(view.getContext(), Details.class)
                            .putExtra(IntentEnums.SCHOLARSHIP_DATA.toString(), new ScholarshipsModel.ScholarshipsData(model.getId(), model.getTitletext(), model.getImage(), model.getTime()))
                            .putExtra(IntentEnums.TYPE.toString(), Constants.Cases.SCHOLARSHIP_DETAILS));
                    break;
                case Constants.Cases.NOTIFICATION_TYPE_ADMISSIONS:
                    view.getContext().startActivity(
                            new Intent(view.getContext(), FullScreenImages.class)
                                    .putExtra(IntentEnums.DEADLINE.toString(), model.getTime())
                                    .putExtra(IntentEnums.IMAGES.toString(), model.getImage().split(";"))
                                    .putExtra(IntentEnums.POSITION.toString(), 0)
                                    .putExtra(IntentEnums.HIDE.toString(), true)
                    );
                    break;
                case Constants.Cases.NOTIFICATION_TYPE_VIDEOS:
                    view.getContext().startActivity(new Intent(view.getContext(), CareerTvDetails.class)
                            .putExtra(IntentEnums.DATA.name(), new VideoDetailsModel.VideoDetailsData(
                                    model.getId(), model.getTitletext(), model.getId(), "", model.getImage()
                            )));
                    break;

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivTypeImage;
        private final TextView tvDescription, tvDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivTypeImage = itemView.findViewById(R.id.iv_image);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }

}
