package com.codegenetics.eduvision.views.activities;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.helper.ConstantData;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IFilterClick;
import com.codegenetics.eduvision.interfaces.ISelectedFiltersClick;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.models.FilterModel;
import com.codegenetics.eduvision.models.KeyValue;
import com.codegenetics.eduvision.models.Rankings.RankingAreaModel;
import com.codegenetics.eduvision.models.Rankings.RankingBoardModel;
import com.codegenetics.eduvision.models.Rankings.RankingDetailsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.FilterRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.KeyValueSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.RankDetailRecyclerAdapter;
import com.codegenetics.eduvision.views.adapters.SelectedFilterRecycler;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class RankingDetails extends AppCompatActivity implements IData, IFilterClick, ISelectedFiltersClick {
    private ServerPresenter presenter;
    private MKLoader loader;
    private View viewNoData, viewData;
    private SelectedFilterRecycler selectedFilterRecycler;
    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private Button btnSearchBottomSheet;
    private View viewArea, viewBoard;
    private CardView cardSecond, cardThird;
    private TextView tvTitle, tv_first_institute_name, tv_first_board, tv_first_score;
    private TextView tv_second_institute_name, tv_second_board, tv_second_score;
    private TextView tv_third_institute_name, tv_third_board, tv_third_score;
    private Spinner spLevel, spCategory, spArea, spBoard;
    private int rankingType;
    private boolean isSpinnerFilled = false;
    private final ArrayList<KeyValue> levelArrayList = new ArrayList<>();
    private final List<FilterModel> selectedFilters = new ArrayList<>();
    private final ArrayList<KeyValue> categoryArrayList = new ArrayList<>();
    private final ArrayList<KeyValue> cityArrayList = new ArrayList<>();
    private final ArrayList<KeyValue> boardArrayList = new ArrayList<>();
    private final ArrayList<RankingDetailsModel.DataBean> arrayList = new ArrayList<>();
    private final ArrayList<FilterModel> filterArrayList = new ArrayList<>();
    private KeyValue city = new KeyValue("", ConstantData.SELECT);
    private KeyValue board = new KeyValue("", ConstantData.SELECT);
    private KeyValue level = new KeyValue("", ConstantData.SELECT);
    private KeyValue category = new KeyValue("", ConstantData.SELECT);
    private RankDetailRecyclerAdapter adapter;
    private FilterRecyclerAdapter filterRecyclerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking_details);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.ranking);
        }

        initViews();

    }

    private void initViews() {
        View v = findViewById(R.id.layout_bottom_sheet);
        mBehavior = BottomSheetBehavior.from(v);
        presenter = ServerPresenter.getInstance(this);
        loader = findViewById(R.id.loader);
        viewData = findViewById(R.id.view_data);
        viewNoData = findViewById(R.id.no_data);
        tvTitle = findViewById(R.id.tv_title);
        cardSecond = findViewById(R.id.card_second);
        cardThird = findViewById(R.id.card_third);
        tv_first_institute_name = findViewById(R.id.tv_first_institute_name);
        tv_first_board = findViewById(R.id.tv_first_board_name);
        tv_first_score = findViewById(R.id.tv_first_score);
        tv_second_institute_name = findViewById(R.id.tv_second_institute_name);
        tv_second_board = findViewById(R.id.tv_second_board_name);
        tv_second_score = findViewById(R.id.tv_second_score);
        tv_third_institute_name = findViewById(R.id.tv_third_institute_name);
        tv_third_board = findViewById(R.id.tv_third_board_name);
        tv_third_score = findViewById(R.id.tv_third_score);

        RecyclerView rvFilters = findViewById(R.id.rv_filters);
        rvFilters.setHasFixedSize(true);
        rvFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        filterRecyclerAdapter = new FilterRecyclerAdapter(filterArrayList, this);
        rvFilters.setAdapter(filterRecyclerAdapter);

        RecyclerView rvFiltersSelected = findViewById(R.id.rv_filters_selected);
        rvFiltersSelected.setHasFixedSize(true);
        rvFiltersSelected.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectedFilterRecycler = new SelectedFilterRecycler(selectedFilters, this);
        rvFiltersSelected.setAdapter(selectedFilterRecycler);

        RecyclerView rvRanking = findViewById(R.id.recycler_view_ranking);
        rvRanking.setHasFixedSize(true);
        rvRanking.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new RankDetailRecyclerAdapter(arrayList);
        rvRanking.setAdapter(adapter);

        getIntentData();

    }

    private void getIntentData() {
        if (getIntent() != null) {
            rankingType = getIntent().getIntExtra(Constants.Keys.RANKING_TYPE, 0);
            filterArrayList.addAll(ConstantData.getInstance().getRankingFilter());

            switch (rankingType) {
                case Constants.VALUES.AREA_WISE_RANKING:
                    tvTitle.setText(R.string.ranking_title2);
                    filterArrayList.add(new FilterModel(0, "City", SearchFilter.AREA));
                    filterRecyclerAdapter.notifyDataSetChanged();
                    presenter.getData(AppEnums.DIST_PROVINCE);
                    break;
                case Constants.VALUES.BOARD_WISE_RANKING:
                    tvTitle.setText(R.string.ranking_title1);
                    filterArrayList.add(new FilterModel(0, "Board", SearchFilter.BOARD));
                    filterRecyclerAdapter.notifyDataSetChanged();
                    presenter.getData(AppEnums.LIST_OF_BOARDS);
                    break;
            }
        }
    }

    private void showBottomSheet() {

        try {

            if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            if (mBottomSheetDialog == null) {
                @SuppressLint("InflateParams")
                final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_ranking, null);

                spLevel = view.findViewById(R.id.sp_level);
                spCategory = view.findViewById(R.id.sp_category);
                spArea = view.findViewById(R.id.sp_area);
                spBoard = view.findViewById(R.id.sp_board);
                viewArea = view.findViewById(R.id.view_area);
                viewBoard = view.findViewById(R.id.view_board);

                TextView tvCancel = view.findViewById(R.id.tv_cancel);
                btnSearchBottomSheet = view.findViewById(R.id.btn_search);

                fillSpinnerData();

                tvCancel.setOnClickListener(view12 -> mBottomSheetDialog.dismiss());

                btnSearchBottomSheet.setOnClickListener(view12 -> {

                    level = (KeyValue) spLevel.getSelectedItem();
                    category = (KeyValue) spCategory.getSelectedItem();

                    if (category.getValue().equalsIgnoreCase(ConstantData.SELECT)) {
                        displayError("Please select category to continue");
                        return;
                    }

                    if (level.getValue().equalsIgnoreCase(ConstantData.SELECT)) {
                        displayError("Please select level to continue");
                        return;
                    }

                    switch (rankingType) {
                        case Constants.VALUES.AREA_WISE_RANKING:
                            city = (KeyValue) spArea.getSelectedItem();

                            if (city.getValue().equalsIgnoreCase(ConstantData.SELECT)) {
                                displayError("Please select area to continue");
                                return;
                            }

                            tvTitle.setText(
                                    String.format(Locale.getDefault(),
                                            "Best %s %s institutes of %s",
                                            category.getValue().toLowerCase(), level.getValue(), city.getValue()
                                    )
                            );
                            fetchRankDetails(city.getValue(), category.getValue(), level.getValue());
                            break;
                        case Constants.VALUES.BOARD_WISE_RANKING:
                            board = (KeyValue) spBoard.getSelectedItem();

                            if (board.getValue().equalsIgnoreCase(ConstantData.SELECT)) {
                                displayError("Please select board to continue");
                                return;
                            }

                            tvTitle.setText(
                                    String.format(Locale.getDefault(),
                                            "Best %s %s institutes of %s",
                                            category.getValue(), level.getValue(), board.getValue()
                                    )
                            );
                            fetchRankDetails(board.getValue(), category.getValue(), level.getValue());
                            break;
                    }

                    arrayList.clear();
                    adapter.notifyDataSetChanged();

                    viewData.setVisibility(View.GONE);

                    mBottomSheetDialog.dismiss();

                });

                mBottomSheetDialog = new BottomSheetDialog(this);
                mBottomSheetDialog.setContentView(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Objects.requireNonNull(mBottomSheetDialog.getWindow())
                            .addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }

            }

            if (mBottomSheetDialog != null) mBottomSheetDialog.show();


        }
        catch (Exception ignored) { }
    }

    private void fillSpinnerData() {

        levelArrayList.add(new KeyValue("", ConstantData.SELECT));
        levelArrayList.addAll(ConstantData.getInstance().getRankingLevel());

        categoryArrayList.add(new KeyValue("", ConstantData.SELECT));
        categoryArrayList.addAll(ConstantData.getInstance().getRankingCategory());

        if (!isSpinnerFilled) {

            KeyValueSpinnerAdapter levelAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, levelArrayList);
            KeyValueSpinnerAdapter categoryAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, categoryArrayList);

            switch (rankingType) {
                case Constants.VALUES.AREA_WISE_RANKING:
                    KeyValueSpinnerAdapter cityAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, cityArrayList);
                    spArea.setAdapter(cityAdapter);
                    viewArea.setVisibility(View.VISIBLE);
                    break;
                case Constants.VALUES.BOARD_WISE_RANKING:
                    KeyValueSpinnerAdapter boardAdapter = new KeyValueSpinnerAdapter(this, R.layout.layout_spinner, boardArrayList);
                    spBoard.setAdapter(boardAdapter);
                    viewBoard.setVisibility(View.VISIBLE);
                    break;
            }

            spLevel.setAdapter(levelAdapter);
            spCategory.setAdapter(categoryAdapter);

            isSpinnerFilled = true;

        }

    }

    private void fetchRankDetails(String board, String category, String level) {
        if (Internet.isConnected(this)) {
            switch (rankingType) {
                case Constants.VALUES.AREA_WISE_RANKING:
                    //board,category,level
                    presenter.getData(AppEnums.AREA_WISE_RANKING, board, category, level);
                    break;
                case Constants.VALUES.BOARD_WISE_RANKING:
                    presenter.getData(AppEnums.BOARD_WISE_RANKING, board, category, level);
                    break;
                default:
                    break;
            }
        } else {
            Internet.showOfflineDialog(this);
        }

        prepareDataForSelectedFilterRecyclerView();
    }

    private void prepareDataForSelectedFilterRecyclerView() {
        selectedFilters.clear();

        if (category != null) {
            if (!category.getValue().equalsIgnoreCase(ConstantData.SELECT))
                selectedFilters.add(new FilterModel(0, category.getValue(), SearchFilter.CATEGORY));
        }

        if (level != null) {
            if (!level.getValue().equalsIgnoreCase(ConstantData.SELECT))
                selectedFilters.add(new FilterModel(0, level.getValue(), SearchFilter.LEVEL));
        }

        switch (rankingType) {
            case Constants.VALUES.AREA_WISE_RANKING:
                if (city != null) {
                    if (!city.getValue().equalsIgnoreCase(ConstantData.SELECT))
                        selectedFilters.add(new FilterModel(0, city.getValue(), SearchFilter.CITY));
                }
                break;
            case Constants.VALUES.BOARD_WISE_RANKING:
                if (board != null) {
                    if (!board.getValue().equalsIgnoreCase(ConstantData.SELECT))
                        selectedFilters.add(new FilterModel(0, board.getValue(), SearchFilter.BOARD));
                }
                break;
        }

        selectedFilterRecycler.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        if (enums.equals(AppEnums.AREA_WISE_RANKING) || enums.equals(AppEnums.BOARD_WISE_RANKING)) {
            loader.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayData(String response) {

        try {

            CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);
            switch (caseModel.getCase()) {
                case Constants.Cases.RANK_CITY:
                    RankingAreaModel rankingAreaModel = new Gson().fromJson(response, RankingAreaModel.class);

                    cityArrayList.add(new KeyValue("", ConstantData.SELECT));

                    if (rankingAreaModel.getProvinces() != null && rankingAreaModel.getProvinces().size() > 0) {
                        for (RankingAreaModel.ProvincesBean bean : rankingAreaModel.getProvinces()) {
                            cityArrayList.add(new KeyValue("", bean.getProvince()));
                        }
                    }

                    if (rankingAreaModel.getDistricts() != null && rankingAreaModel.getDistricts().size() > 0) {
                        for (RankingAreaModel.DistrictsBean bean : rankingAreaModel.getDistricts()) {
                            cityArrayList.add(new KeyValue("", bean.getDistrict()));
                        }
                    }

                    fetchRankDetails("punjab", "large", "matric");

                    break;

                case Constants.Cases.RANK_BOARD:
                    RankingBoardModel boardModel = new Gson().fromJson(response, RankingBoardModel.class);

                    boardArrayList.add(new KeyValue("", ConstantData.SELECT));

                    if (boardModel.getData() != null && boardModel.getData().size() > 0) {
                        for (RankingBoardModel.DataBean bean : boardModel.getData()) {
                            boardArrayList.add(new KeyValue("", bean.getBoard()));
                        }
                    }

                    fetchRankDetails("FBISE, Islamabad", "large", "matric");

                    break;

                case Constants.Cases.RANK_DETAIL_LIST:
                    RankingDetailsModel rankingDetailsModel = new Gson().fromJson(response, RankingDetailsModel.class);

                    if (rankingDetailsModel != null && rankingDetailsModel.getData() != null && rankingDetailsModel.getData().size() > 0) {

                        viewNoData.setVisibility(View.GONE);
                        viewData.setVisibility(View.VISIBLE);

                        RankingDetailsModel.DataBean firstData = rankingDetailsModel.getData().get(0);
                        tv_first_institute_name.setText(firstData.getName());
                        tv_first_board.setText(firstData.getBoard());
                        tv_first_score.setText(String.format(Locale.getDefault(), "Score: %s", firstData.getFinalScore()));

                        if (rankingDetailsModel.getData().size() > 1) {
                            RankingDetailsModel.DataBean secondData = rankingDetailsModel.getData().get(1);
                            tv_second_institute_name.setText(secondData.getName());
                            tv_second_board.setText(secondData.getBoard());
                            tv_second_score.setText(String.format(Locale.getDefault(), "Score: %s", secondData.getFinalScore()));
                        } else {
                            cardSecond.setVisibility(View.GONE);
                        }
                        if (rankingDetailsModel.getData().size() > 2) {
                            RankingDetailsModel.DataBean thirdData = rankingDetailsModel.getData().get(2);
                            tv_third_institute_name.setText(thirdData.getName());
                            tv_third_board.setText(thirdData.getBoard());
                            tv_third_score.setText(String.format(Locale.getDefault(), "Score: %s", thirdData.getFinalScore()));
                        } else {
                            cardThird.setVisibility(View.GONE);
                        }

                        arrayList.addAll(rankingDetailsModel.getData());
                        adapter.notifyDataSetChanged();

                    } else {
                        viewNoData.setVisibility(View.VISIBLE);
                        viewData.setVisibility(View.GONE);
                    }
            }

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFilterSelected(SearchFilter searchFilter) {
        showBottomSheet();
        new Handler().postDelayed(() -> handleBottomSheetViewsOnFilterSelection(searchFilter), 100);
    }

    private void handleBottomSheetViewsOnFilterSelection(SearchFilter searchFilter) {
        switch (searchFilter) {
            case LEVEL:
                spLevel.performClick();
                break;
            case CATEGORY:
                spCategory.performClick();
                break;
            case AREA:
                spArea.performClick();
                break;
            case BOARD:
                spBoard.performClick();
                break;
            default:
                break;
        }
    }

    @Override
    public void onFilterDelete(FilterModel data) {
        switch (data.getSearchFilter()) {
            case LEVEL:
                spLevel.setSelection(0);
                break;
            case CATEGORY:
                spCategory.setSelection(0);
                break;
            case CITY:
                spArea.setSelection(0);
                break;
            case BOARD:
                spBoard.setSelection(0);
            default:
                break;
        }
        btnSearchBottomSheet.performClick();
    }
}