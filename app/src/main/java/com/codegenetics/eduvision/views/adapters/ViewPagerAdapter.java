package com.codegenetics.eduvision.views.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.HomeSlider.HomeSliderModel;
import com.github.chrisbanes.photoview.PhotoView;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {

    private final ArrayList<HomeSliderModel.HomeSlidersData> arrayList;
    private final IViewClickListener listener;

    public ViewPagerAdapter(ArrayList<HomeSliderModel.HomeSlidersData> arrayList, IViewClickListener clickListener) {
        this.arrayList = arrayList;
        this.listener = clickListener;
    }

    @Override
    public int getCount() {
        if (arrayList != null) return arrayList.size();
        else return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        HomeSliderModel.HomeSlidersData data = arrayList.get(position);

        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.view_pager_images_full_screen, container, false);

        final PhotoView photoView = layout.findViewById(R.id.viewPager_ivImage);
        final MKLoader loader = layout.findViewById(R.id.viewPager_loader);
        layout.findViewById(R.id.view_dark).setVisibility(View.GONE);

        photoView.setEnabled(false);

        if (listener != null) {

            Glide.with(photoView)
                    .load(data.getSlide())
                    .placeholder(R.color.gray_light1)
                    .error(R.drawable.ic_no_image)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            new Handler().post(() -> loader.setVisibility(View.GONE));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            loader.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .signature(new ObjectKey(data.getSlide()))
                    .into(photoView);


//            photoView.setOnClickListener(v -> listener.onItemClicked(position));

        } else {

            photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);

            Glide.with(photoView)
                    .load(data.getSlide())
                    .placeholder(R.color.black)
                    .error(R.drawable.ic_no_image)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            new Handler().post(() -> loader.setVisibility(View.GONE));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            loader.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .signature(new ObjectKey(data.getSlide()))
                    .into(photoView);

        }

        container.addView(layout);

        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
