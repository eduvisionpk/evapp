package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.IDisciplineClickListener;
import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel;
import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel.DisciplinesData;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.activities.HomeActivity;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Locale;

public class DisciplineSub extends AppCompatActivity implements
        IData, IDisciplineClickListener {

    private Button btnDone;
    private View viewNoData, viewDisciplines;
    private MKLoader loader;
    private DisciplineSubRecyclerAdapter adapter;
    private final ArrayList<DisciplinesData> arrayList = new ArrayList<>();
    private ArrayList<String> arrayListChecked = new ArrayList<>();

    private ServerPresenter presenter;
    private String levelCode = "", disciplineType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discipline_sub);

        initViews();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(disciplineType);
        }

        fetchTypes();

        btnDone.setOnClickListener(v -> {
            if (arrayListChecked.size() > 0) {
                PlannerInfoSession.getInstance().setDisciplineTypes(arrayListChecked);
                startActivity(new Intent(this, ConfirmRequirements.class)
                        .putExtra(IntentEnums.LEVEL_CODE.name(), getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name()))
                        .putExtra(IntentEnums.LEVEL_NAME.name(), getIntent().getStringExtra(IntentEnums.LEVEL_NAME.name()))
                        .putExtra(IntentEnums.DATA.name(), arrayListChecked)
                );
                finishAffinity();
            } else {
                displayError("Please choose at least 1 discipline.");
            }
        });

    }

    private void initViews() {

        disciplineType = getIntent().getStringExtra(IntentEnums.DISCIPLINE_TYPE.name());
        levelCode = getIntent().getStringExtra(IntentEnums.LEVEL_CODE.name());
        arrayListChecked = getIntent().getStringArrayListExtra(IntentEnums.DATA.name());

        adapter = new DisciplineSubRecyclerAdapter(arrayList, arrayListChecked, this);

        RecyclerView rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        btnDone = findViewById(R.id.btn_done);
        viewNoData = findViewById(R.id.no_data);
        viewDisciplines = findViewById(R.id.view_program_disciplines);
        loader = findViewById(R.id.loader);

        presenter = ServerPresenter.getInstance(this);

    }

    private void fetchTypes() {
        if (Internet.isConnected(this)) {
            presenter.getData(
                    AppEnums.DISCIPLINES_PROGRAMS,
                    disciplineType,
                    levelCode
            );
        } else {
            Internet.showOfflineDialog(this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {
            if (!response.isEmpty()) {

                DisciplinesModel disciplinesModel = new Gson().fromJson(response, DisciplinesModel.class);

                if (disciplinesModel.isSuccess()) {
                    if (disciplinesModel.getData() != null) {

                        arrayList.addAll(disciplinesModel.getData());
                        adapter.notifyDataSetChanged();

                        viewDisciplines.setVisibility(View.VISIBLE);
                        viewNoData.setVisibility(View.GONE);

                    }
                    else {
                        viewDisciplines.setVisibility(View.GONE);
                        viewNoData.setVisibility(View.VISIBLE);
                    }

                }
                else {
                    displayError(disciplinesModel.getMessage());
                }

            }
        }
        catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClicked(String disciplineType, boolean isChecked) {
        if (isChecked) {
            arrayListChecked.add(String.format(Locale.getDefault(), "%s#%s",
                    disciplineType,
                    getIntent().getStringExtra(IntentEnums.DISCIPLINE_TYPE.name())));
            adapter.notifyDataSetChanged();
        } else {
            arrayListChecked.remove(String.format(Locale.getDefault(), "%s#%s",
                    disciplineType,
                    getIntent().getStringExtra(IntentEnums.DISCIPLINE_TYPE.name())));
        }
    }
}