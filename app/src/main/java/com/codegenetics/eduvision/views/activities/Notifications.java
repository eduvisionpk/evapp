package com.codegenetics.eduvision.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.Notifications.NotificationsMainModel;
import com.codegenetics.eduvision.models.Notifications.NotificationsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.NotificationsMainAdapter;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class Notifications extends AppCompatActivity implements IData {

    private MKLoader loader;
    private ServerPresenter presenter;
    private ArrayList<NotificationsMainModel> arrayList;
    private NotificationsMainAdapter adapter;

    private void init() {

        arrayList = new ArrayList<>();
        adapter = new NotificationsMainAdapter(arrayList);

        presenter = ServerPresenter.getInstance(this);
        loader = findViewById(R.id.loader);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        init();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.notifications);
        }

        fetchNotifications();

    }

    private void fetchNotifications() {
        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.NOTIFICATIONS);
        } else {
            Internet.showOfflineDialog(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {

        try {

            if (!response.isEmpty()) {
                NotificationsModel model = new Gson().fromJson(response, NotificationsModel.class);

                if (model.isSuccess()) {

                    if (
                            model.getData() != null && model.getNotificationTypes() != null &&
                                    model.getData().size() > 0 && model.getNotificationTypes().size() > 0
                    ) {

                        for (NotificationsModel.NotificationsTypes notificationsTypes : model.getNotificationTypes()) {

                            ArrayList<NotificationsModel.NotificationsData> notificationsDataArrayList = new ArrayList<>();

                            for (NotificationsModel.NotificationsData notificationsData : model.getData()) {

                                if (notificationsTypes.getType().equalsIgnoreCase(notificationsData.getType())) {
                                    notificationsDataArrayList.add(notificationsData);
                                }

                            }

                            arrayList.add(new NotificationsMainModel(notificationsTypes.getType(), notificationsDataArrayList));

                        }
                    }

                    adapter.notifyDataSetChanged();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

}