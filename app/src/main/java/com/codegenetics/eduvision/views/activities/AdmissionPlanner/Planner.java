package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.helper.DateTimePicker;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.models.Districts.DistrictsModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.activities.HomeActivity;
import com.codegenetics.eduvision.views.adapters.CitiesSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.DistrictsSpinnerAdapter;
import com.codegenetics.eduvision.views.adapters.LevelsSpinnerAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class Planner extends AppCompatActivity implements IData {

//    private static final String TAG = "PlannerActivity";

    private View viewStep1, viewStep2;
    private MKLoader loaderDomicile;
    private Button btnContinue1, btnContinue2, btnCancel;
    private EditText etName, etAddress, etObtainedMarks, etTotalMarks, etPassingYear;
    private AutoCompleteTextView etCity, etDomicile;
    private Spinner spDegree;
    private TextView tvName, tvAddress, tvCity, tvDomicile, tvDob, etDob;
    private TextView tv_personal_info1, tv_personal_info2, tvOneStep1, tvOneStep2;
    private final ArrayList<CitiesModel.CitiesData> citiesDataArrayList = new ArrayList<>();
    private final ArrayList<DistrictsModel.DistrictsData> districtsDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planner);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.step_1_of_3);
        }

        initViews();
        clickEvents();
        fetchDistrictsOfDomicile();

        if (!etName.getText().toString().isEmpty()) btnContinue1.performClick();

    }

    private void initViews() {
        viewStep1 = findViewById(R.id.view_step_1);
        viewStep2 = findViewById(R.id.view_step_2);
        btnContinue1 = findViewById(R.id.btn_continue_step_1);
        btnContinue2 = findViewById(R.id.btn_continue_step_2);
        btnCancel = findViewById(R.id.btn_cancel);

        loaderDomicile = findViewById(R.id.loader_domicile);

        etName = findViewById(R.id.et_name);
        etAddress = findViewById(R.id.et_address);
        etCity = findViewById(R.id.et_city);
        etDomicile = findViewById(R.id.et_domicile);
        etDob = findViewById(R.id.et_dob);
        etObtainedMarks = findViewById(R.id.et_obtained_marks);
        etTotalMarks = findViewById(R.id.et_total_marks);
        etPassingYear = findViewById(R.id.et_passing_year);
        tvName = findViewById(R.id.tv_name);
        tvAddress = findViewById(R.id.tv_address);
        tvCity = findViewById(R.id.tv_city);
        tvDomicile = findViewById(R.id.tv_domicile);
        tvDob = findViewById(R.id.tv_dob);
        spDegree = findViewById(R.id.sp_degree);
        tv_personal_info1 = findViewById(R.id.tv_personal_info1);
        tv_personal_info2 = findViewById(R.id.tv_personal_info2);
        tvOneStep1 = findViewById(R.id.tv_one_step_1);
        tvOneStep2 = findViewById(R.id.tv_one_step_2);

        etName.setText(PlannerInfoSession.getInstance().getName());
        etAddress.setText(PlannerInfoSession.getInstance().getAddress());
        etCity.setText(PlannerInfoSession.getInstance().getCity());
        etDomicile.setText(PlannerInfoSession.getInstance().getDomicile());
        etDob.setText(PlannerInfoSession.getInstance().getDob());
        etObtainedMarks.setText(PlannerInfoSession.getInstance().getObtainedMarks());
        etTotalMarks.setText(PlannerInfoSession.getInstance().getTotalMarks());
        etPassingYear.setText(PlannerInfoSession.getInstance().getPassingYear());

        citiesDataArrayList.addAll(Constants.citiesList);
        CitiesSpinnerAdapter citiesSpinnerAdapter = new CitiesSpinnerAdapter(this, R.layout.layout_spinner, citiesDataArrayList);
        etCity.setAdapter(citiesSpinnerAdapter);

        LevelsSpinnerAdapter levelsSpinnerAdapter = new LevelsSpinnerAdapter(this, R.layout.layout_spinner, Constants.levelsList);
        spDegree.setAdapter(levelsSpinnerAdapter);
        for (int i = 0; i < Constants.levelsList.size(); i++) {
            if (Constants.levelsList.get(i).getLevelName().equals(PlannerInfoSession.getInstance().getDegree())) {
                spDegree.setSelection(i);
                break;
            } else {
                spDegree.setSelection(0);
            }
        }

    }

    private void clickEvents() {
        btnContinue1.setOnClickListener(v -> {
            if (isStepOneValidated()) {
                PlannerInfoSession.getInstance().setName(etName.getText().toString());
                PlannerInfoSession.getInstance().setAddress(etAddress.getText().toString());
                PlannerInfoSession.getInstance().setCity(etCity.getText().toString());
                PlannerInfoSession.getInstance().setDomicile(etDomicile.getText().toString());
                PlannerInfoSession.getInstance().setDob(etDob.getText().toString());
                switchStep(2);
            }
        });

        btnContinue2.setOnClickListener(v -> {
            if (isStepTwoValidated()) {
                PlannerInfoSession.getInstance().setObtainedMarks(etObtainedMarks.getText().toString());
                PlannerInfoSession.getInstance().setTotalMarks(etTotalMarks.getText().toString());
                PlannerInfoSession.getInstance().setPassingYear(etPassingYear.getText().toString());
                PlannerInfoSession.getInstance().setDegree(Constants.levelsList.get(spDegree.getSelectedItemPosition()).getLevelName());
                startActivity(new Intent(this, AddDiscipline.class));
            }
        });

        etDob.setOnClickListener(v -> DateTimePicker.pickProfileDate(this, etDob));

        btnCancel.setOnClickListener(v -> switchStep(1));

        tvOneStep1.setOnClickListener(v -> switchStep(1));

        tvOneStep2.setOnClickListener(v -> switchStep(1));

        tv_personal_info1.setOnClickListener(v -> switchStep(1));

        tv_personal_info2.setOnClickListener(v -> switchStep(1));

    }

    private void fetchDistrictsOfDomicile() {
        if (Internet.isConnected(this)) {
            ServerPresenter.getInstance(this).getData(AppEnums.DISTRICTS);
        } else {
            Internet.showOfflineDialog(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    private void switchStep(int step) {
        switch (step) {
            case 1:
                viewStep1.setVisibility(View.VISIBLE);
                viewStep2.setVisibility(View.GONE);
                break;
            case 2:
                viewStep2.setVisibility(View.VISIBLE);
                viewStep1.setVisibility(View.GONE);
                tvName.setText(etName.getText().toString());
                tvAddress.setText(etAddress.getText().toString());
                tvCity.setText(etCity.getText().toString());
                tvDomicile.setText(etDomicile.getText().toString());
                tvDob.setText(etDob.getText().toString());
                break;
        }
    }

    private boolean isStepOneValidated() {
        if (etName.getText().toString().isEmpty()) {
            etName.setError("Name Required");
            return false;
        } else if (etAddress.getText().toString().isEmpty()) {
            etAddress.setError("Address Required");
            return false;
        } else if (etCity.getText().toString().isEmpty()) {
            etCity.setError("City Required");
            return false;
        } else if (etDomicile.getText().toString().isEmpty()) {
            etDomicile.setError("Domicile Required");
            return false;
        } else if (etDob.getText().toString().isEmpty()) {
            etDomicile.setError("DOB Required");
            return false;
        }
        return true;
    }

    private boolean isStepTwoValidated() {
        if (etObtainedMarks.getText().toString().isEmpty()) {
            etObtainedMarks.setError("Field Required");
            return false;
        }
        if (etTotalMarks.getText().toString().isEmpty()) {
            etTotalMarks.setError("Field Required");
            return false;
        }
        if (etPassingYear.getText().toString().isEmpty()) {
            etPassingYear.setError("Field Required");
            return false;
        }

        return true;
    }

    @Override
    public void showProgress(AppEnums enums) {
        loaderDomicile.setVisibility(View.VISIBLE);
        etDomicile.setEnabled(false);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loaderDomicile.setVisibility(View.GONE);
        etDomicile.setEnabled(true);
    }

    @Override
    public void displayData(String response) {
        try {
            DistrictsModel model = new Gson().fromJson(response, DistrictsModel.class);

            if (model.isSuccess()) {

                if (model.getDistricts() != null && model.getDistricts().size() > 0) {
                    districtsDataArrayList.addAll(model.getDistricts());
                    DistrictsSpinnerAdapter districtsSpinnerAdapter = new DistrictsSpinnerAdapter(this, R.layout.layout_spinner, districtsDataArrayList);
                    etDomicile.setAdapter(districtsSpinnerAdapter);
                }

            } else {
                displayError(model.getMessage());
            }

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}