package com.codegenetics.eduvision.views.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.models.MeritCalculator.MeritCalculatorModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.MeritCalculatorDetailRecycler;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

public class MeritCalculatorDetails extends AppCompatActivity implements IData {
    private final String TAG = MeritCalculatorDetails.class.getSimpleName();
    private String fscOM, fscTM, metricOM, metricTM, percentage, instituteId;
    private ServerPresenter presenter;
    private TextView tvScore;
    private RecyclerView rvInstituteList;
    private MKLoader loader;
    private View viewMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merit_calculator_details);

        init();

    }

    private void init() {
        tvScore = findViewById(R.id.tv_score);
        rvInstituteList = findViewById(R.id.rv_institutes);
        loader = findViewById(R.id.loader);
        viewMain = findViewById(R.id.view_main);
        viewMain.setVisibility(View.GONE);
        presenter = ServerPresenter.getInstance(this);

        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            fscOM = bundle.getString(IntentEnums.FSC_OM.name(), "");
            fscTM = bundle.getString(IntentEnums.FSC_TM.name(), "");
            metricOM = bundle.getString(IntentEnums.METRIC_OM.name(), "");
            metricTM = bundle.getString(IntentEnums.METRIC_TM.name(), "");
            percentage = bundle.getString(IntentEnums.PERCENTAGE.name(), "");
            instituteId = bundle.getString(IntentEnums.INSTITUTE_ID.name(), "");

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle(bundle.getString(IntentEnums.INSTITUTE.name(), "Merit Calculator"));
            }

            calculateMerit();

        }
    }

    private void calculateMerit() {
        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.MERIT_CALCULATOR,
                    metricOM.trim(), /* metric obtained marks */
                    metricTM.trim(), /* metric total marks*/
                    fscOM.trim(), /* fsc obtained marks */
                    fscTM.trim(), /* fsc total marks */
                    percentage.trim(), /* entry test percentage */
                    instituteId /* institute */
            );
        }
        else {
            Internet.showOfflineDialog(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        try {
            if (!response.isEmpty()) {
                MeritCalculatorModel meritCalculatorModel = new Gson().fromJson(response, MeritCalculatorModel.class);

                tvScore.setText(String.format("Your aggregate score is %s", meritCalculatorModel.getAggregate()));

                rvInstituteList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

                if (meritCalculatorModel.getData() != null && meritCalculatorModel.getData().size() > 0) {
                    rvInstituteList.setAdapter(new MeritCalculatorDetailRecycler(meritCalculatorModel.getData()));
                    viewMain.setVisibility(View.VISIBLE);
                }

            }
        } catch (Exception e) {
            Log.e(TAG, "displayData: ", e);
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}