package com.codegenetics.eduvision.views.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.AppEnums;
import com.codegenetics.eduvision.interfaces.IData;
import com.codegenetics.eduvision.interfaces.ITabClickListener;
import com.codegenetics.eduvision.models.Case.CaseModel;
import com.codegenetics.eduvision.models.EntryTest.EntryTestCategories;
import com.codegenetics.eduvision.models.EntryTest.EntryTestModel;
import com.codegenetics.eduvision.presenter.ServerPresenter;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Internet;
import com.codegenetics.eduvision.views.adapters.EntryTestRecycler;
import com.codegenetics.eduvision.views.adapters.EntryTestTabRecycler;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

public class EntryTest extends AppCompatActivity implements IData, ITabClickListener {

    private RecyclerView rvTabs;
    private ServerPresenter presenter;
    private final List<EntryTestCategories.DataBean> categoryList = new ArrayList<>();
    private final List<EntryTestModel.EntryTestData> entryTestDataList = new ArrayList<>();
    private EntryTestTabRecycler tabRecyclerAdapter;
    private EntryTestRecycler entryTestRecyclerAdapter;
    private MKLoader loader;
    private View viewNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_test);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.entry_tests);
        }

        initViews();

        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.ENTRY_TEST_CATEGORIES);
        } else {
            Internet.showOfflineDialog(this);
        }

    }

    private void initViews() {
        presenter = ServerPresenter.getInstance(this);

        viewNoData = findViewById(R.id.no_data);
        loader = findViewById(R.id.loader);

        rvTabs = findViewById(R.id.rv_tabs);
        rvTabs.setHasFixedSize(true);
        rvTabs.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        tabRecyclerAdapter = new EntryTestTabRecycler(categoryList, this);
        rvTabs.setAdapter(tabRecyclerAdapter);

        RecyclerView rvEntryTest = findViewById(R.id.rv_entry_test);
        rvEntryTest.setHasFixedSize(true);
        rvEntryTest.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        entryTestRecyclerAdapter = new EntryTestRecycler(entryTestDataList);
        rvEntryTest.setAdapter(entryTestRecyclerAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        HomeActivity.setMenus(this, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress(AppEnums enums) {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress(AppEnums enums) {
        if (enums == AppEnums.ENTRY_TEST) loader.setVisibility(View.GONE);
    }

    @Override
    public void displayData(String response) {
        if (response != null) {

            CaseModel caseModel = new Gson().fromJson(response, CaseModel.class);

            switch (caseModel.getCase()) {
                case Constants.Cases.ENTRY_TEST_CATEGORIES:
                    EntryTestCategories testCategories = new Gson().fromJson(response, EntryTestCategories.class);
                    if (testCategories != null && testCategories.isSuccess()) {
                        if (testCategories.getData()!=null && testCategories.getData().size() > 0) {

                            categoryList.clear();
                            categoryList.addAll(testCategories.getData());
                            rvTabs.setAdapter(tabRecyclerAdapter);

                            if (Internet.isConnected(this)) {
                                presenter.getData(AppEnums.ENTRY_TEST, categoryList.get(0).getType());
                            } else {
                                Internet.showOfflineDialog(this);
                            }

                        }
                    }
                    break;

                case Constants.Cases.ENTRY_TEST:
                    EntryTestModel entryTestModel = new Gson().fromJson(response, EntryTestModel.class);
                    if (entryTestModel != null && entryTestModel.isSuccess()) {
                        if (entryTestModel.getData() != null && entryTestModel.getData().size() > 0) {
                            entryTestDataList.clear();
                            entryTestDataList.addAll(entryTestModel.getData());
                        }

                        viewNoData.setVisibility(entryTestDataList.size() > 0 ? View.GONE : View.VISIBLE);
                        entryTestRecyclerAdapter.notifyDataSetChanged();
                    }
            }
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTabsSelected(String s) {
        entryTestDataList.clear();
        entryTestRecyclerAdapter.notifyDataSetChanged();

        Constants.log(this, s);

        if (Internet.isConnected(this)) {
            presenter.getData(AppEnums.ENTRY_TEST, s);
        } else {
            Internet.showOfflineDialog(this);
        }

    }
}