package com.codegenetics.eduvision.views.activities.AdmissionPlanner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IDisciplineClickListener;
import com.codegenetics.eduvision.models.Disciplines.DisciplinesModel.DisciplinesData;

import java.util.ArrayList;

public class DisciplineSubRecyclerAdapter extends RecyclerView.Adapter<DisciplineSubRecyclerAdapter.DisciplineSubHolder> {

    private final ArrayList<DisciplinesData> arrayList;
    private final ArrayList<String> arrayListChecked;
    private final IDisciplineClickListener listener;

    public DisciplineSubRecyclerAdapter(ArrayList<DisciplinesData> arrayList, ArrayList<String> arrayListChecked, IDisciplineClickListener listener) {
        this.arrayListChecked = arrayListChecked;
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DisciplineSubHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DisciplineSubHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_discipline_sub, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DisciplineSubHolder holder, int position) {
        holder.bind(arrayList.get(holder.getAdapterPosition()), arrayListChecked);
        holder.itemView.setOnClickListener(v -> {
            if (arrayListChecked.size() < 3 || holder.cb.isChecked()) {
                holder.cb.setChecked(!holder.cb.isChecked());
                listener.onItemClicked(holder.cb.getText().toString(), holder.cb.isChecked());
            } else {
                Toast.makeText(v.getContext(), "You can't choose more than 3 disciplines.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    static class DisciplineSubHolder extends RecyclerView.ViewHolder {

        private final CheckBox cb;

        public DisciplineSubHolder(@NonNull View itemView) {
            super(itemView);
            cb = itemView.findViewById(R.id.cb);
        }

        public void bind(DisciplinesData data, ArrayList<String> arrayListChecked) {
            cb.setText(data.getDisciplineName());
            boolean checked = false;
            for (String item : arrayListChecked) {
                if (item.split("#")[0].equalsIgnoreCase(data.getDisciplineName())) checked = true;
            }
            cb.setChecked(checked);
        }
    }
}
