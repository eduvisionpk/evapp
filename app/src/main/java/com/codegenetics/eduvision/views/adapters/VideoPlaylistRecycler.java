package com.codegenetics.eduvision.views.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IVideoPLayList;
import com.codegenetics.eduvision.models.Videos.VideoDetailsModel;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import static com.codegenetics.eduvision.views.adapters.VideoPlaylistRecycler.*;

public class VideoPlaylistRecycler extends RecyclerView.Adapter<VideoPlaylistHolder> {

    private final ArrayList<VideoDetailsModel.VideoDetailsData> videoPlaylistArrayList;
    private final IVideoPLayList iVideoPLayList;

    public VideoPlaylistRecycler(ArrayList<VideoDetailsModel.VideoDetailsData> videoPlaylistArrayList, IVideoPLayList iVideoPLayList) {
        this.videoPlaylistArrayList = videoPlaylistArrayList;
        this.iVideoPLayList = iVideoPLayList;
    }

    @NonNull
    @Override
    public VideoPlaylistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoPlaylistHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_video_playlist, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VideoPlaylistHolder holder, int position) {
        VideoDetailsModel.VideoDetailsData data = videoPlaylistArrayList.get(position);

        Glide.with(holder.itemView)
                .load(data.getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.loader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.loader.setVisibility(View.GONE);
                        return false;
                    }
                })
                .placeholder(R.color.gray_light1)
                .error(R.drawable.ic_no_image)
                .into(holder.ivThumbnail);

        holder.tvTitle.setText(data.getTitle());
        holder.tvDescription.setText(data.getType());

        holder.itemView.setOnClickListener(v -> iVideoPLayList.onVideoSelected(data));
    }

    @Override
    public int getItemCount() {
        return videoPlaylistArrayList.size();
    }

    public static class VideoPlaylistHolder extends RecyclerView.ViewHolder {

        private final ImageView ivThumbnail;
        private final MKLoader loader;
        private final TextView tvTitle, tvDescription;

        public VideoPlaylistHolder(@NonNull View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.iv_thumbnail);
            tvTitle = itemView.findViewById(R.id.tv_title);
            loader = itemView.findViewById(R.id.loader);
            tvDescription = itemView.findViewById(R.id.tv_description);
        }
    }
}
