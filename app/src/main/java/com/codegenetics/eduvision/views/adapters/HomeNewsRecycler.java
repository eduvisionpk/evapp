package com.codegenetics.eduvision.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.interfaces.IViewClickListener;
import com.codegenetics.eduvision.models.HomeSlider.HomeSliderModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HomeNewsRecycler extends RecyclerView.Adapter<HomeNewsRecycler.HomeNewsHolder> {
    private final ArrayList<HomeSliderModel.HomeNewsScholarshipsData> arrayList;
    private final IViewClickListener listener;

    public HomeNewsRecycler(ArrayList<HomeSliderModel.HomeNewsScholarshipsData> arrayList, IViewClickListener listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public HomeNewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HomeNewsHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_home_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HomeNewsHolder holder, int position) {
        HomeSliderModel.HomeNewsScholarshipsData data = arrayList.get(position);

        holder.tvTitle.setText(data.getTitletext());
        holder.tvDate.setText(data.getTime());

        Glide.with(holder.itemView)
                .load(data.getImage())
                .placeholder(R.drawable.ic_dummy)
                .error(R.drawable.ic_dummy)
                .into(holder.ivLogo);
        
        holder.itemView.setOnClickListener(v -> listener.onItemClicked(position));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class HomeNewsHolder extends RecyclerView.ViewHolder {
        ImageView ivLogo;
        TextView tvTitle, tvDate;

        public HomeNewsHolder(@NonNull View itemView) {
            super(itemView);
            ivLogo = itemView.findViewById(R.id.iv_logo);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
