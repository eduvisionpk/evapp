package com.codegenetics.eduvision.models.Admissions;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdmissionsModel {

    /**
     * success : true
     * message : success
     * records : 12
     * case : disciplines
     * data : [{"institute_id":"1536204462","ins_logo":"https://www.eduvision.edu.pk/images_post/nust.jpg","ins_abb":"HIHS","city_name":"ISLAMABAD","name":"HARMAIN INSTITUTE OF HEALTH SCIENCES","deadline":"2020-09-30","level_code":"11","level_name":"DIPLOMA / CERT (After Matric)","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/harmain-institute-of-health-sciences-islamabad-admission-6-9-20.jpg"},{"institute_id":"672","ins_logo":"https://www.eduvision.edu.pk/uploads/logo/1821496746014490.jpg","ins_abb":"RIU","city_name":"ISLAMABAD","name":"RIPHAH INTERNATIONAL UNIVERSITY","deadline":"2020-09-30","level_code":"3","level_name":"MS / M.PHIL (18 years)","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/riphah-international-university-isb-admission-13-9-20.jpg"},{"institute_id":"1483554624","ins_logo":"https://www.eduvision.edu.pk/images_post/nust.jpg","ins_abb":"MCPT","city_name":"ISLAMABAD","name":"MILLAT COLLEGE OF PHARMACY TECHNECIAN","deadline":"2020-10-03","level_code":"11","level_name":"DIPLOMA / CERT (After Matric)","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/millat-college-of-pharmacy-technician-islamabad-admission-13-9-20.jpg"},{"institute_id":"2797","ins_logo":"https://www.eduvision.edu.pk/uploads/logo/3381331209709754.jpg","ins_abb":"SCN","city_name":"ISLAMABAD","name":"SHIFA COLLEGE OF NURSING","deadline":"2020-10-09","level_code":"7","level_name":"BACHELOR","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/shifa-college-of-nursing-islamabad-admission-18-5-20.jpg"},{"institute_id":"1600664121","ins_logo":"https://www.eduvision.edu.pk/images_post/nust.jpg","ins_abb":"DIHS","city_name":"ISLAMABAD","name":"DAKSON INSTITUTE OF HEALTH SCIENCES","deadline":"2020-10-15","level_code":"11","level_name":"DIPLOMA / CERT (After Matric)","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/dakson-institute-of-health-sciences-islamabad-admission-20-9-20.jpg"},{"institute_id":"1360515204","ins_logo":"https://www.eduvision.edu.pk/uploads/logo/1791478915850636.jpg","ins_abb":"STMU","city_name":"ISLAMABAD","name":"SHIFA TAMEER-E-MILLAT UNIVERSITY","deadline":"2020-10-16","level_code":"3","level_name":"MS / M.PHIL (18 years)","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/stmu-isb-14-8-20.jpg"},{"institute_id":"1355153627","ins_logo":"https://www.eduvision.edu.pk/uploads/logo/2821478915514781.jpg","ins_abb":"AU","city_name":"ISLAMABAD","name":"ABASYN UNIVERSITY (SUB CAMPUS)","deadline":"2020-10-17","level_code":"7","level_name":"BACHELOR","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/abasyn-university-islamabad-admission-20-9-20.jpg"},{"institute_id":"1360515204","ins_logo":"https://www.eduvision.edu.pk/uploads/logo/1791478915850636.jpg","ins_abb":"STMU","city_name":"ISLAMABAD","name":"SHIFA TAMEER-E-MILLAT UNIVERSITY","deadline":"2020-10-23","level_code":"7","level_name":"BACHELOR","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/stmu-14-8-20.jpg"},{"institute_id":"821","ins_logo":"https://www.eduvision.edu.pk/uploads/logo/631328884537937.jpg","ins_abb":"FU","city_name":"ISLAMABAD","name":"FOUNDATION UNIVERSITY","deadline":"2020-10-31","level_code":"7","level_name":"BACHELOR","discipline_type":"Medical Sciences","institute_ad":"https://www.eduvision.edu.pk/admissions/foundation-university-college-of-nursing-islamabad-admission-27-9-20.jpg"}]
     */

    private boolean success;
    private String message;
    private String records;
    @SerializedName("case")
    private String caseX;
    private List<AdmissionsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<AdmissionsData> getData() {
        return data;
    }

    public void setData(List<AdmissionsData> data) {
        this.data = data;
    }

    public static class AdmissionsData {
        /**
         * institute_id : 1536204462
         * ins_logo : https://www.eduvision.edu.pk/images_post/nust.jpg
         * ins_abb : HIHS
         * city_name : ISLAMABAD
         * name : HARMAIN INSTITUTE OF HEALTH SCIENCES
         * deadline : 2020-09-30
         * level_code : 11
         * level_name : DIPLOMA / CERT (After Matric)
         * discipline_type : Medical Sciences
         * institute_ad : https://www.eduvision.edu.pk/admissions/harmain-institute-of-health-sciences-islamabad-admission-6-9-20.jpg
         */

        private String id;
        private String institute_id;
        private String ins_logo;
        private String ins_abb;
        private String city_name;
        private String name;
        private String deadline;
        private String level_code;
        private String level_name;
        private String discipline_type;
        private String discipline_name;
        private String institute_ad;
        private String apply_ev;

        public String getInstitute_id() {
            return institute_id;
        }

        public void setInstitute_id(String institute_id) {
            this.institute_id = institute_id;
        }

        public String getInstituteLogo() {
            return ins_logo;
        }

        public void setIns_logo(String ins_logo) {
            this.ins_logo = ins_logo;
        }

        public String getInstituteAbbrivation() {
            return ins_abb;
        }

        public void setIns_abb(String ins_abb) {
            this.ins_abb = ins_abb;
        }

        public String getCityName() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }

        public String getLevel_code() {
            return level_code;
        }

        public void setLevel_code(String level_code) {
            this.level_code = level_code;
        }

        public String getLevelName() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }

        public String getDisciplineType() {
            return discipline_type;
        }

        public void setDiscipline_type(String discipline_type) {
            this.discipline_type = discipline_type;
        }

        public String getDisciplineName() {
            return discipline_name;
        }

        public void setDiscipline_name(String discipline_name) {
            this.discipline_name = discipline_name;
        }

        public String getInstituteAd() {
            return institute_ad;
        }

        public void setInstitute_ad(String institute_ad) {
            this.institute_ad = institute_ad;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getApply_ev() {
            return apply_ev;
        }

        public void setApply_ev(String apply_ev) {
            this.apply_ev = apply_ev;
        }
    }
}
