package com.codegenetics.eduvision.models.PastPapers;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PastPapersDetailsModel {
    /**
     * success : true
     * message : success
     * case : Papers
     * data : [{"year1":"2015","file1":"","year2":"2016","file2":"","year3":"2017","file3":"","year4":"2018","file4":"https://www.eduvision.edu.pk/exam/past-papers/bise-bahawalpur-physics-10th.pdf","year5":"2019","file5":""},{"year1":"","file1":"","year2":"","file2":"","year3":"2018","file3":"https://www.eduvision.edu.pk/exam/past-papers/bise-bahawalpur-physics-10th-2018.pdf","year4":"","file4":"","year5":"","file5":""},{"year1":"","file1":"","year2":"","file2":"","year3":"","file3":"","year4":"2019","file4":"https://www.eduvision.edu.pk/exam/past-papers/bise-bahawalpur-physics-10th-2019.pdf","year5":"","file5":""}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * year1 : 2015
     * file1 :
     * year2 : 2016
     * file2 :
     * year3 : 2017
     * file3 :
     * year4 : 2018
     * file4 : https://www.eduvision.edu.pk/exam/past-papers/bise-bahawalpur-physics-10th.pdf
     * year5 : 2019
     * file5 :
     */

    private List<PastPapersDetailsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<PastPapersDetailsData> getData() {
        return data;
    }

    public void setData(List<PastPapersDetailsData> data) {
        this.data = data;
    }

    public static class PastPapersDetailsData {
        private String year1;
        private String file1;
        private String year2;
        private String file2;
        private String year3;
        private String file3;
        private String year4;
        private String file4;
        private String year5;
        private String file5;

        public String getYear1() {
            return year1;
        }

        public void setYear1(String year1) {
            this.year1 = year1;
        }

        public String getFile1() {
            return file1;
        }

        public void setFile1(String file1) {
            this.file1 = file1;
        }

        public String getYear2() {
            return year2;
        }

        public void setYear2(String year2) {
            this.year2 = year2;
        }

        public String getFile2() {
            return file2;
        }

        public void setFile2(String file2) {
            this.file2 = file2;
        }

        public String getYear3() {
            return year3;
        }

        public void setYear3(String year3) {
            this.year3 = year3;
        }

        public String getFile3() {
            return file3;
        }

        public void setFile3(String file3) {
            this.file3 = file3;
        }

        public String getYear4() {
            return year4;
        }

        public void setYear4(String year4) {
            this.year4 = year4;
        }

        public String getFile4() {
            return file4;
        }

        public void setFile4(String file4) {
            this.file4 = file4;
        }

        public String getYear5() {
            return year5;
        }

        public void setYear5(String year5) {
            this.year5 = year5;
        }

        public String getFile5() {
            return file5;
        }

        public void setFile5(String file5) {
            this.file5 = file5;
        }
    }
}
