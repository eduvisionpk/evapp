package com.codegenetics.eduvision.models;

import com.codegenetics.eduvision.enums.HomeMenu;

public class HomeModel {

    private int icon;
    private String title;
    private HomeMenu homeMenu;

    public HomeModel(int icon, String title, HomeMenu menu) {
        this.icon = icon;
        this.title = title;
        this.homeMenu = menu;
    }

    

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public HomeMenu getHomeMenu() {
        return homeMenu;
    }

    public void setHomeMenu(HomeMenu menu) {
        this.homeMenu = menu;
    }
}
