package com.codegenetics.eduvision.models.Rankings;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RankingBoardModel {

    /**
     * success : true
     * message : success
     * case : ranking-board-list
     * data : [{"board":"BIEK, Karachi"},{"board":"BISE, Abbottabad"},{"board":"BISE, Bahawalpur"},{"board":"BISE, Balochistan"},{"board":"BISE, Bannu"},{"board":"BISE, DG Khan"},{"board":"BISE, Faisalabad"},{"board":"BISE, Gujranwala"},{"board":"BISE, Hyderabad"},{"board":"BISE, Kohat"},{"board":"BISE, Lahore"},{"board":"BISE, Malakand"},{"board":"BISE, Mardan"},{"board":"BISE, Multan"},{"board":"BISE, Peshawar"},{"board":"BISE, Rawalpindi"},{"board":"BISE, Sahiwal"},{"board":"BISE, Sargodha"},{"board":"BISE, Swat"},{"board":"FBISE, Islamabad"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * board : BIEK, Karachi
     */

    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String board;

        public String getBoard() {
            return board;
        }

        public void setBoard(String board) {
            this.board = board;
        }
    }
}
