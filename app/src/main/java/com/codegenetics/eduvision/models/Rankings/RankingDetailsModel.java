package com.codegenetics.eduvision.models.Rankings;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RankingDetailsModel {

    /**
     * success : true
     * message : success
     * case : ranking-area
     * data : [{"institute_id":"1402646058","id":"12553","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"2035","city_rank":"2","district_rank":"2","name":"USWA COLLEGE[SIHALA]","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"125633782","id":"11644","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"2008","city_rank":"4","district_rank":"4","name":"DHAI Army Public School","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"133837154","id":"11711","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"2005","city_rank":"5","district_rank":"5","name":"SLS Montessori and School","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"1403596419","id":"12603","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"1995","city_rank":"6","district_rank":"6","name":"DR. A.Q. KHAN COLLEGE OF SCIENCE AND TECHNOLOGY[SAFARI VILLAS HIGH COURT ROAD]","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"125633853","id":"11673","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"1928","city_rank":"11","district_rank":"11","name":"Pak Turk International School & College","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"3031","id":"11627","ins_abb":"PAKTURK","board":"FBISE, Islamabad","board_rank":"0","final_score":"1878","city_rank":"17","district_rank":"17","name":"PAK- TURK INTERNATIONAL SCHOOL & COLLEGE","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"133837276","id":"11833","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"1848","city_rank":"19","district_rank":"19","name":"Islamabad Convent School","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"133837175","id":"11732","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"1836","city_rank":"21","district_rank":"21","name":"Islamabad Model School for Girls G-6/1-3","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"125633766","id":"11639","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"1834","city_rank":"22","district_rank":"22","name":"Army Public School & College Alipur Farash","city_name":"ISLAMABAD","district":"Islamabad"},{"institute_id":"1403595632","id":"12601","ins_abb":"","board":"FBISE, Islamabad","board_rank":"0","final_score":"1826","city_rank":"23","district_rank":"23","name":"DR. A.Q. KHAN COLLEGE OF SCIENCE AND TECHNOLOGY[BAHRIA TOWN]","city_name":"ISLAMABAD","district":"Islamabad"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * institute_id : 1402646058
     * id : 12553
     * ins_abb :
     * board : FBISE, Islamabad
     * board_rank : 0
     * final_score : 2035
     * city_rank : 2
     * district_rank : 2
     * name : USWA COLLEGE[SIHALA]
     * city_name : ISLAMABAD
     * district : Islamabad
     */

    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String institute_id;
        private String id;
        private String ins_abb;
        private String board;
        private String board_rank;
        private String final_score;
        private String city_rank;
        private String district_rank;
        private String name;
        private String city_name;
        private String district;

        public String getInstitute_id() {
            return institute_id;
        }

        public void setInstitute_id(String institute_id) {
            this.institute_id = institute_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIns_abb() {
            return ins_abb;
        }

        public void setIns_abb(String ins_abb) {
            this.ins_abb = ins_abb;
        }

        public String getBoard() {
            return board;
        }

        public void setBoard(String board) {
            this.board = board;
        }

        public String getBoard_rank() {
            return board_rank;
        }

        public void setBoard_rank(String board_rank) {
            this.board_rank = board_rank;
        }

        public String getFinalScore() {
            return final_score;
        }

        public void setFinal_score(String final_score) {
            this.final_score = final_score;
        }

        public String getCity_rank() {
            return city_rank;
        }

        public void setCity_rank(String city_rank) {
            this.city_rank = city_rank;
        }

        public String getDistrict_rank() {
            return district_rank;
        }

        public void setDistrict_rank(String district_rank) {
            this.district_rank = district_rank;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }
    }
}
