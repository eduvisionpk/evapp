package com.codegenetics.eduvision.models.Notifications;

import java.util.ArrayList;

public class NotificationsMainModel {

    private String title;
    private ArrayList<NotificationsModel.NotificationsData> arrayList;

    public NotificationsMainModel(String title, ArrayList<NotificationsModel.NotificationsData> arrayList) {
        this.title = title;
        this.arrayList = arrayList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<NotificationsModel.NotificationsData> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<NotificationsModel.NotificationsData> arrayList) {
        this.arrayList = arrayList;
    }
}
