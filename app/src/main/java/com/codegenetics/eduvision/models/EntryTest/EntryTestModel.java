package com.codegenetics.eduvision.models.EntryTest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public
class EntryTestModel {
    /**
     * success : true
     * message : sucess
     * case : test-list
     * data : [{"id":"1","name":"UHS MCAT for Punjab","abb":"UHS Lahore","type":"Medical","deadline":"29 July, 2019","schedule":"25 August, 2019"},{"id":"2","name":"UET Lahore ECAT for engineering","abb":"UET Lahore","type":"Engineering","deadline":"02 July, 2019","schedule":"14 July, 2019"},{"id":"3","name":"HEC Entry Test ETC","abb":"HEC Entry Test","type":"NTS","deadline":"20 June, 2018","schedule":"08 July, 2018"},{"id":"4","name":"ISSB Selection procedure and types of tests","abb":"ISSB Selection Guidelines","type":"Forces","deadline":"","schedule":"July 2018"},{"id":"5","name":"NUMS National University of Medical Sciences MCAT","abb":"NUMS","type":"Medical","deadline":"","schedule":"July/August"},{"id":"19","name":"NUST Entry Test NET","abb":"NUST Islamabad","type":"Engineering","deadline":"25 June, 2019","schedule":" 29 June, 2019"},{"id":"6","name":"Aga Khan Medical Entry Test","abb":"Aga Khan MCAT","type":"Medical","deadline":"28 May, 2018","schedule":""},{"id":"7","name":"UET Peshawar ETEA test for Engineering ","abb":"ETEA Engineering KPK","type":"Engineering","deadline":"30 June, 2019","schedule":"07 July, 2019"},{"id":"8","name":"PIEAS Entry Test ","abb":"PIEAS Islamabad","type":"Engineering","deadline":"30 May, 2019","schedule":"23 June, 2019"},{"id":"9","name":"NTS NAT IM (Pre-Medical)","abb":"NAT IM (Pre-Medical)","type":"NTS","deadline":"July 05, 2019","schedule":"July 21, 2019"},{"id":"10","name":"NTS NAT IE (PRE-ENGINEERING)","abb":"NAT IE (PRE-ENGINEERING)","type":"NTS","deadline":"July 05, 2019","schedule":"July 21, 2019"},{"id":"11","name":"NAT ICS (Computer Sc)","abb":"NAT ICS (Computer Sc)","type":"NTS","deadline":"July 05, 2019","schedule":"July 21, 2019"},{"id":"12","name":"NAT IGS (General Sc)","abb":"NAT IGS (General Sc)","type":"NTS","deadline":"July 05, 2019","schedule":"July 21, 2019"},{"id":"13","name":"NAT I-COM ( Commerce)","abb":"NAT I-COM ( Commerce)","type":"NTS","deadline":"July 05, 2019","schedule":"July 21, 2019"},{"id":"14","name":"NAT-2 A ( Arts and Social Sc)","abb":"NAT-2 A ( Arts and Social Sc)","type":"NTS","deadline":"July 05, 2019","schedule":"July 21, 2019"},{"id":"15","name":"NAT-2 B ( Biological Sciences)","abb":"NAT-2 B ( Bio Sc)","type":"NTS","deadline":"20 June, 2018","schedule":"08 July, 2018"},{"id":"16","name":"NAT-2 M (Management Sc)","abb":"NAT-2 M (Management Sc)","type":"NTS","deadline":"20 June, 2018","schedule":"08 July, 2018"},{"id":"17","name":"NAT-2 P (Physical Sc)","abb":"NAT-2 P (Physical Sc)","type":"NTS","deadline":"20 June, 2018","schedule":"08 July, 2018"},{"id":"18","name":"NAT-2 O (Oriental & Islamic Studies)","abb":"NAT-2 O (Oriental & Islamic Studies)","type":"NTS","deadline":"20 June, 2018","schedule":"08 July, 2018"},{"id":"20","name":"QAU Entry Test Social Sciences","abb":"Quaid e Azam University Islamabad","type":"Social Science","deadline":"15 July, 2019","schedule":"22 July, 2019"},{"id":"21","name":"QAU Entry Test Natural Sciences","abb":"Quaid e Azam University Islamabad","type":"Natural Science","deadline":"","schedule":""},{"id":"22","name":"CASE Entry Test BS CS","abb":"CASE Islamabad","type":"Computer Sciences","deadline":"15 July, 2019","schedule":"20 July, 2019"},{"id":"23","name":"CASE Entry Test BBA","abb":"CASE Islamabad","type":"Management","deadline":"","schedule":""},{"id":"24","name":"IBA Sukkur BBA","abb":"IBA Sukkur","type":"Management","deadline":"01 July 2019","schedule":"07 July, 2019"},{"id":"25","name":"IBA Sukkur Engineering","abb":"IBA Sukkur","type":"Engineering","deadline":"01 July 2019","schedule":"07 July, 2019"},{"id":"26","name":"IBA Sukkur Science","abb":"IBA Sukkur","type":"Natural Science","deadline":"01 July 2019","schedule":"07 July, 2019"},{"id":"27","name":"IBA Sukkur BSCS","abb":"IBA Sukkur","type":"Computer Sciences","deadline":"01 July 2019","schedule":"07 July, 2019"},{"id":"28","name":"GIKI Entry Test","abb":"GIKI Topi-Sawabi","type":"Engineering","deadline":"19 June, 2019","schedule":"30 June, 2019"},{"id":"29","name":"MUET Entry Test","abb":"MUET Jamshoro","type":"Engineering","deadline":"","schedule":""},{"id":"30","name":"COMSATS NTS TEST","abb":"COMSATS","type":"Engineering","deadline":"July 03, 2019","schedule":"July 14, 2019"},{"id":"31","name":"FAST Entry Test","abb":"FAST (Engineering)","type":"Engineering","deadline":"06-July-2018","schedule":"10-July-2018"},{"id":"32","name":"NED Entry Test","abb":"NED Karachi","type":"Engineering","deadline":"","schedule":""},{"id":"33","name":"KPK Khyber Medical University Entry Test","abb":"ETEA MCAT KMU Peshawar","type":"Medical","deadline":"31 July, 2019","schedule":"25 Aug, 2019"},{"id":"34","name":"LUMHS MCAT Medical Entry Test","abb":"LUMHS JAMSHORO","type":"Medical","deadline":"27 October, 2018","schedule":"02 November, 2018"},{"id":"35","name":"","abb":"","type":"NTS","deadline":"","schedule":""},{"id":"36","name":"Air Uni Test","abb":"Air University Islamabad","type":"Engineering","deadline":"28 June, 2019","schedule":"04 July 2018"},{"id":"38","name":"Dow University MCAT Medical Entry test","abb":"DUHS Dow University","type":"Medical","deadline":"26 June, 2018","schedule":"Not Announced"},{"id":"52","name":"PUCIT Entry Test for CS SE programs","abb":"PUCIT Entry Test","type":"Computer Sciences","deadline":"05  July 2018","schedule":"24 July 2018"},{"id":"53","name":"NDU Entry Test","abb":"N D U, Islamabad","type":"Social Science","deadline":"18 June, 2019","schedule":"04 July, 2019"},{"id":"63","name":"IIUI Social Sciences","abb":"International Islamic University, Islamabad","type":"Social Science","deadline":"21 June, 2019","schedule":"28 June, 2019"},{"id":"54","name":"AJK MCAT for Medical Colleges","abb":"AJK MCAT","type":"Medical","deadline":"15 September, 2018","schedule":"01 October, 2018"},{"id":"39","name":"Bahria University Medical College Entry test","abb":"BUMDC MCAT","type":"Medical","deadline":"","schedule":""},{"id":"42","name":"IST Islamabad Entry Test","abb":"IST Islamabad","type":"Engineering","deadline":"31 July, 2019","schedule":"31 July, 2019"},{"id":"51","name":"IIU Islamabad Entry Test","abb":"Islamic University Islamabad","type":"Engineering","deadline":"21 June, 2019","schedule":""},{"id":"55","name":"PMA Long Course Registration","abb":"PMA Long Course Registration Details","type":"Forces","deadline":"March","schedule":"Feb"},{"id":"58","name":"CSS Introduction","abb":"CSS Introduction","type":"CSS","deadline":"","schedule":""},{"id":"59","name":"CSS Eligibility Criteria","abb":"CSS Eligibility","type":"CSS","deadline":"","schedule":""},{"id":"60","name":"CSS Test Centers","abb":"CSS Test Centers","type":"CSS","deadline":"","schedule":""},{"id":"61","name":"CSS Compulsory and Optional Subjects","abb":"CSS Subjects","type":"CSS","deadline":"","schedule":""},{"id":"62","name":"Law Admission Test LAT 2018 for Admission LLB","abb":"HEC-Law LAT","type":"NTS","deadline":"10 July, 2018","schedule":"Not Announced"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * id : 1
     * name : UHS MCAT for Punjab
     * abb : UHS Lahore
     * type : Medical
     * deadline : 29 July, 2019
     * schedule : 25 August, 2019
     */

    private List<EntryTestData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<EntryTestData> getData() {
        return data;
    }

    public void setData(List<EntryTestData> data) {
        this.data = data;
    }

    public static class EntryTestData {
        private String id;
        private String name;
        private String abb;
        private String type;
        private String deadline;
        private String schedule;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAbb() {
            return abb;
        }

        public void setAbb(String abb) {
            this.abb = abb;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }

        public String getSchedule() {
            return schedule;
        }

        public void setSchedule(String schedule) {
            this.schedule = schedule;
        }
    }
}
