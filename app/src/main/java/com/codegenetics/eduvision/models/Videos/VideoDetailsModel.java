package com.codegenetics.eduvision.models.Videos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public
class VideoDetailsModel {
    /**
     * success : true
     * message : sucess
     * case : video-detail
     * data : [{"id":"471","title":"Parents desires and students choice","link":"https://www.youtube.com/watch?v=TxX6TVooBvU","type":"Parenting","image":"https://img.youtube.com/vi/TxX6TVooBvU/hqdefault.jpg"},{"id":"470","title":"Write the Story of life Yourself! | Yousuf Almas | Career Counselor |","link":"https://www.youtube.com/watch?v=zyKxxRbcfrM","type":"General","image":"https://img.youtube.com/vi/zyKxxRbcfrM/hqdefault.jpg"},{"id":"469","title":"Farooq speaks after availing Eduvision Scholarship","link":"https://www.youtube.com/watch?v=zUZfToPJPyU","type":"General","image":"https://img.youtube.com/vi/zUZfToPJPyU/hqdefault.jpg"},{"id":"468","title":"How to Join Pak Army 2020 | Online Registration and Selection Tips","link":"https://www.youtube.com/watch?v=ZQrGRj3ecpQ","type":"Armed Forces","image":"https://img.youtube.com/vi/ZQrGRj3ecpQ/hqdefault.jpg"},{"id":"467","title":"Career In Library Science","link":"https://www.youtube.com/watch?v=ziVzA4cbGNc","type":"Social Sciences","image":"https://img.youtube.com/vi/ziVzA4cbGNc/hqdefault.jpg"},{"id":"466","title":"Career as Engineer by Yousuf Almas","link":"https://www.youtube.com/watch?v=ZHtXoCSIH7s","type":"Engineering","image":"https://img.youtube.com/vi/ZHtXoCSIH7s/hqdefault.jpg"},{"id":"465","title":"Career in Actuarial Science or Actuary","link":"https://www.youtube.com/watch?v=zhPZnlZSudo","type":"Commerce Finance & Accounting","image":"https://img.youtube.com/vi/zhPZnlZSudo/hqdefault.jpg"},{"id":"464","title":"How to Become a Pilot? | Yousuf Almas | Career Counselor","link":"https://www.youtube.com/watch?v=zHBUB8XnGiE","type":"Flying/Aviation","image":"https://img.youtube.com/vi/zHBUB8XnGiE/hqdefault.jpg"},{"id":"463","title":"UET Entry test 2020 Final Dates announced for ECAT","link":"https://www.youtube.com/watch?v=zgHnhr21eZk","type":"Entry Tests","image":"https://img.youtube.com/vi/zgHnhr21eZk/hqdefault.jpg"},{"id":"462","title":"Recommended Careers in Management Science for girls | Career Counselling | Yousuf Almas","link":"https://www.youtube.com/watch?v=Z8NqTP0cbTg","type":"Management Sciences","image":"https://img.youtube.com/vi/Z8NqTP0cbTg/hqdefault.jpg"},{"id":"461","title":"Maximize Your Chances of Success","link":"https://www.youtube.com/watch?v=z_LXnSlPuPc","type":"Career Counseling","image":"https://img.youtube.com/vi/z_LXnSlPuPc/hqdefault.jpg"},{"id":"460","title":"Degrees offered in Medical Sciences in Pakistan other than MBBS and BDS: Allied Health Science","link":"https://www.youtube.com/watch?v=z_HZaxu7ee8","type":"Medical Sciences","image":"https://img.youtube.com/vi/z_HZaxu7ee8/hqdefault.jpg"},{"id":"459","title":"Career as Chemical Plant Operator | Pakistan Career TV |","link":"https://www.youtube.com/watch?v=yTNOZYeTKCg","type":"Chemistry","image":"https://img.youtube.com/vi/yTNOZYeTKCg/hqdefault.jpg"},{"id":"458","title":"Career Planning Study in Pakistan Introduction of career in Biotechnology.mpg","link":"https://www.youtube.com/watch?v=YSDGFm064fo","type":"Biological Sciences","image":"https://img.youtube.com/vi/YSDGFm064fo/hqdefault.jpg"},{"id":"457","title":"Board Topper Comments | Yousuf Almas","link":"https://www.youtube.com/watch?v=yQ75sVKNNYc","type":"General","image":"https://img.youtube.com/vi/yQ75sVKNNYc/hqdefault.jpg"},{"id":"456","title":"Career as Lawyer by Yousuf Almas","link":"https://www.youtube.com/watch?v=YnVqp81o8mg","type":"Social Sciences","image":"https://img.youtube.com/vi/YnVqp81o8mg/hqdefault.jpg"},{"id":"455","title":"Margalla ki Talibat kia banna chahti hain","link":"https://www.youtube.com/watch?v=YKesa33l_Yg","type":"Education Issues","image":"https://img.youtube.com/vi/YKesa33l_Yg/hqdefault.jpg"},{"id":"454","title":"Career In Envirnomental Chemistry","link":"https://www.youtube.com/watch?v=YC0hvrN_nYY","type":"Chemistry","image":"https://img.youtube.com/vi/YC0hvrN_nYY/hqdefault.jpg"},{"id":"453","title":"Self Esteem Four Elements by Yousuf Almas","link":"https://www.youtube.com/watch?v=y6WMirsXrn0","type":"Career Counseling","image":"https://img.youtube.com/vi/y6WMirsXrn0/hqdefault.jpg"},{"id":"452","title":"Poetry Syed Salman Gilani","link":"https://www.youtube.com/watch?v=XZcl0GF1Y_4","type":"General","image":"https://img.youtube.com/vi/XZcl0GF1Y_4/hqdefault.jpg"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * id : 471
     * title : Parents desires and students choice
     * link : https://www.youtube.com/watch?v=TxX6TVooBvU
     * type : Parenting
     * image : https://img.youtube.com/vi/TxX6TVooBvU/hqdefault.jpg
     */

    private List<VideoDetailsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<VideoDetailsData> getData() {
        return data;
    }

    public void setData(List<VideoDetailsData> data) {
        this.data = data;
    }

    public static class VideoDetailsData implements Parcelable {
        private String id;
        private String title;
        private String link;
        private String type;
        private String image;

        public VideoDetailsData(String id, String title, String link, String type, String image) {
            this.id = id;
            this.title = title;
            this.link = link;
            this.type = type;
            this.image = image;
        }

        protected VideoDetailsData(Parcel in) {
            id = in.readString();
            title = in.readString();
            link = in.readString();
            type = in.readString();
            image = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(title);
            dest.writeString(link);
            dest.writeString(type);
            dest.writeString(image);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<VideoDetailsData> CREATOR = new Creator<VideoDetailsData>() {
            @Override
            public VideoDetailsData createFromParcel(Parcel in) {
                return new VideoDetailsData(in);
            }

            @Override
            public VideoDetailsData[] newArray(int size) {
                return new VideoDetailsData[size];
            }
        };

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
