package com.codegenetics.eduvision.models.Career;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public
class CareersModel {

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;

    private ArrayList<CatNamesData> cat_names;

    private ArrayList<CareersData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<CatNamesData> getCat_names() {
        return cat_names;
    }

    public void setCat_names(ArrayList<CatNamesData> cat_names) {
        this.cat_names = cat_names;
    }

    public List<CareersData> getData() {
        return data;
    }

    public void setData(ArrayList<CareersData> data) {
        this.data = data;
    }

    public static class CatNamesData {
        private String cat_name;
        private String catid;

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCatid() {
            return catid;
        }

        public void setCatid(String catid) {
            this.catid = catid;
        }
    }

    public static class CareersData implements Parcelable {
        private String title;
        private String id;
        private String cat_name;
        private String catid;
        private String thumbnail;
        private String image;

        protected CareersData(Parcel in) {
            title = in.readString();
            id = in.readString();
            cat_name = in.readString();
            catid = in.readString();
            thumbnail = in.readString();
            image = in.readString();
        }

        public static final Creator<CareersData> CREATOR = new Creator<CareersData>() {
            @Override
            public CareersData createFromParcel(Parcel in) {
                return new CareersData(in);
            }

            @Override
            public CareersData[] newArray(int size) {
                return new CareersData[size];
            }
        };

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCatid() {
            return catid;
        }

        public void setCatid(String catid) {
            this.catid = catid;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
            dest.writeString(id);
            dest.writeString(cat_name);
            dest.writeString(catid);
            dest.writeString(thumbnail);
            dest.writeString(image);
        }
    }



}
