package com.codegenetics.eduvision.models;

import com.codegenetics.eduvision.enums.SearchFilter;

public class FilterModel {

    private int icon;
    private String title;
    private SearchFilter searchFilter;

    public FilterModel(int icon, String title, SearchFilter searchFilter) {
        this.icon = icon;
        this.title = title;
        this.searchFilter = searchFilter;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SearchFilter getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(SearchFilter searchFilter) {
        this.searchFilter = searchFilter;
    }
}
