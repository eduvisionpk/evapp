package com.codegenetics.eduvision.models.PastPapers;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PastPapersSubjectsModel {
    /**
     * success : true
     * message : success
     * case : Subjects
     * data : [{"subject":"Arabic"},{"subject":"Art and Model Drawing"},{"subject":"Biology"},{"subject":"Chemistry"},{"subject":"Civics"},{"subject":"Computer Hardware"},{"subject":"Computer Science"},{"subject":"Economics"},{"subject":"Education"},{"subject":"English"},{"subject":"English Literature"},{"subject":"Ethics"},{"subject":"Food and Nutrition"},{"subject":"General Math"},{"subject":"General Science"},{"subject":"Geography"},{"subject":"Geography of Pakistan"},{"subject":"Health and Physical Education"},{"subject":"Islamic History"},{"subject":"Islamic Studies"},{"subject":"Islamiyat Compulsory"},{"subject":"Mathematics"},{"subject":"Pak Studies"},{"subject":"Physics"},{"subject":"Urdu"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * subject : Arabic
     */

    private List<SubjectsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<SubjectsData> getData() {
        return data;
    }

    public void setData(List<SubjectsData> data) {
        this.data = data;
    }

    public static class SubjectsData {
        private String subject;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }
    }
}
