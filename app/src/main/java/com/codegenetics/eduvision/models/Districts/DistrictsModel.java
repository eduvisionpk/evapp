package com.codegenetics.eduvision.models.Districts;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DistrictsModel {

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;

    private ArrayList<DistrictsData> districts;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public ArrayList<DistrictsData> getDistricts() {
        return districts;
    }

    public void setDistricts(ArrayList<DistrictsData> districts) {
        this.districts = districts;
    }

    public static class DistrictsData {
        private String district;

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }
    }
}
