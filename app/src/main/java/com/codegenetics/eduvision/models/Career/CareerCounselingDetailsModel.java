package com.codegenetics.eduvision.models.Career;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CareerCounselingDetailsModel {
    /**
     * success : true
     * message : sucess
     * case : cp-detail
     * data : [{"id":"1","updated":"2020-11-21","title":"What is Counseling?","cat_name":"Introduction","content":"<p>The world of work is constantly changing. Every five to ten years some jobs or jobs are lost and some new ones come. This economic aspect of human life is very important. Because the whole life of a human being becomes and moves according to it.<\/p>\r\n\r\n<p>In this sense, the subjects taught in the school, the group adopted in the college and then the degree obtained from the university become very important.<\/p>\r\n\r\n<p>On the other hand, the Creator of man has made someone soft and someone warm, someone fast or slow, good at writing or high in math. In this way, some are made fat and some are made thin. There is&nbsp;a hard soul and a fragile body. Someone is interested in man and someone is interested in things, someone is the king of the world of ideas and thoughts, someone understands arithmetic and statistics.<\/p>\r\n\r\n<p>Career counseling is the process of combining work, education, and a person&#39;s personality and personality traits.<\/p>\r\n\r\n<p>If an acquaintance informs you of all the details and ups and downs of the unknown route you want to travel on, you will travel more easily and in a better way. In the same way, in the journey of life, it is very important to get and organize the correct information of school, college, university, subject and career. This is the secret that will not make you old, nor will it make you old. You will easily be able to tell the story of your life to others. And your story will be both practical and enviable.<\/p>\r\n","thumbnail":"https://www.eduvision.edu.pk/app-image.jpg","image":"https://www.eduvision.edu.pk/app-image.jpg"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * id : 1
     * updated : 2020-11-21
     * title : What is Counseling?
     * cat_name : Introduction
     * content : <p>The world of work is constantly changing. Every five to ten years some jobs or jobs are lost and some new ones come. This economic aspect of human life is very important. Because the whole life of a human being becomes and moves according to it.</p>

     <p>In this sense, the subjects taught in the school, the group adopted in the college and then the degree obtained from the university become very important.</p>

     <p>On the other hand, the Creator of man has made someone soft and someone warm, someone fast or slow, good at writing or high in math. In this way, some are made fat and some are made thin. There is&nbsp;a hard soul and a fragile body. Someone is interested in man and someone is interested in things, someone is the king of the world of ideas and thoughts, someone understands arithmetic and statistics.</p>

     <p>Career counseling is the process of combining work, education, and a person&#39;s personality and personality traits.</p>

     <p>If an acquaintance informs you of all the details and ups and downs of the unknown route you want to travel on, you will travel more easily and in a better way. In the same way, in the journey of life, it is very important to get and organize the correct information of school, college, university, subject and career. This is the secret that will not make you old, nor will it make you old. You will easily be able to tell the story of your life to others. And your story will be both practical and enviable.</p>
     * thumbnail : https://www.eduvision.edu.pk/app-image.jpg
     * image : https://www.eduvision.edu.pk/app-image.jpg
     */

    private List<CareerCounselingDetailsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<CareerCounselingDetailsData> getData() {
        return data;
    }

    public void setData(List<CareerCounselingDetailsData> data) {
        this.data = data;
    }

    public static class CareerCounselingDetailsData {
        private String id;
        private String updated;
        private String title;
        private String cat_name;
        private String content;
        private String thumbnail;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
