package com.codegenetics.eduvision.models.Programs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public
class ProgramsOfferedModel {
    /**
     * success : true
     * message : 8 Records found
     * case : programs-offers
     * data : [{"institute_id":"38","name":"UNIVERSITY OF ENGINEERING & TECHNOLOGY, LAHORE","city_name":"LAHORE","degree_abb":"B.Sc. [Engg.]","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":"127000","deadline":"2020-07-18"},{"institute_id":"3228","name":"IMPERIAL COLLEGE OF BUSINESS STUDIES","city_name":"LAHORE","degree_abb":"B.Sc.","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":"280000","deadline":"2020-10-30"},{"institute_id":"711","name":"THE UNIVERSITY OF LAHORE ( MAIN CAMPUS )","city_name":"LAHORE","degree_abb":"BS","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":"300000","deadline":"2020-09-25"},{"institute_id":"624","name":"UNIVERSITY OF CENTRAL PUNJAB","city_name":"LAHORE","degree_abb":"B.Sc.","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":"362500","deadline":"2020-09-10"},{"institute_id":"825","name":"UNIVERSITY OF MANAGEMENT AND TECHNOLOGY","city_name":"LAHORE","degree_abb":"BS","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":"399750","deadline":"2020-10-17"},{"institute_id":"1409928502","name":"RISE GROUP OF COLLEGES","city_name":"LAHORE","degree_abb":"B.Sc. [Engg.]","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":"0","deadline":"2015-11-15"},{"institute_id":"1312475234","name":"THE LIMIT COLLEGE OF ENGINEERING & TECHNOLOGY","city_name":"LAHORE","degree_abb":"BE","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":null,"deadline":"0000-00-00"},{"institute_id":"1472567334","name":"UNIVERSITY COLLEGE OF ENGINEERING, SCIENCE & TECHNOLOGY","city_name":"LAHORE","degree_abb":"B.Sc. [Engg.]","discipline_name":"Mechanical","duration":"4","duration_mode":"Years","fee_local":"0","deadline":"0000-00-00"}]
     */

    private boolean success;
    private String message;
    private String records;
    @SerializedName("case")
    private String caseX;
    private List<ProgramsOfferedData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<ProgramsOfferedData> getData() {
        return data;
    }

    public void setData(List<ProgramsOfferedData> data) {
        this.data = data;
    }

    public static class ProgramsOfferedData {
        /**
         * institute_id : 38
         * name : UNIVERSITY OF ENGINEERING & TECHNOLOGY, LAHORE
         * city_name : LAHORE
         * degree_abb : B.Sc. [Engg.]
         * discipline_name : Mechanical
         * duration : 4
         * duration_mode : Years
         * fee_local : 127000
         * deadline : 2020-07-18
         */

        private String institute_id;
        private String ins_abb;
        private String name;
        private String city_name;
        private String degree_abb;
        private String discipline_name;
        private String duration;
        private String duration_mode;
        private String fee_local;
        private String deadline;

        public ProgramsOfferedData(String institute_id, String ins_abb, String name, String city_name, String degree_abb,
                                   String discipline_name, String duration, String duration_mode, String fee_local, String deadline) {
            this.institute_id = institute_id;
            this.ins_abb = ins_abb;
            this.name = name;
            this.city_name = city_name;
            this.degree_abb = degree_abb;
            this.discipline_name = discipline_name;
            this.duration = duration;
            this.duration_mode = duration_mode;
            this.fee_local = fee_local;
            this.deadline = deadline;
        }

        public String getInstituteId() {
            return institute_id;
        }

        public void setInstitute_id(String institute_id) {
            this.institute_id = institute_id;
        }

        public String getIns_abb() {
            return ins_abb;
        }

        public void setIns_abb(String ins_abb) {
            this.ins_abb = ins_abb;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCityName() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getDegreeAbb() {
            return degree_abb;
        }

        public void setDegree_abb(String degree_abb) {
            this.degree_abb = degree_abb;
        }

        public String getDiscipline_name() {
            return discipline_name;
        }

        public void setDiscipline_name(String discipline_name) {
            this.discipline_name = discipline_name;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getDurationMode() {
            return duration_mode;
        }

        public void setDuration_mode(String duration_mode) {
            this.duration_mode = duration_mode;
        }

        public String getFeeLocal() {
            return fee_local;
        }

        public void setFee_local(String fee_local) {
            this.fee_local = fee_local;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }
    }
}
