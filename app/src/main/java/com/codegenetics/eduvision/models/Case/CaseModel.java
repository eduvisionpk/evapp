package com.codegenetics.eduvision.models.Case;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CaseModel {


    @SerializedName("case")
    @Expose
    private String Case;


    public String getCase() {
        return Case;
    }

    public void setCase(String aCase) {
        Case = aCase;
    }
}
