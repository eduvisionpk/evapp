package com.codegenetics.eduvision.models.Notifications;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public
class NotificationsModel {

    /**
     * success : true
     * message : sucess
     * case : notifications
     * records : 25
     * data : [{"id":"3432","time":"25-11-2020","titletext":"PMC announces Special MDCAT for COVID-19 positive students","image":"http://www.eduvision.edu.pk/edu_news/images/national-mdcat-2020.webp","type":"News"},{"id":"3431","time":"25-11-2020","titletext":"Punjab university postponed BA BSc and MA MSc exams","image":"http://www.eduvision.edu.pk/edu_news/images/exams.webp","type":"News"},{"id":"2792","time":"25-11-2020","titletext":"BISE Bahawalpur Inter FA FSc 2nd year Special Exams Result 2020","image":"http://www.eduvision.edu.pk/edu_news/images/bise-bahwalpur-board.webp","type":"News"},{"id":"2790","time":"25-11-2020","titletext":"BISE Sargodha Board Inter HSSC FA FSc 2nd year Special Exams Result 2020","image":"http://www.eduvision.edu.pk/edu_news/images/bise-sargodha-board.webp","type":"News"},{"id":"2787","time":"25-11-2020","titletext":"BISE Gujranwala Board Inter FA FSc 2nd year Special Exams Result 2020","image":"http://www.eduvision.edu.pk/edu_news/images/bise-gujranwala-board.webp","type":"News"},{"id":"705","time":"22-11-2020","titletext":"Sundar STEM School offers full scholarship for grade 9 to A-level","image":"http://www.eduvision.edu.pk/scholarships/images/sundar-stem-school.webp","type":"Scholarships"},{"id":"642","time":"22-11-2020","titletext":"HEC Stipendium Hungaricum Scholarship for Bachelor Masters and PhD","image":"http://www.eduvision.edu.pk/scholarships/images/hec-stipendium-hungaricum-scholarship.png","type":"Scholarships"},{"id":"704","time":"21-11-2020","titletext":"PEEF Scholarship for AJK Students","image":"http://www.eduvision.edu.pk/scholarships/images/peef-scholarship-ajk.webp","type":"Scholarships"},{"id":"1120","time":"27-11-2020","titletext":"GOVERNMENT M.A.O COLLEGE, LAHORE","image":"http://www.eduvision.edu.pk/admissions/government-college-mao-college-lahore-admission-27-11-20.jpg","type":"Admissions"},{"id":"1353571614","time":"26-11-2020","titletext":"ARMY MEDICAL CORPS SCHOOL AND CENTER, ABBOTTABAD","image":"http://www.eduvision.edu.pk/admissions/institute-army-medical-corps-school-&-centre-abbottabad-admission-26-11-20.jpg","type":"Admissions"},{"id":"56","time":"25-11-2020","titletext":"ISRA UNIVERSITY/HOSPITAL, HYDERABAD","image":"http://www.eduvision.edu.pk/admissions/sindh-med-fec-admission-25-11-20.jpg","type":"Admissions"},{"id":"30","time":"25-11-2020","titletext":"QUAID-E-AWAM UNIVERSITY OF ENGINEERING, SCIENCES & TECHNOLOGY, NAWAB SHAH","image":"http://www.eduvision.edu.pk/admissions/quaid-e-awam-university-of-engineering,-science-&-technology-nawabshah-admission-25-11-20.jpg","type":"Admissions"},{"id":"1598007943","time":"25-11-2020","titletext":"KARAKURAM INTERNATIONAL UNIVERSITY, DIAMER CAMPUS, CHILAS","image":"http://www.eduvision.edu.pk/admissions/karakoram-international-university-chilas-admission-25-11-20.jpg","type":"Admissions"},{"id":"1556631298","time":"25-11-2020","titletext":"CIVIL HOSPITAL, B S A M S COMMUNITY MIDWIFERY SCHOOL, JACCOBABAD","image":"http://www.eduvision.edu.pk/admissions/sindh-med-fec-admission-25-11-20.jpg","type":"Admissions"},{"id":"1323344426","time":"25-11-2020","titletext":"MURSHID HOSPITAL AND HEALTH CARE CENTRE, KARACHI","image":"http://www.eduvision.edu.pk/admissions/sindh-med-fec-admission-25-11-20.jpg","type":"Admissions"},{"id":"1556632258","time":"25-11-2020","titletext":"CIVIL HOSPITAL, COMMUNITY MIDWIFERY SCHOOL, NAUSHERO FEROZE","image":"http://www.eduvision.edu.pk/admissions/sindh-med-fec-admission-25-11-20.jpg","type":"Admissions"},{"id":"1572962272","time":"25-11-2020","titletext":"THAR INSTITUTE OF NURSING AND ALLIED HEALTH SCIENCES, UMERKOT","image":"http://www.eduvision.edu.pk/admissions/sindh-med-fec-admission-25-11-20.jpg","type":"Admissions"},{"id":"1534048779","time":"25-11-2020","titletext":"Horizon School of Nursing & Health Sciences, KARACHI","image":"http://www.eduvision.edu.pk/admissions/sindh-med-fec-admission-25-11-20.jpg","type":"Admissions"},{"id":"https://www.youtube.com/watch?v=9fpmDp-ngv4","time":"30-11--0001","titletext":"Career Scope of Environmental Sciences","image":"https://img.youtube.com/vi/9fpmDp-ngv4/hqdefault.jpg","type":"Videos"},{"id":"https://www.youtube.com/watch?v=8Q7lDX_4QOg","time":"27-11-2020","titletext":"6 Alternatives of Psychology | Yousuf Almas","image":"https://img.youtube.com/vi/8Q7lDX_4QOg/hqdefault.jpg","type":"Videos"},{"id":"https://www.youtube.com/watch?v=ftd69Xf3vSM","time":"27-11-2020","titletext":"Building Good Working Relationship | Part-1 | Yousuf Almas | Career Counselor","image":"https://img.youtube.com/vi/ftd69Xf3vSM/hqdefault.jpg","type":"Videos"},{"id":"https://www.youtube.com/watch?v=hY1RO_N0IuI","time":"27-11-2020","titletext":"Student Quarries | B.Tech | Pharm D/ DVM | Diploma | Yousuf Almas | Career Counselor","image":"https://img.youtube.com/vi/hY1RO_N0IuI/hqdefault.jpg","type":"Videos"},{"id":"https://www.youtube.com/watch?v=5oiaTKrkO30","time":"27-11-2020","titletext":"3 Chronic Illness | Yousuf Almas | Career Counselor","image":"https://img.youtube.com/vi/5oiaTKrkO30/hqdefault.jpg","type":"Videos"}]
     * Types : [{"type":"News"},{"type":"Scholarships"},{"type":"Admissions"},{"type":"Videos"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private String records;
    /**
     * id : 3432
     * time : 25-11-2020
     * titletext : PMC announces Special MDCAT for COVID-19 positive students
     * image : http://www.eduvision.edu.pk/edu_news/images/national-mdcat-2020.webp
     * type : News
     */

    private List<NotificationsData> data;
    /**
     * type : News
     */

    private List<NotificationsTypes> Types;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public List<NotificationsData> getData() {
        return data;
    }

    public void setData(List<NotificationsData> data) {
        this.data = data;
    }

    public List<NotificationsTypes> getNotificationTypes() {
        return Types;
    }

    public void setTypes(List<NotificationsTypes> Types) {
        this.Types = Types;
    }

    public static class NotificationsData {
        private String id;
        private String time;
        private String titletext;
        private String image;
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTitletext() {
            return titletext;
        }

        public void setTitletext(String titletext) {
            this.titletext = titletext;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class NotificationsTypes {
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
