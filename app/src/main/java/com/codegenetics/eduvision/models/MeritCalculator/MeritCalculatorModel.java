package com.codegenetics.eduvision.models.MeritCalculator;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public
class MeritCalculatorModel {

    /**
     * success : true
     * message : success
     * case : merit
     * aggregate : 90.6045
     * data : [{"institute":"QUAID-E-AZAM MEDICAL COLLEGE / VICTORIA HOSPITAL, BAHAWAL PUR","discipline":"Medicine - MBBS"},{"institute":"SARGODHA MEDICAL COLLEGE, SARGODHA","discipline":"Medicine - MBBS"},{"institute":"PUNJAB MEDICAL COLLEGE / ALLIED HOSPITAL, FAISALABAD","discipline":"Medicine - MBBS"},{"institute":"KHAWAJA MUHAMMAD SAFDAR MEDICAL COLLEGE, SIALKOT","discipline":"Medicine - MBBS"},{"institute":"SAHIWAL MEDICAL COLLEGE, SAHIWAL","discipline":"Medicine - MBBS"},{"institute":"NAWAZ SHARIF MEDICAL COLLEGE [GUJRAT], GUJRAT","discipline":"Medicine - MBBS"},{"institute":"SHEIKH ZAYED MEDICAL COLLEGE/HOSPITAL, RAHIM YAR KHAN","discipline":"Medicine - MBBS"},{"institute":"D. G. KHAN MEDICAL COLLEGE, D.G.KHAN","discipline":"Medicine - MBBS"},{"institute":"FEDERAL MEDICAL AND DENTAL COLLEGE, ISLAMABAD","discipline":"Medicine - MBBS"},{"institute":"ARMY MEDICAL COLLEGE / CMH, RAWALPINDI","discipline":"Medicine - MBBS"},{"institute":"POONCH MEDICAL COLLEGE, RAWALA KOT","discipline":"Medicine - MBBS"},{"institute":"FMH COLLEGE OF MEDICINE & DENTISTRY, LAHORE","discipline":"Medicine - MBBS"},{"institute":"DOW INTERNATIONAL MEDICAL COLLEGE (OJHA CAMPUS), KARACHI","discipline":"Medicine - MBBS"},{"institute":"DOW MEDICAL COLLEGE (DMC CAMPUS), KARACHI","discipline":"Medicine - MBBS"},{"institute":"CMH LAHORE MEDICAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"SHEIKH ZAYED FEDERAL POSTGRADUATE MEDICAL INSTITUTE / HOSPITAL, LAHORE","discipline":"Medicine - MBBS"},{"institute":"JINNAH SINDH MEDICAL UNIVERSITY, KARACHI","discipline":"Medicine - MBBS"},{"institute":"SHIFA COLLEGE OF MEDICINE/HOSPITAL, ISLAMABAD","discipline":"Medicine - MBBS"},{"institute":"RAHBAR MEDICAL AND DENTAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"SHARIF MEDICAL & DENTAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"KARACHI MEDICAL & DENTAL COLLEGE / ABBASI SHAHEED HOSPITAL, KARACHI","discipline":"Medicine - MBBS"},{"institute":"PAK RED CRESCENT MEDICAL AND DENTAL COLLEGE, KASUR","discipline":"Medicine - MBBS"},{"institute":"SHALAMAR MEDICAL & DENTAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"AZIZ FATIMAH MEDICAL AND DENTAL COLLEGE, FAISALABAD","discipline":"Medicine - MBBS"},{"institute":"BAKHTAWAR AMIN MEDICAL AND DENTAL COLLEGE, MULTAN","discipline":"Medicine - MBBS"},{"institute":"LAHORE MEDICAL & DENTAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"CONTINENTAL MEDICAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"AKHTAR SAEED MEDICAL AND DENTAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"SHAHEED MUHTRAMA BENAZIR BHUTTO MEDICAL COLLEGE, LYARI, KARACHI","discipline":"Medicine - MBBS"},{"institute":"ISLAMABAD MEDICAL & DENTAL COLLEGE, ISLAMABAD","discipline":"Medicine - MBBS"},{"institute":"LIAQUAT NATIONAL MEDICAL COLLEGE, KARACHI","discipline":"Medicine - MBBS"},{"institute":"Islamic International Medical College, RAWALPINDI","discipline":"Medicine - MBBS"},{"institute":"SINDH MEDICAL COLLEGE / JINNAH HOSPITAL, KARACHI","discipline":"Medicine - MBBS"},{"institute":"ZIA-UD-DIN MEDICAL UNIVERSITY, KARACHI","discipline":"Medicine - MBBS"},{"institute":"MULTAN MEDICAL AND DENTAL COLLEGE, MULTAN","discipline":"Medicine - MBBS"},{"institute":"AL- ALEEM MEDICAL COLLEGE, GULAB DEVI EDUCATIONAL COMPLEX, LAHORE","discipline":"Medicine - MBBS"},{"institute":"RASHID LATIF MEDICAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"CENTRAL PARK MEDICAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"JINNAH MEDICAL & DENTAL COLLEGE, KARACHI","discipline":"Medicine - MBBS"},{"institute":"FRONTIER MEDICAL COLLEGE, ABBOTTABAD","discipline":"Medicine - MBBS"},{"institute":"KHYBER MEDICAL COLLEGE / KHYBER TEACHING HOSPITAL, PESHAWAR","discipline":"Medicine - MBBS"},{"institute":"AL-NAFEES MEDICAL COLLEGE & HOSPITAL, ISLAMABAD","discipline":"Medicine - MBBS"},{"institute":"HBS MEDICAL AND DENTAL COLLEGE, ISLAMABAD","discipline":"Medicine - MBBS"},{"institute":"UNIVERSITY COLLEGE OF MEDICINE AND DENTISTRY, LAHORE","discipline":"Medicine - MBBS"},{"institute":"Foundation University Medical College, ISLAMABAD","discipline":"Medicine - MBBS"},{"institute":"LIAQUAT COLLEGE OF MEDICINE & DENTISTRY, KARACHI","discipline":"Medicine - MBBS"},{"institute":"UNITED MEDICAL AND DENTAL COLLEGE, KARACHI","discipline":"Medicine - MBBS"},{"institute":"BAQAI MEDICAL UNIVERSITY/HOSPITAL, KARACHI","discipline":"Medicine - MBBS"},{"institute":"SAIDU MEDICAL COLLEGE SWAT, SWAT","discipline":"Medicine - MBBS"},{"institute":"ISRA UNIVERSITY/HOSPITAL, HYDERABAD","discipline":"Medicine - MBBS"},{"institute":"AVICENNA MEDICAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"AYUB MEDICAL COLLEGE / AYUB HOSPITAL, ABBOTTABAD","discipline":"Medicine - MBBS"},{"institute":"KHYBER GIRLS MEDICAL COLLEGE, PESHAWAR","discipline":"Medicine - MBBS"},{"institute":"AZRA NAHEED MEDICAL COLLEGE, LAHORE","discipline":"Medicine - MBBS"},{"institute":"Mohi-UD-Din Islamic Medical College, MIR PUR (AJK)","discipline":"Medicine - MBBS"},{"institute":"BAHRIA UNIVERSITY MEDICAL & DENTAL COLLEGE, KARACHI","discipline":"Medicine - MBBS"},{"institute":"BACHA KHAN MEDICAL COLLEGE, MARDAN","discipline":"Medicine - MBBS"},{"institute":"AMNA INAYAT MEDICAL COLLEGE, SHEIKHUPURA","discipline":"Medicine - MBBS"},{"institute":"UNIVERSITY MEDICAL & DENTAL COLLEGE, FAISALABAD","discipline":"Medicine - MBBS"},{"institute":"INDEPENDENT MEDICAL COLLEGE, FAISALABAD","discipline":"Medicine - MBBS"},{"institute":"SAIDU MEDICAL COLLEGE, SAIDU SHARIF","discipline":"Medicine - MBBS"},{"institute":"RAI MEDICAL COLLEGE, SARGODHA","discipline":"Medicine - MBBS"},{"institute":"WOMEN MEDICAL AND DENTAL COLLEGE, ABBOTTABAD","discipline":"Medicine - MBBS"},{"institute":"YUSRA MEDICAL & DENTAL COLLEGE, ISLAMABAD","discipline":"Medicine - MBBS"},{"institute":"SIR SYED COLLEGE OF MEDICAL SCIENCES FOR GIRLS, KARACHI","discipline":"Medicine - MBBS"},{"institute":"M. ISLAM MEDICAL & DENTAL COLLEGE, GUJRANWALA","discipline":"Medicine - MBBS"},{"institute":"SIALKOT MEDICAL COLLEGE, SIALKOT","discipline":"Medicine - MBBS"},{"institute":"AL-TIBRI MEDICAL COLLEGE, KARACHI","discipline":"Medicine - MBBS"},{"institute":"SAHARA MEDICAL COLLEGE, NAROWAL","discipline":"Medicine - MBBS"},{"institute":"NOWSHERA MEDICAL COLLEGE, NOWSHERA","discipline":"Medicine - MBBS"},{"institute":"QUETTA INSTITUTE OF MEDICAL SCIENCES, QUETTA","discipline":"Medicine - MBBS"},{"institute":"GAJJU KHAN MEDICAL COLLEGE, SWABI","discipline":"Medicine - MBBS"},{"institute":"MUHAMMAD MEDICAL COLLEGE, MIR PUR KHAS","discipline":"Medicine - MBBS"},{"institute":"BANNU MEDICAL COLLEGE, BANNU","discipline":"Medicine - MBBS"},{"institute":"GOMAL MEDICAL COLLEGE, D.I. KHAN","discipline":"Medicine - MBBS"},{"institute":"KMU INSTITUTE OF MEDICAL SCIENCES, KOHAT","discipline":"Medicine - MBBS"},{"institute":"WAH MEDICAL COLLEGE, WAH","discipline":"Medicine - MBBS"},{"institute":"ISLAM MEDICAL COLLEGE, SIALKOT","discipline":"Medicine - MBBS"},{"institute":"LIAQUAT UNIVERSITY OF MEDICAL AND HEALTH SCIENCES, JAMSHORO","discipline":"Medicine - MBBS"},{"institute":"THE AGA KHAN UNIVERSITY MEDICAL COLLEGE, KARACHI","discipline":"Medicine - MBBS"},{"institute":"KABIR MEDICAL COLLEGE, PESHAWAR","discipline":"Medicine - MBBS"},{"institute":"NAWAB SHAH  MEDICAL COLLEGE FOR WOMEN, NAWAB SHAH","discipline":"Medicine - MBBS"},{"institute":"CHANDKA MEDICAL COLLEGE / DHQ HOSPITAL, LARKANA","discipline":"Medicine - MBBS"},{"institute":"BOLAN MEDICAL COLLEGE / BOLAN HOSPITAL, QUETTA","discipline":"Medicine - MBBS"},{"institute":"HAMDARD UNIVERSITY, KARACHI","discipline":"Medicine - MBBS"},{"institute":"JINNAH MEDICAL COLLEGE, PESHAWAR","discipline":"Medicine - MBBS"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private String aggregate;
    /**
     * institute : QUAID-E-AZAM MEDICAL COLLEGE / VICTORIA HOSPITAL, BAHAWAL PUR
     * discipline : Medicine - MBBS
     */

    private ArrayList<MeritCalculatorData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public String getAggregate() {
        return aggregate;
    }

    public void setAggregate(String aggregate) {
        this.aggregate = aggregate;
    }

    public ArrayList<MeritCalculatorData> getData() {
        return data;
    }

    public void setData(ArrayList<MeritCalculatorData> data) {
        this.data = data;
    }

    public static class MeritCalculatorData {
        private String institute;
        private String discipline;

        public String getInstitute() {
            return institute;
        }

        public void setInstitute(String institute) {
            this.institute = institute;
        }

        public String getDiscipline() {
            return discipline;
        }

        public void setDiscipline(String discipline) {
            this.discipline = discipline;
        }
    }
}
