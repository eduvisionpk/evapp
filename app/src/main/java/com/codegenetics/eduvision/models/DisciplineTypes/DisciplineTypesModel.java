package com.codegenetics.eduvision.models.DisciplineTypes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DisciplineTypesModel {

    /**
     * status : true
     * message : success
     * case : discipline-type
     * data : [{"DISCIPLINE_TYPE":"School & College Education (FA/FSc/BA/BSc)"},{"DISCIPLINE_TYPE":"Medical Sciences"},{"DISCIPLINE_TYPE":"Engineering"},{"DISCIPLINE_TYPE":"Technical"},{"DISCIPLINE_TYPE":"Computer Sciences & Information Technology"},{"DISCIPLINE_TYPE":"Management Sciences"},{"DISCIPLINE_TYPE":"Social Sciences"},{"DISCIPLINE_TYPE":"Biological & Life Sciences"},{"DISCIPLINE_TYPE":"Chemical & Material Sciences"},{"DISCIPLINE_TYPE":"Physics & Numerical Sciences"},{"DISCIPLINE_TYPE":"Earth & Environmental Sciences"},{"DISCIPLINE_TYPE":"Agricultural Sciences"},{"DISCIPLINE_TYPE":"Religious Studies"},{"DISCIPLINE_TYPE":"Art & Design"},{"DISCIPLINE_TYPE":"Media Studies"},{"DISCIPLINE_TYPE":"Commerce / Finance & Accounting"},{"DISCIPLINE_TYPE":"Education"},{"DISCIPLINE_TYPE":"Languages"},{"DISCIPLINE_TYPE":"Training"},{"DISCIPLINE_TYPE":"Professional Other"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private List<DisciplineTypesData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<DisciplineTypesData> getData() {
        return data;
    }

    public void setData(List<DisciplineTypesData> data) {
        this.data = data;
    }

    public static class DisciplineTypesData {
        /**
         * DISCIPLINE_TYPE : School & College Education (FA/FSc/BA/BSc)
         */

        private String discipline_type;

        public DisciplineTypesData(String discipline_type) {
            this.discipline_type = discipline_type;
        }

        public String getDisciplineType() {
            return discipline_type;
        }

        public void setDisciplineType(String discipline_type) {
            this.discipline_type = discipline_type;
        }
    }
}
