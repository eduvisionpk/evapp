package com.codegenetics.eduvision.models.Disciplines;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DisciplinesModel {

    /**
     * status : true
     * message : success
     * case : disciplines
     * data : [{"discipline_name":"Dentistry","discipline_code":"808"},{"discipline_name":"Medical","discipline_code":"911"},{"discipline_name":"Medicine - MBBS","discipline_code":"2583"},{"discipline_name":"Nursing","discipline_code":"433"},{"discipline_name":"Pharmacy","discipline_code":"471"},{"discipline_name":"Physiotherapy","discipline_code":"477"},{"discipline_name":"Surgery","discipline_code":"571"},{"discipline_name":"Critical Care Medicine","discipline_code":"2031"},{"discipline_name":"Emergency & Intensive Care Sciences","discipline_code":"1815"},{"discipline_name":"Food & Nutrition","discipline_code":"256"},{"discipline_name":"Homoeopathic Medical Science","discipline_code":"1580"},{"discipline_name":"Medical Image Technology","discipline_code":"1809"},{"discipline_name":"Medical Laboratory Technology","discipline_code":"1061"},{"discipline_name":"Optometry","discipline_code":"1808"},{"discipline_name":"Physical Therapy","discipline_code":"1101"},{"discipline_name":"Renal Dialysis Technology","discipline_code":"2098"},{"discipline_name":"Speech Language Therapy","discipline_code":"1102"},{"discipline_name":"Vision Sciences","discipline_code":"1799"},{"discipline_name":"Accident & Emergency","discipline_code":"2030"},{"discipline_name":"Anaesthesia Technician","discipline_code":"1949"},{"discipline_name":"Anaesthesiology","discipline_code":"18"},{"discipline_name":"Anesthesia Technology","discipline_code":"4566"},{"discipline_name":"Audiology","discipline_code":"1814"},{"discipline_name":"Audiology & Speech Language Therapy","discipline_code":"4825"},{"discipline_name":"Audiology & Speech Pathology","discipline_code":"1751"},{"discipline_name":"Biochemical Pathology","discipline_code":"1441"},{"discipline_name":"Cardiac Perfusion","discipline_code":"1813"},{"discipline_name":"Cardiology","discipline_code":"85"},{"discipline_name":"Cardiovascular Sciences","discipline_code":"4756"},{"discipline_name":"Clinical Laboratory Sciences","discipline_code":"2341"},{"discipline_name":"Clinical Micro Biology","discipline_code":"1945"},{"discipline_name":"Critical Care Sciences","discipline_code":"2187"},{"discipline_name":"Dental Hygienist","discipline_code":"1489"},{"discipline_name":"Dental Technology","discipline_code":"1811"},{"discipline_name":"Dermatology","discipline_code":"156"},{"discipline_name":"Diagnostic Radiology","discipline_code":"842"},{"discipline_name":"Dialysis Technology","discipline_code":"2059"},{"discipline_name":"Eastern Medicine & Surgery","discipline_code":"1504"},{"discipline_name":"Emergency Medicine","discipline_code":"843"},{"discipline_name":"Epidemiology & Public Health","discipline_code":"2171"},{"discipline_name":"Food Science & Nutrition","discipline_code":"2022"},{"discipline_name":"Food Sciences & Human Nutrition","discipline_code":"1978"},{"discipline_name":"Forensic Studies","discipline_code":"1967"},{"discipline_name":"General Nursing","discipline_code":"275"},{"discipline_name":"Health","discipline_code":"1683"},{"discipline_name":"Health Informatics","discipline_code":"1721"},{"discipline_name":"Health Services Management","discipline_code":"1682"},{"discipline_name":"Healthcare Systems Management","discipline_code":"4742"},{"discipline_name":"Human Development (Early Child Development)","discipline_code":"2196"},{"discipline_name":"Human Diet & Nutrition","discipline_code":"1916"},{"discipline_name":"Human Nutrition & Dietetics Mission","discipline_code":"2274"},{"discipline_name":"Intensive Care Medicine","discipline_code":"1052"},{"discipline_name":"Investigative Ophthalmology","discipline_code":"2311"},{"discipline_name":"Medical Laboratory Sciences","discipline_code":"2297"},{"discipline_name":"Medical Physics","discipline_code":"1443"},{"discipline_name":"Medical Radio Diagnosis","discipline_code":"778"},{"discipline_name":"Medical Technology","discipline_code":"402"},{"discipline_name":"Medical Ultrasound","discipline_code":"1539"},{"discipline_name":"Midwifery","discipline_code":"2082"},{"discipline_name":"Neuro Electrophysiology","discipline_code":"4754"},{"discipline_name":"Neuro Physiology Technology","discipline_code":"2057"},{"discipline_name":"Neuro Sciences","discipline_code":"430"},{"discipline_name":"Nuclear Medicine","discipline_code":"780"},{"discipline_name":"Nursing Generic","discipline_code":"4765"},{"discipline_name":"Nutrition","discipline_code":"2296"},{"discipline_name":"Nutrition & Dietetics","discipline_code":"2013"},{"discipline_name":"Nutritional Sciences","discipline_code":"2159"},{"discipline_name":"Occupational Therapy","discipline_code":"1812"},{"discipline_name":"Operation Theater Technology","discipline_code":"4565"},{"discipline_name":"Operation Theatre Sciences","discipline_code":"2188"},{"discipline_name":"Operation Theatre Technician","discipline_code":"441"},{"discipline_name":"Ophthalmic Technician","discipline_code":"1800"},{"discipline_name":"Ophthalmology","discipline_code":"442"},{"discipline_name":"Optometry & Orthoptics","discipline_code":"2088"},{"discipline_name":"Orthopedics","discipline_code":"446"},{"discipline_name":"Orthoptics","discipline_code":"2310"},{"discipline_name":"Paediatric","discipline_code":"450"},{"discipline_name":"Pathology","discipline_code":"456"},{"discipline_name":"Perfusion Sciences","discipline_code":"4761"},{"discipline_name":"Pharmaceutical Marketing","discipline_code":"2217"},{"discipline_name":"Physiology","discipline_code":"475"},{"discipline_name":"Prosthetics & Orthotics","discipline_code":"1810"},{"discipline_name":"Psychiatry","discipline_code":"507"},{"discipline_name":"Psychological Sciences","discipline_code":"2100"},{"discipline_name":"Public Health","discipline_code":"512"},{"discipline_name":"Pulmonology","discipline_code":"449"},{"discipline_name":"Radiation Oncology","discipline_code":"1105"},{"discipline_name":"Radiographer","discipline_code":"2257"},{"discipline_name":"Radiographic & Imaging Technology","discipline_code":"4648"},{"discipline_name":"Radiologic Technology","discipline_code":"2201"},{"discipline_name":"Radiological Imaging","discipline_code":"4755"},{"discipline_name":"Radiology","discipline_code":"521"},{"discipline_name":"Radiotherapy","discipline_code":"815"},{"discipline_name":"Respiratory Therapy","discipline_code":"2023"},{"discipline_name":"Skin Care","discipline_code":"1957"},{"discipline_name":"Speech & Language Pathology","discipline_code":"1180"},{"discipline_name":"Speech Language & Hearing Sciences","discipline_code":"2293"},{"discipline_name":"Sport Sciences","discipline_code":"563"},{"discipline_name":"Surgery Technology","discipline_code":"2056"},{"discipline_name":"Ultrasound Technology","discipline_code":"1872"},{"discipline_name":"Urology","discipline_code":"597"},{"discipline_name":"Veterinary Medicine","discipline_code":"605"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private ArrayList<DisciplinesData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean status) {
        this.success = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public ArrayList<DisciplinesData> getData() {
        return data;
    }

    public void setData(ArrayList<DisciplinesData> data) {
        this.data = data;
    }

    public static class DisciplinesData {
        /**
         * discipline_name : Dentistry
         * discipline_code : 808
         */

        private String discipline_name;
        private String discipline_code;
        private String career_id;

        public String getDisciplineName() {
            return discipline_name;
        }

        public void setDiscipline_name(String discipline_name) {
            this.discipline_name = discipline_name;
        }

        public String getDisciplineCode() {
            return discipline_code;
        }

        public void setDiscipline_code(String discipline_code) {
            this.discipline_code = discipline_code;
        }

        public String getCareerId() {
            return career_id;
        }

        public void setCareerId(String career_id) {
            this.career_id = career_id;
        }
    }
}
