package com.codegenetics.eduvision.models.EntryTest;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public
class EntryTestDetailsModel {

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;

    private ArrayList<EntryTestDetailsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public ArrayList<EntryTestDetailsData> getData() {
        return data;
    }

    public void setData(ArrayList<EntryTestDetailsData> data) {
        this.data = data;
    }

    public static class EntryTestDetailsData {
        private String id;
        private String name;
        private String abb;
        private String detail;
        private String schedule;
        private String deadline;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAbb() {
            return abb;
        }

        public void setAbb(String abb) {
            this.abb = abb;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getSchedule() {
            return schedule;
        }

        public void setSchedule(String schedule) {
            this.schedule = schedule;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }
    }
}
