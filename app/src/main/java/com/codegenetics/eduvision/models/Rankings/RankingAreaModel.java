package com.codegenetics.eduvision.models.Rankings;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RankingAreaModel {

    /**
     * success : true
     * message : success
     * case : ranking-disctricts
     * provinces : [{"province":"Balochistan"},{"province":"Islamabad"},{"province":"Khyber Pukhtonkhwa"},{"province":"Punjab"}]
     * districts : [{"district":"Abbottabad"},{"district":"Attock"},{"district":"AWARAN"},{"district":"Bahawal Nagar"},{"district":"Bahawal Pur"},{"district":"Bajaur"},{"district":"Bannu"},{"district":"Barkhan"},{"district":"Batagram"},{"district":"Bhakkar"},{"district":"Buner"},{"district":"Chagai"},{"district":"Chakwal"},{"district":"Charsadda"},{"district":"Chiniot"},{"district":"Chitral"},{"district":"D.G. Khan"},{"district":"Dera Bugti"},{"district":"Dera Ismael Khan"},{"district":"Faisalabad"},{"district":"Gujranwala"},{"district":"Gujrat"},{"district":"Gwadar"},{"district":"Hafizabad"},{"district":"Hango"},{"district":"Haripur"},{"district":"HARNAI"},{"district":"Islamabad"},{"district":"Jaffarabad"},{"district":"Jhal Magsi"},{"district":"Jhang"},{"district":"Jhelum"},{"district":"Kachhi"},{"district":"kalat"},{"district":"Karachi"},{"district":"Karak"},{"district":"Kasur"},{"district":"Kech"},{"district":"Khanewal"},{"district":"Kharan"},{"district":"Khushab"},{"district":"Khuzdar"},{"district":"Khyber"},{"district":"Killa Abdullah"},{"district":"Killa Saifullah"},{"district":"Kohat"},{"district":"Kohistan"},{"district":"Kohlu"},{"district":"Kurram"},{"district":"Lahore"},{"district":"Lakki Marwat"},{"district":"Lasbela"},{"district":"Layyah"},{"district":"LEHRI"},{"district":"Lodhran"},{"district":"Loralai"},{"district":"Lower Dir"},{"district":"Malakand"},{"district":"Mandi Bahauddin"},{"district":"Mansehra"},{"district":"Mardan"},{"district":"Mastung"},{"district":"Mianwali"},{"district":"Mohmand"},{"district":"Multan"},{"district":"Musakhel"},{"district":"Muzaffargarh"},{"district":"NANKANA SAHIB"},{"district":"Narowal"},{"district":"Nasirabad"},{"district":"North Waziristan"},{"district":"Nowshera"},{"district":"Nushki"},{"district":"Okara"},{"district":"Orakzai"},{"district":"Pakpattan"},{"district":"Panjgur"},{"district":"Peshawar"},{"district":"PISHIN"},{"district":"Quetta"},{"district":"Rahim Yar Khan"},{"district":"Rajanpur"},{"district":"Rawalpindi"},{"district":"Sahiwal"},{"district":"Sargodha"},{"district":"Shangla"},{"district":"SHEERANI"},{"district":"Sheikhupura"},{"district":"Sialkot"},{"district":"Sibi"},{"district":"SOHBATPUR"},{"district":"South Waziristan"},{"district":"Swabi"},{"district":"Swat"},{"district":"Toba Tek Singh"},{"district":"TORGHAR"},{"district":"Upper Dir"},{"district":"Vehari"},{"district":"WASHUK"},{"district":"ZHOB"},{"district":"Ziarat"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * province : Balochistan
     */

    private List<ProvincesBean> provinces;
    /**
     * district : Abbottabad
     */

    private List<DistrictsBean> districts;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<ProvincesBean> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<ProvincesBean> provinces) {
        this.provinces = provinces;
    }

    public List<DistrictsBean> getDistricts() {
        return districts;
    }

    public void setDistricts(List<DistrictsBean> districts) {
        this.districts = districts;
    }

    public static class ProvincesBean {
        private String province;

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }
    }

    public static class DistrictsBean {
        private String district;

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }
    }
}
