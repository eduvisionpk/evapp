package com.codegenetics.eduvision.models.EntryTest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EntryTestCategories {
    /**
     * success : true
     * message : sucess
     * case : test-category
     * data : [{"type":"Medical"},{"type":"Engineering"},{"type":"CSS"},{"type":"Social Science"},{"type":"Natural Science"},{"type":"NTS"},{"type":"Computer Sciences"},{"type":"Forces"},{"type":"Management"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * type : Medical
     */

    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
