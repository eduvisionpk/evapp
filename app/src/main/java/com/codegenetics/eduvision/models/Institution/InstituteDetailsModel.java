package com.codegenetics.eduvision.models.Institution;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public
class InstituteDetailsModel {

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;

    private ArrayList<InstituteDetailsData> data;

    private ArrayList<InstituteDetailsLevels> levels;

    private ArrayList<InstituteDetailsPrograms> programs;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public ArrayList<InstituteDetailsData> getData() {
        return data;
    }

    public void setData(ArrayList<InstituteDetailsData> data) {
        this.data = data;
    }

    public ArrayList<InstituteDetailsLevels> getLevels() {
        return levels;
    }

    public void setLevels(ArrayList<InstituteDetailsLevels> levels) {
        this.levels = levels;
    }

    public ArrayList<InstituteDetailsPrograms> getPrograms() {
        return programs;
    }

    public void setPrograms(ArrayList<InstituteDetailsPrograms> programs) {
        this.programs = programs;
    }

    public static class InstituteDetailsData {
        private String institute_id;
        private String ins_abb;
        private String ins_logo;
        private String address;
        private String phone1;
        private String email1;
        private String url;
        private String name;
        private String established_date;
        private String chartered;
        private String ins_type;
        private String ins_level;
        private String ins_category;
        private String city_name;

        public String getInstitute_id() {
            return institute_id;
        }

        public void setInstitute_id(String institute_id) {
            this.institute_id = institute_id;
        }

        public String getIns_abb() {
            return ins_abb;
        }

        public void setIns_abb(String ins_abb) {
            this.ins_abb = ins_abb;
        }

        public String getIns_logo() {
            return ins_logo;
        }

        public void setIns_logo(String ins_logo) {
            this.ins_logo = ins_logo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone1() {
            return phone1;
        }

        public void setPhone1(String phone1) {
            this.phone1 = phone1;
        }

        public String getEmail1() {
            return email1;
        }

        public void setEmail1(String email1) {
            this.email1 = email1;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEstablished_date() {
            return established_date;
        }

        public void setEstablished_date(String established_date) {
            this.established_date = established_date;
        }

        public String getChartered() {
            return chartered;
        }

        public void setChartered(String chartered) {
            this.chartered = chartered;
        }

        public String getIns_type() {
            return ins_type;
        }

        public void setIns_type(String ins_type) {
            this.ins_type = ins_type;
        }

        public String getIns_level() {
            return ins_level;
        }

        public void setIns_level(String ins_level) {
            this.ins_level = ins_level;
        }

        public String getIns_category() {
            return ins_category;
        }

        public void setIns_category(String ins_category) {
            this.ins_category = ins_category;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }
    }

    public static class InstituteDetailsLevels {
        private String level_name;

        public String getLevel_name() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }
    }

    public static class InstituteDetailsPrograms {
        private String institute_id;
        private String ins_abb;
        private String name;
        private String city_name;
        private String deadline;
        private String sub_level;
        private String discipline_name;
        private String discipline_type;
        private String duration;
        private String duration_mode;
        private String fee_local;
        private String level_name;
        private String degree_abb;

        public String getInstitute_id() {
            return institute_id;
        }

        public void setInstitute_id(String institute_id) {
            this.institute_id = institute_id;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }

        public String getSub_level() {
            return sub_level;
        }

        public void setSub_level(String sub_level) {
            this.sub_level = sub_level;
        }

        public String getDiscipline_name() {
            return discipline_name;
        }

        public void setDiscipline_name(String discipline_name) {
            this.discipline_name = discipline_name;
        }

        public String getDiscipline_type() {
            return discipline_type;
        }

        public void setDiscipline_type(String discipline_type) {
            this.discipline_type = discipline_type;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getDuration_mode() {
            return duration_mode;
        }

        public void setDuration_mode(String duration_mode) {
            this.duration_mode = duration_mode;
        }

        public String getFee_local() {
            return fee_local;
        }

        public void setFee_local(String fee_local) {
            this.fee_local = fee_local;
        }

        public String getLevel_name() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }

        public String getDegree_abb() {
            return degree_abb;
        }

        public void setDegree_abb(String degree_abb) {
            this.degree_abb = degree_abb;
        }

        public String getIns_abb() {
            return ins_abb == null ? "" : ins_abb;
        }

        public void setIns_abb(String ins_abb) {
            this.ins_abb = ins_abb;
        }

        public String getName() {
            return name == null ? "" : name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCity_name() {
            return city_name == null ? "" : city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }
    }

}
