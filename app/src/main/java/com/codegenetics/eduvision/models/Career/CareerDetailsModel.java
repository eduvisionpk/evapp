package com.codegenetics.eduvision.models.Career;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public
class CareerDetailsModel {

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;

    private ArrayList<CareerDetailsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<CareerDetailsData> getData() {
        return data;
    }

    public void setData(ArrayList<CareerDetailsData> data) {
        this.data = data;
    }

    public static class CareerDetailsData {

        private String id;
        private String title;
        private String story;
        private String cat_name;
        private String catid;
        private String image;
        private String link;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStory() {
            return story;
        }

        public void setStory(String story) {
            this.story = story;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCatid() {
            return catid;
        }

        public void setCatid(String catid) {
            this.catid = catid;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
