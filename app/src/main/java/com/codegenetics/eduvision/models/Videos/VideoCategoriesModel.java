package com.codegenetics.eduvision.models.Videos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public
class VideoCategoriesModel {
    /**
     * success : true
     * message : sucess
     * case : video-cat
     * data : [{"type":"Career Counseling"},{"type":"Education"},{"type":"Biological Sciences"},{"type":"Physics and Math"},{"type":"Computer Science and IT"},{"type":"Engineering"},{"type":"Management Sciences"},{"type":"General"},{"type":"Social Sciences"},{"type":"Agriculture Sciences"},{"type":"Earth and Environment Sciences"},{"type":"Religious Studies"},{"type":"Commerce Finance & Accounting"},{"type":"Medical Sciences"},{"type":"Art and Design"},{"type":"Students"},{"type":"Parenting"},{"type":"Armed Forces"},{"type":"CSS/ PMS"},{"type":"Entry Tests"},{"type":"Job Hunting"},{"type":"Chemistry"},{"type":"Education Issues"},{"type":"Languages"},{"type":"Earth and Environmental Scienc"},{"type":"Admission Guidance"},{"type":"Media Studies"},{"type":"Children"},{"type":"Exam Preparation"},{"type":"Flying/Aviation"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * type : Career Counseling
     */

    private ArrayList<VideoCategoriesData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public ArrayList<VideoCategoriesData> getData() {
        return data;
    }

    public void setData(ArrayList<VideoCategoriesData> data) {
        this.data = data;
    }

    public static class VideoCategoriesData implements Parcelable {
        private String type;

        protected VideoCategoriesData(Parcel in) {
            type = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(type);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<VideoCategoriesData> CREATOR = new Creator<VideoCategoriesData>() {
            @Override
            public VideoCategoriesData createFromParcel(Parcel in) {
                return new VideoCategoriesData(in);
            }

            @Override
            public VideoCategoriesData[] newArray(int size) {
                return new VideoCategoriesData[size];
            }
        };

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
