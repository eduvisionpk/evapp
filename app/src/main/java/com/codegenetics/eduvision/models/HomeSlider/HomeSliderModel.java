package com.codegenetics.eduvision.models.HomeSlider;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public
class HomeSliderModel {
    /**
     * success : true
     * message : sucess
     * case : home-screen
     * data : [{"id":"3388","time":"2020-10-08","titletext":"Private medical colleges allowed to set their fee structure","image":"http://www.eduvision.edu.pk/edu_news/images/pmc.webp","type":"news"},{"id":"3389","time":"2020-10-08","titletext":"Government schools students required to submit Form B","image":"http://www.eduvision.edu.pk/edu_news/images/government-schools.webp","type":"news"},{"id":"690","time":"2020-10-07","titletext":"Pakistan Engineering Council PEC Scholarship","image":"http://www.eduvision.edu.pk/scholarships/images/pec-scholarship-2020.webp","type":"scholarships"},{"id":"3387","time":"2020-10-07","titletext":"PMC National MDCAT Syllabus 2020","image":"http://www.eduvision.edu.pk/edu_news/images/national-mdcat-2020.webp","type":"news"},{"id":"3330","time":"2020-10-07","titletext":"Daanish Schools teaching jobs 2020","image":"http://www.eduvision.edu.pk/edu_news/images/daanish-school.webp","type":"news"},{"id":"582","time":"2020-10-06","titletext":"Scotland Pakistan Scottish Scholarship for Undergraduate studies","image":"http://www.eduvision.edu.pk/scholarships/images/scottish-scholarship-2020.webp","type":"scholarships"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private ArrayList<HomeNewsScholarshipsData> data;
    private ArrayList<HomeSlidersData> sliders;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public ArrayList<HomeNewsScholarshipsData> getData() {
        return data;
    }

    public void setData(ArrayList<HomeNewsScholarshipsData> data) {
        this.data = data;
    }

    public ArrayList<HomeSlidersData> getSliders() {
        return sliders;
    }

    public void setSliders(ArrayList<HomeSlidersData> sliders) {
        this.sliders = sliders;
    }

    public static class HomeNewsScholarshipsData {
        /**
         * id : 3388
         * time : 2020-10-08
         * titletext : Private medical colleges allowed to set their fee structure
         * image : http://www.eduvision.edu.pk/edu_news/images/pmc.webp
         * type : news
         */

        private String id;
        private String time;
        private String titletext;
        private String image;
        private String catname;
        private String catid;
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTitletext() {
            return titletext;
        }

        public void setTitletext(String titletext) {
            this.titletext = titletext;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCatname() {
            return catname;
        }

        public void setCatname(String catname) {
            this.catname = catname;
        }

        public String getCatid() {
            return catid;
        }

        public void setCatid(String catid) {
            this.catid = catid;
        }
    }

    public static class HomeSlidersData {
        private String slide;

        public String getSlide() {
            return slide;
        }

        public void setSlide(String slide) {
            this.slide = slide;
        }
    }
}
