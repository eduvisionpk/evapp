package com.codegenetics.eduvision.models.Programs;

public class ProgramsModel {

    private final int icon;
    private final String title;

    public ProgramsModel(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

}
