package com.codegenetics.eduvision.models.PastPapers;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public
class PastPapersBoardsClassesModel {
    /**
     * success : true
     * message : success
     * case : past-papers
     * boards : [{"board":"FBISE"},{"board":"AIOU"},{"board":"AJK University"},{"board":"Bise Bahawalpur"},{"board":"Bise Gujranwala"},{"board":"Bise D G Khan"},{"board":"Bise Faisalabad"},{"board":"Bise Lahore"},{"board":"Bise Multan"},{"board":"Bise Rawalpindi"},{"board":"Bise Sargodha"},{"board":"Bise Sahiwal"},{"board":"Bise Hyderabad"}]
     * classes : [{"class":"10th"},{"class":"1st Year"},{"class":"9th"},{"class":"MA MSc"},{"class":"2nd Year"},{"class":"MEd"},{"class":""}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * board : FBISE
     */

    private List<BoardsData> boards;
    /**
     * class : 10th
     */

    private List<ClassesData> classes;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<BoardsData> getBoards() {
        return boards;
    }

    public void setBoards(List<BoardsData> boards) {
        this.boards = boards;
    }

    public List<ClassesData> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassesData> classes) {
        this.classes = classes;
    }

    public static class BoardsData {
        private String board;

        public String getBoard() {
            return board;
        }

        public void setBoard(String board) {
            this.board = board;
        }
    }

    public static class ClassesData {
        @SerializedName("class")
        private String classX;

        public String getClassX() {
            return classX;
        }

        public void setClassX(String classX) {
            this.classX = classX;
        }
    }
}
