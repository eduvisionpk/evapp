package com.codegenetics.eduvision.models.News;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsDetailsModel {

    /**
     * success : true
     * message : sucess
     * case : news-detail
     * data : [{"id":"3382","image":"http://www.eduvision.edu.pk/edu_news/images/medical-college-admission-test-mdcat.webp","titletext":"PMC National MDCAT 2020","maintext":"<p>Pakistan Medical Commission, PMC will conduct National MDCAT 2020 in the second week of November. National MDCAT 2020 will be conducted by the National University of Medical Sciences NUMS. NUMS Syllabus will be followed for entry test. Students can download the syllabus already available on the NUMS website.&nbsp;<\/p>\r\n\r\n<p>The exact date of the National MDCAT will be announced soon.<strong> Passing marks of MDCAT are set as 60%<\/strong>. Admissions to all public sector medical colleges will be held in November / December. Admissions to NUMS affiliated medical colleges will be carried out by NUMS. Similarly, Aga Khan University will carry out the admission process for their Medical College.&nbsp;<\/p>\r\n\r\n<p>Admissions to all other private medical colleges for session 2020-21 will be carried out PMC through a centralized&nbsp;automated system with MDCAT given 50% weightage and 50% weightage to FSc marks. It is also reported that all medical colleges in Kyrgyzstan are currently placed on a restricted black list and degree awarded shall not be considered PMC as valid for the purpose of grant of license to practice in Pakistan.<\/p>\r\n\r\n<p>&nbsp;<\/p>\r\n\r\n<p>Keep visiting this page for the latest updates regarding National MDCAT 2020.<\/p>\r\n\r\n<p>&nbsp;<\/p>\r\n","ad":"http://www.eduvision.edu.pk/edu_news/images/pmc-press-release.jpg"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private List<NewsDetailsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<NewsDetailsData> getData() {
        return data;
    }

    public void setData(List<NewsDetailsData> data) {
        this.data = data;
    }

    public static class NewsDetailsData {
        /**
         * id : 3382
         * image : http://www.eduvision.edu.pk/edu_news/images/medical-college-admission-test-mdcat.webp
         * titletext : PMC National MDCAT 2020
         * maintext : <p>Pakistan Medical Commission, PMC will conduct National MDCAT 2020 in the second week of November. National MDCAT 2020 will be conducted by the National University of Medical Sciences NUMS. NUMS Syllabus will be followed for entry test. Students can download the syllabus already available on the NUMS website.&nbsp;</p>

         <p>The exact date of the National MDCAT will be announced soon.<strong> Passing marks of MDCAT are set as 60%</strong>. Admissions to all public sector medical colleges will be held in November / December. Admissions to NUMS affiliated medical colleges will be carried out by NUMS. Similarly, Aga Khan University will carry out the admission process for their Medical College.&nbsp;</p>

         <p>Admissions to all other private medical colleges for session 2020-21 will be carried out PMC through a centralized&nbsp;automated system with MDCAT given 50% weightage and 50% weightage to FSc marks. It is also reported that all medical colleges in Kyrgyzstan are currently placed on a restricted black list and degree awarded shall not be considered PMC as valid for the purpose of grant of license to practice in Pakistan.</p>

         <p>&nbsp;</p>

         <p>Keep visiting this page for the latest updates regarding National MDCAT 2020.</p>

         <p>&nbsp;</p>
         * ad : http://www.eduvision.edu.pk/edu_news/images/pmc-press-release.jpg
         */

        private String id;
        private String image;
        private String titletext;
        private String maintext;
        private String ad;
        private String link;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitletext() {
            return titletext;
        }

        public void setTitletext(String titletext) {
            this.titletext = titletext;
        }

        public String getMaintext() {
            return maintext;
        }

        public void setMaintext(String maintext) {
            this.maintext = maintext;
        }

        public String getAd() {
            return ad;
        }

        public void setAd(String ad) {
            this.ad = ad;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
