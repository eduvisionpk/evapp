package com.codegenetics.eduvision.models;

import com.codegenetics.eduvision.enums.ProgramsEnums;

import java.util.ArrayList;

public class ProgramsMainModel {

    private String title;
    private ProgramsEnums programsEnums;
    private ArrayList<ProgramsInnerModel> arrayList;

    public ProgramsMainModel(String title, ProgramsEnums programsEnums, ArrayList<ProgramsInnerModel> arrayList) {
        this.title = title;
        this.programsEnums = programsEnums;
        this.arrayList = arrayList;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ProgramsInnerModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<ProgramsInnerModel> arrayList) {
        this.arrayList = arrayList;
    }

    public ProgramsEnums getProgramsEnums() {
        return programsEnums;
    }

    public void setProgramsEnums(ProgramsEnums programsEnums) {
        this.programsEnums = programsEnums;
    }
}
