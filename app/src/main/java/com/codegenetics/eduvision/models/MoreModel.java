package com.codegenetics.eduvision.models;

import com.codegenetics.eduvision.enums.MoreMenu;

public class MoreModel {

    private int icon;
    private String title;
    private MoreMenu moreMenu;

    public MoreModel(int icon, String title, MoreMenu moreMenu) {
        this.icon = icon;
        this.title = title;
        this.moreMenu = moreMenu;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MoreMenu getMoreMenu() {
        return moreMenu;
    }

    public void setMoreMenu(MoreMenu moreMenu) {
        this.moreMenu = moreMenu;
    }
}
