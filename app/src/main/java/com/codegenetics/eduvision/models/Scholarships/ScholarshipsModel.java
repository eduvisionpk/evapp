package com.codegenetics.eduvision.models.Scholarships;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScholarshipsModel {
    /**
     * success : true
     * message : sucess
     * case : scholarship-list
     * data : [{"scholarship_id":"680","scholarship_name":"Fully funded Overseas PhD Scholarship by University of Kotli","logo":"http://www.eduvision.edu.pk/scholarships/images/scholarship-overseas.webp"},{"scholarship_id":"104","scholarship_name":"COMSATS labour quota Scholarship and admission under Punjab Worker welfare board","logo":"http://www.eduvision.edu.pk/scholarships/images/comsats.jpg"},{"scholarship_id":"673","scholarship_name":"NUML Faculty Development MS MPhil foreign scholarship","logo":"http://www.eduvision.edu.pk/scholarships/images/numl.webp"},{"scholarship_id":"208","scholarship_name":"LUMS National Outreach Program NOP Scholarship","logo":"http://www.eduvision.edu.pk/scholarships/images/lums-nop-national-outreach-program-scholarship.png"},{"scholarship_id":"643","scholarship_name":"IBA Karachi National Talent Hunt Program Scholarship by Ihsan Trust","logo":"http://www.eduvision.edu.pk/scholarships/images/iba-national-talent-hunt-program-.jpg"},{"scholarship_id":"80","scholarship_name":"IBA Sukkur Sindh talent hunt program scholarship 2019","logo":"http://www.eduvision.edu.pk/scholarships/images/scholarships.png"},{"scholarship_id":"79","scholarship_name":"Bahria University Scholarship Program","logo":"http://www.eduvision.edu.pk/scholarships/images/scholarships.png"},{"scholarship_id":"65","scholarship_name":"COMSATS announced CIIT Endowment Fund Scholarships ","logo":"http://www.eduvision.edu.pk/scholarships/images/scholarships.png"},{"scholarship_id":"100","scholarship_name":"COMSATS Endowment Fund Scholarships ","logo":"http://www.eduvision.edu.pk/scholarships/images/usaid.png"},{"scholarship_id":"120","scholarship_name":"Liaquat University of Medical and Health Sciences Announced Scholarships for the Undergraduate Students","logo":"http://www.eduvision.edu.pk/scholarships/images/hec logo.png"},{"scholarship_id":"573","scholarship_name":"GC University Lahore Endowment Fund Trust","logo":"http://www.eduvision.edu.pk/scholarships/images/GC-Scholarship-1752018.jpg"},{"scholarship_id":"501","scholarship_name":"LUMS University Merit & Need Based Scholarship","logo":"http://www.eduvision.edu.pk/scholarships/images/Lums-Logo-742018.jpg"},{"scholarship_id":"197","scholarship_name":"Sardar Bahadur Khan Womens University Quetta Scholarships","logo":"http://www.eduvision.edu.pk/scholarships/images/Sardar-Bahadur-Khan-University-2017.jpg"},{"scholarship_id":"188","scholarship_name":"University of Education Lahore Scholarships","logo":"http://www.eduvision.edu.pk/scholarships/images/UOEL.jpg"},{"scholarship_id":"206","scholarship_name":"Aga Khan University AKU Challenging Scholarship Programme 2017","logo":"http://www.eduvision.edu.pk/scholarships/images/Aga-Khan-University-2017.png"},{"scholarship_id":"192","scholarship_name":"Institute of Management Sciences Peshawar Scholarships","logo":"http://www.eduvision.edu.pk/scholarships/images/IMSP.jpeg"},{"scholarship_id":"180","scholarship_name":"University of Sindh Jamshoro","logo":"http://www.eduvision.edu.pk/scholarships/images/UOSJ.png"},{"scholarship_id":"189","scholarship_name":"University Of The Punjab Lahore Scholarship","logo":"http://www.eduvision.edu.pk/scholarships/images/UOPL.png"},{"scholarship_id":"179","scholarship_name":"University of karachi","logo":"http://www.eduvision.edu.pk/scholarships/images/UOK.png"},{"scholarship_id":"194","scholarship_name":"University Of Engineering and Technology Peshawar Scholarships","logo":"http://www.eduvision.edu.pk/scholarships/images/UETP.png"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private List<ScholarshipsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<ScholarshipsData> getData() {
        return data;
    }

    public void setData(List<ScholarshipsData> data) {
        this.data = data;
    }

    public static class ScholarshipsData implements Parcelable {
        /**
         * scholarship_id : 680
         * scholarship_name : Fully funded Overseas PhD Scholarship by University of Kotli
         * logo : http://www.eduvision.edu.pk/scholarships/images/scholarship-overseas.webp
         */

        private String scholarship_id;
        private String scholarship_name;
        private String logo;
        private String updated;

        public ScholarshipsData(String scholarship_id, String scholarship_name, String logo) {
            this.scholarship_id = scholarship_id;
            this.scholarship_name = scholarship_name;
            this.logo = logo;
        }

        public ScholarshipsData(String scholarship_id, String scholarship_name, String logo, String updated) {
            this.scholarship_id = scholarship_id;
            this.scholarship_name = scholarship_name;
            this.logo = logo;
            this.updated = updated;
        }

        protected ScholarshipsData(Parcel in) {
            scholarship_id = in.readString();
            scholarship_name = in.readString();
            logo = in.readString();
            updated = in.readString();
        }

        public static final Creator<ScholarshipsData> CREATOR = new Creator<ScholarshipsData>() {
            @Override
            public ScholarshipsData createFromParcel(Parcel in) {
                return new ScholarshipsData(in);
            }

            @Override
            public ScholarshipsData[] newArray(int size) {
                return new ScholarshipsData[size];
            }
        };

        public String getScholarshipId() {
            return scholarship_id;
        }

        public void setScholarship_id(String scholarship_id) {
            this.scholarship_id = scholarship_id;
        }

        public String getScholarshipName() {
            return scholarship_name;
        }

        public void setScholarship_name(String scholarship_name) {
            this.scholarship_name = scholarship_name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(scholarship_id);
            parcel.writeString(scholarship_name);
            parcel.writeString(logo);
            parcel.writeString(updated);
        }
    }
}
