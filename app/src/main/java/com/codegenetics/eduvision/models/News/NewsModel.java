package com.codegenetics.eduvision.models.News;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsModel {

    /**
     * success : true
     * message : sucess
     * case : news-list
     * data : [{"id":"2795","titletext":"FPSC announces CSS Exams Schedule 2021","image":"http://www.eduvision.edu.pk/edu_news/images/caa-exams.webp"},{"id":"3385","titletext":"Join PAF as civilian 2020","image":"http://www.eduvision.edu.pk/edu_news/images/join-paf.webp"},{"id":"2311","titletext":"HEC Alerts students and Prents","image":"http://www.eduvision.edu.pk/edu_news/images/hec-parents-alerts.webp"},{"id":"2596","titletext":"Join Pak Army  147 PMA Long Course Online Registration 2020","image":"http://www.eduvision.edu.pk/edu_news/images/join-pak-army.webp"},{"id":"3382","titletext":"PMC National MDCAT 2020","image":"http://www.eduvision.edu.pk/edu_news/images/medical-college-admission-test-mdcat.webp"},{"id":"3384","titletext":"Deeni Madaris registration process starts","image":"http://www.eduvision.edu.pk/edu_news/images/deeni-madaris.webp"},{"id":"2870","titletext":"University of Education UE Merit List 2020","image":"http://www.eduvision.edu.pk/edu_news/images/university-of-education-admission-merit-2019.jpg"},{"id":"3380","titletext":"University of Sargodha SU Merit list 2020","image":"http://www.eduvision.edu.pk/edu_news/images/university-of-sargodha.webp"},{"id":"3383","titletext":"BISE Lahore Board Matric 10th class Supply registration schedule 2020","image":"http://www.eduvision.edu.pk/edu_news/images/exams.webp"},{"id":"3381","titletext":"Primary schools to reopen from tomorrow","image":"http://www.eduvision.edu.pk/edu_news/images/primary-schools.webp"},{"id":"3379","titletext":"University of Okara Merit list 2020","image":"http://www.eduvision.edu.pk/edu_news/images/university-of-okara.webp"},{"id":"3378","titletext":"Hazara University HU Mansehra Merit list 2020","image":"http://www.eduvision.edu.pk/edu_news/images/hazara-university-mansehra.webp"},{"id":"2722","titletext":"Join Pakistan Army Medical Corps as M Cadets / Doctor 2020","image":"http://www.eduvision.edu.pk/edu_news/images/army-doctors-m.webp"},{"id":"3377","titletext":"Primary schools will be reopened on September 30","image":"http://www.eduvision.edu.pk/edu_news/images/shafqat-mahmood.webp"},{"id":"3376","titletext":"All MDCAT 2020 cancelled: new dates will be announced by PMC","image":"http://www.eduvision.edu.pk/edu_news/images/medical-admissions.webp"},{"id":"3375","titletext":"Cabinet Approved New Education Policy: This is Indian Cabinet","image":"http://www.eduvision.edu.pk/edu_news/images/education-policy.webp"},{"id":"3374","titletext":"President approved PMC Bill 2020","image":"http://www.eduvision.edu.pk/edu_news/images/president-approved.webp"},{"id":"3373","titletext":"Medical Entry test MDCAT 2020 date not finalized","image":"http://www.eduvision.edu.pk/edu_news/images/medical-college-admission-test-mdcat.webp"},{"id":"2792","titletext":"BISE Bahawalpur Inter FA FSc 2nd year Result 2020","image":"http://www.eduvision.edu.pk/edu_news/images/bise-bahwalpur-board.webp"},{"id":"2790","titletext":"BISE Sargodha Board Inter HSSC FA FSc 2nd year Result 2020","image":"http://www.eduvision.edu.pk/edu_news/images/bise-sargodha-board.webp"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private List<NewsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<NewsData> getData() {
        return data;
    }

    public void setData(List<NewsData> data) {
        this.data = data;
    }

    public static class NewsData implements Parcelable {
        /**
         * id : 2795
         * titletext : FPSC announces CSS Exams Schedule 2021
         * image : http://www.eduvision.edu.pk/edu_news/images/caa-exams.webp
         */

        private String id;
        private String titletext;
        private String image;
        private String catname;
        private String catid;
        private String time;

        public NewsData(String id, String titletext, String image) {
            this.id = id;
            this.titletext = titletext;
            this.image = image;
        }

        public NewsData(String id, String titletext, String image, String time) {
            this.id = id;
            this.titletext = titletext;
            this.image = image;
            this.time = time;
        }

        public NewsData(String id, String titletext, String image, String catname, String catid, String time) {
            this.id = id;
            this.titletext = titletext;
            this.image = image;
            this.catname = catname;
            this.catid = catid;
            this.time = time;
        }

        protected NewsData(Parcel in) {
            id = in.readString();
            titletext = in.readString();
            image = in.readString();
            catname = in.readString();
            catid = in.readString();
            time = in.readString();
        }

        public static final Creator<NewsData> CREATOR = new Creator<NewsData>() {
            @Override
            public NewsData createFromParcel(Parcel in) {
                return new NewsData(in);
            }

            @Override
            public NewsData[] newArray(int size) {
                return new NewsData[size];
            }
        };

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitleText() {
            return titletext;
        }

        public void setTitletext(String titletext) {
            this.titletext = titletext;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCatname() {
            return catname;
        }

        public void setCatname(String catname) {
            this.catname = catname;
        }

        public String getCatid() {
            return catid;
        }

        public void setCatid(String catid) {
            this.catid = catid;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(id);
            parcel.writeString(titletext);
            parcel.writeString(image);
            parcel.writeString(catname);
            parcel.writeString(catid);
            parcel.writeString(time);
        }
    }
}
