package com.codegenetics.eduvision.models.Career;

import java.util.ArrayList;

public class CareersMainModel {

    private String title;
    private String categoryId;
    private ArrayList<CareersModel.CareersData> arrayList;

    public CareersMainModel(String title, String categoryId, ArrayList<CareersModel.CareersData> arrayList) {
        this.title = title;
        this.categoryId = categoryId;
        this.arrayList = arrayList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<CareersModel.CareersData> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<CareersModel.CareersData> arrayList) {
        this.arrayList = arrayList;
    }
}
