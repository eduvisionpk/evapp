package com.codegenetics.eduvision.models.Scholarships;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScholarshipDetailsModel {

    /**
     * success : true
     * message : sucess
     * case : scholarship-detail
     * data : [{"updated":"2019-09-27","scholarship_id":"635","logo":"http://www.eduvision.edu.pk/scholarships/images/ibm-phd-fellowship-2020.png","scholarship_name":"IBM PhD Fellowship 2020 fully funded scholarship","detail":"<p>IBM has announced Two-Year Worldwide IBM PhD Fellowship 2020 for exceptional PhD students around the world in the field of computer science and information technology. Almost US$ 7 0,000/- will be provided to the students selected under IBM PhD Fellowship award. In addition to American, Australian, a number of Asian students from Chian, Japan and other countries have won the awards last year and even Two Indian students also won. We encourage Pakistani students to also apply for this exceptional award for their PhD studies.<\/p>\r\n\r\n<h3><strong>The 2020 IBM PhD Fellowship includes:<\/strong><\/h3>\r\n\r\n<ul>\r\n\t<li>An yearly stipend of US 35000 for Living, travel and conference (students studying in US universities)<\/li>\r\n\t<li>US $ 25,000 for education (students studying in US universities)<\/li>\r\n\t<li>Outside the US, fellowship recipients will receive a competitive stipend for living expenses, travel and to attend conferences for the two academic years 2020-2021 and 2021-2022.&nbsp; Fellowship stipends vary by country.<\/li>\r\n<\/ul>\r\n\r\n<p>&nbsp;<strong>Deadline:<\/strong>&nbsp;Nominations for the 2020 IBM PhD Fellowship Award program will be accepted from September 19 to October 24, 2019<\/p>\r\n\r\n<p>IBM gives importance to strong collaboration with faculty, students and universities. This doctoral fellowship award is aimed at serving the same. Focus areas include the following topics of particular interest:<\/p>\r\n\r\n<ul>\r\n\t<li>AI / Cognitive Computing<\/li>\r\n\t<li>Cloud / Open Source<\/li>\r\n\t<li>VideoCategoriesData Science<\/li>\r\n\t<li>Internet of Things<\/li>\r\n\t<li>Quantum Computing<\/li>\r\n\t<li>Security / Cyber Security<\/li>\r\n<\/ul>\r\n\r\n<h3><strong>Eligibility Criteria for IBM PhD Fellowship 2020<\/strong><\/h3>\r\n\r\n<ul>\r\n\t<li>PhD students must be nominated by a doctoral faculty member<\/li>\r\n\t<li>Students must be a full-time student in a PhD program over the two consecutive academic years of the award or forfeit their fellowship.<\/li>\r\n\t<li>Students should have three years remaining in their graduate program at the time of nomination.<\/li>\r\n\t<li>Students from US embargoed countries are not eligible for the program.<\/li>\r\n<\/ul>\r\n\r\n<p>Award recipients will be selected based on their overall potential for research excellence, the degree to which their technical interests align with those of IBM, and their academic progress to-date, as evidenced by publications and endorsements from their faculty advisor and department head. Students receiving a comparable fellowship or internship from another company or institution (does not include academic scholarships) during the same IBM funded academic period may not be eligible for an IBM PhD Fellowship.<\/p>\r\n\r\n<p>For more program details visit official link <a href=\"http://www.ibm.com/university/phdfellowship\">www.ibm.com/university/phdfellowship<\/a><\/p>\r\n\r\n<p><a href=\"http://www.ibm.com/university/phdfellowship\">http://www.ibm.com/university/phdfellowship<\/a><\/p>\r\n","ad":""}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private List<ScholarshipDetailsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<ScholarshipDetailsData> getData() {
        return data;
    }

    public void setData(List<ScholarshipDetailsData> data) {
        this.data = data;
    }

    public static class ScholarshipDetailsData {

        private String updated;
        private String scholarship_id;
        private String logo;
        private String scholarship_name;
        private String detail;
        private String ad;
        private String link;

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getScholarshipId() {
            return scholarship_id;
        }

        public void setScholarship_id(String scholarship_id) {
            this.scholarship_id = scholarship_id;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getScholarshipName() {
            return scholarship_name;
        }

        public void setScholarship_name(String scholarship_name) {
            this.scholarship_name = scholarship_name;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getAd() {
            return ad;
        }

        public void setAd(String ad) {
            this.ad = ad;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
