package com.codegenetics.eduvision.models;

import com.codegenetics.eduvision.enums.ProgramsEnums;

public class ProgramsInnerModel {

    private int url;
    private String title;
    private ProgramsEnums programsEnums;

    public ProgramsInnerModel(String title, int url, ProgramsEnums programsEnums) {
        this.title = title;
        this.url = url;
        this.programsEnums = programsEnums;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUrl() {
        return url;
    }

    public void setUrl(int url) {
        this.url = url;
    }

    public ProgramsEnums getProgramsEnums() {
        return programsEnums;
    }

    public void setProgramsEnums(ProgramsEnums programsEnums) {
        this.programsEnums = programsEnums;
    }
}
