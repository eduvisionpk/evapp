package com.codegenetics.eduvision.models.Levels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LevelsModel {

    /**
     * status : true
     * message : success
     * case : levels
     * data : [{"level_name":"BACHELOR","level_code":"7"},{"level_name":"MASTER (MA / MSc)","level_code":"4"},{"level_name":"MS / M.PHIL (18 years)","level_code":"3"},{"level_name":"DOCTORATE","level_code":"2"},{"level_name":"MATRIC/ O-LEVEL","level_code":"13"},{"level_name":"INTERMEDIATE / A-LEVEL","level_code":"10"},{"level_name":"PROFESSIONAL (CA/ACCA/FLYING ETC..)","level_code":"17"},{"level_name":"DIPLOMA / CERT (After INTER)","level_code":"8"},{"level_name":"DIPLOMA / CERT (After Matric)","level_code":"11"},{"level_name":"PG DIPLOMA / CERT","level_code":"5"},{"level_name":"MAJOR TRAINING [MEDICAL]","level_code":"16"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    private List<LevelsData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public List<LevelsData> getData() {
        return data;
    }

    public void setData(List<LevelsData> data) {
        this.data = data;
    }

    public static class LevelsData {
        /**
         * level_name : BACHELOR
         * level_code : 7
         */

        private String level_name;
        private String level_code;

        public LevelsData(String level_name, String level_code) {
            this.level_name = level_name;
            this.level_code = level_code;
        }

        public String getLevelName() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }

        public String getLevelCode() {
            return level_code;
        }

        public void setLevel_code(String level_code) {
            this.level_code = level_code;
        }
    }
}
