package com.codegenetics.eduvision.models.MeritCalculator;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public
class MeritCalculatorListModel {

    /**
     * success : true
     * message : success
     * case : merit-list
     * data : [{"institute":"Medical Merit Calculator for All Medical Colleges","ins":"medical"},{"institute":"UET Lahore and All Punjab Engineering Merit Calculator","ins":"uet-lahore"},{"institute":"NUST Merit Calculator","ins":"nust"},{"institute":"UET Peshawar Merit Calculator","ins":"uet-peshawar"},{"institute":"COMSATS Merit Caclulator","ins":"comsats"},{"institute":"IST Islamabad Merit Calculator","ins":"ist"}]
     */

    private boolean success;
    private String message;
    @SerializedName("case")
    private String caseX;
    /**
     * institute : Medical Merit Calculator for All Medical Colleges
     * ins : medical
     */

    private ArrayList<MeritCalculatorListData> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseX() {
        return caseX;
    }

    public void setCaseX(String caseX) {
        this.caseX = caseX;
    }

    public ArrayList<MeritCalculatorListData> getData() {
        return data;
    }

    public void setData(ArrayList<MeritCalculatorListData> data) {
        this.data = data;
    }

    public static class MeritCalculatorListData {
        private String institute;
        private String ins;

        public String getInstitute() {
            return institute;
        }

        public void setInstitute(String institute) {
            this.institute = institute;
        }

        public String getIns() {
            return ins;
        }

        public void setIns(String ins) {
            this.ins = ins;
        }
    }
}
