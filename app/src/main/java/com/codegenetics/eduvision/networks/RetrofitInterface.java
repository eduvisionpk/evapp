package com.codegenetics.eduvision.networks;


import com.codegenetics.eduvision.utils.Constants;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface RetrofitInterface {


//******************************************************************************************************************************************************
    /* Get Districts  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.DISTRICTS)
    Observable<ResponseBody> getDistricts();

//******************************************************************************************************************************************************
    /* Get Cities  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.CITIES)
    Observable<ResponseBody> getCities();


//******************************************************************************************************************************************************
    /* Get Levels  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.LEVELS)
    Observable<ResponseBody> getLevels();


//******************************************************************************************************************************************************
    /* Get DisciplineTypes  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.DISCIPLINE_TYPES)
    Observable<ResponseBody> getDisciplineTypes();


//******************************************************************************************************************************************************
    /* Get Disciplines  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.DISCIPLINES)
    Observable<ResponseBody> getDisciplines(
            @Query(value = Constants.Keys.DISCIPLINE_TYPE, encoded = true) String disciplineType,
            @Query(Constants.Keys.LEVEL_CODE) String levelCode
    );


//******************************************************************************************************************************************************
    /* Admissions  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.ADMISSIONS)
    Observable<ResponseBody> getAdmissions(
            @Query(Constants.Keys.PAGE_NUMBER) String pageNumber,
            @Query(Constants.Keys.CITY_CODE) String cityCode,
            @Query(Constants.Keys.LEVEL_CODE) String levelCode,
            @Query(value = Constants.Keys.DISCIPLINE_NAME, encoded = true) String disciplineName,
            @Query(value = Constants.Keys.DISCIPLINE_TYPE, encoded = true) String disciplineType
    );


//******************************************************************************************************************************************************
    /* Programs  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.PROGRAMS)
    Observable<ResponseBody> getPrograms(
            @Query(Constants.Keys.PAGE_NUMBER) String pageNumber,
            @Query(value = Constants.Keys.DISCIPLINE_TYPE, encoded = true) String disciplineType,
            @Query(value = Constants.Keys.DISCIPLINE, encoded = true) String discipline,
            @Query(value = Constants.Keys.CITY_NAME, encoded = true) String city,
            @Query(Constants.Keys.LEVEL_CODE) String levelCode
    );


//******************************************************************************************************************************************************
    /* Institute Details (profile)  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.INSTITUTE_DETAILS)
    Observable<ResponseBody> getInstituteDetails(
            @Query(Constants.Keys.INSTITUTE_ID) String instituteId
    );


//******************************************************************************************************************************************************
    /* Home Screen Slider  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.HOME_SLIDER)
    Observable<ResponseBody> getSliderData();


//******************************************************************************************************************************************************
    /* News  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.NEWS)
    Observable<ResponseBody> getNews(
            @Query(Constants.Keys.PAGE_NUMBER) String pageNumber,
            @Query(Constants.Keys.CATEGORY_ID) String categoryId
    );


//******************************************************************************************************************************************************
    /* News Details  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.NEWS_DETAILS)
    Observable<ResponseBody> getNewsDetail(
            @Query(Constants.Keys.ID) String newsId
    );


//******************************************************************************************************************************************************
    /* Scholarships  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.SCHOLARSHIPS)
    Observable<ResponseBody> getScholarships(
            @Query(Constants.Keys.PAGE_NUMBER) String pageNumber,
            @Query(value = Constants.Keys.AUTHORITY, encoded = true) String authority,
            @Query(value = Constants.Keys.LEVEL, encoded = true) String level,
            @Query(value = Constants.Keys.FIELD, encoded = true) String field,
            @Query(value = Constants.Keys.CATEGORY, encoded = true) String category,
            @Query(value = Constants.Keys.TYPE, encoded = true) String type
    );


//******************************************************************************************************************************************************
    /* Scholarship Details  */
//******************************************************************************************************************************************************


    @GET(Constants.UrlPaths.SCHOLARSHIP_DETAILS)
    Observable<ResponseBody> getScholarshipDetail(
            @Query(Constants.Keys.ID) String scholarshipId
    );


//******************************************************************************************************************************************************
    /* Career List  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.CAREER_LIST)
    Observable<ResponseBody> getCareers(@Query(Constants.Keys.CATEGORY_ID) String param);

//******************************************************************************************************************************************************
    /* Career Details  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.CAREER_DETAILS)
    Observable<ResponseBody> getCareerDetails(@Query(Constants.Keys.ID) String param);

//******************************************************************************************************************************************************
    /* Video Categories  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.VIDEO_CATEGORIES)
    Observable<ResponseBody> getVideoCategories();

//******************************************************************************************************************************************************
    /* Video Details  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.VIDEO_DETAILS)
    Observable<ResponseBody> getVideoDetails(
            @Query(Constants.Keys.PAGE_NUMBER) String pageNumber,
            @Query(Constants.Keys.TYPE) String type
    );

//******************************************************************************************************************************************************
    /* Entry Tests  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.ENTRY_TEST)
    Observable<ResponseBody> getEntryTests(
            @Query(Constants.Keys.TYPE) String type
    );

    @GET(Constants.UrlPaths.ENTRY_TEST_CATEGORIES)
    Observable<ResponseBody> getEntryTestCategories();


//******************************************************************************************************************************************************
    /* Entry Test Details  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.ENTRY_TEST_DETAILS)
    Observable<ResponseBody> getEntryTestDetails(
            @Query(Constants.Keys.ID) String id
    );

//******************************************************************************************************************************************************
    /* Merit Calculators List  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.MERIT_CALCULATORS_LIST)
    Observable<ResponseBody> getMeritCalculatorsList();

//******************************************************************************************************************************************************
    /* Merit Calculator  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.MERIT_CALCULATOR)
    Observable<ResponseBody> calculateMerit(
            @Query(Constants.Keys.METRIC_OBTAINED_MARKS) String metricObtainedMarks,
            @Query(Constants.Keys.METRIC_TOTAL_MARKS) String metricTotalMarks,
            @Query(Constants.Keys.FSC_OBTAINED_MARKS) String fscObtainedMarks,
            @Query(Constants.Keys.FSC_TOTAL_MARKS) String fscTotalMarks,
            @Query(Constants.Keys.ENTRY_TEST_PERCENTAGE) String entryTestPercentage,
            @Query(Constants.Keys.INSTITUTE) String institute
    );

//******************************************************************************************************************************************************
    /* Notificaitons  */
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.NOTIFICATIONS)
    Observable<ResponseBody> getNotifications();

//******************************************************************************************************************************************************
    //Career counseling
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.CAREER_COUNSELING)
    Observable<ResponseBody> careerCounseling();

    @GET(Constants.UrlPaths.CAREER_COUNSELING_DETAILS)
    Observable<ResponseBody> careerCounselingDetails(@Query(Constants.Keys.ID) String id);

//******************************************************************************************************************************************************
    //Past Papers
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.PAST_PAPERS_BOARDS_CLASSES)
    Observable<ResponseBody> getPastPaperBoardsAndClasses();


    @GET(Constants.UrlPaths.PAST_PAPERS_SUBJECTS)
    Observable<ResponseBody> getPastPapersSubjects(
            @Query(Constants.Keys.BOARD) String board,
            @Query(Constants.Keys.CLASS) String class_
    );

    @GET(Constants.UrlPaths.PAST_PAPERS_DETAILS)
    Observable<ResponseBody> getPastPapersDetails(
            @Query(Constants.Keys.BOARD) String boards,
            @Query(Constants.Keys.CLASS) String class_,
            @Query(Constants.Keys.SUBJECT) String subject
    );

//******************************************************************************************************************************************************
    //Ranking
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.LIST_OF_BOARDS)
    Observable<ResponseBody> getListOfBoards();

    @GET(Constants.UrlPaths.DIST_PROVINCE)
    Observable<ResponseBody> getProvinceAndDistricts();

    @GET(Constants.UrlPaths.BOARD_WISE_RANKING)
    Observable<ResponseBody> getBoardWiseRanking(
            @Query(Constants.Keys.BOARD) String board,
            @Query(Constants.Keys.CATEGORY_) String category,
            @Query(Constants.Keys.LEVEL) String level
    );

    @GET(Constants.UrlPaths.AREA_WISE_RANKING)
    Observable<ResponseBody> getAreaWiseRanking(
            @Query(Constants.Keys.CITY) String city,
            @Query(Constants.Keys.CATEGORY_) String category,
            @Query(Constants.Keys.LEVEL) String level
    );


//******************************************************************************************************************************************************
    //Admission Planner
//******************************************************************************************************************************************************

    @GET(Constants.UrlPaths.LOW_FEE_INSTITUTES)
    Observable<ResponseBody> getLowFeeInstitutes(
            @Query(Constants.Keys.DISCIPLINE_TYPE) String disciplineType,
            @Query(Constants.Keys.DISCIPLINE) String disciplineName,
            @Query(Constants.Keys.LEVEL_CODE) String levelCode,
            @Query(Constants.Keys.CITY_NAME) String cityName
            );

    @GET(Constants.UrlPaths.LOW_MERIT_INSTITUTES)
    Observable<ResponseBody> getLowMeritInstitutes(
            @Query(Constants.Keys.DISCIPLINE_TYPE) String disciplineType,
            @Query(Constants.Keys.DISCIPLINE) String disciplineName,
            @Query(Constants.Keys.LEVEL_CODE) String levelCode,
            @Query(Constants.Keys.CITY_NAME) String cityName
    );

    @GET(Constants.UrlPaths.HIGH_MERIT_INSTITUTES)
    Observable<ResponseBody> getHighMeritInstitutes(
            @Query(Constants.Keys.DISCIPLINE_TYPE) String disciplineType,
            @Query(Constants.Keys.DISCIPLINE) String disciplineName,
            @Query(Constants.Keys.LEVEL_CODE) String levelCode,
            @Query(Constants.Keys.CITY_NAME) String cityName
    );


//******************************************************************************************************************************************************
//******************************************************************************************************************************************************

}
