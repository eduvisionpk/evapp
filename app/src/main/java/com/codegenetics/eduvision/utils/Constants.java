package com.codegenetics.eduvision.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import androidx.core.app.RemoteInput;
import androidx.core.graphics.drawable.IconCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.FirebaseConstants;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.models.Cities.CitiesModel;
import com.codegenetics.eduvision.models.DisciplineTypes.DisciplineTypesModel;
import com.codegenetics.eduvision.models.Levels.LevelsModel;
import com.codegenetics.eduvision.models.MessagesModels.User;
import com.codegenetics.eduvision.models.NotifyModel;
import com.codegenetics.eduvision.services.MessageReceiver;
import com.codegenetics.eduvision.views.activities.CareerTv;
import com.codegenetics.eduvision.views.activities.HomeActivity;
import com.codegenetics.eduvision.views.activities.Login;
import com.codegenetics.eduvision.views.activities.Messages;
import com.codegenetics.eduvision.views.activities.Scholarships;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tuyenmonkey.mkloader.MKLoader;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.AlbumLoader;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.text.Html.fromHtml;

@SuppressWarnings("ALL")
public class Constants {

    private static InterstitialAd mInterstitialAd;

    private static Constants INSTANCE = null;

    private static String firebaseToken = "";

    private Constants() { }

    public static Constants getInstance() {
        if (INSTANCE == null) INSTANCE =  new Constants();
        return INSTANCE;
    }

    private AlertDialog alertDialog;
    private MKLoader alertDialogLoader;
    private TextView alertDialogUpdate;
    public static final String BASE_URL = "https://www.eduvision.edu.pk/";
    public static final String BOOK_A_SESSION_LINK = "https://docs.google.com/forms/d/e/1FAIpQLSfxpHFttGB794lwZvRt3WqnhIIk1oP-gQGVkk79q9sHB_lCSw/viewform";

    public static final String INVALID_EMAIL = "Invalid Email\nPlease follow this pattern\n(name@domain.com)";
    public static final String INVALID_PASSWORD = "Invalid Password\nPassword should be alphanumeric at least 6 in length, including Upper/Lowercase & number.";

    public static HashMap<String, ArrayList<NotifyModel>> map = new HashMap<>();
    public static int adCount = 0;
    private static int adFailure = 0;

    public static ArrayList<LevelsModel.LevelsData> levelsList = new ArrayList<>();
    public static ArrayList<CitiesModel.CitiesData> citiesList = new ArrayList<>();
    public static ArrayList<DisciplineTypesModel.DisciplineTypesData> disciplineTypeList = new ArrayList<>();
    public static List<UnifiedNativeAd> mNativeAds = new ArrayList<>();

    public void loadInterstitial(Context context) {

        Log.e("AdTestInt", "onInterstitialAdLoadCalled");

        if (mInterstitialAd == null || !mInterstitialAd.isLoaded() || !mInterstitialAd.isLoading()) {

            Log.e("AdTestInt", "onInterstitialAdLoad started");

            mInterstitialAd = new InterstitialAd(context);
            mInterstitialAd.setAdUnitId(context.getResources().getString(R.string.interstitial_ad_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.e("AdTestInt", "onInterstitialAdLoad Loaded");
                }

                @Override
                public void onAdFailedToLoad(LoadAdError loadAdError) {
                    super.onAdFailedToLoad(loadAdError);
                    if (loadAdError!=null)
                        Log.e("AdTestInt", "onInterstitialLoad Failed: " + loadAdError.toString());
                }

            });
        }
        else {
            Log.e("AdTestInt", mInterstitialAd == null ? "interstitial ad  is null": "already loaded InterstitialAd");
        }
    }

    public void showInterstitial(Context context) {

        Log.e("AdTestInt", "onGoogleInterstitialAdCalled");

        if (context != null && adFailure < 2) {

            if (mInterstitialAd != null && mInterstitialAd.isLoaded() ) {

                Log.e("AdTestInt", "onGoogleInterstitialAd showing Already Loaded");
                mInterstitialAd.setAdListener(new AdListener(){
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();

                        mInterstitialAd = null;
                        loadInterstitial(context);
                    }
                });
                mInterstitialAd.show();
                Constants.adCount = Constants.adFailure = 0;

            }
            else {

                Log.e("AdTestInt", "onGoogleInterstitialAd Loading ...");

                mInterstitialAd = null;
//
//                mInterstitialAd = new InterstitialAd(context);
//                mInterstitialAd.setAdUnitId(context.getResources().getString(R.string.test_interstitial_ad_id));
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                loadInterstitial(context);
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        Log.e("AdTestInt", "onGoogleInterstitialAdLoaded");
                        if (mInterstitialAd != null) mInterstitialAd.show();
                        Constants.adCount = Constants.adFailure = 0;
                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();

                        mInterstitialAd = null;
                        loadInterstitial(context);
                    }

                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);

                        showInterstitial(context);
                        ++adFailure;

                        if (loadAdError!=null)
                            Log.e("AdTestInt", "onGoogleAdInterstitialFailed: " + loadAdError.toString());
                    }


                });
            }


        } else {
            adFailure = 0;
        }

    }

    public void hideKeyboard(Activity activity) {
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            try {
                InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputManager != null)
                    inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public void showKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null)
                inputManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    public String getEncodedText(String text) {

        if (text==null || text.isEmpty()) return text;

        text = text.replace("\n", "<br/>");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            text =  String.valueOf(fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
        } else {
            text =  String.valueOf(fromHtml(text));
        }

        return text;
    }

    public String splitVideoIdFromURL(String url) {

        if (url.contains("v=")) {
            String[] u = url.split("v=");
            if (!url.contains("&")) url = u[1];
            else {
                u = u[1].split("&");
                url = u[0];
            }
        } else {
            String[] u = url.split("be/");
            url = u[1];
        }
        return url;
    }

    public boolean isValidPassword(String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{6,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public String getFirebaseToken() {

        firebaseToken = Session.getInstance().getDeviceId();

        log("FirebaseToken", firebaseToken);

        if (firebaseToken.isEmpty()) {
            FirebaseInstanceId
                    .getInstance()
                    .getInstanceId()
                    .addOnSuccessListener(instanceIdResult -> {
                        firebaseToken = instanceIdResult.getToken();
                        Session.getInstance().setDeviceId(firebaseToken);
                    });
        }
        return firebaseToken;
    }

    public String encode(String value) {
        value = value.toLowerCase();
        String email = Base64.encodeToString(value.getBytes(), Base64.DEFAULT);
        if (email.endsWith("\n")) {
            email = email.substring(0, email.length()-1);
        }
        return email;
    }

    public String decode(String value) {
        return new String(Base64.decode(value, Base64.DEFAULT));
    }

    public User getMyDetails() {
        return new User(
                String.valueOf(Session.getInstance().getEmail()),
                Session.getInstance().getName(),
                Session.getInstance().getImage(),
                true
        );
    }

    public Date getDate (long milliseconds){
        return new Date(milliseconds);

    }

    public void share(Context context, String data) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, data);
        context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.app_name)));
    }

    public void rateApp(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        );
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public void openLink(Context context, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    public static void log(@Nullable Context context, String msg) {
        if (msg != null) {
            if (context!=null) Log.e(context.getClass().getName(), msg);
            else Log.e("Log Value", msg);
        }
    }

    public static void log(String tag, String msg) {
        if (msg != null) {
            if (tag != null) Log.e(tag, msg);
            else Log.e("Log Value", msg);
        }
    }

    public static void log(@Nullable Context context, int msg) {
        if (context != null) log(context, String.valueOf(msg));

    }

    public int getIcon(String type) {
        switch (type) {
            default:
            case "Medical Sciences":
                return R.drawable.medical_science;
            case "Engineering":
                return R.drawable.engineering;
            case "Technical":
                return R.drawable.tehnical;
            case "Computer Sciences & Information Technology":
                return R.drawable.computer_science;
            case "Art & design":
                return R.drawable.arts;
            case "Management Sciences":
                return R.drawable.management_science;
            case "Social Sciences":
                return R.drawable.social_science;
            case "Biological & Life Sciences":
                return R.drawable.biological_science;
            case "Chemical & Material Sciences":
                return R.drawable.chemical_science;
            case "Physics & Numerical Sciences":
                return R.drawable.physics_science;
            case "Earth & Environmental Sciences":
                return R.drawable.earth_science;
            case "Agriculture Sciences":
                return R.drawable.agriculture_science;
            case "Religious Studies":
                return R.drawable.religion_study;
            case "Media Sciences":
                return R.drawable.media_science;
            case "Commerce / Finance & Accounting":
                return R.drawable.accounts;
        }
    }

    public void showClosingAppDialog(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder( context);
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setTitle("Closing App");
        builder.setMessage("Are you sure you want to close app?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", (dialog, which) -> context.finishAffinity());
        builder.setNegativeButton("No", null);
        builder.show();
    }

    public void showLogoutDialog(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder( context);
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", (dialog, which) -> {
            Session.getInstance().setLogout();
            context.startActivity(new Intent(context, Login.class));
            context.finish();
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    public void showVerificationDialog(Activity activity, FirebaseUser user) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_email_verification, null);
        dialogBuilder.setView(dialogView);

        TextView tvResend = dialogView.findViewById(R.id.tv_resend);
        TextView tvCancel = dialogView.findViewById(R.id.tv_cancel);

        tvResend.setOnClickListener(v -> {
            if (alertDialog != null) alertDialog.dismiss();
            sendVerificationEmail(activity, user);
        });

        tvCancel.setOnClickListener(v -> {
            if (alertDialog != null) alertDialog.dismiss();
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        Window window2 = alertDialog.getWindow();

        if (window2 != null) {
            window2.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }


    }

    private void sendVerificationEmail(Activity activity, FirebaseUser user) {

        user.sendEmailVerification()
                .addOnCompleteListener(activity, task -> {

            if (task.isSuccessful()) {
                Toast.makeText(activity,
                        "Verification email sent to " + user.getEmail(),
                        Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(activity,
                        "Failed to send verification email. Please try again later.",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void showForgotDetailsDialog(Activity activity) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_forget_password, null);
        dialogBuilder.setView(dialogView);

        EditText etInput = dialogView.findViewById(R.id.et_email);
        TextView tvSend = dialogView.findViewById(R.id.tv_send);
        TextView tvCancel = dialogView.findViewById(R.id.tv_cancel);

        tvSend.setOnClickListener(v -> {
            String input = etInput.getText().toString().trim();

            if (input.isEmpty() || !isValidEmail(input)) {
                Toast.makeText(activity, INVALID_EMAIL, Toast.LENGTH_SHORT).show();
                return;
            }

            sendResetPasswordEmail(activity, input);

            if (alertDialog != null) alertDialog.dismiss();

        });

        tvCancel.setOnClickListener(v -> {
            if (alertDialog != null) alertDialog.dismiss();
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        Window window2 = alertDialog.getWindow();

        if (window2 != null) {
            window2.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

    }

    private void sendResetPasswordEmail(Activity activity, String emailAddress) {
        FirebaseAuth auth = FirebaseAuth.getInstance();

        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(activity, "Reset password email has been send to " + emailAddress, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, activity.getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void showChangePasswordDialog(Activity activity) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_change_password, null);
        dialogBuilder.setView(dialogView);

        EditText etCurrentPassword = dialogView.findViewById(R.id.et_current_password);
        EditText etNewPassword = dialogView.findViewById(R.id.et_new_password);
        ImageView ivCurrentPassword = dialogView.findViewById(R.id.iv_current_password);
        ImageView ivNewPassword = dialogView.findViewById(R.id.iv_new_password);
        alertDialogUpdate = dialogView.findViewById(R.id.tv_update);
        TextView tvCancel = dialogView.findViewById(R.id.tv_cancel);
        alertDialogLoader = dialogView.findViewById(R.id.loader);

        ivCurrentPassword.setOnClickListener(view -> {
            if (etCurrentPassword.getTransformationMethod() == null) {
                etCurrentPassword.setTransformationMethod(new PasswordTransformationMethod());
                ivCurrentPassword.setImageResource(R.drawable.ic_visibility_on);
            } else {
                etCurrentPassword.setTransformationMethod(null);
                ivCurrentPassword.setImageResource(R.drawable.ic_visibility_off);
            }
            etCurrentPassword.setSelection(etCurrentPassword.getText().toString().length());
        });

        ivNewPassword.setOnClickListener(view -> {
            if (etNewPassword.getTransformationMethod() == null) {
                etNewPassword.setTransformationMethod(new PasswordTransformationMethod());
                ivNewPassword.setImageResource(R.drawable.ic_visibility_on);
            } else {
                etNewPassword.setTransformationMethod(null);
                ivNewPassword.setImageResource(R.drawable.ic_visibility_off);
            }
            etNewPassword.setSelection(etNewPassword.getText().toString().length());
        });

        alertDialogUpdate.setOnClickListener(v -> {
            String currentPassword = etCurrentPassword.getText().toString().trim();
            String newPassword = etNewPassword.getText().toString().trim();


            if (currentPassword.isEmpty() || !isValidPassword(currentPassword)) {
                Toast.makeText(activity, INVALID_PASSWORD, Toast.LENGTH_SHORT).show();
                return;
            }

            if (newPassword.isEmpty() || !isValidPassword(newPassword)) {
                Toast.makeText(activity, INVALID_PASSWORD, Toast.LENGTH_SHORT).show();
                return;
            }

            hideKeyboard(activity);

            showProgress();

            updatePassword(activity, currentPassword, newPassword);

        });

        tvCancel.setOnClickListener(v -> {
            if (alertDialog != null) alertDialog.dismiss();
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        Window window2 = alertDialog.getWindow();

        if (window2 != null) {
            window2.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

    }

    private void updatePassword(Activity activity, String currentPassword, String newPassword) {

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        AuthCredential credential = EmailAuthProvider
                .getCredential(Session.getInstance().getEmail(), currentPassword);

        currentUser.reauthenticate(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser.updatePassword(newPassword)
                                .addOnCompleteListener(task1 -> {
                                    if (task1.isSuccessful()) {
                                        Toast.makeText(activity, "Password updated successfully.", Toast.LENGTH_SHORT).show();
                                        if (alertDialog != null) alertDialog.dismiss();
                                    } else {
                                        hideProgress();
                                        Toast.makeText(activity, activity.getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else {
                        hideProgress();
                        if (task.getException() != null) {
                            Toast.makeText(activity, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, activity.getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }

    private void showProgress() {

        if (alertDialogUpdate != null) alertDialogUpdate.setVisibility(View.GONE);
        if (alertDialogLoader != null) alertDialogLoader.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (alertDialogUpdate != null) alertDialogUpdate.setVisibility(View.VISIBLE);
        if (alertDialogLoader != null) alertDialogLoader.setVisibility(View.GONE);
    }

    private int getRandomNumber() {
        return new Random().nextInt(1000 - 1) + 1;
    }

    private Intent getReplyMessageIntent(Context context, int notificationId, String title) {
        Log.e("NotificationTest", "getReplyMessageIntent=" + notificationId);
        Intent intent = new Intent(context, MessageReceiver.class);
        intent.setAction(MessageReceiver.REPLY_ACTION);
        intent.putExtra(IntentEnums.ID.toString(), notificationId);
        intent.putExtra(IntentEnums.TITLE.toString(), title);
        return intent;
    }

    public static CharSequence getReplyMessage(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            Log.e("NotificationTest", "getReplyMessage" );
            return remoteInput.getCharSequence(MessageReceiver.KEY_REPLY);
        }
        return null;
    }

    public static void updateNotification(Context context, int notifyId) {

//        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "my_channel_01")
//                .setContentText(message)
//                .setDefaults(Notification.DEFAULT_SOUND)
//                .setAutoCancel(true)
//                .setSmallIcon(R.drawable.ic_notification_logo)
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setStyle(new NotificationCompat.BigTextStyle());
//
//        Notification notification = builder.build();
        Log.e("NotificationTest", "cancelling=" + notifyId);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) notificationManager.cancel(notifyId);

        //if (notificationManager != null) notificationManager.notify(notifyId, notification);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void NotificationForOreo(Context context, String title, String message, String type) {

        int i = new Random().nextInt(10000 - 999) + 999;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");
        String cid = "my_channel_01";

        String description = "Eduvision";

        int importance = NotificationManager.IMPORTANCE_HIGH;

        NotificationChannel mChannel = new NotificationChannel(cid, title, importance);

        mChannel.setDescription(description);

        mChannel.enableLights(true);

        mChannel.setLightColor(Color.RED);

        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        Intent intent;

        switch (type) {
            case "video":
                intent = new Intent(context, CareerTv.class);
                break;
            case "scholarship":
                intent = new Intent(context, Scholarships.class);
                break;
            default:
                intent = new Intent(context, HomeActivity.class);
                break;

        }

        PendingIntent pendingIntent = PendingIntent.getActivity(context, i, intent, 0);

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_app_logo)
                .setNumber(i)
                .setDefaults(Notification.DEFAULT_ALL)
                .setStyle(new Notification.BigTextStyle())
                .setChannelId(cid);

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
        notificationManager.notify(i, notification);

    }

    public void NotificationBelowOreo(Context context, String title,  String message, String type) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");

        int nid = new Random().nextInt(10000 - 999) + 999;


        Intent intent;

        switch (type) {
            case "video":
                intent = new Intent(context, CareerTv.class);
                break;
            case "scholarship":
                intent = new Intent(context, Scholarships.class);
                break;
            default:
                intent = new Intent(context, HomeActivity.class);
                break;

        }


        PendingIntent pendingIntent = PendingIntent.getActivity(context, nid,
                intent, 0);
        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setPriority(Notification.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_app_logo)
                .setStyle(new Notification.BigTextStyle().bigText(message))
                .setDefaults(Notification.DEFAULT_ALL);

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;

        notificationManager.notify(nid, notification);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void messageNotificationForOreo(Context context, String title, String message, String userId) {

        AsyncTask.execute(() -> {

            int notificationId = 1;

            ArrayList<NotifyModel> arrayList = map.get(FirebaseConstants.CONSULTANT);
            if (arrayList == null) arrayList = new ArrayList<>();

            NotifyModel model = new NotifyModel();
            model.setMessage(message);
            model.setImgId(R.drawable.ic_profile_avatar);
            model.setUsername(title);

            arrayList.add( model);
            if (arrayList.size() > 5) arrayList.remove(0);
            map.put(FirebaseConstants.CONSULTANT, arrayList);

            Log.e("NotificationTest", "notificationGenerated=" + notificationId + " message=" + message);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("RSSPullService");
            String cid = "my_channel_01";

            String description = "Travelfic";

            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(cid, title, importance);

            mChannel.setDescription(description);

            mChannel.enableLights(true);

            mChannel.setLightColor(Color.RED);

            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            Intent intent = new Intent(context, Messages.class)
                    .putExtra(IntentEnums.GO_TO_HOME.toString(), true);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, notificationId, intent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, cid)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_app_logo)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setWhen(System.currentTimeMillis())
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setNumber(new Random().nextInt())
                    .setLights(Color.BLUE, 500, 500)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setChannelId(cid);

            RemoteInput remoteInput = new RemoteInput.Builder(MessageReceiver.KEY_REPLY).setLabel("Reply").build();

            Intent replyIntent = getReplyMessageIntent(context, notificationId, title);
            replyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            PendingIntent replyPendingIntent = PendingIntent.getBroadcast(
                    context,
                    notificationId,
                    replyIntent,
                    PendingIntent.FLAG_ONE_SHOT
            );

            NotificationCompat.Action action = new NotificationCompat.Action.Builder(
                    R.drawable.ic_app_logo, "Reply", replyPendingIntent
            ).addRemoteInput(remoteInput)
                    .setAllowGeneratedReplies(true)
                    .build();

            builder.addAction(action).setContentIntent(replyPendingIntent);

            FutureTarget<Bitmap> submit = Glide.with(context).asBitmap().load(R.drawable.ic_profile_avatar).submit();

            final Bitmap[] bitmap = {null};

            try {
                bitmap[0] = submit.get();

                IconCompat iconCompat;

                if (bitmap[0] == null) iconCompat = IconCompat.createWithResource(context, R.drawable.ic_user_male);
                else iconCompat = IconCompat.createWithBitmap(bitmap[0]);

                Person person = new Person.Builder()
                        .setName(title)
                        .setIcon(iconCompat)
                        .build();

                Glide.with(context).clear(submit);

                NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(person);

                for (NotifyModel s : arrayList) {

                    messagingStyle.addMessage(new NotificationCompat.MessagingStyle.Message(
                            s.getMessage(), System.currentTimeMillis(), person
                    ));

                }
                builder.setStyle(messagingStyle);


            }
            catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }


            builder.setContentIntent(pendingIntent);

            Notification notification = builder.build();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
            notificationManager.notify(notificationId, notification);


        });

    }

    public void messageNotificationBelowOreo(Context context, String title, String message, String userId) {

        AsyncTask.execute(() -> {

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("RSSPullService");

            int nid = 1;

            ArrayList<NotifyModel> arrayList = map.get(FirebaseConstants.CONSULTANT);
            if (arrayList == null) arrayList = new ArrayList<>();

            NotifyModel model = new NotifyModel();
            model.setMessage(message);
            model.setImgId(R.drawable.ic_profile_avatar);
            model.setUsername(title);

            arrayList.add( model);
            if (arrayList.size() > 5) arrayList.remove(0);
            map.put(FirebaseConstants.CONSULTANT, arrayList);

            PendingIntent replyPendingIntent;
            NotificationCompat.Action action;

            Intent intent = new Intent(context, Messages.class)
                    .putExtra(IntentEnums.GO_TO_HOME.toString(), true);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, nid, intent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, String.valueOf(nid))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setPriority(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? NotificationManager.IMPORTANCE_HIGH: NotificationCompat.PRIORITY_HIGH)
                    .setSmallIcon(R.drawable.ic_profile_avatar)
                    .setLights(Color.BLUE, 500, 500)
                    .setDefaults(Notification.DEFAULT_ALL);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                RemoteInput remoteInput = new RemoteInput.Builder(MessageReceiver.KEY_REPLY).setLabel("Reply").build();

                Intent replyIntent = getReplyMessageIntent(context, nid, title);
                replyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                replyPendingIntent = PendingIntent.getBroadcast(
                        context,
                        0,
                        replyIntent,
                        PendingIntent.FLAG_ONE_SHOT
                );

                action = new NotificationCompat.Action.Builder(
                        R.drawable.ic_app_logo, "Reply", replyPendingIntent
                ).addRemoteInput(remoteInput)
                        .setAllowGeneratedReplies(true)
                        .build();

                builder.addAction(action);
                if (replyPendingIntent!=null) builder.setContentIntent(replyPendingIntent);


            }
            //
//        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
//
//        for (String s : arrayList) inboxStyle.addLine(s);
//
//        builder.setStyle(inboxStyle);
            FutureTarget<Bitmap> submit = Glide.with(context).asBitmap().load(R.drawable.ic_profile_avatar).submit();
            final Bitmap[] bitmap = {null};

            try {

                bitmap[0] = submit.get();

                IconCompat iconCompat;

                if (bitmap[0] == null) iconCompat = IconCompat.createWithResource(context, R.drawable.ic_user_male);
                else iconCompat = IconCompat.createWithBitmap(bitmap[0]);

                Person person = new Person.Builder()
                        .setName(title)
                        .setIcon(iconCompat)
                        .build();

                Glide.with(context).clear(submit);

                NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(person);

                for (NotifyModel s : arrayList) {

                    messagingStyle.addMessage(new NotificationCompat.MessagingStyle.Message(
                            s.getMessage(), System.currentTimeMillis(), person
                    ));

                }
                builder.setStyle(messagingStyle);


            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }

            builder.setContentIntent(pendingIntent);

            Notification notification = builder.build();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;

            notificationManager.notify(nid, notification);



        });
//
//        try {
//
//            FutureTarget<Bitmap> submit = Glide.with(context).asBitmap().load(avatar).submit();
//            builder.setLargeIcon((submit.get()));
//            Glide.with(context).clear(submit);
//
//
//            Person person = new Person.Builder()
//                    .setName(title)
//                    .setIcon(IconCompat.createWithBitmap(submit.get()))
//                    .build();
//
//
//            NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(person);
//
//            for (String s : arrayList) messagingStyle.addMessage(s, System.currentTimeMillis(), person);
//
//            builder.setStyle(messagingStyle);
//
//
//        } catch (ExecutionException | InterruptedException e) {
//            e.printStackTrace();
//        }


    }

    public static class Cases {
        public static final String CITIES = "cities";
        public static final String LEVELS = "levels";
        public static final String DISCIPLINE_TYPE = "discipline-type";
        public static final String DISCIPLINES = "disciplines";
        public static final String DISCIPLINES_PROGRAMS = "disciplines";
        public static final String SIGN_UP = "sign-up";
        public static final String SIGN_IN = "sign-in";
        public static final String ADMISSIONS = "Admissions";
        public static final String PROGRAMS = "programs-offers";
        public static final String INSTITUTE_DETAILS = "ins-profile";
        public static final String INSTITUTE_PROGRAMS = "ins-programs";
        public static final String HOME_SLIDER = "home-screen";
        public static final String SCHOLARSHIPS = "scholarship-list";
        public static final String SCHOLARSHIP_DETAILS = "scholarship-detail";
        public static final String NEWS = "news-list";
        public static final String NEWS_DETAILS = "news-detail";
        public static final String CAREER_LIST = "career-list";
        public static final String CAREER_DETAILS = "career-detail";
        public static final String VIDEO_DETAILS = "video-detail";
        public static final String VIDEO_CATEGORIES = "video-cat";
        public static final String ENTRY_TEST_CATEGORIES = "test-category";
        public static final String ENTRY_TEST = "test-list";
        public static final String ENTRY_TEST_DETAILS = "test-detail";
        public static final String MERIT_CALCULATORS_LIST = "merit-list";
        public static final String MERIT_CALCULATOR = "merit";
        public static final String NOTIFICATIONS = "notifications";
        public static final String PAST_PAPERS_BOARDS_CLASSES = "past-papers";
        public static final String PAST_PAPERS_SUBJECTS = "Subjects";
        public static final String PAST_PAPERS_DETAILS = "Papers";
        public static final String CAREER_COUNSELING = "cp-sections";

        public static final String ALL_NEWS = "";
        public static final String GENERAL_NEWS = "1";
        public static final String EXAM_NEWS = "8";
        public static final String PARENT_NEWS = "2";
        public static final String EVENT_NEWS = "7";
        public static final String COUNSELING_NEWS = "4";
        public static final String JOBS = "6";

        public static final String NOTIFICATION_TYPE_NEWS = "news";
        public static final String NOTIFICATION_TYPE_SCHOLARSHIPS = "scholarships";
        public static final String NOTIFICATION_TYPE_ADMISSIONS = "admissions";
        public static final String NOTIFICATION_TYPE_VIDEOS = "videos";
        public static final String RANK_CITY = "ranking-disctricts";
        public static final String RANK_BOARD = "ranking-board-list";
        public static final String RANK_DETAIL_LIST = "ranking-area";

    }

    public static class UrlPaths {
        public static final String DISTRICTS = "admissions/evapp1020/districts.php";
        public static final String CITIES = "admissions/evapp1020/cities.php";
        public static final String LEVELS = "admissions/evapp1020/levels.php";
        public static final String DISCIPLINE_TYPES = "admissions/evapp1020/discipline-type.php";
        public static final String DISCIPLINES = "admissions/evapp1020/disciplines.php";
        public static final String ADMISSIONS = "admissions/evapp1020/admissions.php";
        public static final String PROGRAMS = "admissions/evapp1020/program-offers.php";
        public static final String INSTITUTE_DETAILS = "admissions/evapp1020/ins-profile.php";
        public static final String SCHOLARSHIPS = "admissions/evapp1020/scholarships.php";
        public static final String SCHOLARSHIP_DETAILS = "admissions/evapp1020/scholarships-detail.php";
        public static final String HOME_SLIDER = "edu_news/evapp/home-screen.php";
        public static final String NEWS = "edu_news/evapp/news-list.php";
        public static final String NEWS_DETAILS = "edu_news/evapp/news-detail.php";
        public static final String CAREER_LIST = "admissions/evapp1020/careers-lists.php";
        public static final String CAREER_DETAILS = "admissions/evapp1020/careers-detail.php";
        public static final String VIDEO_DETAILS = "admissions/evapp1020/video-detail.php";
        public static final String VIDEO_CATEGORIES = "admissions/evapp1020/video-cat.php";
        public static final String ENTRY_TEST = "admissions/evapp1020/test-list.php";
        public static final String ENTRY_TEST_CATEGORIES = "admissions/evapp1020/test-category.php";
        public static final String ENTRY_TEST_DETAILS = "admissions/evapp1020/test-detail.php";
        public static final String MERIT_CALCULATORS_LIST = "admissions/evapp1020/merit.php";
        public static final String MERIT_CALCULATOR = "admissions/evapp1020/merit-calculator.php";
        public static final String NOTIFICATIONS = "edu_news/evapp/notification.php";
        public static final String CAREER_COUNSELING = "admissions/evapp1020/cp-sections.php";
        public static final String CAREER_COUNSELING_DETAILS = "admissions/evapp1020/cp-detail.php?";
        public static final String PAST_PAPERS_BOARDS_CLASSES = "admissions/evapp1020/past-papers.php";
        public static final String PAST_PAPERS_SUBJECTS = "admissions/evapp1020/subjects.php";
        public static final String PAST_PAPERS_DETAILS = "admissions/evapp1020/papers.php";

        public static final String LIST_OF_BOARDS = "admissions/evapp1020/ranking-board-list.php";
        public static final String DIST_PROVINCE = "admissions/evapp1020/ranking-districts.php";
        public static final String BOARD_WISE_RANKING = "admissions/evapp1020/ranking-board.php";
        public static final String AREA_WISE_RANKING = "admissions/evapp1020/ranking-area.php";

        public static final String LOW_FEE_INSTITUTES = "admissions/evapp1020/low-fee.php";
        public static final String LOW_MERIT_INSTITUTES = "admissions/evapp1020/low-merit.php";
        public static final String HIGH_MERIT_INSTITUTES = "admissions/evapp1020/high-merit.php";

    }

    public static class Keys {

        public static final String LEVEL_CODE = "level_code";
        public static final String DISCIPLINE_TYPE = "discipline_type";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String NAME = "name";
        public static final String MOBILE = "mobile_no";
        public static final String CITY = "city";
        public static final String CITY_NAME = "city_name";
        public static final String USER_ID = "user_id";
        public static final String PAGE_NUMBER = "page_no";
        public static final String CATEGORY_ID = "catid";
        public static final String CITY_CODE = "city_code";
        public static final String DISCIPLINE_NAME = "discipline_name";
        public static final String DISCIPLINE = "discipline";
        public static final String INSTITUTE = "ins";
        public static final String INSTITUTE_ID = "institute_id";
        public static final String ID = "id";
        public static final String AUTHORITY = "authority";
        public static final String LEVEL = "level";
        public static final String FIELD = "field";
        public static final String CATEGORY = "cat";
        public static final String CATEGORY_ = "category";
        public static final String TYPE = "type";
        public static final String BOARD = "board";
        public static final String CLASS = "class";
        public static final String SUBJECT = "subject";

        public static final String METRIC_OBTAINED_MARKS = "mssc";
        public static final String METRIC_TOTAL_MARKS = "tssc";
        public static final String FSC_OBTAINED_MARKS = "mfsc";
        public static final String FSC_TOTAL_MARKS = "tfsc";
        public static final String ENTRY_TEST_PERCENTAGE = "met";
        public static final String RANKING_TYPE = "ranking_type";

    }

    public static class VALUES{
        public static final int AREA_WISE_RANKING = 1;
        public static final int BOARD_WISE_RANKING = 2;
    }

    //Used for 'com.yanzhenjie:album:2.1.3' library to load images
    public static class MediaLoader implements AlbumLoader {

        @Override
        public void load(ImageView imageView, AlbumFile albumFile) {
            load(imageView, albumFile.getPath());
        }

        @Override
        public void load(ImageView imageView, String url) {

            Glide.with(imageView)
                    .load(url)
                    .placeholder(R.color.gray_dark)
                    .error(R.color.gray_dark)
                    .fitCenter()
                    .centerCrop()
                    .into(imageView);

        }
    }

}
