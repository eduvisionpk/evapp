package com.codegenetics.eduvision.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.codegenetics.eduvision.R;

import java.util.Objects;

public class Internet {

    private static AlertDialog alertDialog;

    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (manager != null) {
            info = manager.getActiveNetworkInfo();
        }
        return info != null && info.isConnectedOrConnecting();
    }


    @SuppressLint("InflateParams")
    public static void showOfflineDialog(Activity activity) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_no_internet, null);
        dialogBuilder.setView(dialogView);

        TextView tvRetry = dialogView.findViewById(R.id.tv_retry);

        tvRetry.setOnClickListener(v -> {
            if (alertDialog != null) alertDialog.dismiss();
            activity.startActivity(new Intent(activity, activity.getClass()));
            activity.finish();
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        Window window2 = alertDialog.getWindow();

        if (window2 != null) {
            window2.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

    }



}
