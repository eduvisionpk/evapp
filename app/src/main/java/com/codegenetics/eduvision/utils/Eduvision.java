package com.codegenetics.eduvision.utils;

import android.os.StrictMode;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import io.paperdb.Paper;

public class Eduvision extends MultiDexApplication implements LifecycleObserver {

    public static boolean isBackgrounded = false;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);

        Paper.init(getApplicationContext());

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        isBackgrounded = true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        isBackgrounded = false;
    }


}
