package com.codegenetics.eduvision.enums;

public enum IntentEnums {

    POSITION,
    IMAGES,
    DEADLINE,
    HIDE,
    EMAIL,
    PASSWORD,
    DISCIPLINE_TYPE,
    DISCIPLINE_NAME,
    LEVEL_CODE,
    LEVEL_NAME,
    CITY,
    URL,
    TITLE,
    TYPE,
    DATA,

    CATEGORY_ID,
    CATEGORY_NAME,
    CAREER_ID,
    COUNSELING_ID,
    INSTITUTE_ID,
    INSTITUTE,
    SCHOLARSHIP_DATA,
    NEWS_DATA,

    FSC_OM,
    FSC_TM,
    METRIC_OM,
    METRIC_TM,
    PERCENTAGE,

    GO_TO_HOME,
    USER_ID,
    ID,
    NAME,
    MESSAGE,
    DOB,
    DOMICILE,
    PASSING_YEAR,
    DEGREE,



}
