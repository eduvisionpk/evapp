package com.codegenetics.eduvision.enums;

public enum MoreMenu {
    CHANGE_PASSWORD,
    CONTACT_US,
    PRIVACY_POLICY,
    TERMS_CONDITION,
    ABOUT_US,
    RATE_US,
    SHARE,
    LOGOUT,
}
