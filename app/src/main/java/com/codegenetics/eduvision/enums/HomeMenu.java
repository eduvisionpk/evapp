package com.codegenetics.eduvision.enums;

public enum HomeMenu {
    NEWS,
    SCHOLARSHIPS,
    ADMISSION,
    ADMISSION_PLANNER,
    PROGRAM,
    CAREERS,
    CAREERS_TV,
    MERIT_CALCULATOR,
    ENTRY_TEST,
    PAST_PAPER,
    CAREER_COUNSELING,
    BOOK_A_COUNSELING_SESSION,
    RANKING,JOBS

}
