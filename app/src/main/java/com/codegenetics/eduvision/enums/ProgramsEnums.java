package com.codegenetics.eduvision.enums;

public enum ProgramsEnums {

    MEDICAL_SCIENCES,
    ENGINEERING,
    COMPUTER_SCIENCE_IT,
    ART_DESIGN,
    MANAGEMENT_SCIENCES,
    SOCIAL_SCIENCES,
    BIOLOGICAL_SCIENCES,
    CHEMICAL_SCIENCES,
    PHYSICAL_NUMERICAL_SCIENCES,
    AGRICULTURE,
    TECHNICAL,
    RELIGIOUS_STUDIES,
    MEDIA_STUDIES,
    COMMERCE_FINANCE_ACCOUNTING,

}
