package com.codegenetics.eduvision.enums;

public enum SearchFilter {
    FILTER, CITY, LEVEL, DISCIPLINE_TYPE, COURSES,
    OFFERING_AUTHORITY, FILED_OF_STUDY, CATEGORY, SCHOLARSHIP_TYPE, AREA, BOARD,
}
