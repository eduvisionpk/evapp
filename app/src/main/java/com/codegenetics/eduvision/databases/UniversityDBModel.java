package com.codegenetics.eduvision.databases;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.codegenetics.eduvision.models.Institution.InstituteDetailsModel;
import com.codegenetics.eduvision.models.Programs.ProgramsOfferedModel;

@Entity(tableName = "universities")
public class UniversityDBModel implements Parcelable {

    @PrimaryKey(autoGenerate = true) private int u_id;

    private String institute_id;
    private String ins_abb;
    private String name;
    private String city_name;
    private String degree_abb;
    private String discipline_name;
    private String duration;
    private String duration_mode;
    private String fee_local;
    private String deadline;

    public static UniversityDBModel getDBModel(ProgramsOfferedModel.ProgramsOfferedData data) {
        return new UniversityDBModel(
                data.getInstituteId() + "_" + data.getDiscipline_name(),
                data.getIns_abb(),
                data.getName(),
                data.getCityName(),
                data.getDegreeAbb(),
                data.getDiscipline_name(),
                data.getDuration(),
                data.getDurationMode(),
                data.getFeeLocal(),
                data.getDeadline()
        );
    }

    public static UniversityDBModel getDBModel(InstituteDetailsModel.InstituteDetailsPrograms data) {
        return new UniversityDBModel(
                data.getInstitute_id() + "_" + data.getDiscipline_name(),
                "",
                data.getDiscipline_name(),
                data.getCity_name(),
                data.getDegree_abb(),
                data.getDiscipline_name(),
                data.getDuration(),
                data.getDuration_mode(),
                data.getFee_local(),
                data.getDeadline()
        );
    }

    public static ProgramsOfferedModel.ProgramsOfferedData getModel(UniversityDBModel data) {
        return new ProgramsOfferedModel.ProgramsOfferedData(
                data.institute_id.split("_")[0],
                data.ins_abb,
                data.name,
                data.city_name,
                data.degree_abb,
                data.discipline_name,
                data.duration,
                data.duration_mode,
                data.fee_local,
                data.deadline
        );
    }

    public UniversityDBModel(String institute_id, String ins_abb, String name, String city_name, String degree_abb,
                             String discipline_name, String duration, String duration_mode, String fee_local,
                             String deadline) {
        this.institute_id = institute_id;
        this.ins_abb = ins_abb;
        this.name = name;
        this.city_name = city_name;
        this.degree_abb = degree_abb;
        this.discipline_name = discipline_name;
        this.duration = duration;
        this.duration_mode = duration_mode;
        this.fee_local = fee_local;
        this.deadline = deadline;
    }

    protected UniversityDBModel(Parcel in) {
        u_id = in.readInt();
        institute_id = in.readString();
        ins_abb = in.readString();
        name = in.readString();
        city_name = in.readString();
        degree_abb = in.readString();
        discipline_name = in.readString();
        duration = in.readString();
        duration_mode = in.readString();
        fee_local = in.readString();
        deadline = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(u_id);
        dest.writeString(institute_id);
        dest.writeString(ins_abb);
        dest.writeString(name);
        dest.writeString(city_name);
        dest.writeString(degree_abb);
        dest.writeString(discipline_name);
        dest.writeString(duration);
        dest.writeString(duration_mode);
        dest.writeString(fee_local);
        dest.writeString(deadline);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UniversityDBModel> CREATOR = new Creator<UniversityDBModel>() {
        @Override
        public UniversityDBModel createFromParcel(Parcel in) {
            return new UniversityDBModel(in);
        }

        @Override
        public UniversityDBModel[] newArray(int size) {
            return new UniversityDBModel[size];
        }
    };

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getInstitute_id() {
        return institute_id;
    }

    public void setInstitute_id(String institute_id) {
        this.institute_id = institute_id;
    }

    public String getIns_abb() {
        return ins_abb;
    }

    public void setIns_abb(String ins_abb) {
        this.ins_abb = ins_abb;
    }

    public String getCity_name() {
        return city_name;
    }

    public String getDegree_abb() {
        return degree_abb;
    }

    public String getDuration_mode() {
        return duration_mode;
    }

    public String getFee_local() {
        return fee_local;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getDiscipline_name() {
        return discipline_name;
    }

    public void setDiscipline_name(String discipline_name) {
        this.discipline_name = discipline_name;
    }

    public String getDuration() {
        return duration;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public void setDegree_abb(String degree_abb) {
        this.degree_abb = degree_abb;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setDuration_mode(String duration_mode) {
        this.duration_mode = duration_mode;
    }

    public void setFee_local(String fee_local) {
        this.fee_local = fee_local;
    }
}
