package com.codegenetics.eduvision.databases;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ScholarshipDAO {

    @Insert
    void addNewItem(ScholarshipDBModel model);

    @Query("Select * from scholarships order by s_id DESC")
    LiveData<List<ScholarshipDBModel>> getAll();

    @Query("Select count(*) from scholarships where scholarship_id =:id")
    int getItem(String id);

    @Query("delete from scholarships where scholarship_id =:id")
    void deleteItem(String id);

}
