package com.codegenetics.eduvision.databases;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AdmissionsDAO {

    @Insert
    void addNewItem(AdmissionsDBModel model);

    @Query("Select * from admissions order by n_id DESC")
    LiveData<List<AdmissionsDBModel>> getAll();

    @Query("Select * from admissions where id =:id")
    AdmissionsDBModel getItem(String id);

    @Query("update admissions set applyStatus =:status where id =:id")
    void updateApplyStatus(String id, int status);

    @Query("update admissions set appliedThroughEduvision =:status where id =:id")
    void updateEduvisionStatus(String id, int status);

    @Query("delete from admissions where id =:id")
    void deleteItem(String id);

    @Query("delete from admissions")
    void deleteAll();

}
