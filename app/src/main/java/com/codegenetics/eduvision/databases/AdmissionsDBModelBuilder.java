package com.codegenetics.eduvision.databases;

public class AdmissionsDBModelBuilder {
    private int n_id;
    private String id;
    private int applyStatus;
    private int appliedThroughEduvision;
    private String institute_id;
    private String ins_logo;
    private String ins_abb;
    private String city_name;
    private String name;
    private String deadline;
    private String level_code;
    private String level_name;
    private String discipline_type;
    private String discipline_name;
    private String institute_ad;
    private String apply_ev;

    public AdmissionsDBModelBuilder setN_id(int n_id) {
        this.n_id = n_id;
        return this;
    }

    public AdmissionsDBModelBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public AdmissionsDBModelBuilder setApplyStatus(int applyStatus) {
        this.applyStatus = applyStatus;
        return this;
    }

    public AdmissionsDBModelBuilder setAppliedThroughEduvision(int appliedThroughEduvision) {
        this.appliedThroughEduvision = appliedThroughEduvision;
        return this;
    }

    public AdmissionsDBModelBuilder setInstitute_id(String institute_id) {
        this.institute_id = institute_id;
        return this;
    }

    public AdmissionsDBModelBuilder setIns_logo(String ins_logo) {
        this.ins_logo = ins_logo;
        return this;
    }

    public AdmissionsDBModelBuilder setIns_abb(String ins_abb) {
        this.ins_abb = ins_abb;
        return this;
    }

    public AdmissionsDBModelBuilder setCity_name(String city_name) {
        this.city_name = city_name;
        return this;
    }

    public AdmissionsDBModelBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AdmissionsDBModelBuilder setDeadline(String deadline) {
        this.deadline = deadline;
        return this;
    }

    public AdmissionsDBModelBuilder setLevel_code(String level_code) {
        this.level_code = level_code;
        return this;
    }

    public AdmissionsDBModelBuilder setLevel_name(String level_name) {
        this.level_name = level_name;
        return this;
    }

    public AdmissionsDBModelBuilder setDiscipline_type(String discipline_type) {
        this.discipline_type = discipline_type;
        return this;
    }

    public AdmissionsDBModelBuilder setDiscipline_name(String discipline_name) {
        this.discipline_name = discipline_name;
        return this;
    }

    public AdmissionsDBModelBuilder setInstitute_ad(String institute_ad) {
        this.institute_ad = institute_ad;
        return this;
    }

    public AdmissionsDBModelBuilder setApply_ev(String apply_ev) {
        this.apply_ev = apply_ev;
        return this;
    }

    public AdmissionsDBModel createAdmissionsDBModel() {
        return new AdmissionsDBModel(n_id, id, applyStatus, appliedThroughEduvision, institute_id,
                ins_logo, ins_abb, city_name, name, deadline, level_code, level_name,
                discipline_type, discipline_name, institute_ad, apply_ev);
    }
}