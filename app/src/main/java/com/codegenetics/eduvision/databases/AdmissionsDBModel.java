package com.codegenetics.eduvision.databases;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "admissions")
public class AdmissionsDBModel {

    @PrimaryKey(autoGenerate = true) private int n_id;
    private String id;
    private int applyStatus; /* 0=not_applied ; 1=applied ; 2=admission_confirmed  */
    private int appliedThroughEduvision; /* 0=not_applied ; 1=applied */
    private String institute_id;
    private String ins_logo;
    private String ins_abb;
    private String city_name;
    private String name;
    private String deadline;
    private String level_code;
    private String level_name;
    private String discipline_type;
    private String discipline_name;
    private String institute_ad;
    private String apply_ev;

    public AdmissionsDBModel(int n_id, String id, int applyStatus, int appliedThroughEduvision,
                             String institute_id, String ins_logo, String ins_abb, String city_name,
                             String name, String deadline, String level_code, String level_name,
                             String discipline_type, String discipline_name, String institute_ad,
                             String apply_ev) {
        this.n_id = n_id;
        this.id = id;
        this.applyStatus = applyStatus;
        this.appliedThroughEduvision = appliedThroughEduvision;
        this.institute_id = institute_id;
        this.ins_logo = ins_logo;
        this.ins_abb = ins_abb;
        this.city_name = city_name;
        this.name = name;
        this.deadline = deadline;
        this.level_code = level_code;
        this.level_name = level_name;
        this.discipline_type = discipline_type;
        this.discipline_name = discipline_name;
        this.institute_ad = institute_ad;
        this.apply_ev = apply_ev;
    }

    public int getN_id() {
        return n_id;
    }

    public void setN_id(int n_id) {
        this.n_id = n_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(int applyStatus) {
        this.applyStatus = applyStatus;
    }

    public int getAppliedThroughEduvision() {
        return appliedThroughEduvision;
    }

    public void setAppliedThroughEduvision(int appliedThroughEduvision) {
        this.appliedThroughEduvision = appliedThroughEduvision;
    }

    public String getInstitute_id() {
        return institute_id;
    }

    public void setInstitute_id(String institute_id) {
        this.institute_id = institute_id;
    }

    public String getIns_logo() {
        return ins_logo;
    }

    public void setIns_logo(String ins_logo) {
        this.ins_logo = ins_logo;
    }

    public String getIns_abb() {
        return ins_abb;
    }

    public void setIns_abb(String ins_abb) {
        this.ins_abb = ins_abb;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getLevel_code() {
        return level_code;
    }

    public void setLevel_code(String level_code) {
        this.level_code = level_code;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public String getDiscipline_type() {
        return discipline_type;
    }

    public void setDiscipline_type(String discipline_type) {
        this.discipline_type = discipline_type;
    }

    public String getDiscipline_name() {
        return discipline_name;
    }

    public void setDiscipline_name(String discipline_name) {
        this.discipline_name = discipline_name;
    }

    public String getInstitute_ad() {
        return institute_ad;
    }

    public void setInstitute_ad(String institute_ad) {
        this.institute_ad = institute_ad;
    }

    public String getApply_ev() {
        return apply_ev;
    }

    public void setApply_ev(String apply_ev) {
        this.apply_ev = apply_ev;
    }
}
