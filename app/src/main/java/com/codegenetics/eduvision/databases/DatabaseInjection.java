package com.codegenetics.eduvision.databases;

import android.content.Context;

public class DatabaseInjection {

    public static UniversitiesDAO getUniversitiesDAO(Context context) {
        return AppDatabase.getInstance(context).getUniversitiesDAO();
    }

    public static NewsDAO getNewsDAO(Context context) {
        return AppDatabase.getInstance(context).getNewsDAO();
    }

    public static ScholarshipDAO getScholarshipsDAO(Context context) {
        return AppDatabase.getInstance(context).getScholarshipDAO();
    }

    public static AdmissionsDAO getAdmissionsDAO(Context context) {
        return AppDatabase.getInstance(context).getAdmissionsDAO();
    }

}
