package com.codegenetics.eduvision.databases;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UniversitiesDAO {

    @Insert
    void addNewItem(UniversityDBModel model);

    @Query("Select * from universities order by u_id DESC")
    LiveData<List<UniversityDBModel>> getAll();

    @Query("Select count(*) from universities where institute_id =:id")
    int getItem(String id);

    @Query("delete from universities where institute_id =:id")
    void deleteItem(String id);

}
