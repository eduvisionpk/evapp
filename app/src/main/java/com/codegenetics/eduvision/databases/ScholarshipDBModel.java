package com.codegenetics.eduvision.databases;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.codegenetics.eduvision.models.Scholarships.ScholarshipsModel;

@Entity(tableName = "scholarships")
public class ScholarshipDBModel implements Parcelable {

    @PrimaryKey(autoGenerate = true) private int s_id;

    private String scholarship_id;
    private String scholarship_name;
    private String logo;
    private String date;

    public static ScholarshipDBModel getDBModel(ScholarshipsModel.ScholarshipsData data) {
        return new ScholarshipDBModel(
                data.getScholarshipId(),
                data.getScholarshipName(),
                data.getLogo(),
                data.getUpdated()
        );
    }

    public static ScholarshipsModel.ScholarshipsData getModel(ScholarshipDBModel data) {
        return new ScholarshipsModel.ScholarshipsData(
                data.scholarship_id,
                data.scholarship_name,
                data.logo,
                data.date
        );
    }

    public ScholarshipDBModel(String scholarship_id, String scholarship_name, String logo, String date) {
        this.scholarship_id = scholarship_id;
        this.scholarship_name = scholarship_name;
        this.logo = logo;
        this.date = date;
    }

    protected ScholarshipDBModel(Parcel in) {
        s_id = in.readInt();
        scholarship_id = in.readString();
        scholarship_name = in.readString();
        logo = in.readString();
        date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(s_id);
        dest.writeString(scholarship_id);
        dest.writeString(scholarship_name);
        dest.writeString(logo);
        dest.writeString(date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScholarshipDBModel> CREATOR = new Creator<ScholarshipDBModel>() {
        @Override
        public ScholarshipDBModel createFromParcel(Parcel in) {
            return new ScholarshipDBModel(in);
        }

        @Override
        public ScholarshipDBModel[] newArray(int size) {
            return new ScholarshipDBModel[size];
        }
    };

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getScholarship_id() {
        return scholarship_id;
    }

    public void setScholarship_id(String scholarship_id) {
        this.scholarship_id = scholarship_id;
    }

    public String getScholarship_name() {
        return scholarship_name;
    }

    public void setScholarship_name(String scholarship_name) {
        this.scholarship_name = scholarship_name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
