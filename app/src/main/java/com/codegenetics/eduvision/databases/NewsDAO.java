package com.codegenetics.eduvision.databases;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NewsDAO {

    @Insert
    void addNewItem(NewsDBModel model);

    @Query("Select * from news order by n_id DESC")
    LiveData<List<NewsDBModel>> getAll();

    @Query("Select count(*) from news where id =:id")
    int getItem(String id);

    @Query("delete from news where id =:id")
    void deleteItem(String id);

}
