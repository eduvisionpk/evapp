package com.codegenetics.eduvision.databases;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.codegenetics.eduvision.models.News.NewsModel;

@Entity(tableName = "news")
public class NewsDBModel implements Parcelable {

    @PrimaryKey(autoGenerate = true) private int n_id;
    private String id;
    private String titletext;
    private String image;
    private String time;

    public static NewsDBModel getDBModel(NewsModel.NewsData data) {
        return new NewsDBModel(
                data.getId(),
                data.getTitleText(),
                data.getImage(),
                data.getTime()
        );
    }

    public static NewsModel.NewsData getModel(NewsDBModel data) {
        return new NewsModel.NewsData(
                data.id,
                data.titletext,
                data.image,
                data.time
        );
    }

    public NewsDBModel(String id, String titletext, String image, String time) {
        this.id = id;
        this.titletext = titletext;
        this.image = image;
        this.time = time;
    }

    protected NewsDBModel(Parcel in) {
        n_id = in.readInt();
        id = in.readString();
        titletext = in.readString();
        image = in.readString();
        time = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(n_id);
        dest.writeString(id);
        dest.writeString(titletext);
        dest.writeString(image);
        dest.writeString(time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewsDBModel> CREATOR = new Creator<NewsDBModel>() {
        @Override
        public NewsDBModel createFromParcel(Parcel in) {
            return new NewsDBModel(in);
        }

        @Override
        public NewsDBModel[] newArray(int size) {
            return new NewsDBModel[size];
        }
    };

    public int getN_id() {
        return n_id;
    }

    public void setN_id(int n_id) {
        this.n_id = n_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitletext() {
        return titletext;
    }

    public void setTitletext(String titletext) {
        this.titletext = titletext;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
