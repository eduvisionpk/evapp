package com.codegenetics.eduvision.databases;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.codegenetics.eduvision.BuildConfig;

@Database(entities = {UniversityDBModel.class, NewsDBModel.class, ScholarshipDBModel.class, AdmissionsDBModel.class},
        version = BuildConfig.DATABASE_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract UniversitiesDAO getUniversitiesDAO();
    public abstract NewsDAO getNewsDAO();
    public abstract ScholarshipDAO getScholarshipDAO();
    public abstract AdmissionsDAO getAdmissionsDAO();

    static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,
                    "eduvision-database")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }


}
