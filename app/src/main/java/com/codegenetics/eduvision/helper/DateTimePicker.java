package com.codegenetics.eduvision.helper;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.TextView;

import com.codegenetics.eduvision.R;

import java.util.Calendar;

public class DateTimePicker {

    public static void pickProfileDate(Context context, final TextView textView) {
        final String[] months = context.getResources().getStringArray(R.array.months);
        int mYear, mMonth, mDay;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                (view, year, month, dayOfMonth) ->
                        textView.setText(new StringBuilder()
                                .append(months[month]).append(" ")
                                .append(dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth).append(", ")
                                .append(year)), mYear, mMonth, mDay
        );

        c.set(mYear - 11, mMonth, mDay - 1);
        long value = c.getTimeInMillis();

        datePickerDialog.getDatePicker().setMaxDate(value);

        datePickerDialog.show();
    }

}
