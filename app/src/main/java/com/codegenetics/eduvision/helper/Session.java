package com.codegenetics.eduvision.helper;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.codegenetics.eduvision.views.activities.HomeActivity;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;

public class Session {

    private static Session INSTANCE;

    private final String LOGIN_DETAILS = "login_details";
    private final String REMEMBER_DETAILS = "remember_details";
    private final String USER_ID = "user_id";
    private final String NAME = "name";
    private final String CITY = "city";
    private final String EMAIL = "email";
    private final String PHONE = "phone";
    private final String IMAGE = "image";
    private final String LOGIN = "login";
    private final String REMEMBER_ME = "remember_me";
    private final String DEVICE_ID = "device_id";

    private Session() {

    }

    public static Session getInstance() {
        if (INSTANCE == null) INSTANCE = new Session();
        return INSTANCE;
    }

    public void setLogin(FirebaseUser user, boolean rememberMe, AppCompatActivity activity) {

        try {

            setUserLoginState(true);
            setRememberMe(rememberMe);
            setEmail(user.getEmail());

            FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.USERS)
                    .child(user.getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                            if (snapshot.getValue() != null) {

                                setUserId(user.getUid());
                                setName(snapshot.child(FirebaseConstants.ChildNodes.FULL_NAME).getValue(String.class));
                                setPhone(snapshot.child(FirebaseConstants.ChildNodes.MOBILE_NUMBER).getValue(String.class));
                                setCity(snapshot.child(FirebaseConstants.ChildNodes.CITY_NAME).getValue(String.class));

                                if (snapshot.hasChild(FirebaseConstants.ChildNodes.IMAGE))
                                    setImage(snapshot.child(FirebaseConstants.ChildNodes.IMAGE).getValue(String.class));

                            }

                            activity.startActivity(new Intent(activity, HomeActivity.class));
                            activity.finish();

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            activity.startActivity(new Intent(activity, HomeActivity.class));
                            activity.finish();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setLogout() {
        setUserLoginState(false);
        Paper.book(LOGIN_DETAILS).destroy();
    }

    public void setRememberMe(boolean rememberMe){ Paper.book(REMEMBER_DETAILS).write(REMEMBER_ME, rememberMe); }

    public boolean isRememberMe(){ return Paper.book(REMEMBER_DETAILS).read(REMEMBER_ME,false); }

    public void setEmail(String email) {
        Paper.book(REMEMBER_DETAILS).write(EMAIL, email);
    }

    public String getEmail() {
        return Paper.book(REMEMBER_DETAILS).read(EMAIL, "");
    }

    public String getPhone() { return Paper.book(LOGIN_DETAILS).read(PHONE, ""); }

    public void setPhone(String phone) {
        Paper.book(LOGIN_DETAILS).write(PHONE, phone);
    }

    public void setUserId(String userId) { Paper.book(LOGIN_DETAILS).write(USER_ID, userId); }

    public String getCurrentUserId() { return Paper.book(LOGIN_DETAILS).read(USER_ID, ""); }

    public void setName(String name) { Paper.book(LOGIN_DETAILS).write(NAME, name); }

    public String getName() { return Paper.book(LOGIN_DETAILS).read(NAME, ""); }

    public void setDeviceId(String deviceId) { Paper.book(LOGIN_DETAILS).write(DEVICE_ID, deviceId); }

    public String getDeviceId() { return Paper.book(LOGIN_DETAILS).read(DEVICE_ID, ""); }

    public void setCity(String city) { Paper.book(LOGIN_DETAILS).write(CITY, city); }

    public String getCity() { return Paper.book(LOGIN_DETAILS).read(CITY, ""); }

    public void setImage(String image) { Paper.book(LOGIN_DETAILS).write(IMAGE, image); }

    public String getImage() { return Paper.book(LOGIN_DETAILS).read(IMAGE, ""); }

    public void setUserLoginState(boolean isLogin){ Paper.book(LOGIN_DETAILS).write(LOGIN, isLogin); }

    public boolean isUserLogin(){ return Paper.book(LOGIN_DETAILS).read(LOGIN,false); }


}
