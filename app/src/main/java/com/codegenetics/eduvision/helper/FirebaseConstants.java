package com.codegenetics.eduvision.helper;

public class FirebaseConstants {

    public static final String RECEIVED = "received";
    public static final String SENT = "sent";
    public static final String CONSULTANT = "consultant";

    public static class ParentNodes {
        public static final String USER_FORMS = "user_forms";
        public static final String USERS = "users";
        public static final String CHATS = "chats";
        public static final String MESSAGES = "messages";
    }

    public static class ChildNodes {
        public static final String FULL_NAME = "full_name";
        public static final String MOBILE_NUMBER = "mobile_number";
        public static final String CITY_CODE = "city_code";
        public static final String CITY_NAME = "city_name";
        public static final String IMAGE = "image";
        public static final String UNREAD = "unread";
        public static final String DATE = "date";
        public static final String STATUS = "status";
        public static final String USER_PHOTO = "user_photo";
        public static final String MESSAGE = "message";
        public static final String LAST_MESSAGE = "last_message";
        public static final String USERNAME = "username";
        public static final String DEVICE_ID = "device_id";
        public static final String EMAIL = "email";
        public static final String INSTITUTE = "institute";
        public static final String PROGRAM = "program";
        public static final String LEVEL = "level";
    }


}
