package com.codegenetics.eduvision.helper;

import androidx.annotation.ColorRes;

import com.codegenetics.eduvision.R;
import com.codegenetics.eduvision.enums.HomeMenu;
import com.codegenetics.eduvision.enums.MoreMenu;
import com.codegenetics.eduvision.enums.ProgramsEnums;
import com.codegenetics.eduvision.enums.SearchFilter;
import com.codegenetics.eduvision.models.FilterModel;
import com.codegenetics.eduvision.models.HomeModel;
import com.codegenetics.eduvision.models.KeyValue;
import com.codegenetics.eduvision.models.MoreModel;
import com.codegenetics.eduvision.models.Programs.ProgramsModel;
import com.codegenetics.eduvision.models.ProgramsInnerModel;
import com.codegenetics.eduvision.models.ProgramsMainModel;

import java.util.ArrayList;
import java.util.List;

public class ConstantData {

    public static ConstantData instance;
    public final static String SELECT = "Select";

    public static ConstantData getInstance() {
        if (instance == null) {
            instance = new ConstantData();
        }
        return instance;
    }

    private ConstantData() {
    }

    public ArrayList<ProgramsModel> getProgramsData() {
        ArrayList<ProgramsModel> mData = new ArrayList<>();
        mData.add(new ProgramsModel(R.drawable.medical_science, "Medical Sciences"));
        mData.add(new ProgramsModel(R.drawable.engineering, "Engineering"));
        mData.add(new ProgramsModel(R.drawable.tehnical, "Technical"));
        mData.add(new ProgramsModel(R.drawable.computer_science, "Computer Sciences & Information Technology"));
        mData.add(new ProgramsModel(R.drawable.arts, "Art & design"));
        mData.add(new ProgramsModel(R.drawable.management_science, "Management Sciences"));
        mData.add(new ProgramsModel(R.drawable.social_science, "Social Sciences"));
        mData.add(new ProgramsModel(R.drawable.biological_science, "Biological & Life Sciences"));
        mData.add(new ProgramsModel(R.drawable.chemical_science, "Chemical & Material Sciences"));
        mData.add(new ProgramsModel(R.drawable.physics_science, "Physics & Numerical Sciences"));
        mData.add(new ProgramsModel(R.drawable.earth_science, "Earth & Environmental Sciences"));
        mData.add(new ProgramsModel(R.drawable.agriculture_science, "Agricultural Sciences"));
        mData.add(new ProgramsModel(R.drawable.religion_study, "Religious Studies"));
        mData.add(new ProgramsModel(R.drawable.media_science, "Media Studies"));
        mData.add(new ProgramsModel(R.drawable.accounts, "Commerce / Finance & Accounting"));
        return mData;
    }

    public ArrayList<KeyValue> getAuthoritiesData() {
        ArrayList<KeyValue> mData = new ArrayList<>();
        mData.add(new KeyValue("1", "Higher Education Commission "));
        mData.add(new KeyValue("2", "Punjab Education Endowment Fund "));
        mData.add(new KeyValue("3", "USAID Funded Scholarships "));
        mData.add(new KeyValue("4", "University Scholarships"));
        mData.add(new KeyValue("5", "International Bodies "));
        mData.add(new KeyValue("6", "Others"));
        return mData;
    }

    public List<KeyValue> getLevelData() {
        List<KeyValue> mData = new ArrayList<>();
        mData.add(new KeyValue("1", "All"));
        mData.add(new KeyValue("2", "Matric"));
        mData.add(new KeyValue("3", "Inter"));
        mData.add(new KeyValue("4", "Bachelor"));
        mData.add(new KeyValue("5", "Master"));
        mData.add(new KeyValue("6", "MS / MPhil"));
        mData.add(new KeyValue("7", "PhD"));
        mData.add(new KeyValue("8", "Diploma / Certificate"));
        return mData;
    }

    public List<KeyValue> getFieldData() {
        List<KeyValue> mData = new ArrayList<>();
        mData.add(new KeyValue("1", "All"));
        mData.add(new KeyValue("2", "Medical"));
        mData.add(new KeyValue("3", "Engineering & IT"));
        mData.add(new KeyValue("4", "Natural Science"));
        mData.add(new KeyValue("5", "Social Science"));
        mData.add(new KeyValue("6", "Agriculture"));
        mData.add(new KeyValue("7", "Management and Business"));
        return mData;
    }

    public List<KeyValue> getCatData() {
        List<KeyValue> mData = new ArrayList<>();
        mData.add(new KeyValue("1", "Both"));
        mData.add(new KeyValue("2", "Merit Basis"));
        mData.add(new KeyValue("3", "Need Basis"));
        return mData;
    }

    public List<KeyValue> getTypeData() {
        List<KeyValue> mData = new ArrayList<>();
        mData.add(new KeyValue("", "Both"));
        mData.add(new KeyValue("1", "Local"));
        mData.add(new KeyValue("2", "International"));
        return mData;
    }

    public List<MoreModel> getMoreData() {
        List<MoreModel> mData = new ArrayList<>();
        mData.add(new MoreModel(R.drawable.ic_change_password, "Change Password", MoreMenu.CHANGE_PASSWORD));
        mData.add(new MoreModel(R.drawable.ic_contact_us, "Contact Us", MoreMenu.CONTACT_US));
        mData.add(new MoreModel(R.drawable.ic_privacy_policy, "Privacy policy", MoreMenu.PRIVACY_POLICY));
        mData.add(new MoreModel(R.drawable.ic_terms_conditions, "Terms & Condition", MoreMenu.TERMS_CONDITION));
        mData.add(new MoreModel(R.drawable.ic_about_us, "About Us", MoreMenu.ABOUT_US));
        mData.add(new MoreModel(R.drawable.ic_rate_us, "Rate Us", MoreMenu.RATE_US));
        mData.add(new MoreModel(R.drawable.ic_share, "Share", MoreMenu.SHARE));
        mData.add(new MoreModel(R.drawable.ic_logout, "Logout", MoreMenu.LOGOUT));
        return mData;
    }

    public List<HomeModel> getHomeData() {
        List<HomeModel> mData = new ArrayList<>();
        mData.add(new HomeModel(R.drawable.ic_admission_home, "Admission", HomeMenu.ADMISSION));
        mData.add(new HomeModel(R.drawable.ic_admission_planner, "Admission Planner", HomeMenu.ADMISSION_PLANNER));
        mData.add(new HomeModel(R.drawable.ic_career_counseling, "Career Counseling", HomeMenu.CAREER_COUNSELING));
        mData.add(new HomeModel(R.drawable.ic_news_home, "News/Blog", HomeMenu.NEWS));
        mData.add(new HomeModel(R.drawable.ic_scholarship_home, "Scholarships", HomeMenu.SCHOLARSHIPS));
        mData.add(new HomeModel(R.drawable.ic_chat_home, "Chat with Counselor", HomeMenu.BOOK_A_COUNSELING_SESSION));
        mData.add(new HomeModel(R.drawable.ic_job, "Jobs", HomeMenu.JOBS));
        mData.add(new HomeModel(R.drawable.ic_programs_home, "Programs", HomeMenu.PROGRAM));
        mData.add(new HomeModel(R.drawable.ic_ranking_home, "Ranking", HomeMenu.RANKING));
        mData.add(new HomeModel(R.drawable.ic_merit_calculator, "Merit Calculator", HomeMenu.MERIT_CALCULATOR));
        mData.add(new HomeModel(R.drawable.ic_past_paper, "Past Papers", HomeMenu.PAST_PAPER));
        mData.add(new HomeModel(R.drawable.ic_entry_test, "Entry Test", HomeMenu.ENTRY_TEST));
        mData.add(new HomeModel(R.drawable.ic_career_home, "Careers", HomeMenu.CAREERS));
        mData.add(new HomeModel(R.drawable.ic_career_tv, "Careers TV", HomeMenu.CAREERS_TV));

        return mData;
    }

    public List<FilterModel> getAdmissionFilter() {
        List<FilterModel> mData = new ArrayList<>();
        mData.add(new FilterModel(R.drawable.ic_filter_primary, "Filter", SearchFilter.FILTER));
        mData.add(new FilterModel(0, "Discipline Type", SearchFilter.DISCIPLINE_TYPE));
        mData.add(new FilterModel(0, "Level", SearchFilter.LEVEL));
        mData.add(new FilterModel(0, "City", SearchFilter.CITY));
        mData.add(new FilterModel(0, "Courses", SearchFilter.COURSES));
        return mData;
    }

    public List<FilterModel> getScholarshipFilters() {
        List<FilterModel> mData = new ArrayList<>();
        mData.add(new FilterModel(R.drawable.ic_filter_primary, "Filter", SearchFilter.FILTER));
        mData.add(new FilterModel(0, "Offering authority", SearchFilter.OFFERING_AUTHORITY));
        mData.add(new FilterModel(0, "Level", SearchFilter.LEVEL));
        mData.add(new FilterModel(0, "Field of study", SearchFilter.FILED_OF_STUDY));
        mData.add(new FilterModel(0, "Category", SearchFilter.CATEGORY));
        mData.add(new FilterModel(0, "Type", SearchFilter.SCHOLARSHIP_TYPE));
        return mData;
    }

    public List<FilterModel> getProgramFilters() {
        List<FilterModel> mData = new ArrayList<>();
        mData.add(new FilterModel(R.drawable.ic_filter_primary, "Filter", SearchFilter.FILTER));
        mData.add(new FilterModel(0, "Field of study", SearchFilter.FILED_OF_STUDY));
        mData.add(new FilterModel(0, "Level", SearchFilter.LEVEL));
        return mData;
    }

    public ArrayList<ProgramsMainModel> getProgramTypes() {
        ArrayList<ProgramsMainModel> arrayList = new ArrayList<>();

        arrayList.add(new ProgramsMainModel("Medical Sciences", ProgramsEnums.MEDICAL_SCIENCES, getProgramsSubTypes(ProgramsEnums.MEDICAL_SCIENCES)));
        arrayList.add(new ProgramsMainModel("Engineering", ProgramsEnums.ENGINEERING, getProgramsSubTypes(ProgramsEnums.ENGINEERING)));
        arrayList.add(new ProgramsMainModel("Computer Science & IT", ProgramsEnums.COMPUTER_SCIENCE_IT, getProgramsSubTypes(ProgramsEnums.COMPUTER_SCIENCE_IT)));
        arrayList.add(new ProgramsMainModel("Art & Design", ProgramsEnums.ART_DESIGN, getProgramsSubTypes(ProgramsEnums.ART_DESIGN)));
        arrayList.add(new ProgramsMainModel("Management Sciences", ProgramsEnums.MANAGEMENT_SCIENCES, getProgramsSubTypes(ProgramsEnums.MANAGEMENT_SCIENCES)));
        arrayList.add(new ProgramsMainModel("Social Sciences", ProgramsEnums.SOCIAL_SCIENCES, getProgramsSubTypes(ProgramsEnums.SOCIAL_SCIENCES)));
        arrayList.add(new ProgramsMainModel("Biological Sciences", ProgramsEnums.BIOLOGICAL_SCIENCES, getProgramsSubTypes(ProgramsEnums.BIOLOGICAL_SCIENCES)));
        arrayList.add(new ProgramsMainModel("Chemical Sciences", ProgramsEnums.CHEMICAL_SCIENCES, getProgramsSubTypes(ProgramsEnums.CHEMICAL_SCIENCES)));
        arrayList.add(new ProgramsMainModel("Physical & Numerical Sciences", ProgramsEnums.PHYSICAL_NUMERICAL_SCIENCES, getProgramsSubTypes(ProgramsEnums.PHYSICAL_NUMERICAL_SCIENCES)));
        arrayList.add(new ProgramsMainModel("Agriculture", ProgramsEnums.AGRICULTURE, getProgramsSubTypes(ProgramsEnums.AGRICULTURE)));
        arrayList.add(new ProgramsMainModel("Technical", ProgramsEnums.TECHNICAL, getProgramsSubTypes(ProgramsEnums.TECHNICAL)));
        arrayList.add(new ProgramsMainModel("Religious Studies", ProgramsEnums.RELIGIOUS_STUDIES, getProgramsSubTypes(ProgramsEnums.RELIGIOUS_STUDIES)));
        arrayList.add(new ProgramsMainModel("Media Studies", ProgramsEnums.MEDIA_STUDIES, getProgramsSubTypes(ProgramsEnums.MEDIA_STUDIES)));
        arrayList.add(new ProgramsMainModel("Commerce/ Finance/ Accounting", ProgramsEnums.COMMERCE_FINANCE_ACCOUNTING, getProgramsSubTypes(ProgramsEnums.COMMERCE_FINANCE_ACCOUNTING)));

        return arrayList;
    }

    public ArrayList<ProgramsInnerModel> getProgramsSubTypes(ProgramsEnums programsEnums) {
        ArrayList<ProgramsInnerModel> arrayList = new ArrayList<>();

        switch (programsEnums) {
            default:
            case MEDICAL_SCIENCES:
                arrayList.add(new ProgramsInnerModel("Medicine - Mbbs", R.drawable.medicine, programsEnums));
                arrayList.add(new ProgramsInnerModel("Dentistry", R.drawable.dentistry, programsEnums));
                arrayList.add(new ProgramsInnerModel("Nursing", R.drawable.nursing, programsEnums));
                arrayList.add(new ProgramsInnerModel("Pharmacy", R.drawable.pharmacy, programsEnums));
                arrayList.add(new ProgramsInnerModel("Physiotherapy", R.drawable.physiotherapy, programsEnums));
                arrayList.add(new ProgramsInnerModel("Veterinary Medicine", R.drawable.veterinary_medicine, programsEnums));
                break;
            case ENGINEERING:
                arrayList.add(new ProgramsInnerModel("Electrical", R.drawable.electrical_engineering, programsEnums));
                arrayList.add(new ProgramsInnerModel("Mechanical", R.drawable.mechanical_engineering, programsEnums));
                arrayList.add(new ProgramsInnerModel("Petroleum", R.drawable.petroleum_engineering, programsEnums));
                arrayList.add(new ProgramsInnerModel("Civil", R.drawable.civil_engineering, programsEnums));
                arrayList.add(new ProgramsInnerModel("Chemical", R.drawable.chemical_engineering, programsEnums));
                arrayList.add(new ProgramsInnerModel("Computer", R.drawable.computer_hardware_engineering, programsEnums));
                break;
            case COMPUTER_SCIENCE_IT:
                arrayList.add(new ProgramsInnerModel("Computer Science", R.drawable.computer_sciences, programsEnums));
                arrayList.add(new ProgramsInnerModel("Information Technology", R.drawable.information_technology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Software Engineering", R.drawable.sofware_engineering, programsEnums));
                arrayList.add(new ProgramsInnerModel("Cyber Security", R.drawable.cyber_security, programsEnums));
                arrayList.add(new ProgramsInnerModel("Information Security", R.drawable.information_security, programsEnums));
                arrayList.add(new ProgramsInnerModel("Artificial Intelligence", R.drawable.artificial_intelligence, programsEnums));
                break;
            case ART_DESIGN:
                arrayList.add(new ProgramsInnerModel("Architecture", R.drawable.architecture, programsEnums));
                arrayList.add(new ProgramsInnerModel("Fine Arts", R.drawable.fine_art, programsEnums));
                arrayList.add(new ProgramsInnerModel("Architecture & Planning", R.drawable.architecture_planning, programsEnums));
                arrayList.add(new ProgramsInnerModel("Interior Design", R.drawable.interior_designing, programsEnums));
                arrayList.add(new ProgramsInnerModel("Calligraphy", R.drawable.calligraphy, programsEnums));
                arrayList.add(new ProgramsInnerModel("Ceramic Design", R.drawable.ceramic_design, programsEnums));
                break;
            case MANAGEMENT_SCIENCES:
                arrayList.add(new ProgramsInnerModel("Business Administration", R.drawable.business_administration, programsEnums));
                arrayList.add(new ProgramsInnerModel("Human Resources Management", R.drawable.human_resource_management, programsEnums));
                arrayList.add(new ProgramsInnerModel("Marketing", R.drawable.marketing, programsEnums));
                arrayList.add(new ProgramsInnerModel("Public Administration", R.drawable.public_administration, programsEnums));
                arrayList.add(new ProgramsInnerModel("Disaster Management", R.drawable.disaster_management, programsEnums));
                arrayList.add(new ProgramsInnerModel("Hotel Management", R.drawable.hotel_management, programsEnums));
                break;
            case SOCIAL_SCIENCES:
                arrayList.add(new ProgramsInnerModel("Economics", R.drawable.economics, programsEnums));
                arrayList.add(new ProgramsInnerModel("Political Science", R.drawable.political_science, programsEnums));
                arrayList.add(new ProgramsInnerModel("History", R.drawable.history, programsEnums));
                arrayList.add(new ProgramsInnerModel("International Relations", R.drawable.international_relations, programsEnums));
                arrayList.add(new ProgramsInnerModel("Law", R.drawable.law, programsEnums));
                arrayList.add(new ProgramsInnerModel("Pakistan Studies", R.drawable.pakistan_studies, programsEnums));
                break;
            case BIOLOGICAL_SCIENCES:
                arrayList.add(new ProgramsInnerModel("Biochemistry", R.drawable.biochemistry, programsEnums));
                arrayList.add(new ProgramsInnerModel("Biology", R.drawable.biology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Biotechnology", R.drawable.biotechnology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Biotechnology & Genetic", R.drawable.biotechnology_and_genetic_engineering, programsEnums));
                arrayList.add(new ProgramsInnerModel("Botany", R.drawable.botany, programsEnums));
                arrayList.add(new ProgramsInnerModel("Diet & Nutritional Sciences", R.drawable.diet_and_nutrition, programsEnums));
                break;
            case CHEMICAL_SCIENCES:
                arrayList.add(new ProgramsInnerModel("Chemistry", R.drawable.chemistry, programsEnums));
                arrayList.add(new ProgramsInnerModel("Analytical Chemistry", R.drawable.analytical_chemistry, programsEnums));
                arrayList.add(new ProgramsInnerModel("Industrial Chemistry", R.drawable.industrial_chemistry, programsEnums));
                arrayList.add(new ProgramsInnerModel("Textile Science", R.drawable.textile_science, programsEnums));
                arrayList.add(new ProgramsInnerModel("Apparel Manufacturing & Merchandizing", R.drawable.apparel_manufacturing, programsEnums));
//                arrayList.add(new ProgramsInnerModel("", "", programsEnums));
                break;
            case PHYSICAL_NUMERICAL_SCIENCES:
                arrayList.add(new ProgramsInnerModel("High Energy Physics", R.drawable.high_energy_physics, programsEnums));
                arrayList.add(new ProgramsInnerModel("Mathematics", R.drawable.mathematics, programsEnums));
                arrayList.add(new ProgramsInnerModel("Nanotechnology", R.drawable.nanotechnology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Physics", R.drawable.physics_science, programsEnums));
                arrayList.add(new ProgramsInnerModel("Space Sciences", R.drawable.space_science, programsEnums));
                arrayList.add(new ProgramsInnerModel("Statistics", R.drawable.statistics, programsEnums));
                break;
            case AGRICULTURE:
                arrayList.add(new ProgramsInnerModel("Agriculture", R.drawable.agriculture, programsEnums));
                arrayList.add(new ProgramsInnerModel("Agronomy", R.drawable.agronomy, programsEnums));
                arrayList.add(new ProgramsInnerModel("Food Science & Technology", R.drawable.food_science_and_technology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Food Engineering", R.drawable.hotel_management, programsEnums));
                arrayList.add(new ProgramsInnerModel("Plant Breeding & Genetics", R.drawable.plant_breeding_and_genetics, programsEnums));
                arrayList.add(new ProgramsInnerModel("Soil Science", R.drawable.soil_sciences, programsEnums));
                break;
            case TECHNICAL:
                arrayList.add(new ProgramsInnerModel("Auto & Diesel", R.drawable.auto_diesel_technology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Aviation Technology", R.drawable.aviation_technology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Chemical Technology", R.drawable.chemical_technology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Civil Technology", R.drawable.civil_technology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Mechanical Technology", R.drawable.mechanical_technology, programsEnums));
                arrayList.add(new ProgramsInnerModel("Automobile", R.drawable.automobile_technology, programsEnums));
                break;
            case RELIGIOUS_STUDIES:
                arrayList.add(new ProgramsInnerModel("Islamic Studies", R.drawable.islamic_studies, programsEnums));
                arrayList.add(new ProgramsInnerModel("Islamic Thought & Civilization", R.drawable.islamic_thought_and_civilization, programsEnums));
                arrayList.add(new ProgramsInnerModel("Comparative Religion", R.drawable.comparative_reliogion, programsEnums));
                arrayList.add(new ProgramsInnerModel("Dars-e-Nizami", R.drawable.dars_e_nizami, programsEnums));
                arrayList.add(new ProgramsInnerModel("Islamic & Religious Studies", R.drawable.islamic_and_religious_studies, programsEnums));
                arrayList.add(new ProgramsInnerModel("Islamic History", R.drawable.islamic_history, programsEnums));
                break;
            case MEDIA_STUDIES:
                arrayList.add(new ProgramsInnerModel("Journalism", R.drawable.journalism, programsEnums));
                arrayList.add(new ProgramsInnerModel("Mass Communication", R.drawable.mass_communication, programsEnums));
                arrayList.add(new ProgramsInnerModel("Film/TV", R.drawable.film_tv, programsEnums));
                arrayList.add(new ProgramsInnerModel("Media Sciences", R.drawable.media_sciences, programsEnums));
                arrayList.add(new ProgramsInnerModel("Visual Studies", R.drawable.visual_studies, programsEnums));
                arrayList.add(new ProgramsInnerModel("Advertising", R.drawable.advertising, programsEnums));
                break;
            case COMMERCE_FINANCE_ACCOUNTING:
                arrayList.add(new ProgramsInnerModel("Accountancy (CA)", R.drawable.accountancy, programsEnums));
                arrayList.add(new ProgramsInnerModel("Banking", R.drawable.banking, programsEnums));
                arrayList.add(new ProgramsInnerModel("Commerce", R.drawable.commerce, programsEnums));
                arrayList.add(new ProgramsInnerModel("Islamic Banking", R.drawable.islamic_banking, programsEnums));
                arrayList.add(new ProgramsInnerModel("Banking & Finance", R.drawable.banking_finance, programsEnums));
                arrayList.add(new ProgramsInnerModel("Finance & Accounting", R.drawable.accounting_fiance, programsEnums));
                break;
        }

        return arrayList;
    }

    public String getProgramType(ProgramsEnums programsEnums) {

        switch (programsEnums) {
            default:
            case MEDICAL_SCIENCES:
                return "Medical Sciences";
            case ENGINEERING:
                return "Engineering";
            case COMPUTER_SCIENCE_IT:
                return "Computer Sciences & Information Technology";
            case ART_DESIGN:
                return "Art & Design";
            case MANAGEMENT_SCIENCES:
                return "Management Sciences";
            case SOCIAL_SCIENCES:
                return "Social Sciences";
            case BIOLOGICAL_SCIENCES:
                return "Biological & Life Sciences";
            case CHEMICAL_SCIENCES:
                return "Chemical & Material Sciences";
            case PHYSICAL_NUMERICAL_SCIENCES:
                return "Physics & Numerical Sciences";
            case AGRICULTURE:
                return "Agricultural Sciences";
            case TECHNICAL:
                return "Technical";
            case RELIGIOUS_STUDIES:
                return "Religious Studies";
            case MEDIA_STUDIES:
                return "Media Studies";
            case COMMERCE_FINANCE_ACCOUNTING:
                return "Commerce / Finance & Accounting";
        }


    }

    public int getCareerTvCategoryImage(String type) {
        switch (type) {
            case "Medical Sciences":
                return R.drawable.medical_science;
            case "Career Counseling":
                return R.drawable.ic_career_home;
            case "Education":
                return R.drawable.statistics;
            case "Biological Sciences":
                return R.drawable.biological_science;
            case "Engineering":
                return R.drawable.engineering;
            case "Computer Science and IT":
                return R.drawable.computer_science;
            case "General":
                return R.drawable.biotechnology_and_genetic_engineering;
            case "Chemistry":
                return R.drawable.chemical_science;
            case "Physics and Math":
                return R.drawable.physics_science;
            case "Religious Studies":
                return R.drawable.religion_study;
            case "Earth and Environment Sciences":
            case "Earth and Environmental Scienc":
                return R.drawable.earth_science;
            case "Students":
                return R.drawable.botany;
            case "Parenting":
                return R.drawable.interior_designing;
            case "Armed Forces":
                return R.drawable.biochemistry;
            case "CSS/ PMS":
                return R.drawable.mass_communication;
            case "Entry Tests":
                return R.drawable.international_relations;
            case "Job Hunting":
                return R.drawable.soil_sciences;
            case "Media Studies":
                return R.drawable.media_science;
            case "Art and Design":
                return R.drawable.arts;
            case "Agriculture Sciences":
                return R.drawable.agriculture_science;
            case "Education Issues":
                return R.drawable.law;
            case "Languages":
                return R.drawable.tehnical;
            case "Admission Guidance":
                return R.drawable.ceramic_design;
            case "Children":
                return R.drawable.architecture_planning;
            case "Commerce Finance & Accounting":
                return R.drawable.accounts;
            case "Social Sciences":
                return R.drawable.social_science;
            case "Management Sciences":
                return R.drawable.management_science;
            case "Exam Preparation":
                return R.drawable.information_security;
            case "Flying/Aviation":
                return R.drawable.artificial_intelligence;
            default:
                return R.drawable.biology;
        }
    }

    public @ColorRes int getCareerCounselingColor(int position) {
        switch (position % 10) {
            default:
            case 0:
                return R.color.counseling_1;
            case 1:
                return R.color.counseling_2;
            case 2:
                return R.color.counseling_3;
            case 3:
                return R.color.counseling_4;
            case 4:
                return R.color.counseling_5;
            case 5:
                return R.color.counseling_6;
            case 6:
                return R.color.counseling_7;
            case 7:
                return R.color.counseling_8;
            case 8:
                return R.color.counseling_9;
            case 9:
                return R.color.counseling_10;
        }
    }

    public List<FilterModel> getRankingFilter() {
        List<FilterModel> mData = new ArrayList<>();
        mData.add(new FilterModel(R.drawable.ic_filter_primary, "Filter", SearchFilter.FILTER));
        mData.add(new FilterModel(0, "Category", SearchFilter.CATEGORY));
        mData.add(new FilterModel(0, "Level", SearchFilter.LEVEL));
        return mData;
    }

    public  List<KeyValue> getRankingLevel(){
        List<KeyValue>data = new ArrayList<>();
        data.add(new KeyValue("","Matric"));
        data.add(new KeyValue("","Inter"));
        return data;
    }

    public  List<KeyValue> getRankingCategory(){
        List<KeyValue>data = new ArrayList<>();
        data.add(new KeyValue("","Small"));
        data.add(new KeyValue("","Medium"));
        data.add(new KeyValue("","Large"));
        return data;
    }

}
