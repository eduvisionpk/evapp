package com.codegenetics.eduvision.helper;

public class NavigationMenu {
    public static final int HOME = 0;
    public static final int ADMISSION = 1;
    public static final int PROGRAM = 2;
    public static final int CAREER_COUNSELING = 3;
    public static final int MORE = 4;
    public static final String SET_CURRENT_ITEM = "SET_CURRENT_ITEM";

}
