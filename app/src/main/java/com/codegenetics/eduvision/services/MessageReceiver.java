package com.codegenetics.eduvision.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.codegenetics.eduvision.enums.IntentEnums;
import com.codegenetics.eduvision.helper.FirebaseConstants;
import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class MessageReceiver extends BroadcastReceiver {

    public static String REPLY_ACTION = "com.travelfic.notification.REPLY_ACTION";
    public static String KEY_REPLY = "reply_message";

    private String myUserId, myUserName, myUserAvatar;
    private DatabaseReference referenceMessages, referenceChats;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        Log.e("NotificationReceiver", "message received");

        if (context != null) {

            if (!Session.getInstance().getCurrentUserId().isEmpty()) {

                if (intent.getAction() != null) {
                    if (!REPLY_ACTION.equalsIgnoreCase(intent.getAction())) return;
                }

                CharSequence message = Constants.getReplyMessage(intent);

                Toast.makeText(context, "Message sent", Toast.LENGTH_SHORT).show();

                // get notification id to update notification
                int notifyId = intent.getIntExtra(IntentEnums.ID.toString(), 1);

                //clearing all previously stored notifications
                Constants.map.put(FirebaseConstants.CONSULTANT, new ArrayList<>());

                //updating notification
                Constants.updateNotification(context, notifyId);

                Log.e("NotificationTest", "Message Receiver onReceiveCalled=" + notifyId);

                referenceMessages = FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.MESSAGES);
                referenceChats = FirebaseDatabase.getInstance().getReference(FirebaseConstants.ParentNodes.CHATS);

                myUserId = Session.getInstance().getCurrentUserId();
                myUserName = Session.getInstance().getName();
                myUserAvatar = Session.getInstance().getImage();

                if (message != null) {
                    Map<String, String> map = new HashMap<>();
                    map.put(FirebaseConstants.ChildNodes.DATE, String.valueOf(System.currentTimeMillis()));
                    map.put(FirebaseConstants.ChildNodes.LAST_MESSAGE, message.toString());
                    addToUser1Chat(map);
                }
            }
        }
    }


    private void addToUser1Chat(final Map<String, String> map) {

        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.SENT);
        map.put(FirebaseConstants.ChildNodes.UNREAD, "0");

        referenceChats
                .child(myUserId)
                .setValue(map, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        addToUser2Chat(map);
                    } else {
                        addToUser1Chat(map);
                    }
                });


    }

    private void addToUser2Chat(final Map<String, String> map) {

        referenceChats
                .child(FirebaseConstants.CONSULTANT)
                .child(myUserId)
                .child(FirebaseConstants.ChildNodes.UNREAD)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {
                            map.put(FirebaseConstants.ChildNodes.UNREAD,
                                    String.valueOf(Integer.parseInt(Objects.requireNonNull(dataSnapshot.getValue(String.class))) + 1)
                            );
                        } else {
                            map.put(FirebaseConstants.ChildNodes.UNREAD, String.valueOf(Integer.parseInt("0") + 1));
                        }

                        String token = Constants.getInstance().getFirebaseToken();

                        map.put(FirebaseConstants.ChildNodes.DEVICE_ID, token.isEmpty() ? Constants.getInstance().getFirebaseToken() : token);
                        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.RECEIVED);
                        map.put(FirebaseConstants.ChildNodes.USER_PHOTO, myUserAvatar);
                        map.put(FirebaseConstants.ChildNodes.USERNAME, myUserName);


                        referenceChats
                                .child(FirebaseConstants.CONSULTANT)
                                .child(myUserId)
                                .setValue(map, (databaseError, databaseReference) -> {
                                    if (databaseError != null) {
                                        addToUser2Chat(map);
                                    } else {
                                        addToUser1Messages(map);
                                    }
                                });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }

    private void addToUser1Messages(final Map<String, String> map) {

        map.put(FirebaseConstants.ChildNodes.MESSAGE, map.get(FirebaseConstants.ChildNodes.LAST_MESSAGE));
        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.SENT);

        map.remove(FirebaseConstants.ChildNodes.DATE);
        map.remove(FirebaseConstants.ChildNodes.UNREAD);
        map.remove(FirebaseConstants.ChildNodes.DEVICE_ID);
        map.remove(FirebaseConstants.ChildNodes.LAST_MESSAGE);
        map.remove(FirebaseConstants.ChildNodes.USER_PHOTO);
        map.remove(FirebaseConstants.ChildNodes.USERNAME);

        referenceMessages
                .child(myUserId)
                .child(String.valueOf(System.currentTimeMillis()))
                .setValue(map, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        addToUser2Messages(map);
                    }
                });

    }

    private void addToUser2Messages(final Map<String, String> map) {
        map.put(FirebaseConstants.ChildNodes.STATUS, FirebaseConstants.RECEIVED);

        referenceMessages
                .child(FirebaseConstants.CONSULTANT)
                .child(myUserId)
                .child(String.valueOf(System.currentTimeMillis()))
                .setValue(map);
    }


}


