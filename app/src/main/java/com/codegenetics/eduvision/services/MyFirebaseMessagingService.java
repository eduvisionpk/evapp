package com.codegenetics.eduvision.services;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.codegenetics.eduvision.helper.Session;
import com.codegenetics.eduvision.utils.Constants;
import com.codegenetics.eduvision.utils.Eduvision;
import com.codegenetics.eduvision.views.activities.Messages;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private FirebaseUser currentUser;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("testingNotification", "onMessageReceived");
        try {

            if (remoteMessage.getNotification() != null) {
                handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), null, null);
            } else if (remoteMessage.getData().containsKey("type")) {

                currentUser = FirebaseAuth.getInstance().getCurrentUser();

                if (currentUser != null) {

                    if (!Session.getInstance().getCurrentUserId().isEmpty()) {

                        Log.e("testingNotification", "onMessageReceived");
                        Log.e("testingNotification", remoteMessage.toString());

                        Map<String, String> data = remoteMessage.getData();

                        String userId = data.get("user_pkid");

                        if (Session.getInstance().getCurrentUserId().equalsIgnoreCase(userId)) {

                            handleNotification(
                                    data.get("title"),
                                    data.get("body"),
                                    data.get("type"),
                                    data.get("user_pkid")
                            );

                        }
                    }


                }

            } else {
                Map<String, String> data = remoteMessage.getData();
                String vId, type;

                if (data.containsKey("id")) {
                    vId = data.get("id");
                    type = data.get("type");
                } else {
                    vId = null;
                    type = null;
                }
                handleNotification(data.get("title"), data.get("body"), vId, type);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void handleNotification(String... params) {

        Log.e("testingNotification",
                "title : " + params[0] + "\n" +
                        "message : " + params[1] + "\n" +
                        "type : " + params[2] + "\n" +
                        "userid : " + params[3] + "\n"
        );

        if (params[2].equalsIgnoreCase("new_message")) {

            if (!Messages.isActive) {
                sendNotificationNow(params);
            }
            else if (Eduvision.isBackgrounded){
                sendNotificationNow(params);
            }
            else if (!currentUser.getUid().equalsIgnoreCase(params[3])) {
                sendNotificationNow(params);
            }


        }
        else {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                Constants.getInstance().NotificationForOreo(getApplicationContext(),
                        params[0], params[1], params[2]);
            else
                Constants.getInstance().NotificationBelowOreo(getApplicationContext(),
                        params[0], params[1], params[2]);


        }

    }


    private void sendNotificationNow(String[] params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            Constants.getInstance().messageNotificationForOreo(getApplicationContext(),
                    params[0], params[1], params[3]);
        else
            Constants.getInstance().messageNotificationBelowOreo(getApplicationContext(),
                    params[0], params[1], params[3]);
    }


}
